/*
 * Copyright (C) 2013 Paul Burke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ipaulpro.afilechooser;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.ListView;

/**
 * Fragment that displays a list of Files in a given path.
 *
 * @version 2013-12-11
 * @author paulburke (ipaulpro)
 */
public class FileListFragment extends ListFragment implements
        LoaderManager.LoaderCallbacks<List<FileRenderer.Tuple>> {

    /**
     * Interface to listen for events.
     */
    public interface Callbacks {
        /**
         * Called when a file is selected from the list.
         *
         * @param file The file selected
         */
        public void onFileSelected(File file, String mime);
    }

    private static final int LOADER_ID = 0;

    private FileListAdapter mAdapter;
    private String mPath;
    private List<FileRenderer> mFileRenderers;
    private String[] mSymLinks;

    private Callbacks mListener;

    /**
     * Create a new instance with the given file path.
     *
     * @param path The absolute path of the file (directory) to display.
     * @return A new Fragment with the given file path.
     */
    public static FileListFragment newInstance(String path, final String[] filters, final String[] symlinks) {
        FileListFragment fragment = new FileListFragment();
        Bundle args = new Bundle();
        args.putString(FileChooserActivity.PATH, path);
        args.putStringArray(FileChooserActivity.EXTRA_FILE_FILTERS, filters);
        args.putStringArray(FileChooserActivity.EXTRA_ROOT_SYMLINKS, symlinks);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (Callbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement FileListFragment.Callbacks");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    	mPath = Environment.getExternalStorageDirectory().getAbsolutePath();
    	mFileRenderers = new ArrayList<FileRenderer>();
    	mSymLinks = null;

        if (getArguments() != null) {
            mPath = getArguments().getString(FileChooserActivity.PATH);
            String[] fileFilterClasses = getArguments().getStringArray(FileChooserActivity.EXTRA_FILE_FILTERS);
            if (fileFilterClasses != null) {
                for (int i = 0;  i < fileFilterClasses.length; i++) {
                    try {
                        Class<?> clazz = Class.forName(fileFilterClasses[i]);
                        Constructor<?> ctor = clazz.getConstructor();
                        Object object = ctor.newInstance();
                        FileRenderer casted = (FileRenderer)object;
                        mFileRenderers.add(casted);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            mSymLinks = getArguments().getStringArray(FileChooserActivity.EXTRA_ROOT_SYMLINKS);
        }
        mAdapter = new FileListAdapter(getActivity(), mFileRenderers);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        setEmptyText(getString(R.string.empty_directory));
        setListAdapter(mAdapter);
        setListShown(false);

        getLoaderManager().initLoader(LOADER_ID, null, this);

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        FileListAdapter adapter = (FileListAdapter) l.getAdapter();
        if (adapter != null) {
            FileRenderer.Tuple file = adapter.getItem(position);
            mPath = file.file.getAbsolutePath();
            mListener.onFileSelected(file.file, file.mime);
        }
    }

    @Override
    public Loader<List<FileRenderer.Tuple>> onCreateLoader(int id, Bundle args) {
        return new FileLoader(getActivity(), mPath, mFileRenderers, mSymLinks);
    }

    @Override
    public void onLoadFinished(Loader<List<FileRenderer.Tuple>> loader, List<FileRenderer.Tuple> data) {
        mAdapter.setListItems(data);

        if (isResumed())
            setListShown(true);
        else
            setListShownNoAnimation(true);
    }

    @Override
    public void onLoaderReset(Loader<List<FileRenderer.Tuple>> loader) {
        mAdapter.clear();
    }
}
