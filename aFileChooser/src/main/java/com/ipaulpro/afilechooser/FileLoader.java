/*
 * Copyright (C) 2013 Paul Burke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ipaulpro.afilechooser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.os.FileObserver;
import android.support.v4.content.AsyncTaskLoader;

import com.ipaulpro.afilechooser.utils.FileUtils;

/**
 * Loader that returns a list of Files in a given file path.
 *
 * @version 2013-12-11
 * @author paulburke (ipaulpro)
 */
public class FileLoader extends AsyncTaskLoader<List<FileRenderer.Tuple>> {

	private static final int FILE_OBSERVER_MASK = FileObserver.CREATE
			| FileObserver.DELETE | FileObserver.DELETE_SELF
			| FileObserver.MOVED_FROM | FileObserver.MOVED_TO
			| FileObserver.MODIFY | FileObserver.MOVE_SELF;

	private FileObserver mFileObserver;

	private List<FileRenderer.Tuple> mData;
	private final String mPath;
	private final List<FileRenderer> mFileRenderers;
	private final String[] mSymLinks;

	private static class Symlink extends java.io.File {
		private static final long serialVersionUID = 1L;
		private final String mDisplayName;

		public Symlink(String displayName, File dst) {
			super(dst.getAbsolutePath());
			mDisplayName = displayName;
		}

		@Override
		public String getName() {
			return mDisplayName;
		}
	}

    public static class DirRenderer implements FileRenderer {

        @Override
        public Tuple render(File file) {
            if (file.isDirectory()) {
                return new Tuple(file);
            }
            return null;
        }
    }

	public FileLoader(Context context, String path, final List<FileRenderer> fileRenderers, final String[] symlinks) {
		super(context);
		mPath = path;
		mSymLinks = symlinks;
		mFileRenderers = fileRenderers;
	}

    private FileRenderer.Tuple renderFile(File file) {
        for (FileRenderer renderer : mFileRenderers) {
            FileRenderer.Tuple tuple = renderer.render(file);
            if (tuple != null) {
                return tuple;
            }
        }
        return null;
    }

	@Override
	public List<FileRenderer.Tuple> loadInBackground() {

        ArrayList<FileRenderer.Tuple> list = new ArrayList<FileRenderer.Tuple>();

        // Current directory File instance
        final File pathDir = new File(mPath);

        // List file in this directory with the directory filter
        final File[] dirs = pathDir.listFiles(FileUtils.sDirFilter);
        if (dirs != null) {
            // Sort the folders alphabetically
            Arrays.sort(dirs, FileUtils.sComparator);
            // Add each folder to the File list for the list adapter
            for (File dir : dirs) {
                FileRenderer.Tuple rendered = renderFile(dir);
                if (rendered != null) {
                    list.add(rendered);
                }
            }
        }

        // List file in this directory with the file filter
        final File[] files = pathDir.listFiles();
        if (files != null) {
            // Sort the files alphabetically
            Arrays.sort(files, FileUtils.sComparator);
            // Add each file to the File list for the list adapter
            for (File file : files) {
                FileRenderer.Tuple rendered = renderFile(file);
                if (rendered != null) {
                    list.add(rendered);
                }
            }
        }

        if (mSymLinks != null) {
        	for (int i = 0; i < mSymLinks.length;) {
                File symlink = new Symlink(mSymLinks[i++], new File(mSymLinks[i++]));
                FileRenderer.Tuple rendered = renderFile(symlink);
                if (rendered != null) {
                    list.add(rendered);
                }
        	}
        }

        return list;
	}

	@Override
	public void deliverResult(List<FileRenderer.Tuple> data) {
		if (isReset()) {
			onReleaseResources(data);
			return;
		}

		List<FileRenderer.Tuple> oldData = mData;
		mData = data;

		if (isStarted())
			super.deliverResult(data);

		if (oldData != null && oldData != data)
			onReleaseResources(oldData);
	}

	@Override
	protected void onStartLoading() {
		if (mData != null)
			deliverResult(mData);

		if (mFileObserver == null) {
			mFileObserver = new FileObserver(mPath, FILE_OBSERVER_MASK) {
				@Override
				public void onEvent(int event, String path) {
					onContentChanged();
				}
			};
		}
		mFileObserver.startWatching();

		if (takeContentChanged() || mData == null)
			forceLoad();
	}

	@Override
	protected void onStopLoading() {
		cancelLoad();
	}

	@Override
	protected void onReset() {
		onStopLoading();

		if (mData != null) {
			onReleaseResources(mData);
			mData = null;
		}
	}

	@Override
	public void onCanceled(List<FileRenderer.Tuple> data) {
		super.onCanceled(data);

		onReleaseResources(data);
	}

	protected void onReleaseResources(List<FileRenderer.Tuple> data) {

		if (mFileObserver != null) {
			mFileObserver.stopWatching();
			mFileObserver = null;
		}
	}
}