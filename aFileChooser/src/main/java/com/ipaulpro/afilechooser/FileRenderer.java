package com.ipaulpro.afilechooser;

import java.io.File;
import java.io.IOException;

public interface FileRenderer {

    public final static int KFileIcon = R.drawable.ic_file;
    public final static int KDirIcon = R.drawable.ic_folder;

    public class Tuple {

        public Tuple(File _file) {
            file = _file;
            text = file.getName();
            icon = file.isDirectory() ? KDirIcon : KFileIcon;
        }

        public Tuple setText(String _text) {
            text = _text;
            return this;
        }

        public Tuple setIcon(int _icon) {
            icon = _icon;
            return this;
        }

        public Tuple setMime(String _mime) {
            mime = _mime;
            return this;
        }

        public int icon;
        public String text;
        public File file;
        public String mime;
    }

    public Tuple render(File file);
}
