//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.Gpx;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Stack;
import java.util.TimeZone;
import java.util.Vector;

import net.remekzajac.Geo.Topo;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;



public class Gpx
{
	public static final String KMimeType = "application/gpx+xml";
	public static final String KGpx		 = "gpx";
	public static final String KMeta	 = "metadata";

	public static final String KWpt  	 = "wpt";

	public static final String KTrk		 = "trk";
	public static final String KTrkSeg 	 = "trkseg";
	public static final String KTrkPt  	 = "trkpt";

	public static final String KRte		 = "rte";
	public static final String KRtePt    = "rtept";


	private static class MetaAuthor {

	}

	public static class MetaData {

		private final static String KNameTag 	   = "name";
		private final static String KDescTag       = "desc";
		private final static String KTimeTag       = "time";
		private final static String KExtensionsTag = "extensions";
		private final static String KTimeOffsetTag = "timeZoneOffset";

		private String mName;
		private String mDescription;
		private String mExtensions;

		private String toString(String tagName, String tagContent) {
			if (tagContent != null) {
				return String.format(java.util.Locale.US, KWriteStringTag, tagName, tagContent, tagName);
			}
			return "";
		}

		public MetaData() {}
		public MetaData setName(String value ) { mName = value ; return this; }
		public MetaData setDescription(String value ) { mDescription = value ; return this; }
		public MetaData setExtensions(String value ) { mExtensions = value ; return this; }
		@Override
		public String toString() {
			Calendar time = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
			time.setTimeInMillis(System.currentTimeMillis());

			String timeStr = String.format(java.util.Locale.US, KWriteTime,
				time.get(Calendar.YEAR),
				time.get(Calendar.MONTH),
				time.get(Calendar.DAY_OF_MONTH),
				time.get(Calendar.HOUR_OF_DAY),
				time.get(Calendar.MINUTE),
				time.get(Calendar.SECOND)
			);
			String extensionsString = toString(
				KTimeOffsetTag,
				String.format(java.util.Locale.US,
					"%.2f",
					TimeZone.getDefault().getOffset(
						System.currentTimeMillis()
					)/(1000.0*60*60)
				)
			);
			if (mExtensions != null) {
				extensionsString += mExtensions;
			}
			return toString(KNameTag, mName)
				 + toString(KDescTag, mDescription)
				 + toString(KNameTag, timeStr)
				 + toString(KExtensionsTag, extensionsString);
		}
	}

	private static final String KPreamble =
			"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
			+ "<gpx creator=\"%s\" version=\"1.1\" xmlns=\"http://www.topografix.com/GPX/1/1\" "
			+ "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd\">"
			+ "<metadata>%s</metadata>";

	private static final String KWriteStringTag = "<%s>%s</%s>";
	private static final String KWriteFloatTag = "<%s>%f</%s>";
	private static final String KWriteIntegerTag = "<%s>%d</%s>";
	private static final String KWriteTime = "%04d-%02d-%02dT%02d:%02d:%02dZ";

	private static final String[] KReadTimeFormats = {
		"yyyy-MM-dd\'T\'HH:mm:ss\'Z\'",
		"yyyy-MM-dd\'T\'HH:mm:ss"
	};


	/*
	 * Writes a gpx file.
	 */
	public static class Writer {
		protected Stack<String> mStateStack;
		private OutputStream mOutStream;
		public Writer()
		{
			mStateStack = new Stack<String>();
		}

        private String emptyIfNull(String str) {
            return str == null ? "" : str;
        }

		public void open(OutputStream out,
						 String creator,
						 MetaData meta) throws IOException
		{
			mOutStream = out;

			String preamble = String.format(java.util.Locale.US,
				KPreamble,
				emptyIfNull(creator),
                meta.toString()
            );
			mOutStream.write(preamble.getBytes());
			mStateStack.push(KGpx);
		}

		public void close() throws IOException {
			while(mStateStack.size() > 0) {
				mOutStream.write(String.format(java.util.Locale.US, "</%s>", mStateStack.pop()).getBytes());
			}
		}

		private void writeTag(String name, String value, boolean optional) throws IOException {
			assert optional || value != null: "Mandatory GPX field :"+name;
			if (value != null) {
				mOutStream.write(String.format(java.util.Locale.US, KWriteStringTag, name, value, name).getBytes());
			}
		}

		private void writeTag(String name, Double value, boolean optional) throws IOException {
			assert optional || value != null: "Mandatory GPX field :"+name;
			if (value != null) {
				mOutStream.write(String.format(java.util.Locale.US, KWriteFloatTag, name, value, name).getBytes());
			}
		}

		private void writeTag(String name, Integer value, boolean optional) throws IOException {
			assert optional || value != null: "Mandatory GPX field :"+name;
			if (value != null) {
				mOutStream.write(String.format(java.util.Locale.US, KWriteIntegerTag, name, value, name).getBytes());
			}
		}

		private void openTag(String name) throws IOException {
			mStateStack.push(name);
			mOutStream.write(String.format(java.util.Locale.US, "<%s>", name).getBytes());
		}

		private void closeTag(String name) throws IOException {
			assert mStateStack.peek() == name: "Illegal GPX write state for closeTag :"+name;
			mStateStack.remove(mStateStack.size()-1);
			mOutStream.write(String.format(java.util.Locale.US, "</%s>", name).getBytes());
		}

		public void openTrack(String name) throws IOException {
			assert mStateStack.peek() == KGpx: "Illegal GPX writer state for .openTrack :"+mStateStack.peek();
			openTag(KTrk);
			writeTag("name", name, true);
		}

		public void closeTrack() throws IOException {
			closeTag(KTrk);
		}

		public void openTrackSegment() throws IOException
		{
			assert mStateStack.peek() == KTrk: "Illegal GPX writer state for .openTrackSegment :"+mStateStack.peek();
			openTag(KTrkSeg);
		}

		public void closeTrackSegment() throws IOException
		{
			closeTag(KTrkSeg);
		}

		public void openRoute(String name) throws IOException {
			assert mStateStack.peek() == KGpx: "Illegal GPX writer state for .openRoute :"+mStateStack.peek();
			openTag(KRte);
			writeTag("name", name, true);
		}

		public void closeRoute() throws IOException {
			closeTag(KRte);
		}

		public void wptpt(Double lat, Double lon, String name, String description, Long atime, Integer elevation) throws IOException {
			String tag = null;
			if (mStateStack.peek() == KGpx) {
				tag = KWpt;
			} else if (mStateStack.peek() == KTrkSeg) {
				tag = KTrkPt;
			} else if (mStateStack.peek() == KRte) {
				tag = KRte;
			} else {
				assert false: "Can't write a wpt (waypoint) in this writer state :"+mStateStack.peek();
			}
			wptpt(tag, lat, lon, name, description, atime, elevation);
		}

		protected void wptpt(String type, Double lat, Double lon, String name, String description, Long atime, Integer elevation) throws IOException
		{
			assert (mStateStack.peek() == KGpx && type == KWpt ||
					mStateStack.peek() == KTrkSeg && type == KTrkPt||
					mStateStack.peek() == KRte && type == KRtePt)
					: "Illegal GPX writer state for .wptpt("+type+", ...) :"+mStateStack.peek();

			mOutStream.write(String.format(java.util.Locale.US, "<%s lat=\"%f\" lon=\"%f\">", type, lat, lon).getBytes());
			if (atime != null) {
				Calendar time = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
				time.setTimeInMillis(atime);

				String timeStr = String.format(java.util.Locale.US, KWriteTime,
					time.get(Calendar.YEAR),
					time.get(Calendar.MONTH),
					time.get(Calendar.DAY_OF_MONTH),
					time.get(Calendar.HOUR_OF_DAY),
					time.get(Calendar.MINUTE),
					time.get(Calendar.SECOND)
				);
				writeTag("time", timeStr, false);
			}

			writeTag("ele", elevation, true);
			writeTag("name", name, true);
			writeTag("desc", description, true);
			mOutStream.write(String.format(java.util.Locale.US, "</%s>", type).getBytes());
		}
	}

	/*
	 * Writes a track (i.e.: a GPS recording)
	 */
	public static class TrackWriter extends Writer {

		private final Vector<Topo.Point> mWpts;
		public TrackWriter()
		{
			mWpts = new Vector<Topo.Point>();
		}

		public void commitTrackSegment(String name) throws IOException
		{
			if (mStateStack.peek() != KTrkSeg) {
				if (mStateStack.peek() != KTrk) {
					openTrack(name);
				}
				openTrackSegment();
			}
			while(mWpts.size() > 0)
			{
				Topo.Point next = mWpts.remove(0);
				wptpt(
					KTrkPt,
					next.mLat,
					next.mLon,
					next.mName,
					next.mDescripion,
					next.mTime,
					next.mEle
				);
			}
		}

		public void trackPoint(Double lat, Double lon, Long atime, Integer elevation) {
			mWpts.add(new Topo.Point(lat, lon).addTime(atime).addEle(elevation));
		}
	}






	/*
	 * Reads gpx file.
	 */
	public static class Reader implements Topo.CollectionSource
	{
		/*
		 * Representation of any node inside a gpx file
		 */
		public abstract class Node
		{
			protected final String 				mNodeName;
			protected final Topo.CollectionBase mCollection;
			protected Node(Topo.CollectionBase aCollection) throws XmlPullParserException
			{
				mNodeName = mXPPParser.getName();
				mCollection = aCollection;
			}

			protected void parse() throws XmlPullParserException, IOException, Exception
			{
				int nodeType = mXPPParser.getEventType();
				mXPPParser.next();
				int eventType = mXPPParser.getEventType();
			    while (eventType != XmlPullParser.END_DOCUMENT)
			    {
			    	if (mInput == null) {
			    		//input stream has been closed. abort
			    		return;
			    	}
			    	if(eventType == XmlPullParser.START_TAG)
			        {
			    		if (mXPPParser.getName().equals("name"))
			            {
			    			if (mCollection != null) {
			    				mCollection.addAttribute(Topo.Attributes.KName, parseText(mXPPParser));
			    			}
			            }
			    		else if (mXPPParser.getName().equals("desc") && mCollection != null)
			    		{
			    			if (mCollection != null) {
			    				mCollection.addAttribute(Topo.Attributes.KDescription, parseText(mXPPParser));
			    			}
			    		}
			    		else
			    		{
			    			next();
			    		}
			        }
				    else if (eventType == XmlPullParser.END_TAG && mXPPParser.getName().equals(mNodeName))
				    {
				    	finished();
				    	mStateStack.pop();
				    	return;
				    }
			        eventType = mXPPParser.next();
				}
			    if (nodeType != XmlPullParser.START_DOCUMENT) {
			    	throw new Exception("Can't read track");
			    }
			}

			public String type()
			{
				return mNodeName;
			}

			protected void next() throws XmlPullParserException, IOException, Exception {
				skipSubtree(mXPPParser);
			}

			protected void finished() {}
		}

		/*
		 * The root (above <gpx>) node
		 */
		private class RootNode extends Node
		{
			public RootNode() throws XmlPullParserException
			{
				super(null);
			}

			@Override
			protected void next() throws XmlPullParserException, IOException, Exception
			{
	    		if (mXPPParser.getName().equals(KGpx))
	            {
	    			mStateStack.push(new GpxNode());
	    			mStateStack.peek().parse();
	    			return;
	            }
	    		throw new Exception("Can't read track, sole <"+KGpx+"> node expected");
			}
		}

		/*
		 * The <gpx> node
		 */
		private class GpxNode extends Node
		{
			public GpxNode() throws XmlPullParserException
			{
				super(null);
			}

			@Override
			public void next() throws XmlPullParserException, IOException, Exception
			{
	    		if (mXPPParser.getName().equals(KMeta))
	            {
	    			mStateStack.push(new MetaParser());
	    			mStateStack.peek().parse();
	            }
	    		else if (mXPPParser.getName().equals(KTrk))
	    		{
	    			mStateStack.push(new TrkParser());
	    			mStateStack.peek().parse();
	    		}
	    		else if (mXPPParser.getName().equals(KRte))
	    		{
	    			mStateStack.push(new RteParser(mDstCollection.addPath()));
	    			mStateStack.peek().parse();
	    		}
	    		else if (mXPPParser.getName().equals(KWpt))
	    		{
	    			mStateStack.push(new WptParser(mDstCollection));
	    			mStateStack.peek().parse();
	    		}
	    		else
	    		{
	    			super.next();
	    		}
			}
		}


		/*
		 * Represents the <metadata> node of the gpx file
		 */
		private class MetaParser extends Node
		{
			public MetaParser() throws XmlPullParserException
			{
				super(mDstCollection);
			}
		}

		private class TrkParser extends Node
		{
			public TrkParser() throws XmlPullParserException
			{
				super(null);
			}

			@Override
			public void next() throws XmlPullParserException, IOException, Exception
			{
	    		if (mXPPParser.getName().equals(KTrkSeg))
	    		{
	    			mStateStack.push(new TrkSegmentParser(mDstCollection.addPath()));
	    			mStateStack.peek().parse();
	    		}
	    		else
	    		{
	    			super.next();
	    		}
			}
		}

		private class TrkSegmentParser extends Node
		{
			public TrkSegmentParser(Topo.Path aPath) throws XmlPullParserException
			{
				super(aPath);
			}

			@Override
			public void next() throws XmlPullParserException, IOException, Exception
			{
	    		if (mCollection != null && mXPPParser.getName().equals(KTrkPt))
	    		{
	    			mStateStack.push(new WptParser(mCollection));
	    			mStateStack.peek().parse();
	    		}
	    		else
	    		{
	    			super.next();
	    		}
			}
		}

		private class RteParser extends Node
		{
			public RteParser(Topo.Path aPath) throws XmlPullParserException
			{
				super(aPath);
			}

			@Override
			public void next() throws XmlPullParserException, IOException, Exception
			{
				if (mCollection != null && mXPPParser.getName().equals(KRtePt))
	    		{
	    			mStateStack.push(new WptParser(mCollection));
	    			mStateStack.peek().parse();
	    		}
	    		else
	    		{
	    			super.next();
	    		}
			}
		}

		private class WptParser extends Node
		{
			private final Topo.Point mWpt;

			public WptParser(Topo.CollectionBase aWptCollection) throws XmlPullParserException
			{
				super(aWptCollection);
				mWpt = new Topo.Point();
				mWpt.mLat = (Double.parseDouble(
						mXPPParser.getAttributeValue(null, "lat")));
				mWpt.mLon = (Double.parseDouble(
						mXPPParser.getAttributeValue(null, "lon")));
			}

			@Override
			public void next() throws XmlPullParserException, IOException, Exception
			{
	    		if (mXPPParser.getName().equals("ele"))
	            {
	    			mWpt.mEle = Double.valueOf(parseText(mXPPParser)).intValue();
	            }
	    		else if (mXPPParser.getName().equals("name"))
	            {
	    			mWpt.mName = parseText(mXPPParser);
	            }
	    		else if (mXPPParser.getName().equals("desc"))
	            {
	    			mWpt.mDescripion = parseText(mXPPParser);
	            }
	    		else if (mXPPParser.getName().equals("time"))
	    		{
	    			String timeString = parseText(mXPPParser);
	    			for (String formatString : KReadTimeFormats)
	    			{
	            		SimpleDateFormat date = new SimpleDateFormat(formatString);
	            		try
						{
							mWpt.mTime = date.parse(timeString).getTime();
						}
	            		catch (ParseException e)
						{
							continue;
						}
	            		break;
	    			}
	    		}
	    		else
	    		{
	    			super.next();
	    		}
			}

			@Override
			protected void finished() {
				mCollection.append(mWpt);
			}
		}

		private java.io.Reader      mInput;
		protected XmlPullParser 	mXPPParser;
		private final Stack<Node> 	mStateStack;
		private Topo.Collection  	mDstCollection;
        private String              mName;
		public Reader(java.io.Reader input, String name)
		{
			mInput = input;
			mName  = name;
			mStateStack = new Stack<Node>();
		}

		public void copy(Topo.Collection aDstCollection) throws Exception, IOException {
			XmlPullParserFactory factory;
			mDstCollection = aDstCollection;
			try
			{
				factory = XmlPullParserFactory.newInstance();
				mXPPParser = factory.newPullParser();
				mXPPParser.setInput(mInput);
				mStateStack.push(new RootNode());
    			mStateStack.peek().parse();
			}
			catch (XmlPullParserException e)
			{
				throw new Exception(e.getMessage());
			}
			catch (IOException e)
			{
				throw e;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

        @Override
        public String name() {
            return mName;
        }

        private static String parseText(XmlPullParser xpp) throws XmlPullParserException, IOException, Exception
		{
			xpp.next();
			int eventType = xpp.getEventType();
		    while (eventType != XmlPullParser.END_DOCUMENT)
		    {
		    	if(eventType == XmlPullParser.START_TAG)
		    	{
		    		skipSubtree(xpp);
		    	}
		    	else if(eventType == XmlPullParser.TEXT)
		        {
		    		return xpp.getText();
		        }
		    	else if (eventType == XmlPullParser.END_TAG)
		    	{
		    		break;
		    	}
		        eventType = xpp.next();
			}
		    return "";
		}

		static protected void skipSubtree(XmlPullParser xpp) throws XmlPullParserException, IOException, Exception
		{
			String subtreeTag = xpp.getName();
			int depth = 0;

			xpp.next();
			int eventType = xpp.getEventType();
	        while (eventType != XmlPullParser.END_DOCUMENT)
	        {
	            if(eventType == XmlPullParser.START_TAG)
	            {
	            	depth++;
	            }
	            else if(eventType == XmlPullParser.END_TAG)
	            {
	            	String leavingName = xpp.getName();
	            	if (depth-- == 0)
	            	{
	            		if (leavingName.equals(subtreeTag))
	            		{
	            			return;
	            		}
	            		break;
	            	}
	            }
	            eventType = xpp.next();
	        }
	        throw new Exception("Couldn't find closing tag </"+subtreeTag+">");
		}
	}
}
