package net.remekzajac.Geo;

import java.io.IOException;

public class Topo
{
	public static class Attributes {
		public static final String KName = "name";
		public static final String KDescription = "desc";
		public static final String KIcon = "icon";
		public static final String KColour = "colour";
	}

	public static class PointBuilder {
		private Double mLat;
		private Double mLon;
		private Integer mEleMts;
		private String mName;
		private String mDescription;

		public PointBuilder addLat(double lat) {
			mLat = lat;
			return this;
		}

		public boolean hasLat() { return mLat != null; }

		public PointBuilder addLon(double lon) {
			mLon = lon;
			return this;
		}

		public PointBuilder addEle(int eleMts) {
			mEleMts = eleMts;
			return this;
		}

		public PointBuilder addName(String name) {
			mName = name;
			return this;
		}

		public PointBuilder addDescription(String description) {
			mDescription = description;
			return this;
		}

		public Topo.Point build() {
			if (!(mLat != null && mLon != null)) {
				throw new IllegalArgumentException("Incomplete Topo.Point");
			}
			return new Topo.Point(mLat, mLon).addEle(mEleMts).addMeta(mName, mDescription);
		}
	}

	public static class Point {
		public Point() {}

		public Point(Double  lat,
					 Double  lon) {
			mLat = lat;
			mLon = lon;
		}

		public Point addTime(long time) {
			mTime = time;
			return this;
		}

        public Point addEle(Integer ele) {
			mEle = ele;
			return this;
		}

        public Point addMeta(String name, String description) {
			mName = name;
			mDescripion = description;
			return this;
		}

		public Double  mLat;
		public Double  mLon;
		public Long    mTime;
		public Integer mEle;
		public String  mName;
		public String  mDescripion;
	}

	public interface CollectionBase {
		public void append(Point aPoint);
		public void addAttribute(String aName, String aValue);
	}

	public interface Collection extends CollectionBase {
		public Path addPath();
	}

	public interface Path extends CollectionBase {};


	public interface CollectionSource {
		public static class Exception extends java.lang.Exception
		{
			private static final long serialVersionUID = 1L;
			public Exception(String detailedError)
			{
				super(detailedError);
			}
		}

		public void copy(Collection aDstCollection) throws Exception, IOException;
		public String name();
	}
}
