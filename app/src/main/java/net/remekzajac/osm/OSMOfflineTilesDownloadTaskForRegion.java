//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.osm;

import android.os.AsyncTask;
import android.util.Log;

import net.remekzajac.utils.Callback;
import net.zikes.OSMMapsApplication;
import net.remekzajac.maps.GeoDegPoint;
import net.remekzajac.tilestorage.IITileStorage;
import net.remekzajac.tilestorage.OfflineTileStorage;
import net.remekzajac.utils.IntegerPoint;


public class OSMOfflineTilesDownloadTaskForRegion extends OSMOfflineTilesDownloadTask
{
	private final GeoDegPoint	 		mNW;
	private final GeoDegPoint	 		mSE;
	private final int			 		mCoarsestZoom;
	public OSMOfflineTilesDownloadTaskForRegion(OfflineTileStorage offlineTiles, GeoDegPoint nw, GeoDegPoint se, int fromZoom) {
		super(offlineTiles, OSMMapsApplication.instance().tileFactory());
		mNW = nw;
		mSE = se;
		mCoarsestZoom = fromZoom;
	}

	private int sizeEstimateTiles(int zoom, GeoDegPoint nw, GeoDegPoint se) {
		IntegerPoint tileNW = mTileFactory.deg2num(nw, zoom)[0];
		IntegerPoint tileSE = mTileFactory.deg2num(se, zoom)[0];
		int result = (tileSE.x()-tileNW.x()+1)*(tileSE.y()-tileNW.y()+1);
		return result;
	}

	private long tilesToDownload() {
		Long tilesToDownload = Long.valueOf(0);
        for (int zoom = mTileFactory.zoomRange().bigger(); zoom >= mCoarsestZoom; zoom--) {
            tilesToDownload += sizeEstimateTiles(zoom, mNW, mSE);
        }
		return tilesToDownload;
	}

	private long sizeEstimate()
	{
		return tilesToDownload()*KSizePerTileKB;
	}

	public void sizeEstimate(SizeEstimateObserver observer)
	{
		new SizeEstimateTask(observer).execute();
	}

	public void execute(DownloadObserver observer)
	{
		if (mDownloadTask == null)
		{
			mDownloadTask = new DownloadTask();
		}
		super.execute(observer);
	}

	private class SizeEstimateTask extends AsyncTask<Void, Void, Long>
	{
		private final SizeEstimateObserver mObserver;
		public SizeEstimateTask(SizeEstimateObserver observer)
		{
			mObserver = observer;
		}

		@Override
		protected Long doInBackground(Void... arg0)
		{
			sizeOfExistingTilesInOffline();
			return sizeEstimate();
		}

		@Override
		protected void onPostExecute(Long result)
		{
			mObserver.sizeEstimate(result, sizeOfExistingTilesInOffline(), true);
		}
	}

	private class DownloadTask extends OSMOfflineTilesDownloadTask.DownloadTask	{

        public DownloadTask() {
            super(tilesToDownload(), tilesToDownload());
        }

		@Override
		protected Void doInBackground(Void... params)
		{
			for (int zoom = mCoarsestZoom; zoom <= mTileFactory.zoomRange().bigger(); zoom++)
			{
                try {
                    downloadForZoom(
						mTileFactory.deg2num(mNW, zoom)[0],
						mTileFactory.deg2num(mSE, zoom)[0],
						zoom
					);
                } catch (InterruptedException e) {
                    return null;
                }
                if (isCancelled()) {
                    return null;
                }
			}
			return super.doInBackground(params);
		}

		private void downloadForZoom(IntegerPoint tileNW, IntegerPoint tileSE, int zoom) throws InterruptedException {
			for (int tileX = tileNW.x(); tileX <= tileSE.x(); tileX++)
			{
				for (int tileY = tileNW.y(); tileY <= tileSE.y(); tileY++)
				{
                    downloadTile(mOfflineTiles.futureTile(
						new IntegerPoint(tileX, tileY), zoom)
					);
                    if (isCancelled())
                    {
                        Log.d("Tiles", "Cancelled, aborting");
                        return;
                    }
				}
			}
		 }
	}

	public void setRangeMts(int rangeMts)
	{
		//noop for region
	}
}
