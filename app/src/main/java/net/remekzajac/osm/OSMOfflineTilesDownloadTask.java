//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.osm;

import android.os.AsyncTask;
import android.util.Log;

import net.remekzajac.maps.IITileFactory;
import net.remekzajac.tilestorage.OfflineTileStorage;

import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public abstract class OSMOfflineTilesDownloadTask implements OfflineTileStorage.OfflineTilesDownloadTask
{
	protected final static int KSizePerTileKB    = 6; //TODO, better to extract from an actual tile (as factory).
	protected final static int KFailureTolerance = 3;
    private   final static int KPoolSize         = 4;

	@SuppressWarnings("serial")
	protected static class TileProcessingException extends Exception {}

	@SuppressWarnings("serial")
	protected static class PersistentDownloadErrorException extends TileProcessingException {}

	@SuppressWarnings("serial")
	protected static class ProcessingCancelled extends TileProcessingException {}

	protected abstract class DownloadTask extends AsyncTask<Void, Long, Void>
	{
        protected AtomicLong mProcessedTiles;
        protected AtomicLong mFailedTiles;
        private   Semaphore  mSempaphore;
        protected long mExpectedVisits;
		protected DownloadObserver mObserver;
        private   ThreadPoolExecutor mPool;
		final private float mFractionToShow;
		//estTilesToProcess can be a huge number with plenty repetitions
		//especially when the swiping track downloader acts
		//so we need to show the user something more representative as
		//the number on the progress bar
		protected DownloadTask(long estVisitsToProcess, long expectedIndividualTiles) {
            mPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(KPoolSize);
            mSempaphore     = new Semaphore(KPoolSize*3);
            mExpectedVisits = estVisitsToProcess;
            mProcessedTiles = new AtomicLong(0);
            mFailedTiles    = new AtomicLong(0);
			mFractionToShow = (float)expectedIndividualTiles/estVisitsToProcess;
		}

		@Override
		protected void onPostExecute(Void result)
		{
		    mObserver.progress(mExpectedVisits, mExpectedVisits);
			mOfflineTiles.offlineTaskStatusChange(null);
		}

        @Override
        protected Void doInBackground(Void... params)
        {
            try {
                while (mPool.getTaskCount()!=mPool.getCompletedTaskCount() && !isCancelled()) {
                    publishProgress(mProcessedTiles.get());
                    Thread.sleep(1000);
                }
                mPool.shutdown();
                mPool.awaitTermination(60, TimeUnit.SECONDS);
            }   catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void downloadTile(final OfflineTileStorage.Tile aTile) throws InterruptedException {
            if (mProcessedTiles.get() % 10 == 0) {
                publishProgress(mProcessedTiles.get());
            }
            mSempaphore.acquire();
            mPool.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        aTile.download();
                    } catch (Exception e) {
                        if (mFailedTiles.addAndGet(1) > KFailureTolerance) {
                            Log.e("Tiles", "Persistent error in downloading tiles, aborting");
                            DownloadTask.this.cancel(true);
                        }
                    }
                    mProcessedTiles.addAndGet(1);
                    mSempaphore.release();
                }
            });
        }


        @Override
        protected void onProgressUpdate(Long... progress) {
            mObserver.progress(
				(int)(Math.min(
                	progress[0], mExpectedVisits -1
            	) * mFractionToShow),
				(int)Math.max(mExpectedVisits*mFractionToShow, 1.0)
			);
        }
	}


	protected final OfflineTileStorage 	mOfflineTiles;
	protected final IITileFactory	 	mTileFactory;
	protected DownloadTask				mDownloadTask;
	private	Long						mSizeOnDisc;
	protected OSMOfflineTilesDownloadTask(OfflineTileStorage offlineTiles, IITileFactory tileFactory)
	{
		mOfflineTiles = offlineTiles;
		mTileFactory = tileFactory;
	}

	public void execute(DownloadObserver observer)
	{
		if (mDownloadTask != null)
		{
			if (mDownloadTask.getStatus() == AsyncTask.Status.FINISHED)
			{
				observer.progress(100,100); //TODO?
			}
			else
			{
				mDownloadTask.mObserver = observer;
				if (mDownloadTask.getStatus() == AsyncTask.Status.PENDING)
				{
					mDownloadTask.execute();
					mOfflineTiles.offlineTaskStatusChange(this);
				}
			}
		}
	}

	public void cancel()
	{
		if (mDownloadTask != null)
		{
			mDownloadTask.cancel(true);
		}
	}

	public long sizeOfExistingTilesInOffline()
	{
		if (mSizeOnDisc == null)
		{
			mSizeOnDisc = Long.valueOf(mOfflineTiles.sizeOnDiscKB());
		}
		return mSizeOnDisc.longValue();
	}
}
