//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.osm;

import net.remekzajac.utils.DoubleLineSegment;
import net.remekzajac.utils.DoubleVector;
import net.remekzajac.utils.SAsyncTask;
import net.zikes.OSMMapsApplication;
import net.remekzajac.maps.GeoDegPoint;
import net.remekzajac.tilestorage.OfflineTileStorage;
import net.remekzajac.tracks.TrackGeoPointRegister;
import net.remekzajac.tracks.TracksView;

import android.util.Log;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

public class OSMOfflineTilesDownloadTaskForTrack extends OSMOfflineTilesDownloadTask
{
	private interface IITileProcessor
	{
		public boolean process(OfflineTileStorage.Tile tile) throws TileProcessingException, InterruptedException;
	}

    final private static GeoDegPoint KReferencePoint = new GeoDegPoint(0.0,0.0);
    final private static double KDivisionCutOffComparableDistance = KReferencePoint.comparableDistanceFrom(KReferencePoint.offsetMts(30,30));

    /**
     * TileWalker re-visits massive number of tiles and the real number hit is a fraction of visits.
     */
    static public int tileEstimate(int visits) {
        return (int)Math.pow(visits, 0.65);
    }

	private class TileWalker
	{
        final private IITileProcessor mProcessor;
        private boolean mNotCancelled;
		public TileWalker(IITileProcessor processor) {
			mProcessor = processor;
            mNotCancelled = true;
		}

		protected void processTiles(int fromZoom, int toZoom, TracksView.TrackDocument[] trackDocuments) throws TileProcessingException, InterruptedException {
			for (int zoom = fromZoom; zoom <= toZoom; zoom++) {
				processTiles(zoom, trackDocuments);
                if (!mNotCancelled) {
                    return;
                }
			}
		}

		private void processTiles(int zoom, TracksView.TrackDocument[] trackDocuments) throws TileProcessingException, InterruptedException {
			for (TracksView.TrackDocument track: trackDocuments) {
                processTiles(zoom, track.geoPoints());
                if (!mNotCancelled) {
                    return;
                }
			}
		}

		private void processTiles(int zoom, TrackGeoPointRegister geoPointRegister) throws TileProcessingException, InterruptedException {
			TrackGeoPointRegister.TrackPoint current = geoPointRegister.head();
            double perimeter = new DoubleVector(
                new GeoDegPoint(current.location()),
                new GeoDegPoint(current.location()).offsetMts(mBandWidthMts, mBandWidthMts)
            ).lenght();
			while (current != geoPointRegister.tail() && mNotCancelled) {
				if (current.next() == null) {
					break;
				}
				processTiles(
                    zoom,
                    new GeoDegPoint(current.location()), new GeoDegPoint(current.next().location()),
                    perimeter
                );
				current = current.next();
			}
            processTilesWithPeremeter(zoom, new GeoDegPoint(current.location()), perimeter);
		}

		private void processTiles(int zoom, GeoDegPoint a, GeoDegPoint b, double perimeter) throws TileProcessingException, InterruptedException {
            if (!mNotCancelled) {
                return;
            }
            if (new DoubleVector(a,b).lenght() > perimeter) {
                GeoDegPoint centre = new GeoDegPoint(
                    new DoubleLineSegment(a,b).centre()
                );
                processTiles(zoom, a, centre, perimeter);
                processTiles(zoom, centre, b, perimeter);
            } else {
                processTilesWithPeremeter(zoom, a, perimeter);
            }
		}

        private class BSDSearch extends Stack<OfflineTileStorage.Tile> {
            private final Set<OfflineTileStorage.Tile> mOpenSet = new HashSet<OfflineTileStorage.Tile>();
            private Set<OfflineTileStorage.Tile> mClosedSet = new HashSet<OfflineTileStorage.Tile>();
            private final GeoDegPoint mReferencePoint;
            private final double mGeoFence;
            private final DoubleVector mTileCentreVector;
            public BSDSearch(GeoDegPoint referencePoint, double radius, DoubleVector tileCentreVector) {
                mReferencePoint = referencePoint;
                mGeoFence = radius;
                mTileCentreVector = tileCentreVector;
            }
            @Override
            public OfflineTileStorage.Tile push(OfflineTileStorage.Tile tile) {
                if (!(mOpenSet.contains(tile) ||
                      mClosedSet.contains(tile))) {
                    double distanceFromTile = new DoubleVector(
                        mReferencePoint,
                        mTileFactory.num2deg(tile.tileXY(), tile.zoom()).add(mTileCentreVector)
                    ).lenght();
                    if (distanceFromTile < mGeoFence) {
                        mOpenSet.add(tile);
                        super.push(tile);
                    }
                }
                return tile;
            }

            @Override
            public synchronized OfflineTileStorage.Tile pop() {
                OfflineTileStorage.Tile popped = super.pop();
                mOpenSet.remove(popped);
                mClosedSet.add(popped);
                return popped;
            }

            public Set<OfflineTileStorage.Tile> closedSet() {
                return mClosedSet;
            }
        }

        private void processTilesWithPeremeter(int zoom, GeoDegPoint referencePoint, double radius) throws InterruptedException, TileProcessingException {
            OfflineTileStorage.Tile seedTile = mOfflineTiles.futureTile(
                mTileFactory.deg2num(referencePoint, zoom)[0], zoom
            );
            OfflineTileStorage.Tile nextTile = seedTile.down().right();
            DoubleVector acrossTileVector = new DoubleVector(
                mTileFactory.num2deg(seedTile.tileXY(), seedTile.zoom()),
                mTileFactory.num2deg(nextTile.tileXY(), nextTile.zoom())
            );
            BSDSearch bsdSearch = new BSDSearch(
                referencePoint,
                radius,
                new DoubleVector(
                    acrossTileVector.x()/2.0,
                    acrossTileVector.y()/2.0
                )
            );
            bsdSearch.push(seedTile);

            while (!bsdSearch.isEmpty()) {
                OfflineTileStorage.Tile tile = bsdSearch.pop();
                bsdSearch.push(tile.left());
                bsdSearch.push(tile.right());
                bsdSearch.push(tile.up());
                bsdSearch.push(tile.down());
            }

            for (OfflineTileStorage.Tile tile : bsdSearch.closedSet()) {
                mNotCancelled = mProcessor.process(tile);
                if (!mNotCancelled) {
                    break;
                }
            }
        }
	}

    private final static int   KReportProgressEveryTiles = 500;
    private class CountingTileProcessor extends SAsyncTask<Void, Integer> implements IITileProcessor {
		private int  						mCount;
		private final SizeEstimateObserver 	mObserver;
		private final TileWalker			mWalker;
		public CountingTileProcessor(SizeEstimateObserver observer) {
            super("CountingTileProcessor");
			mObserver = observer;
			mWalker = new TileWalker(this);
		}

		public boolean process(OfflineTileStorage.Tile tile) {
            if (++mCount % KReportProgressEveryTiles == 0) {
                this.postProgress(mCount);
            }
            return true;
		}

		@Override
		protected void onProgress(Integer tilesNo) {
			mObserver.sizeEstimate(tileEstimate(tilesNo)*KSizePerTileKB, sizeOfExistingTilesInOffline(), false);
		}


        @Override
        protected Void backgroundProcess() throws Exception {
            sizeOfExistingTilesInOffline();
            try {
                mWalker.processTiles(mFromZoom, mTileFactory.zoomRange().bigger(), mTrackDocuments);
            } catch (TileProcessingException | InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onTaskComplete(TaskResult<Void> result) {
            mCount = mCount;
            mObserver.sizeEstimate(tileEstimate(mCount)*KSizePerTileKB, sizeOfExistingTilesInOffline(), true);
            mMaxProjectedTiles = mCount;
        }
    }

	private class DownloadingTileProcessor extends OSMOfflineTilesDownloadTask.DownloadTask implements IITileProcessor
	{
		private final TileWalker		mWalker;
		public DownloadingTileProcessor(int count)
		{
            super(count, tileEstimate(count));
			mWalker = new TileWalker(this);
		}

		public boolean process(OfflineTileStorage.Tile tile) throws TileProcessingException, InterruptedException {
            downloadTile(tile);
            return !isCancelled();
		}

		@Override
		protected Void doInBackground(Void... params)
		{
			try {
				mWalker.processTiles(mFromZoom, mTileFactory.zoomRange().bigger(), mTrackDocuments);
			} catch (TileProcessingException | InterruptedException e) {
				e.printStackTrace();
			}
			return super.doInBackground(params);
		}

	}

	private final TracksView.TrackDocument[]    mTrackDocuments;
	private final int 							mFromZoom;
	private CountingTileProcessor				mCountingTileProcessor;
	private int									mBandWidthMts;
	private Integer   							mMaxProjectedTiles;
	public OSMOfflineTilesDownloadTaskForTrack(OfflineTileStorage offlineTiles, TracksView.TrackDocument[] trackDocuments, int fromZoom)
	{
		super(offlineTiles, OSMMapsApplication.instance().tileFactory());
		mFromZoom = fromZoom;
		mTrackDocuments = trackDocuments;
	}

	public void sizeEstimate(SizeEstimateObserver observer) {
		cancel();
		mCountingTileProcessor = new CountingTileProcessor(observer);
		mCountingTileProcessor.execute();
	}

	public void setRangeMts(int rangeMts) {
		mBandWidthMts = rangeMts;
	}

	@Override
	public void execute(DownloadObserver observer) {
		if (mDownloadTask == null) {
			mDownloadTask = new DownloadingTileProcessor(mMaxProjectedTiles);
		}
		super.execute(observer);
	}

	@Override
	public void cancel() {
		super.cancel();
		if (mCountingTileProcessor != null) {
			mCountingTileProcessor.cancel();
		}
	}
}
