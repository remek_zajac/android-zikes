//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.osm;

import net.remekzajac.maps.GeoDegPoint;
import net.remekzajac.maps.GeoDegPoint.Bounds;
import net.remekzajac.maps.IITileFactory;
import net.remekzajac.maps.ReferenceMap;
import net.remekzajac.utils.DoublePoint;
import net.remekzajac.utils.DoubleVector;
import net.remekzajac.utils.IntegerPoint;
import net.remekzajac.utils.IntegerVector;

import android.location.Location;
import android.util.Log;

public class OSMTilesReferenceMap implements ReferenceMap {
	public static final int 	KTilesWidth 			= 256;
	public static final int 	KTilesHeight 			= 256;
	public static final int 	KMaxZoom 				= 18;
	public static final int 	KMinZoom 				= 0;

	public class Rotation implements ReferenceMap.Rotation
	{
		private float mPrevious;
		private float mCurrent;
		public boolean set(float newRotation) {
			newRotation = normalise(newRotation);
			if ( newRotation != mCurrent ) {
				mPrevious = mCurrent;
				mCurrent = newRotation;
				/*
				 * Both current and previous are now in range of -180..180. The 'greater than' relation specifies the direction
				 * of the rotation and so if rotating from 10deg to 350deg the rotation would go around nearly full circle.
				 * However it would be cheaper to turn anti-clockwise and hence from:
				 * * 10deg to -10deg
				 * or
				 * * 370deg to 350deg
				 * We would like to keep the mCurrent constantly normalised for the above comparison to always work, so we'd be
				 * only manipulating the previous.
				 */
				float dangle = mPrevious-newRotation;
				if (dangle > 180) {
					//previous greater
					mPrevious -= 360;
				} else if (dangle < -180) {
					mPrevious += 360;
				}
				return true;
			}
			return false;
		}

		public float previous()
		{
			return mPrevious;
		}

		public float current()
		{
			return mCurrent;
		}

		public float normalise(float angle) {
			return (angle %= 360) >= 0 ? (angle < 180) ? angle : angle - 360 : (angle >= -180) ? angle : angle + 360;
		}

		public void complete(float reachedAngle)
		{
			mPrevious = reachedAngle;
		}

		public boolean pending()
		{
			return mCurrent != mPrevious;
		}
	}


	private class MapTraits extends ReferenceMap.MapTraits
	{
		public Rotation	   	    mRotation;
		private IntegerVector[] mCentreTile;
		public long             mTileWidthMts;
		public MapTraits(GeoDegPoint centre, int zoom, float scale) {
			mZoom = Math.min(Math.max(zoom, KMinZoom), KMaxZoom);
			mScale = scale;
			mZoomRange = OSMTileFactory.KZoomRange;
			mRotation = new Rotation();
			recalibrateForCentre(centre);
		}

		public void recalibrateForCentre(GeoDegPoint centre) {
			mGeoCentre    = centre;
			mCentreTile   = deg2num(mGeoCentre, mZoom);
			DoubleVector centreTile = new DoubleVector(centreTile());
;			GeoDegPoint centreTileOrigin = num2deg(centreTile, mZoom);
			GeoDegPoint nextTileOrigin   = num2deg(
				centreTile.add(
					new IntegerVector(1,0)
				), mZoom
			);
			mTileWidthMts = nextTileOrigin.distanceFromMts(centreTileOrigin);
		}

		public int set(GeoDegPoint centre, int zoom, float scale) {
			int bWhatsChangedVector = 0;
			zoom = Math.min(Math.max(zoom, KMinZoom), KMaxZoom);
			if (zoom != mZoom) {
				mZoom = zoom;
				bWhatsChangedVector |= ReferenceMap.MapViews.BZoom;
			}

			if (scale != mScale) {
				mScale = scale;
				bWhatsChangedVector |= ReferenceMap.MapViews.BScale;
			}

			if (bWhatsChangedVector != 0 || !geo2pix(centre).equals(geo2pix(mGeoCentre))) {
				recalibrateForCentre(centre);
				bWhatsChangedVector |= ReferenceMap.MapViews.BPan;
			}
			return bWhatsChangedVector;
		}

		public int calculateMinZoomForRegion(GeoDegPoint.Bounds bounds) {
			double tileScreenSizeX = (double)mMapPixBounds.screenRect().width()/(KTilesWidth*mScale);
			double tileScreenSizeY = (double)mMapPixBounds.screenRect().height()/(KTilesHeight*mScale);
			int lonzoom = londelta2zoom((bounds.se().x()-bounds.nw().x())/tileScreenSizeX);
			int latzoom = latdelta2zoom((bounds.nw().y()-bounds.se().y())/tileScreenSizeY);
			return Math.min(lonzoom, latzoom);
		}

		@Override
		public Bounds screenGeoBounds() {
			Bounds result = new Bounds(
				pix2geo(
					new IntegerPoint(mMapPixBounds.screenRect().left, mMapPixBounds.screenRect().top)
				),
				pix2geo(
					new IntegerPoint(mMapPixBounds.screenRect().right, mMapPixBounds.screenRect().bottom)
				)
			);
			return result;
		}

		/**
		 * @return Which tile does the mGeoCentre aligns with
         */
		public IntegerVector centreTile() { return mCentreTile[0]; }

		/**
		 * @return What pix offset (unscaled) does mGeoCentre aligns with
		 *         within centreTile
		 */
		public IntegerVector centreTilePixOffset() { return mCentreTile[1]; }

		@Override
		public net.remekzajac.maps.ReferenceMap.Rotation rotation()
		{
			return mRotation;
		}
	}

    static public IntegerVector[] deg2num(GeoDegPoint geoPoint, int zoom) {
    	Double lat_rad = Math.toRadians(geoPoint.y());
    	int n = (int)Math.pow(2, zoom);
    	double xtiled = (geoPoint.x()+180.0) / 360.0 * n;
    	double ytiled = ((1.0-Math.log(Math.tan(lat_rad) + (1 / Math.cos(lat_rad))) / Math.PI) / 2.0 *n);
		IntegerVector tileNo  = new IntegerVector((int)xtiled, (int)ytiled);
		IntegerVector tilePix = new IntegerVector(
			(int)((xtiled-tileNo.x()) * KTilesWidth),
			(int)((ytiled-tileNo.y()) * KTilesWidth)
		);
    	return new IntegerVector[] {tileNo, tilePix};
    }

    static public GeoDegPoint num2deg(DoublePoint tileXY, int zoom) {
    	int n = (int)Math.pow(2, zoom);
    	Double lon_deg = 1.0 * tileXY.x() / n * 360.0 - 180.0;
    	Double lat_rad = Math.atan(Math.sinh(Math.PI * (1 - 2.0 * tileXY.y() / n)));
    	return GeoDegPoint.createCappedToExtremas(lon_deg, Math.toDegrees(lat_rad));
    }

    //the method returns the finest zoom the supplied longitudal range of a single tile will entirely fit
    static public int londelta2zoom(Double lonDelta) {
    	return (int)(Math.log(360.0/lonDelta)/Math.log(2));
    }

    //the method returns the finest zoom the supplied latitudal range of a single tile will entirely fit
    static public int latdelta2zoom(double latDelta) {
        double latdeltaradians = Math.toRadians(Math.abs(latDelta));
        double result = Math.log(Math.PI/Math.log(Math.tan(latdeltaradians)+(1.0/Math.cos(latdeltaradians))))/Math.log(2)+1;
        return (int)result;
    }

	public class RotationRequest implements ReferenceMap.Request {
		private float	mRotation;
		private int		mWhatsAlsoChanged;
		public RotationRequest() {
			mWhatsAlsoChanged = 0;
			mRotation = mMapTraits.mRotation.current();
		}

		protected int doExecute() {
			int result = mWhatsAlsoChanged;
			if (mMapTraits.mRotation.set(mRotation)) {
				result |= ReferenceMap.MapViews.BRotation;
			}
			return result;
		}

		public void execute() {
			if (mContinuedPanAndScale) {
				mCachedRequests = this;
			}
			else {
				invalidate(doExecute());
			}
		}

		public net.remekzajac.maps.ReferenceMap.Request pan(GeoDegPoint newCentre) { return this; }
		public net.remekzajac.maps.ReferenceMap.Request pan(Location newCentre) { return this; }
		public net.remekzajac.maps.ReferenceMap.Request pan(IntegerPoint newCentre) { return this; }
		public net.remekzajac.maps.ReferenceMap.Request zoom(int newZoom) { return this; }
		public net.remekzajac.maps.ReferenceMap.Request dzoom(int zoomDelta) { return this; }
		public net.remekzajac.maps.ReferenceMap.Request scale(float newScale) { return this; }
		public net.remekzajac.maps.ReferenceMap.Request region(Bounds bounds) { return this; }
		public net.remekzajac.maps.ReferenceMap.Request rotation(float newRotation) {
			mRotation = newRotation;
			return this;
		}

		public net.remekzajac.maps.ReferenceMap.Request notify(int whatsAlsoChanged) {
			mWhatsAlsoChanged |= whatsAlsoChanged;
			return this;
		}
	}

	public class Request extends RotationRequest
	{
		public GeoDegPoint		mCentre;
		public int				mZoom;
		public float			mScale;
		public Request() {
			mCentre = mMapTraits.mGeoCentre;
			mZoom = mMapTraits.mZoom;
			mScale = mMapTraits.mScale;
		}

		@Override
		public int doExecute() {
			int whatsChangedVector = super.doExecute();
			return whatsChangedVector | mMapTraits.set(mCentre, mZoom, mScale);
		}

		@Override
		public net.remekzajac.maps.ReferenceMap.Request pan(GeoDegPoint newCentre) {
			mCentre = newCentre;
			return this;
		}

		@Override
		public net.remekzajac.maps.ReferenceMap.Request pan(Location newCentre) {
			mCentre = new GeoDegPoint(newCentre);
			return this;
		}

		@Override
		public net.remekzajac.maps.ReferenceMap.Request pan(IntegerPoint newCentre) {
			mCentre = OSMTilesReferenceMap.this.pix2geo(newCentre);
			return this;
		}

		@Override
		public net.remekzajac.maps.ReferenceMap.Request zoom(int newZoom) {
			mZoom = newZoom;
			return this;
		}

		@Override
		public net.remekzajac.maps.ReferenceMap.Request dzoom(int zoomDelta) {
			mZoom += zoomDelta;
			return this;
		}

		@Override
		public net.remekzajac.maps.ReferenceMap.Request scale(float newScale) {
			mScale = newScale;
			return this;
		}

		@Override
		public net.remekzajac.maps.ReferenceMap.Request region(Bounds bounds) {
			mZoom = mMapTraits.calculateMinZoomForRegion(bounds);
			mCentre = bounds.centre();
			return this;
		}
	}

	private final MapTraits 		mMapTraits;
	private MapViews 				mView;
	private final float 			mNativeScale;
	private ReferenceMap.Request	mCachedRequests;
	private boolean					mContinuedPanAndScale;
	public OSMTilesReferenceMap(GeoDegPoint centre, int zoom, float scale) {
		mNativeScale = scale;
		mMapTraits = new MapTraits(centre, zoom, mNativeScale);
	}

	public ReferenceMap.Request newRequest() {
		return mContinuedPanAndScale ? new RotationRequest() : new Request();
	}

	public void	continuedPanAndScale(GeoDegPoint newCentre, float scale) {
		mContinuedPanAndScale = true;
		invalidate(mMapTraits.set(newCentre, mMapTraits.mZoom, scale*mMapTraits.mScale));
	}

	public void finishPanAndScale() {
		mContinuedPanAndScale = false;
		int dzoom = Math.round((float)(Math.log(mMapTraits.mScale/mNativeScale)/Math.log(2)));
		if (dzoom != 0) {
			mCachedRequests = mCachedRequests != null ? mCachedRequests : new Request();
			mCachedRequests.zoom(mMapTraits.mZoom + dzoom);
			mCachedRequests.scale(mNativeScale);
		}

		if (mCachedRequests != null) {
			mCachedRequests.execute();
			mCachedRequests = null;
		}
	}

	public void newView(MapViews view) {
		mView = view;
		mMapTraits.mMapPixBounds = view.mapPixBounds();
		mMapTraits.recalibrateForCentre(mMapTraits.mGeoCentre);
	}

	public void invalidate(int bWhatsChangedVector) {
		if (bWhatsChangedVector != 0 && mView != null) {
			mView.invalidate(bWhatsChangedVector);
		}
	}

	public int mts2Pix(int distanceInMts) {
		return (int)(distanceInMts*(KTilesWidth*mMapTraits.mScale)/mMapTraits.mTileWidthMts);
	}

	public long pix2Mts(long distanceInPix) {
		return (long)(distanceInPix*mMapTraits.mTileWidthMts/(KTilesWidth*mMapTraits.mScale));
	}

	public ReferenceMap.MapTraits getTraits()
	{
		return mMapTraits;
	}

	public float zoom2Scale(int zoom) {
		return mMapTraits.mScale*(float)Math.pow(2, mMapTraits.mZoom-zoom);
	}

	public boolean isShown()
	{
		return mView != null;
	}

	public IntegerPoint geo2pix(GeoDegPoint location) {
		IntegerVector[] tilePix = deg2num(
			location, mMapTraits.mZoom
		);

		IntegerVector tileOffset = new IntegerVector(
			mMapTraits.centreTile(), tilePix[0]
		);
		IntegerVector pixOffset = new IntegerVector(
			mMapTraits.centreTilePixOffset(), tilePix[1]
		).multiply(mMapTraits.mScale);

		IntegerVector offsetFromCentrePix = tileOffset.multiply(KTilesWidth*mMapTraits.mScale).add(pixOffset);
		IntegerPoint pix = mMapTraits.mMapPixBounds.mapCentre().add(offsetFromCentrePix);
		return pix;
	}


	public IntegerPoint geo2pix(Location location) {
		if (location != null) {
			return geo2pix(
				new GeoDegPoint(
					location.getLongitude(), location.getLatitude()
				)
			);
		}
		return null;
	}

	public GeoDegPoint pix2geo(IntegerPoint pixXY) {
		IntegerVector dpix = new IntegerVector(
			mMapTraits.mMapPixBounds.mapCentre(),
			pixXY
		).divide(mMapTraits.mScale)
		 .add(mMapTraits.centreTilePixOffset());
		DoubleVector dpixF = new DoubleVector(
			dpix.x(), dpix.y()
		).divide(KTilesWidth)
		 .add(mMapTraits.centreTile());
		return num2deg(dpixF, mMapTraits.mZoom);
	}

}
