//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.remekzajac.osm;

import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

import net.remekzajac.utils.DoublePoint;
import net.remekzajac.utils.IntegerVector;
import net.zikes.R;
import net.remekzajac.maps.GeoDegPoint;
import net.remekzajac.maps.IITileFactory;
import net.remekzajac.tilestorage.IIOfflineTileStorage;
import net.remekzajac.utils.AsyncTaskQueue;
import net.remekzajac.utils.IntegerPoint;
import net.remekzajac.utils.IntegerRange;
import net.remekzajac.utils.TimeDamHandler;
import net.remekzajac.utils.ZoomTree2D;

public class OSMOfflineTileFactory implements IITileFactory, IIOfflineTileStorage.Observer
{
	//static private int				DNoTiles;
	//static private int				DNoOpenTiles;
	static private int				KTileContourStrokePercentOfTileWidth = 8;
	static private int				KMinStrokeWidth						 = 4;
	static private int				KNotifyNotMoreOftenThanMs			 = 1000;
	static private int				KShallowCheckedMask					 = 1;
	static private int				KDeepCheckMask					 	 = 3;
	static private int				KExistsMask							 = 4;
	static private int				KMarkedForDeletionMask			     = 8;
	static private int				KOpenMask							 = 16;
	private class Tile extends ZoomTree2D.ZoomTree2DElement<Tile> implements IIOfflineTileStorage.Tile, IITileFactory.Tile
	{
		private TimeDamHandler				mTimeDamCallbackHandler;
		private AsyncTaskQueue.Task<Void>	mEvaluateTask;
		private AtomicInteger				mStatus;
		/**************************************************************************
		 *
		 *
		 *
		 * Construction and accessors
		 *
		 *
		 *
		 **************************************************************************/
		public Tile(int x, int y, int zoom)
		{
			super(x,y,OSMOfflineTileFactory.this.zoomRange().bigger()-zoom);
			mStatus = new AtomicInteger();
			assert(isValidTile(x,y,zoom));
			//Log.d("Tiles", "tiles: open:"+ ++DNoOpenTiles+" in existence:"+ ++DNoTiles);
		}

		public Tile(Tile parent, ChildIndex placement)
		{
			super(parent, placement);
			mStatus = new AtomicInteger();
			//Log.d("Tiles", "tiles: open:"+ DNoOpenTiles+" in existence:"+ ++DNoTiles);
		}

		public Tile open()
		{
			refCount(1);
			setStatus(KOpenMask);
			return this;
		}

		public int zoom()
		{
			return OSMOfflineTileFactory.this.zoomRange().bigger()-_zoom();
		}

		public String subpath()
		{
			return OSMTileFactory.Tile.subpath(x(), y(), zoom());
		}

		public IntegerPoint tileXY()
		{
			return new IntegerPoint(x(), y());
		}

		public String diagnoseString()
		{
			String status = "[";
			if (getStatus(KShallowCheckedMask))
			{
				if ( getStatus(KDeepCheckMask) )
				{
					status += "deep-checked  , ";
				}
				else
				{
					status += "shalow-checked, ";
				}

				if (getStatus(KExistsMask))
				{
					status += "exists, ";
				}
				else
				{
					status += "absent, ";
				}
			}
			else
			{
				status += "unchecked     , ";
			}

			if (isInView())
			{
				status += "in view    ";
			}
			else
			{
				status += "not in view";
			}
			status += ']';
			return super.diagnoseString()+status;
		}

		private Tile findOrCreate(int x, int y, int zoom)
		{
			Tile result = null;
			if (isValidTile(x, y, zoom))
			{
				result = this.get(x, y, OSMOfflineTileFactory.this.zoomRange().bigger()-zoom);
				if (result == null)
				{
					result = new Tile(x, y, zoom);
				}
			}
			return result;
		}

		private boolean getStatus(int mask)
		{
			return (mStatus.get() & mask) != 0;
		}

		private void setStatus(int mask)
		{
			mStatus.set(mStatus.get() | mask);
		}

		private void clearStatus(int mask)
		{
			mStatus.set(mStatus.get() & ~mask);
		}

		private boolean setStatus(int mask, boolean value)
		{
			if (value)
			{
				setStatus(mask);
			}
			else
			{
				clearStatus(mask);
			}
			return value;
		}

		public String toString()
		{
			return "tile[x:"+x()+" y:"+y()+" zoom:"+zoom()+"]";
		}

		/*protected void finalize()
		{
			Log.d("Tiles", "tiles: open:"+ DNoOpenTiles+" in existence:"+ --DNoTiles);			;
		}	*/



		/**************************************************************************
		 *
		 *
		 *
		 * Drawing
		 *
		 *
		 *
		 **************************************************************************/
		public boolean draw(Canvas canvas, Rect rect, boolean showInfo)
		{
			if ( getStatus(KDeepCheckMask) )
			{
				if (hasTiles())
				{
					return doDraw(canvas, rect.left, rect.top, rect.width());
				}
			}
			else
			{
				canvas.drawBitmap(mWaitingForTileBitmap, null, rect, mWaitingBitmapPaint);
			}
			return false;
		}

		private boolean doDraw(Canvas canvas, int pixx, int pixy, int pixScaleTo)
		{
			int strokeWidth = KTileContourStrokePercentOfTileWidth*pixScaleTo/100;
			if (strokeWidth >= KMinStrokeWidth)
			{
				int pixScaleToAtLowerZoom = pixScaleTo/2;
				doDraw(canvas, child(ChildIndex.ETopLeft), 		pixx, 						pixy,						pixScaleToAtLowerZoom);
				doDraw(canvas, child(ChildIndex.ETopRight), 	pixx+pixScaleToAtLowerZoom, pixy,						pixScaleToAtLowerZoom);
				doDraw(canvas, child(ChildIndex.EBottomLeft), 	pixx, 						pixy+pixScaleToAtLowerZoom, pixScaleToAtLowerZoom);
				doDraw(canvas, child(ChildIndex.EBottomRight), 	pixx+pixScaleToAtLowerZoom, pixy+pixScaleToAtLowerZoom, pixScaleToAtLowerZoom);
				if (!getStatus(KExistsMask))
				{
					return false;
				}
				mContourPaint.setStyle(Paint.Style.STROKE);
				mContourPaint.setStrokeWidth(strokeWidth);
				strokeWidth /= 2;
			}
			else
			{
				strokeWidth = 0;
				mContourPaint.setStyle(Paint.Style.FILL);
			}


			Rect rect = new Rect(pixx+strokeWidth,
					 pixy+strokeWidth,
					 pixx+pixScaleTo-strokeWidth,
					 pixy+pixScaleTo-strokeWidth);
			canvas.drawRect(rect, mContourPaint);

			return false;
		}

		private void doDraw(Canvas canvas, Tile tile, int pixx, int pixy, int pixScaleTo)
		{
			if (tile != null)
			{
				tile.doDraw(canvas, pixx, pixy, pixScaleTo);
			}
		}



		/**************************************************************************
		 *
		 *
		 *
		 * Evaluation
		 *
		 *
		 *
		 **************************************************************************/
		public boolean request(final Callback callback)
		{
			mTimeDamCallbackHandler = new TimeDamHandler(KNotifyNotMoreOftenThanMs, new Runnable() {
                public void run() {
                	Log.d("Tiles", "Invalidating: "+Tile.this.diagnoseString());
                	callback.callback(Tile.this);
                }});

			if (getStatus(KDeepCheckMask))
			{
				return true;
			}

			mEvaluateTask = new AsyncTaskQueue.Task<Void>() {
				public Void backgroundProcess()
				{
					deepEvaluate();
					return null;
				}

				public void foregroundFinished(Void result)
				{
					mEvaluateTask = null;
					notifyChange(true);
				}
			};
			mTileTaskQ.add(mEvaluateTask);
			return false;
		}

		public boolean deepEvaluate()
		{
			if (!getStatus(KDeepCheckMask))
			{
				shallowEvaluate();
				if (zoom() < OSMOfflineTileFactory.this.zoomRange().bigger())
				{
					evaluateChild(ChildIndex.ETopLeft);
					evaluateChild(ChildIndex.ETopRight);
					evaluateChild(ChildIndex.EBottomLeft);
					evaluateChild(ChildIndex.EBottomRight);
				}
				setStatus(KDeepCheckMask);
			}
			return hasTiles();
		}

		private boolean shallowEvaluate()
		{
			setStatus(KShallowCheckedMask);
			return setStatus(KExistsMask, mTileStorage.exists(this));
		}

		private void evaluateChild(ChildIndex placement)
		{
			Tile potentialChild = getOrPut(new Tile(this, placement));
			if (!potentialChild.deepEvaluate())
			{
				delete(placement);
			}
		}

		public void notifyChange(boolean foreground)
		{
			if (mTimeDamCallbackHandler != null)
			{
				if (foreground)
				{
					mTimeDamCallbackHandler.execute();
				}
				else
				{
					mTimeDamCallbackHandler.postDam();
				}
			}
			else if (parent() != null)
			{
				parent().notifyChange(foreground);
			}
		}

		private boolean hasTiles()
		{
			assert(getStatus(KDeepCheckMask));
			return (getStatus(KExistsMask) || hasChildren()) ? true : false;
		}

		public boolean exists()
		{
			if (getStatus(KShallowCheckedMask) )
			{
				if ( getStatus(KDeepCheckMask) )
				{
					return hasTiles();
				}
				return getStatus(KExistsMask);
			}
			return shallowEvaluate();
		}

		//TODO: this may be unnecessary
		public boolean isInView()
		{
			if (getStatus(KOpenMask))
			{
				return true;
			}
			else if (parent() != null)
			{
				return parent().isInView();
			}
			return false;
		}



		/**************************************************************************
		 *
		 *
		 *
		 * Maintenance (deleting the tile objects from the tree, not files from the disk)
		 *
		 *
		 *
		 **************************************************************************/
		public void close()
		{
			assert(getStatus(KOpenMask));
			clearStatus(KOpenMask);
			mTileTaskQ.cancel(mEvaluateTask);
			mEvaluateTask = null;
			mTimeDamCallbackHandler = null;
			mTileTaskQ.add(new AsyncTaskQueue.Task<Void>() {
				public Void backgroundProcess()
				{
					return null;
				}

				public void foregroundFinished(Void result)
				{
					//Put at the end of the task queue, to avoid deleting the tile node when it's
					//still necessary due to rezooming.
					deleteMe();
					refCount(-1);
				}
			});
			//Log.d("Tiles", "tiles: open:"+ --DNoOpenTiles+" in existence:"+ DNoTiles);
		}

		public void deleteMe()
		{
			//So we're going bottom up on this and we do _not_ delete if:
			//(1) this has any children, which (since we went bottom up) means that the children
			//    had a reason to stay and so this has to stay to visualise these children (see (2) and (3)).
			//(2) this is currently held by whoever called the factory method and never
			//    called close() - essentially the ref count is held open. For all practical
			//    purposes, the whoever is the TileWindow, which means this is currently and directly
			//    (as opposed to indirectly, see (3)).
			//(3) this is currently in view (has a parent that meets (2)) and represents an
			//    exiting tile (holds relevant things to display).
			for (Iterator it = childrenIterator(); it.hasNext();it.next().deleteMe());
			if (!(hasChildren() || getStatus(KOpenMask) || (exists() && isInView())))
			{
				super.deleteMe();
			}
		}

		/**************************************************************************
		 *
		 *
		 *
		 * Deleting tile files
		 *
		 *
		 *
		 **************************************************************************/
		public void delete()
		{
			mTileTaskQ.add(new AsyncTaskQueue.Task<Void>() {
				public Void backgroundProcess()
				{
					deleteBackground();
					return null;
				}

				public void foregroundFinished(Void result)
				{
				}
			});
		}

		private void deleteBackground()
		{
			for (Iterator it = childrenIterator(); it.hasNext();)
			{
				it.next().deleteBackground();
			}
			if (exists())
			{
				deleteRegion();
			}
		}

		private void deleteRegion()
		{
			assert(exists());
			Stack<Tile> deletionStack = new Stack<Tile>();
			deletionStack.push(this);
			while (!deletionStack.isEmpty())
			{
				Tile tile = deletionStack.pop();
				for (int i = 0; i < 4; i++)
				{
					Tile sibling = findOrCreate(tile.x()+ZoomTree2D.KSiblingIterationSteps[i][0],
												tile.y()+ZoomTree2D.KSiblingIterationSteps[i][1],
												tile.zoom());
					if (sibling != null && sibling.exists() && !sibling.getStatus(KMarkedForDeletionMask))
					{
						sibling.setStatus(KMarkedForDeletionMask);
						deletionStack.push(sibling);
					}
				}

				mTileStorage.delete(tile);
				tile.clearStatus(KExistsMask);
				tile.notifyChange(false);
				tile.deleteMe();
			}
		}
	}


	public IITileFactory.Tile createTile(int tileX, int tileY, int forZoom) {
		return mTileTree.getOrPut(new Tile(tileX, tileY, forZoom)).open();
	}

	public IntegerVector[] deg2num(GeoDegPoint geoPoint, int zoom) {
		return OSMTilesReferenceMap.deg2num(geoPoint, zoom);
	}

	public GeoDegPoint num2deg(IntegerPoint tileXY, int zoom) {
		return OSMTilesReferenceMap.num2deg(
			new DoublePoint(tileXY.x(), tileXY.y()
			), zoom);
	}

	public int maxTiles(int zoom)
	{
		return (int)Math.pow(2, zoom);
	}

	public IntegerRange zoomRange()
	{
		return OSMTileFactory.KZoomRange;
	}

	public IntegerPoint nativeTileSize()
	{
		return OSMTileFactory.KNativeTileSize;
	}

	protected final static int 			KInfoBitmapsOpacity = 128;
	private final IIOfflineTileStorage 	mTileStorage;
	private final Paint					mContourPaint;
	private final Paint					mWaitingBitmapPaint;
	private AsyncTaskQueue<Void>		mTileTaskQ;
	protected final Bitmap				mWaitingForTileBitmap;
	private final ZoomTree2D<Tile> 		mTileTree;
	private int 						mHandedOutTiles;
	public OSMOfflineTileFactory(Context context, IIOfflineTileStorage tileStorage) {
		mTileStorage = tileStorage;
		mContourPaint = new Paint();
		mContourPaint.setAntiAlias(true);
		mContourPaint.setDither(true);
		mContourPaint.setStrokeJoin(Paint.Join.ROUND);
		mContourPaint.setColor(context.getResources().getColor(R.color.offlineTileContour));
		mWaitingBitmapPaint = new Paint();
		mWaitingBitmapPaint.setAlpha(KInfoBitmapsOpacity);
		mTileTaskQ = new AsyncTaskQueue<Void>();
		mWaitingForTileBitmap = Bitmap.createScaledBitmap(
					BitmapFactory.decodeResource(context.getResources(), R.drawable.waiting),
					OSMTilesReferenceMap.KTilesWidth, OSMTilesReferenceMap.KTilesHeight, false);
		mTileTree = new ZoomTree2D<Tile>();
	}

	public void diagnose()
	{
		mTileTree.diagnose();
	}

	public boolean isValidTile(int x, int y, int zoom)
	{
		if (zoomRange().contains(zoom))
		{
			int maxTiles = maxTiles(zoom);
			if (x >= 0 && x <= maxTiles && y >= 0 && y <= maxTiles)
			{
				return true;
			}
		}
		return false;
	}

	private void refCount(int increment)
	{
		if ((mHandedOutTiles += increment) == 0)
		{
			mTileTree.reset();
		}
	}

	public void tileChange(String substring, boolean exists)
	{
		/*if (mHandedOutTiles > 0)
		{
			Scanner scanner = new Scanner(substring).useDelimiter("/");
			int zoom = scanner.nextInt();
			int x = scanner.nextInt();
			int y = scanner.nextInt();
			Tile updatedTile = new Tile(x, y, zoom);
			if (updatedTile.exists())
			{
				mTileTree.getOrPut(updatedTile);
			}
			else
			{
				if ((updatedTile = mTileTree.get(updatedTile)) != null)
				{
					updatedTile.
				}
			}

		}*/
	}
}
