//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.osm;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import net.remekzajac.utils.DoublePoint;
import net.remekzajac.utils.IntegerVector;
import net.zikes.OSMMapsApplication;
import net.zikes.R;
import net.remekzajac.maps.GeoDegPoint;
import net.remekzajac.maps.IITileFactory;
import net.remekzajac.tilestorage.IITileStorage;
import net.remekzajac.tilestorage.StoredBitmapTileFactory;
import net.remekzajac.utils.IntegerPoint;
import net.remekzajac.utils.IntegerRange;

public class OSMBitmapTileFactory extends StoredBitmapTileFactory implements IITileFactory
{
	//private static int DLoadedTiles;
	private class Tile extends StoredBitmapTileFactory.Tile implements IITileFactory.Tile
	{
		private final OSMTileFactory.Tile 	mOSMTile;
		private IITileFactory.Tile.Callback mCallback;

		public Tile(int tileX, int tileY, int zoom)
		{
			mOSMTile = new OSMTileFactory.Tile(tileX, tileY, zoom);
			//Log.d("Tiles", "Live tile count:"+ ++DLoadedTiles);
		}

		public IntegerPoint tileXY()
		{
			return mOSMTile.tileXY();
		}

		public int zoom()
		{
			return mOSMTile.zoom();
		}

		public void foregroundFinished(IITileStorage.ProcessedTile.Status status)
		{
			super.foregroundFinished(status);
			if (status != IITileStorage.ProcessedTile.Status.ECancelled)
			{ //if we're cancelled, nobody is looking for an answer.
				assert mCallback != null;
				mCallback.callback(this);
			}
			mCallback = null;
		}


		public String subpath()
		{
			return mOSMTile.subpath();
		}

		public boolean request(Callback callback)
		{
			mCallback = callback;
			return super.request();
		}

		/*protected void finalize()
		{
			Log.d("Tiles", "Tile count:"+ --DLoadedTiles);
		}*/
	}

	private static class InfoBitmaps implements StoredBitmapTileFactory.InfoBitmaps
	{
		private final Bitmap 		mFailedTileBitmap;
		private final Bitmap 		mDownlodedTileBitmap;
		private final Bitmap 		mWaitingForTileBitmap;
		protected final static int 	KInfoBitmapsOpacity = 64;
		public InfoBitmaps(Context context)
		{
			mFailedTileBitmap = Bitmap.createScaledBitmap(
					BitmapFactory.decodeResource(context.getResources(), R.drawable.failedtile),
					OSMTilesReferenceMap.KTilesWidth, OSMTilesReferenceMap.KTilesHeight, false); //TODO, the OSM dependency should be removed.
			mDownlodedTileBitmap = Bitmap.createScaledBitmap(
					BitmapFactory.decodeResource(context.getResources(), R.drawable.downloaded),
					OSMTilesReferenceMap.KTilesWidth, OSMTilesReferenceMap.KTilesHeight, false);
			mWaitingForTileBitmap = Bitmap.createScaledBitmap(
					BitmapFactory.decodeResource(context.getResources(), R.drawable.waiting),
					OSMTilesReferenceMap.KTilesWidth, OSMTilesReferenceMap.KTilesHeight, false);
		}

		public Bitmap failed()
		{
			return mFailedTileBitmap;
		}

		public Bitmap downloaded()
		{
			return mDownlodedTileBitmap;
		}

		public Bitmap waiting()
		{
			return mWaitingForTileBitmap;
		}

		public int opacity()
		{
			return KInfoBitmapsOpacity;
		}

	}

	public OSMBitmapTileFactory(IITileStorage tileStorage, Context context) {
		super(new InfoBitmaps(context), tileStorage);
	}

	public IITileFactory.Tile createTile(int tileX, int tileY, int forZoom) {
		return new Tile(tileX, tileY, forZoom);
	}

	public IntegerVector[] deg2num(GeoDegPoint geoPoint, int zoom) {
		return OSMTilesReferenceMap.deg2num(geoPoint, zoom);
	}

	public GeoDegPoint num2deg(IntegerPoint tileXY, int zoom) {
		return OSMTilesReferenceMap.num2deg(
			new DoublePoint(tileXY.x(), tileXY.y()
			), zoom);
	}

	public int maxTiles(int zoom)
	{
		return (int)Math.pow(2, zoom);
	}

	public IntegerRange zoomRange()
	{
		return OSMTileFactory.KZoomRange;
	}

	public IntegerPoint nativeTileSize()
	{
		return OSMTileFactory.KNativeTileSize;
	}
}
