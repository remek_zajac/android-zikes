//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.osm;

import android.util.Log;
import net.remekzajac.utils.IntegerPoint;
import net.remekzajac.utils.IntegerRange;

public abstract class OSMTileFactory
{
	public static class Tile
	{
		public void testValidity(int tileX, int tileY, int zoom)
		{
			if (!(zoom >= OSMTilesReferenceMap.KMinZoom && zoom <= OSMTilesReferenceMap.KMaxZoom &&
				tileX >= 0 && tileX < Math.pow(2, zoom) &&
				tileY >= 0 && tileY < Math.pow(2, zoom)))
			{
				Log.d("RemekApp", String.format("Invalid tile requested %d", zoom));
			}
		}

		protected final IntegerPoint mTileXY;
		protected final int	mZoom;
		public Tile(int tileX, int tileY, int zoom) {
			mTileXY = new IntegerPoint(tileX, tileY);
			mZoom = zoom;
			//testValidity(tileX, tileY, zoom);
		}

        public Tile(IntegerPoint tileXY, int zoom) {
            mTileXY = tileXY;
            mZoom = zoom;
            //testValidity(tileX, tileY, zoom);
        }


		public String subpath()
		{
			return subpath( mTileXY.x(), mTileXY.y(), mZoom);
		}

		public static String subpath(int x, int y, int zoom) {
			return String.format("%d/%d/%d", zoom, x, y);
		}

		public IntegerPoint tileXY() {
			return mTileXY;
		}

		public int zoom()
		{
			return mZoom;
		}

		public int gridDistance(Tile from) {
            int max = (int)Math.pow(2, mZoom);
            int halfMax = max/2;
            int dx = Math.abs(mTileXY.x() - from.tileXY().x());
            int dy = Math.abs(mTileXY.y() - from.tileXY().y());
            if (dx > halfMax) { //the world is round
                dx = max - dx;
            }
            if (dy > halfMax) {
                dy = max - dy;
            }
            return dx+dy;
		}

        public Tile left() {
            int x = mTileXY.x()-1;
            if (x < 0) {
                x = (int)Math.pow(2, mZoom);
            }
            return new Tile(x, mTileXY.y(), mZoom);
        }
        public Tile right() {
            int x = mTileXY.x()+1;
            int max = (int)Math.pow(2, mZoom);
            if (x > max) {
                x = 0;
            }
            return new Tile(x, mTileXY.y(), mZoom);
        }
        public Tile up() {
            int y = mTileXY.y()-1;
            if (y < 0) {
                y = (int)Math.pow(2, mZoom);
            }
            return new Tile(mTileXY.x(), y, mZoom);
        }
        public Tile down() {
            int y = mTileXY.y()+1;
            int max = (int)Math.pow(2, mZoom);
            if (y > max) {
                y = 0;
            }
            return new Tile(mTileXY.x(), y, mZoom);
        }

        @Override
        public boolean equals(Object other) {
            if((other == null) || (getClass() != other.getClass())){
                return false;
            }
            Tile otherTile = (Tile)other;
            return otherTile.tileXY().equals(tileXY()) && otherTile.zoom() == zoom();
        }


	}

	public static IntegerRange KZoomRange = new IntegerRange(OSMTilesReferenceMap.KMinZoom, OSMTilesReferenceMap.KMaxZoom);
	public static IntegerPoint KNativeTileSize = new IntegerPoint(OSMTilesReferenceMap.KTilesWidth, OSMTilesReferenceMap.KTilesHeight);
}
