//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************


package net.remekzajac.tracks;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import net.zikes.MainActivity;
import net.zikes.OSMMapsApplication;
import net.zikes.R;
import net.remekzajac.maps.MapView;
import net.remekzajac.maps.ReferenceMap;
import net.remekzajac.tracks.TrackSegmentsView.TrackPointProximitySensor;
import net.remekzajac.utils.IntegerPoint;
import net.remekzajac.utils.StringUtils;
import android.app.Activity;
import android.graphics.Canvas;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

public class TracksView extends MapView.SubView {

	public static class Distances {
		public final long mFromOriginMts;
		public final long mToDestinationMts;
		public Distances(long fromOriginMts, long toDestinationMts) {
			mFromOriginMts = fromOriginMts;
			mToDestinationMts = toDestinationMts;
		}
		public Distances reverse() {
			return new Distances(mToDestinationMts, mFromOriginMts);
		}
	}

	public interface TrackDocument
	{
		public enum RotationPolicy{
		    EOff,			//Never rotate
		    EFollow,		//follow the track in order of pix points
		    EReverseFollow, //follow the track in reverse order
		}

		public TrackGeoPointRegister	geoPoints();

		public void						follow(boolean follow);
		public boolean					isBeingFollowed();
		public void 					nextRotation();
		public RotationPolicy 			rotationPolicy();
		public TrackView 				createTrackViewFactory(TracksView tracksView, ReferenceMap referenceMap);
	}

	public abstract static class TrackDocumentBase1 implements TrackDocument {
		protected boolean mFollowing;
		public void follow(boolean follows) {
			mFollowing = follows;
		}
		public boolean isBeingFollowed() {
			return mFollowing;
		}
	}

	public abstract static class TrackDocumentBase2 extends TrackDocumentBase1 {
		protected final TrackGeoPointRegister	mGeoPoints;
		protected TrackDocumentBase2(TrackGeoPointRegister geoPoints) {
			mGeoPoints = geoPoints;
		}
		public TrackView createTrackViewFactory(TracksView tracksView, ReferenceMap referenceMap) {
			return new TrackSegmentsView(tracksView, this, referenceMap);
		}
		public TrackGeoPointRegister geoPoints() {
			return mGeoPoints;
		}
	}

	public static class SegmentDocument extends TrackDocumentBase2 {
		protected RotationPolicy 	mRotationPolicy;
		public SegmentDocument(TrackGeoPointRegister geoPoints) {
			super(geoPoints);
			mRotationPolicy = RotationPolicy.EFollow;
		}

		public void nextRotation() {
			switch (mRotationPolicy)
			{
			case EOff:
				mRotationPolicy = RotationPolicy.EFollow;
				break;
			case EFollow:
				mRotationPolicy = RotationPolicy.EReverseFollow;
				break;
			case EReverseFollow:
				mRotationPolicy = RotationPolicy.EFollow;
				break;
			}
		}

		public RotationPolicy rotationPolicy()
		{
			return mRotationPolicy;
		}
	}



	public interface TrackView {
		public interface ProximitySensor {
			public long distanceToReference();
		}
		public static final int KMaxDistanceForFollowingMts = 100;
		public ProximitySensor invalidate(int bWhatsChangedVector, IntegerPoint referenceForProximitySensor);
		public float getRotationAngle();
		public void draw(Canvas canvas) ;
		public void windroseClick();
		public TrackView follow(boolean follow, TrackView previousFollowedTrack);
		public TracksView.Distances distances();
		public TracksView.TrackDocument document();
	}



	/*****************************************************************************************************************
	 *
	 *
	 * A visitor pattern object collecting TrackProximitySensor sensor seeking for the one closest
	 * to the given reference
	 *
	 *
	  *****************************************************************************************************************/
	static public class TracksProximitySensor {
		private long mDistanceToReference = Long.MAX_VALUE;
		public TrackView nearestView;
		public TracksProximitySensor newCandiateTrack(TrackView.ProximitySensor candidate, TrackView forView) {
			if (candidate != null) {
				if (candidate.distanceToReference() < mDistanceToReference) {
					nearestView = forView;
					mDistanceToReference = candidate.distanceToReference();
				}
			}
			return this;
		}
	}


	private static final int KMaxZoomForRotation			= 16;
	private final Map<TrackDocument, TrackView>  			mTrackDocumentViews;
	private final ReferenceMap 								mReferenceMap;
	private View											mCockpit;
	private TextView										mDistanceToDestinationView;
	private TextView										mDistanceFromOriginView;
	private TextView										mDEleMtsView;
	private ProfileProgressView     						mProgressBar;
	private TrackView 										mFollowedTrackView;
	private final boolean									mDisableScreenRotationWhenRiding;

	//alternative c'tors
	//public TrackView(Context context)
	//public TrackView(Context context, AttributeSet attrs, int defStyle)

	public TracksView(MapView parent) {
		super(parent);
		mReferenceMap = OSMMapsApplication.instance().referenceMap();
		mTrackDocumentViews = new HashMap<TrackDocument, TrackView>();
        if (OSMMapsApplication.instance().preferences().pref_ScreenOrientation.get()
			.equals(getContext().getResources().getString(R.string.pref_screenOrientation_StickyWhenRiding))) {
        	mDisableScreenRotationWhenRiding = true;
        } else {
        	mDisableScreenRotationWhenRiding = false;
        }
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub

	}

	public void setCockpitControl(View cockpitView) {
		mCockpit = cockpitView;
		mDistanceToDestinationView = (TextView)cockpitView.findViewById(R.id.distanceToDestination);
		mDistanceFromOriginView = (TextView)cockpitView.findViewById(R.id.distanceFromOrigin);
		mProgressBar = (ProfileProgressView) cockpitView.findViewById(R.id.profileProgressView);
		mDEleMtsView = (TextView) cockpitView.findViewById(R.id.dele);
	}

	public void add(TrackDocument trackDocument) {
		mTrackDocumentViews.put(trackDocument, trackDocument.createTrackViewFactory(this, mReferenceMap));
		setVisible(true);
		invalidate();
		OSMMapsApplication.instance().showWindroseTip();
	}


	public void remove(TrackDocument trackDocument) {
		mTrackDocumentViews.remove(trackDocument);
		if (mTrackDocumentViews.size() == 0) {
			hide();
		}
	}

	public boolean shows(TrackDocument trackDocument) {
		return mTrackDocumentViews.containsKey(trackDocument);
	}

	public Set<TrackDocument> shownDocuments() {
        return mTrackDocumentViews.keySet();
	}

	public void windroseClick() {
		if (!OSMMapsApplication.instance().locationTracker().startFollowing(KMaxZoomForRotation) &&
			mFollowedTrackView != null) {
			mFollowedTrackView.windroseClick();
		}
	}

	private void hide() {
		setVisible(false);
		mReferenceMap.newRequest().rotation(0f).execute();
		mCockpit.setVisibility(View.GONE);
		mFollowedTrackView = null;
	}

	public void riding(boolean riding) {
		if (riding) {
			((Activity)getContext()).getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			if (mDisableScreenRotationWhenRiding) {
				((MainActivity)getContext()).disableOrientationChanges();
			}
			mCockpit.setVisibility(View.VISIBLE);
			mReferenceMap.newRequest().rotation(mFollowedTrackView.getRotationAngle()).execute();
		} else {
			((Activity)getContext()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			if (mDisableScreenRotationWhenRiding) {
				((MainActivity)getContext()).enableOrientationChanges();
			}
			mCockpit.setVisibility(View.GONE);
			mReferenceMap.newRequest().rotation(0f).execute();
		}
	}

	@Override
	public void doInvalidate(int bWhatsChangedVector) {
		if (mTrackDocumentViews.size() == 0 || !isShown()) {//no documents to draw
			return;
		}

		if (bWhatsChangedVector == ReferenceMap.MapViews.BRotation) {
			//if it's only rotation that changing, don't bother recalculating
			return;
		}


		IntegerPoint warmLocationPix = mReferenceMap.geo2pix(
				OSMMapsApplication.instance().locationTracker().warmLocation());
		TracksProximitySensor nearestTrackPointSensor =	new TracksProximitySensor();
		for (TrackView trackView : mTrackDocumentViews.values()) {
			nearestTrackPointSensor.newCandiateTrack(
				trackView.invalidate(bWhatsChangedVector, warmLocationPix),
				trackView
			);
		}

		if (((bWhatsChangedVector & ReferenceMap.MapViews.BLocation) != 0) &&
			(warmLocationPix != null)) {
			//location changed, let's first find what track we're following, if any
			mFollowedTrackView = nearestTrackPointSensor.nearestView.follow(true, mFollowedTrackView);
		}

		riding(mFollowedTrackView != null &&
			   OSMMapsApplication.instance().locationTracker().isFollowing());

		if (mCockpit.isShown())
		{
			AutomaticRecorder recorder = OSMMapsApplication.instance().locationTracker().automaticRecorder();
			TextView textView = (TextView)mCockpit.findViewById(R.id.diagnostics);
			textView.setText(recorder.diagnosticString());
			if (mFollowedTrackView != null) {
				Distances distances = mFollowedTrackView.distances();
				StringUtils stringUtils = OSMMapsApplication.instance().stringUtils();
				mDistanceToDestinationView.setText(stringUtils.metresToMetric(distances.mToDestinationMts).toString());
				mDistanceFromOriginView.setText(stringUtils.metresToMetric(distances.mFromOriginMts).toString());
				mDEleMtsView.setText("");
				Integer dEleMts = mFollowedTrackView.document().geoPoints().stats().dEleMts();
				if (dEleMts != null) {
					mDEleMtsView.setText(
						String.format(
							java.util.Locale.US,
							mParent.getResources().getString(R.string.dEleMts)+"  ", dEleMts
						)
					);
				}
				mProgressBar.setProgress(
                    (float)distances.mFromOriginMts/(distances.mFromOriginMts+distances.mToDestinationMts),
                    mFollowedTrackView.document().geoPoints()
                );
			}
		}
	}


	@Override
	public void draw(Canvas canvas) {
		for (TrackView trackView : mTrackDocumentViews.values()) {
			trackView.draw(canvas);
		}
	}

	@Override
	public void onDestroy() {
	}
}
