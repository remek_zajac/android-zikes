//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.remekzajac.tracks;

import android.content.Context;

import net.remekzajac.Geo.Topo;
import net.remekzajac.maps.GeoDegPoint;
import net.zikes.OSMMapsApplication;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class FromViaToRegister extends GeoPOIRegister {

    private boolean mHasFrom;
    private boolean mHasTo;

    public FromViaToRegister(Context context) {
        super(context);
    }

    public void setFrom(POI poi) {
        if (mHasFrom) {
            mPOIs.set(0, new DrawablePOI(poi));
        } else {
            mPOIs.add(0, new DrawablePOI(poi));
        }
        mHasFrom = true;
        expandBounds(poi);
    }

    public void setTo(POI poi) {
        if (mHasTo) {
            mPOIs.set(mPOIs.size()-1, new DrawablePOI(poi));
        } else {
            mPOIs.add(new DrawablePOI(poi));
        }
        mHasTo = true;
        expandBounds(poi);
    }

    public void addVia(POI poi) {
        if (mHasFrom) {
            mPOIs.add(1, new DrawablePOI(poi));
        } else if (mHasTo) {
            mPOIs.add(0, new DrawablePOI(poi));
        } else {
            mPOIs.add(new DrawablePOI(poi));
        }
        expandBounds(poi);
    }

    public void remove(DrawablePOI toRemove) {
        mBounds = null;
        for (int i = 0; i < mPOIs.size();) {
            DrawablePOI poi = mPOIs.get(i);
            if (toRemove == poi) {
                if (i == 0 && mHasFrom) {
                    mHasFrom = false;
                }
                if (i == mPOIs.size()-1 && mHasTo) {
                    mHasTo = false;
                }
                mPOIs.remove(i);
            } else {
                expandBounds(poi);
                i++;
            }
        }
    }

    public GeoDegPoint getFrom() {
        if (mHasFrom) {
            return mPOIs.firstElement().bounds().centre();
        } else if (OSMMapsApplication.instance().locationTracker().warmLocation() != null) {
            return new GeoDegPoint(OSMMapsApplication.instance().locationTracker().warmLocation());
        }
        return null;
    }

    public boolean canRoute() {
        return getFrom() != null && mHasTo;
    }

    public boolean isFromWarmLocation() {
        return !mHasFrom;
    }

    public List<Topo.Point> getRoutingMilestones() {
        List<Topo.Point> milestones = new ArrayList<Topo.Point>();
        final GeoDegPoint from = getFrom();
        final GeoDegPoint to   = mPOIs.lastElement().bounds().centre();
        if (isFromWarmLocation()) {
            milestones.add(from.topo());
        }

        Collections.sort(mPOIs, new Comparator<POI>() {
            @Override
            public int compare(POI lhs, POI rhs) {
                double lhsDistanceToFrom = lhs.bounds().centre().comparableDistanceFrom(from);
                double lhsDistanceToTo   = lhs.bounds().centre().comparableDistanceFrom(to);
                double rhsDistanceToFrom = rhs.bounds().centre().comparableDistanceFrom(from);
                double rhsDistanceToTo   = rhs.bounds().centre().comparableDistanceFrom(to);
                if (lhsDistanceToFrom > rhsDistanceToFrom) {
                    if (lhsDistanceToTo < rhsDistanceToTo) {
                        return 1;
                    }
                } else {
                    if (lhsDistanceToTo > rhsDistanceToTo) {
                        return -1;
                    }
                }
                return 0;
            }
        });
        for (POI transitPoint : mPOIs) {
            milestones.add(
                transitPoint.bounds().centre().topo()
            );
        }
        return milestones;
    }

}
