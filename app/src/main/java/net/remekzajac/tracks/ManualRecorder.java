//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.tracks;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import net.zikes.OSMMapsApplication;
import net.zikes.R;
import net.zikes.SettingsActivity;
import net.remekzajac.Gpx.Gpx;
import net.remekzajac.tracks.LocationTracker.Recorder;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.text.format.Time;

public class ManualRecorder implements LocationTracker.Recorder {
	private static final String KDefaultDescription = "Activity recorded with Zikes Android App";
	public interface TrackWriter {
		public void newLocation(Location location);
		public File commit(String fileNamePrefix, File folder, String trackName, String trackDescription) throws IOException;
	}

	public static class GpxTrackWriter extends Gpx.TrackWriter implements TrackWriter {
		private Context mContext;
		public GpxTrackWriter(Context context)
		{
			mContext = context;
		}

		public void newLocation(Location location) {
			Integer altitude = null;
			if (location.hasAltitude()) {
				altitude = (int)location.getAltitude();
			}
			super.trackPoint(
				location.getLatitude(),
				location.getLongitude(),
				location.getTime(),
				altitude
			);
		}

		public void commit(File file, String trackName, String trackDescription)
				throws IOException
		{
			file.createNewFile();
			OutputStream out = new FileOutputStream(file);
			try {
				super.open(out,
					"http://"+OSMMapsApplication.instance().getString(R.string.zikes_frontend_url), //creator
					new Gpx.MetaData()
						.setDescription(trackDescription != null ? trackDescription : KDefaultDescription)
						.setExtensions("<appVersion>"+OSMMapsApplication.instance().appVersion()+"</appVersion>")
				);
				super.commitTrackSegment(trackName);
				super.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			out.close();
		}

		public File commit(String fileNamePrefix, File folder, String trackName, String trackDescription) throws IOException {
			File file = null;
            Time time = new Time();
            time.setToNow();
            String fileName = String.format(
				java.util.Locale.US,
				"%s_%d%02d%02d-%02d%02d%02d.gpx",
				fileNamePrefix,
				time.year,
				time.month+1, /*months are zero indexed*/
				time.monthDay,
				time.hour,
				time.minute,
				time.second
			);
			file = new File(folder, fileName);
			commit(file, trackName, trackDescription);
			return file;
		}
	}

	private TrackWriter mTrackWriter;
	private Context		mContext;
	private String		mTrackName;
	private Listener	mListener;
	public ManualRecorder(Context context, Listener listener) {
		mContext = context;
		mListener = listener;
	}

	public void newLocation(Location location) {
		if (mTrackWriter != null && location != null &&
			location.getAccuracy() <= OSMMapsApplication.instance().preferences().pref_Location_recAccuracyMts.get()) {
			mTrackWriter.newLocation(location);
		}
	}


	public void start() throws IOException {
		if (mTrackWriter == null) {
			mTrackWriter = new GpxTrackWriter(mContext);
			if (mListener != null) {
				mListener.onStateChange(null);
			}
		}
	}

	@SuppressLint("DefaultLocale")
	public void stop(boolean save, String trackDescription) throws IOException {
		if (save && mTrackWriter != null && mListener != null) {
			//as written, manual recorder is a fire and forget - it saves the track,
			//calls the listeners to do something about it and deletes the track
			//(no storage management here yet - so let the user do all that thru web).
			File folder = new File(
				OSMMapsApplication.instance().preferences().pref_DefaultCacheLocation.get()
			);
			File file = mTrackWriter.commit("recorded", folder, mTrackName, trackDescription);
			mTrackWriter = null;
			if (mListener != null) {
				mListener.onStateChange(file);
			}
			file.delete();
		}
	}

	public State state() {
		return mTrackWriter != null ? Recorder.State.ERecording : Recorder.State.EIdle;
	}

	public void setTrackName(String trackName)
	{
		mTrackName = trackName;
	}
}
