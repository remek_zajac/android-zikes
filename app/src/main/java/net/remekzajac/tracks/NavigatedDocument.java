//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.tracks;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import net.remekzajac.Geo.Topo;
import net.remekzajac.utils.SAsyncTask;
import net.remekzajac.utils.StringUtils;
import net.zikes.OSMMapsApplication;
import net.zikes.R;
import net.remekzajac.maps.GeoDegPoint;
import net.remekzajac.maps.ReferenceMap;
import net.remekzajac.tracks.GeoPOIRegister.POI;
import net.remekzajac.tracks.TrackSegmentsView.TrackPoint;
import net.remekzajac.tracks.TrackSegmentsView.TrackPointProximitySensor;
import net.remekzajac.tracks.TracksView.TrackDocument;
import net.remekzajac.utils.DoublePoint;
import net.remekzajac.utils.IntegerPoint;
import net.zikes.backend.RoutingRequest;
import net.zikes.backend.StringAndFileUtils;
import net.zikes.backend.json.Preference;

import org.json.JSONException;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.location.Location;
import android.text.Html;
import android.util.Log;
import android.widget.Toast;

public class NavigatedDocument extends OSMMapsApplication.Document {
	/*
	 * StickyNavigatedDocument is one that keeps rerouting to the preset destination
	 * when the current proposal stops being followed.
	 * The class splits into two:
	 * (1) a rolling record of journey so far - kept directly by StickyNavigatedDocument
	 * (2) a (potentially) changing track to follow - represented by the Future objects.
	 * The rolling record is there to leave a visible snail trail and by design, that
	 * trail is always the followed track (from the TracksView perspective).
	 */
	private class StickyNavigatedDocument extends TracksView.TrackDocumentBase1 {

		/*
		 *
		 * Special view for the document
		 *
		 */
		private class StickyNavigationDocumentView implements TracksView.TrackView {
			private TrackSegmentsView 					mFutureTrackView;
			private final ReferenceMap                  mReferenceMap;
			private final TrackSegmentsView				mPastTrackView;
			private final TracksView 					mParent;

			public StickyNavigationDocumentView(TracksView tracksView, ReferenceMap referenceMap) {
				mParent = tracksView;
				mReferenceMap = referenceMap;
				mPastTrackView = new TrackSegmentsView(
					mParent,
					mPastDocument,
					mReferenceMap
				);
				update();
			}

			public void update() {
				if (mFutureDocument != null) {
					mFutureTrackView = new TrackSegmentsView(
						mParent,
						mFutureDocument,
						mReferenceMap
					);
				}
			}

			@Override
			public float getRotationAngle() {
				float rotationAngle = 0;
				if (rotationPolicy() != TrackDocument.RotationPolicy.EOff ) {
					if (mPastTrackView.mNearestTrackLine.previousPixLine() != null) { //if we started leaving the snail trail, use it.
						rotationAngle = (float)((mPastTrackView.mNearestTrackLine.previousPixLine().angleDegRelY()+180)%360);
					} else if (mFutureTrackView != null && mFutureTrackView.mTrackDocument.isBeingFollowed()) {
						//if we haven't moved much yet, but there's already plotted track available nearby, use that instead
						rotationAngle = (float)((mFutureTrackView.mNearestTrackLine.nearestPixLine().angleDegRelY()+180)%360);
					}

					if (rotationPolicy() == TrackDocument.RotationPolicy.EReverseFollow) {
						rotationAngle = (rotationAngle+180)%360;
					}
				}

				return rotationAngle;
			}

			@Override
			public void draw(Canvas canvas) {
				if (mFutureTrackView != null) {
					mFutureTrackView.draw(canvas);
				}
				mPastTrackView.draw(canvas);
				/*if (mNearestTrackLine.nearestPixLine() != null)
				{
					canvas.drawLine(mNearestTrackLine.nearestPixLine().begining().x(), mNearestTrackLine.nearestPixLine().begining().y(),
							mNearestTrackLine.nearestPixLine().end().x(), mNearestTrackLine.nearestPixLine().end().y(), new Paint());
				}*/
			}

			@Override
			public void windroseClick() {
				mPastTrackView.windroseClick();
			}


			@Override
			public TracksView.Distances distances() {
				long toDestinationMts = 0;
				if (mFutureTrackView != null) {
					toDestinationMts = mFutureTrackView.distances().mToDestinationMts;
				}
				return new TracksView.Distances(
					mPastTrackView.document().geoPoints().stats().distanceMts(),
					toDestinationMts
				);
			}

			@Override
			public TrackDocument document() {
				return StickyNavigatedDocument.this;
			}

			@Override
			public TracksView.TrackView follow(boolean follow, TracksView.TrackView previousFollowedTrack) {
				if (mPastTrackView.follow(follow, previousFollowedTrack) != null ||
				   (mFutureTrackView != null && mFutureTrackView.follow(follow, previousFollowedTrack) != null)) {
					return this;
				}
				return null;
			}

			@Override
			public TrackPointProximitySensor invalidate(int bWhatsChangedVector, IntegerPoint referenceForProximitySensor)
			{
				Location locationToAdd = OSMMapsApplication.instance().locationTracker().warmLocation();
				if (mFutureTrackView != null) {
					IntegerPoint reference = (referenceForProximitySensor != null ?
							referenceForProximitySensor : (mPastTrackView.mTrackPointTail != null ?
								mPastTrackView.mTrackPointTail.pixPoint() : null));
					mFutureTrackView.invalidate(bWhatsChangedVector, reference);
					mFutureTrackView.mNearestTrackLine = new TrackSegmentsView.TrackProximitySensor(mFutureTrackView.mNearestTrackPoint);

					if ((bWhatsChangedVector & ReferenceMap.MapViews.BLocation) != 0) {
						long distanceMts = mReferenceMap.pix2Mts(mFutureTrackView.mNearestTrackLine.pixDistanceToTrack());
						if ( locationToAdd != null && distanceMts <= KMaxDistanceForFollowingMts) {
							//we have a warm new location and we're following the mFutureTrackView
							if (!mFutureTrackView.mTrackDocument.isBeingFollowed()) {
								mFutureTrackView.mTrackDocument.follow(true);
							}
							//if the current location is:
							// - close to an exiting mFutureTrackView trackpoint, we'll add that trackpoint instead
							// otherwise,
							// - if still very close to one of the mFutureTrackView lines, we'll add its projection onto that line
							// otherwise,
							// - i.e.: we're close enough to follow mFutureTrackView, but not close enough to project, we'll simply add unchanged current location
							if (mFutureTrackView.mNearestTrackLine.pixDistanceToTrack() < KMaxDistanceToProjectMts) {
								locationToAdd = new Location(locationToAdd);
								if (locationToAdd.distanceTo(
										mFutureTrackView.mNearestTrackLine.nearestTrackPoint().geoPoint().location()) < KMinTrackPointGranularityMts) {
									locationToAdd.setLatitude(mFutureTrackView.mNearestTrackLine.nearestTrackPoint().geoPoint().location().getLatitude());
									locationToAdd.setLongitude(mFutureTrackView.mNearestTrackLine.nearestTrackPoint().geoPoint().location().getLongitude());
									mFutureTrackView.mTrackPointHead = mFutureTrackView.mNearestTrackPoint.nearestTrackPoint();
								} else {
									DoublePoint projection = mFutureTrackView.mNearestTrackLine.nearestGeoLine().projection(new GeoDegPoint(locationToAdd));
									locationToAdd.setLatitude(projection.y());
									locationToAdd.setLongitude(projection.x());
									if (mFutureTrackView.mNearestTrackPoint.nearestTrackPoint().prev() != null) {
										mFutureTrackView.mTrackPointHead = mFutureTrackView.mNearestTrackPoint.nearestTrackPoint().prev();
									}

								}
								mFutureTrackView.mTrackPointHead.cutOffPrev();
							}

							mTrialsRemaining = KMaxRefollowingRetries;
						} else {
							stoppedBeingFollowed();
						}
					}
				}
				if ((bWhatsChangedVector & ReferenceMap.MapViews.BLocation) != 0 &&
					 locationToAdd != null &&
					 mPastTrackView.document().geoPoints().tail().location().distanceTo(locationToAdd) > KMinTrackPointGranularityMts) {
					 mPastTrackView.document().geoPoints().append(locationToAdd);
				}

				return mPastTrackView.invalidate(bWhatsChangedVector, referenceForProximitySensor);
			}
		}


		private int								mTrialsRemaining;
		private StickyNavigationDocumentView    mThisTrackView;
		private TrackGeoPointRegister           mMergedGeoPoints;
		private final TrackDocument             mPastDocument;
		private TrackDocument             		mFutureDocument;
		public StickyNavigatedDocument() {
			mPastDocument = new TracksView.SegmentDocument(
				new TrackGeoPointRegister().append(
					OSMMapsApplication.instance().locationTracker().warmLocation()
				)
			);
			mTrialsRemaining = KMaxRefollowingRetries;
		}


		private void stoppedBeingFollowed() {
			if (OSMMapsApplication.instance().locationTracker().warmLocation() != null) {
				GeoDegPoint here = new GeoDegPoint(OSMMapsApplication.instance().locationTracker().warmLocation());
				int timeSinceLastRequestSec = (int)((System.currentTimeMillis()-mLastRequestReturnedMs)/1000);
				long distanceSinceLastRequestMts = 0;
				if (mLastRequestIssuedFrom != null) //shouldn't be otherwise
				{
					distanceSinceLastRequestMts = mLastRequestIssuedFrom.distanceFromMts(here);
				}
				if (mOngoingRequest == null
					&& timeSinceLastRequestSec > KMinTimeBetweenRetriesSec			//prevent from pounding
					&& distanceSinceLastRequestMts > KMinDistanceBetweenRetriesMts  //the server too often
				   )
				{
					if (--mTrialsRemaining <= 0)
					{
						Toast.makeText(activity(), activity().getString(R.string.error_tiredRouting), Toast.LENGTH_LONG).show();
						clearDocument();
					} else {
						newRequest(here).execute();
					}
				}
			}
		}


		@Override
		public void nextRotation() {}

		@Override
		public RotationPolicy rotationPolicy() {return RotationPolicy.EFollow;}

		@Override
		public TrackGeoPointRegister geoPoints() {
			if (mMergedGeoPoints == null) {
				mMergedGeoPoints = mPastDocument.geoPoints().clone();
				if (mFutureDocument != null) {
					mMergedGeoPoints.append(mFutureDocument.geoPoints().clone());
				}
			}
			return mMergedGeoPoints;
		}

		public void update(TrackGeoPointRegister future) {
			mLastRequestReturnedMs = System.currentTimeMillis();
			mFutureDocument = new TracksView.SegmentDocument(future);
			mThisTrackView.update();
			mMergedGeoPoints = null;
		}

		@Override
		public TracksView.TrackView createTrackViewFactory(TracksView tracksView, ReferenceMap referenceMap) {
			mThisTrackView = new StickyNavigationDocumentView(tracksView, referenceMap);
			return mThisTrackView;
		}
	}


	public static final String 		    KDocName = "OSMMapsApplication.NavigatedDocument";
	private static final int 			KMaxRefollowingRetries = 10;
	private static final int			KMinTimeBetweenRetriesSec = 20;
	private static final int			KMinDistanceBetweenRetriesMts = 50;
	private static final int			KMinTrackPointGranularityMts = 15;
	private static final int			KMaxDistanceToProjectMts = 40;
	private TracksView.TrackDocument	mPlottedTrackDocument;
	private StickyNavigatedDocument		mStickyDocument;
	private NavigationRequest			mOngoingRequest;
	private FromViaToRegister			mFromViaToRegistry;
	private long 						mLastRequestReturnedMs;
	private GeoDegPoint					mLastRequestIssuedFrom;
	private File                        mPreferenceFile;
	public NavigatedDocument() {
		super(KDocName);
		mFromViaToRegistry = new FromViaToRegister(activity());
		activity().locationIndicatorView().add(mFromViaToRegistry, false);
	}

	public static NavigatedDocument instance() {
		NavigatedDocument result = (NavigatedDocument)OSMMapsApplication.instance().getDocument(KDocName);
		return result != null ? result : new NavigatedDocument();
	}

	public boolean from(GeoDegPoint from) {
		return from(new POI(new GeoDegPoint.Bounds(from), activity().getString(R.string.navigate_from)));
	}

	public boolean from(POI from) {
		mFromViaToRegistry.setFrom(from);
		return routeIfEnoughData();
	}

	public boolean to(POI to) {
		mFromViaToRegistry.setTo(to);
		return routeIfEnoughData();
	}

	public boolean to(GeoDegPoint to) {
		return to(new POI(new GeoDegPoint.Bounds(to), activity().getString(R.string.navigate_to)));
	}

	public boolean via(POI via) {
		mFromViaToRegistry.addVia(via);
		return routeIfEnoughData();
	}

	public boolean remove(GeoPOIRegister.DrawablePOI poi) {
		mFromViaToRegistry.remove(poi);
		return routeIfEnoughData();
	}

	public boolean via(GeoDegPoint via) {
		return via(new POI(new GeoDegPoint.Bounds(via), activity().getString(R.string.navigate_via)));
	}

	public boolean hasPoint(GeoPOIRegister.DrawablePOI poi) {
		return poi.parent() == mFromViaToRegistry;
	}

	public void save(String trackName) throws IOException {
		if (isShown()) {
			ManualRecorder.GpxTrackWriter writer = new ManualRecorder.GpxTrackWriter(activity());
			TrackGeoPointRegister.TrackPoint next = mPlottedTrackDocument.geoPoints().head();
			while (next != null) {
				writer.newLocation(next.location());
				next = next.next();
			}
			writer.commit(
				"plotted",
				OSMMapsApplication.instance().preferences().pref_BackendSyncFolder.get(true),
				trackName,
				null
			);
		}
	}

	public boolean isShown()
	{
		return activity().tracksView().shows(mPlottedTrackDocument);
	}

	public GeoDegPoint getFrom() {
		return mFromViaToRegistry.getFrom();
	}

	@SuppressLint("DefaultLocale")
	private boolean routeIfEnoughData() {
		activity().locationIndicatorView().invalidate();
		GeoDegPoint from = getFrom();
		if (mFromViaToRegistry.canRoute()) {

			if (mFromViaToRegistry.isFromWarmLocation()) {
				if (mStickyDocument == null) {
					mPlottedTrackDocument = mStickyDocument = new StickyNavigatedDocument();
					activity().tracksView().add(mStickyDocument);
				}
			}

			newRequest(from).execute();
			return true;
		}
		return false;
	}


    @SuppressLint("DefaultLocale")
	public NavigationRequest newRequest(final GeoDegPoint from) {
		mPreferenceFile = new File(
			OSMMapsApplication.instance().preferences().pref_RoutingPreference.get()
		);
        mLastRequestIssuedFrom = from;
		mOngoingRequest = null;
        mOngoingRequest = new NavigationRequest(
            mFromViaToRegistry.getRoutingMilestones(),
			mPreferenceFile
        );
		return mOngoingRequest;
	}


	private class NavigationRequest extends SAsyncTask<TrackGeoPointRegister, Void>
	{
		private final List<Topo.Point>  mMilestones;
		private final Preference.Routing mPreference;
		private NavigationRequest(List<Topo.Point> milestones, File preference) {
			mMilestones = milestones;
            mPreference = new Preference.Routing(preference);
        }

        private void showProgressDialog() {
            if (activity() != null) {
                activity().setProgressBarIndeterminateVisibility(true);
            }
        }

        private void dismissProgressDialog() {
            if (activity() != null) {
                activity().setProgressBarIndeterminateVisibility(false);
            }
        }

		public void execute() {
            try {
				mPreference.parseMeta();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				Toast.makeText(
					activity(),
					activity().getString(R.string.error_noPreference),
					Toast.LENGTH_LONG).show();
				return;
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(
                    activity(),
                    Html.fromHtml(
                        String.format(
                            activity().getString(R.string.error_readPreference),
                            activity().getResources().getColor(R.color.zikesBrightBlueBkgd),
                            mPreference.file().getAbsolutePath()
                        )
                    ), Toast.LENGTH_LONG).show();
                return;
            }
			showProgressDialog();
            Toast.makeText(
                activity(),
                Html.fromHtml(
                    String.format(
                        activity().getString(R.string.info_Routing),
                        activity().getResources().getColor(R.color.zikesBrightBlueBkgd),
                        mPreference.meta().name
                    )
                ), Toast.LENGTH_LONG).show();
            super.execute();
		}

		@Override
		protected TrackGeoPointRegister backgroundProcess() throws Exception {
            TrackGeoPointRegister register = new TrackGeoPointRegister();
            RoutingRequest.route(
                mMilestones,
                mPreference,
                register
            );
            return register;
		}


        @Override
        protected void onTaskComplete(SAsyncTask.TaskResult<TrackGeoPointRegister> result) {
			dismissProgressDialog();
			mOngoingRequest = null;
            try {
                TracksView.SegmentDocument document = new TracksView.SegmentDocument(result.result());
                if (mStickyDocument != null) {
                    mStickyDocument.update(document.geoPoints());
                }
                else {
					clearDocument();
                    mPlottedTrackDocument = document;
                    activity().tracksView().add(mPlottedTrackDocument);
                }
                activity().invalidateOptionsMenu();
			} catch (RoutingRequest.NoRoute e) {
				Toast.makeText(
					activity(),
					Html.fromHtml(
						activity().getString(R.string.routing_error_no_route)
					), Toast.LENGTH_LONG
				).show();
				clear();
            } catch (RoutingRequest.NearestPointNotFound e) {
				Toast.makeText(
					activity(),
					Html.fromHtml(
						String.format(
							activity().getString(R.string.routing_error_nearest_point_not_found),
							e.toleranceMts
						)
					), Toast.LENGTH_LONG
				).show();
				clear();
			} catch (RoutingRequest.ServerOverload e) {
				Toast.makeText(
					activity(),
					Html.fromHtml(
						String.format(
							activity().getString(R.string.routing_error_server_overload),
							StringUtils.bigNumberToString(e.analysedJunctions)
						)
					), Toast.LENGTH_LONG
				).show();
				clear();
			} catch (Exception e) {
				e.printStackTrace();
				Toast.makeText(
					activity(),
					Html.fromHtml(
						activity().getString(R.string.error_CantRoute)
					), Toast.LENGTH_LONG
				).show();
				/*OSMMapsApplication.instance().authenticatedBackend().logException(
					e, "When parsing ad-hoc route"
				);*/
				clear();
            }
		}
	}

	private void clearDocument() {
		if (mPlottedTrackDocument != null) {
			activity().tracksView().remove(mPlottedTrackDocument);
		}
		mPlottedTrackDocument = null;
		mStickyDocument = null;
	}

	@Override
	public void newActivity() {
		if (mPlottedTrackDocument != null) {
			activity().tracksView().add(mPlottedTrackDocument);
			activity().locationIndicatorView().add(mFromViaToRegistry, false);
		} if (mOngoingRequest != null) {
			mOngoingRequest.showProgressDialog();
		}
		if (mPreferenceFile != null &&
			mPreferenceFile.getAbsolutePath() != OSMMapsApplication.instance().preferences().pref_RoutingPreference.get()) {
			routeIfEnoughData();
		}
	}

	@Override
	public void outgoingActivity() {
		if (mOngoingRequest != null) {
			mOngoingRequest.dismissProgressDialog();
		}
	}

	@Override
	public void clear() {
		clearDocument();
		activity().locationIndicatorView().remove(mFromViaToRegistry);
		mFromViaToRegistry = new FromViaToRegister(activity());
		activity().locationIndicatorView().add(mFromViaToRegistry, false);
		super.clear();
	}
}
