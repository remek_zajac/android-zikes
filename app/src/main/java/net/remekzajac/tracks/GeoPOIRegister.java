//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.tracks;

import java.util.Iterator;
import java.util.Vector;

import net.remekzajac.maps.GeoDegPoint;
import net.remekzajac.utils.IntegerPoint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

public class GeoPOIRegister
{
	public static class POI
	{
		final protected GeoDegPoint.Bounds 	mBounds;
		final protected String				mName;
		final protected String	  		 	mDescription;
		public POI(GeoDegPoint.Bounds bounds, String name, String description)
		{
			mBounds = bounds;
			mName = name;
			mDescription = description;
		}

		public POI(GeoDegPoint.Bounds bounds, String name)
		{
			mBounds = bounds;
			mName = name;
			mDescription = null;
		}

		protected POI(POI from)
		{
			mBounds = from.mBounds;
			mDescription = from.mDescription;
			mName = from.mName;
		}

		public String name()
		{
			return mName;
		}

		public GeoDegPoint.Bounds bounds()
		{
			return mBounds;
		}
	}

	public class DrawablePOI extends POI
	{
		private static final String 	KCharactersWithLowHangingTentacles = "pqjg";
		private boolean 				mHasLowHangingTentacles;
		private long					mDistanceToLocationMts;
		public DrawablePOI(POI from) {
			super(from);
			mHasLowHangingTentacles = hasLowHangingTentacles();
		}

		private boolean hasLowHangingTentacles() {
			if (mName != null) {
				for (int i = 0; i < KCharactersWithLowHangingTentacles.length(); i++) {
					if (mName.contains(KCharactersWithLowHangingTentacles.subSequence(i, i+1))) {
						return true;
					}
				}
			}
			return false;
		}

		public void drawIcon(Canvas canvas, IntegerPoint pix) {
			mPOIDrawable.setBounds(pix.x()-KPositionIndicatiorHalfSizePix, pix.y()-KPositionIndicatiorHalfSizePix,
					pix.x()+KPositionIndicatiorHalfSizePix, pix.y()+KPositionIndicatiorHalfSizePix);
			mPOIDrawable.draw(canvas);
		}

		public long setDistanceTo(GeoDegPoint location) {
			return mDistanceToLocationMts = bounds().centre().distanceFromMts(location);
		}

		public long getDistanceToSetLocation() {
			return mDistanceToLocationMts;
		}

		public GeoPOIRegister parent() { return GeoPOIRegister.this; }

		public void drawDescription(Canvas canvas, IntegerPoint pix) {
			if (mName != null)
			{
				Rect textBounds = new Rect();
				mDesctiptionTextPaint.getTextBounds(mName, 0, mName.length(), textBounds);
				int descriptionPixOffsetX = pix.x() + KDescriptionOffsetPixX;
				int descriptionPixOffsetY = pix.y() + KDescriptionOffsetPixY;
				textBounds.offsetTo(descriptionPixOffsetX,
					descriptionPixOffsetY-textBounds.height()+
						(mHasLowHangingTentacles?KDescriptionBoxExtraYOffset:0));
				textBounds.set(
					textBounds.left-KDescriptionBoxMarginPixX,
					textBounds.top-KDescriptionBoxMarginPixY,
					textBounds.right+KDescriptionBoxMarginPixX,
					textBounds.bottom+KDescriptionBoxMarginPixY);
				RectF textBoundsf = new RectF(textBounds);
				RectF shadeTextBoundsF = new RectF(textBoundsf);
				shadeTextBoundsF.offset(-KDescriptionBoxShadeOffset, KDescriptionBoxShadeOffset);
				canvas.drawRoundRect(shadeTextBoundsF, KDescriptionBoxRoundRadius, KDescriptionBoxRoundRadius, mDesctiptionBoxShadePaint);
				canvas.drawRoundRect(textBoundsf, KDescriptionBoxRoundRadius, KDescriptionBoxRoundRadius, mDesctiptionBoxBkgPaint);
				canvas.drawRoundRect(textBoundsf, KDescriptionBoxRoundRadius, KDescriptionBoxRoundRadius, mDesctiptionBoxStrkPaint);
				canvas.drawText(mName, descriptionPixOffsetX, descriptionPixOffsetY, mDesctiptionTextPaint);
			}
		}
	}

	private final static int 	KPositionIndicatiorHalfSizePix	= 40;
	private final static int 	KDescriptionOffsetPixX 			= 50;
	private final static int 	KDescriptionOffsetPixY 			= -40;
	private final static int 	KDescriptionTextSize 			= 26;
	private final static int 	KDescriptionBoxMarginPixX 		= 8;
	private final static int 	KDescriptionBoxMarginPixY 		= 5;
	private final static int 	KDescriptionBoxExtraYOffset		= 3;
	private final static int 	KDescriptionBoxStrokeWidth		= 1;
	private final static int 	KDescriptionBoxRoundRadius		= 5;
	private final static int 	KDescriptionBoxShadeAlpha		= 80;
	private final static int 	KDescriptionBoxAlpha			= 200;
	private final static int 	KDescriptionBoxShadeOffset		= 5;
	private final static int 	KCutOffOnTruncateRatio			= 4;
	private final Drawable		mPOIDrawable;
	private final Paint			mDesctiptionTextPaint;
	private final Paint			mDesctiptionBoxBkgPaint;
	private final Paint			mDesctiptionBoxStrkPaint;
	private final Paint			mDesctiptionBoxShadePaint;
	final protected Vector<DrawablePOI> mPOIs;
	protected GeoDegPoint.Bounds  mBounds;

	public GeoPOIRegister(Context context)
	{
		mPOIs = new Vector<DrawablePOI>();
		mPOIDrawable = context.getResources().getDrawable(android.R.drawable.star_on);
		mDesctiptionTextPaint = new Paint();
		mDesctiptionTextPaint.setTextSize(KDescriptionTextSize);
		mDesctiptionTextPaint.setColor(Color.DKGRAY);
		mDesctiptionTextPaint.setAntiAlias(true);
		mDesctiptionTextPaint.setDither(true);
		mDesctiptionBoxBkgPaint = new Paint();
		mDesctiptionBoxBkgPaint.setColor(Color.WHITE);
		mDesctiptionBoxBkgPaint.setAlpha(KDescriptionBoxAlpha);
		mDesctiptionBoxStrkPaint = new Paint();
		mDesctiptionBoxStrkPaint.setStrokeJoin(Paint.Join.ROUND);
		mDesctiptionBoxStrkPaint.setStrokeWidth(KDescriptionBoxStrokeWidth);
		mDesctiptionBoxStrkPaint.setColor(Color.DKGRAY);
		mDesctiptionBoxStrkPaint.setStyle(Paint.Style.STROKE);
		mDesctiptionBoxStrkPaint.setAntiAlias(true);
		mDesctiptionBoxStrkPaint.setDither(true);
		mDesctiptionBoxShadePaint = new Paint();
		mDesctiptionBoxShadePaint.setColor(Color.BLACK);
		mDesctiptionBoxShadePaint.setAlpha(KDescriptionBoxShadeAlpha);
	}

	public GeoPOIRegister append(POI poi) {
		mPOIs.add(new DrawablePOI(poi));
		expandBounds(poi);
		return this;
	}

	protected void expandBounds(POI poi) {
		if (mBounds == null) {
			mBounds = poi.bounds().clone();
		} else {
			mBounds.add(poi.bounds());
		}
	}

	public Iterator<DrawablePOI> iterator() {
		return mPOIs.iterator();
	}

	public int size() {
		return mPOIs.size();
	}

	public GeoDegPoint.Bounds bounds()
	{
		return mBounds;
	}

	public void truncateToLocation(GeoDegPoint location)
	{
		if (location != null)
		{
			long closestPOIDistanceMts = Integer.MAX_VALUE;
			for (DrawablePOI poi : mPOIs)
			{
				long distanceMts = poi.setDistanceTo(location);
				if (closestPOIDistanceMts > distanceMts)
				{
					closestPOIDistanceMts = distanceMts;
				}
			}

			mBounds = null;
			long cutoffDistance = closestPOIDistanceMts * KCutOffOnTruncateRatio;
			for (int i = 0; i < mPOIs.size();)
			{
				DrawablePOI poi = mPOIs.get(i);
				if (poi.getDistanceToSetLocation() > cutoffDistance)
				{
					mPOIs.remove(i);
				}
				else
				{
					if (mBounds == null)
					{
						mBounds = poi.bounds();
					}
					else
					{
						mBounds.add(poi.bounds());
					}
					i++;
				}
			}
		}
	}
}
