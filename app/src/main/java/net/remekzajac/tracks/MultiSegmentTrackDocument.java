//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.tracks;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import net.zikes.R;
import net.remekzajac.Geo.Topo;
import net.remekzajac.Gpx.Gpx;
import net.remekzajac.maps.GeoDegPoint;
import net.remekzajac.maps.LocationIndicatorView;
import net.remekzajac.maps.MapView;
import net.remekzajac.maps.ReferenceMap;
import android.content.Context;
import android.util.Log;

import com.ipaulpro.afilechooser.FileRenderer;

public class MultiSegmentTrackDocument implements Topo.Collection
{
	public static class GPXFileRenderer implements FileRenderer {

        public static final String KFileFilter = ".+\\.gpx";

		@Override
		public Tuple render(File file) {
            if (file.getName().matches(KFileFilter)) {
                return new Tuple(file).setMime(Gpx.KMimeType);
            }
            return null;
		}
	}

	protected Vector<net.remekzajac.tracks.TracksView.SegmentDocument>		mTrackSegmentDocuments;
	protected net.remekzajac.tracks.GeoPOIRegister mPOIs;
	protected final Context 							mContext;
	protected TrackGeoPointRegister.Stats 				mStats;
	protected boolean									mIsBeingFollowed;
	protected MapView.SubView							mView;
	protected TrackGeoPointRegister.TrackPoint			mMilestoneReached;
	public MultiSegmentTrackDocument(Context context)
	{
		mContext = context;
	}

	//from Topo.Collection
	public void append(Topo.Point aPoint) {
    	mPOIs.append(
    			new GeoPOIRegister.POI(
    					new GeoDegPoint.Bounds(aPoint.mLon, aPoint.mLat), aPoint.mName, aPoint.mDescripion));
	}

	//from Topo.Collection
	public Topo.Path addPath() {
		TrackGeoPointRegister newGeoPointRegister = new TrackGeoPointRegister();
		mTrackSegmentDocuments.add(new TracksView.SegmentDocument(newGeoPointRegister));
		return newGeoPointRegister;
	}

	//from Topo.Collection
	public void addAttribute(String aName, String aValue) {}


	public void open(Topo.CollectionSource collection) throws Exception
	{
		mTrackSegmentDocuments = new Vector<TracksView.SegmentDocument>();
		mPOIs = new GeoPOIRegister(mContext);
		collection.copy(this);

		if (mTrackSegmentDocuments.size() == 0 && mPOIs.size() == 0)
		{
			throw new Topo.CollectionSource.Exception(mContext.getString(R.string.error_noTrkPtsToDisplay));
		}

		invalidate();
	}

	public void show(TracksView tracksView, LocationIndicatorView poiView) {
		for (TracksView.SegmentDocument segmentDocument : mTrackSegmentDocuments) {
			if (segmentDocument.geoPoints().head() != null) {
				tracksView.add(segmentDocument);
			}
		}
		poiView.add(mPOIs, false);
	}

	public void hide(TracksView tracksView, LocationIndicatorView poiView) {
		for (TracksView.SegmentDocument segmentDocument : mTrackSegmentDocuments) {
			tracksView.remove(segmentDocument);
		}
		poiView.remove(mPOIs);
	}

	public void close()
	{
		mTrackSegmentDocuments = null;
	}

	public TrackGeoPointRegister.Stats stats()
	{
		return mStats;
	}

	public void follow(TrackGeoPointRegister.TrackPoint milestone)
	{
		assert milestone != null;
		mIsBeingFollowed = true;
		if (mMilestoneReached == null || mMilestoneReached != milestone )
		{
			mMilestoneReached = milestone;
			mMilestoneReached.setTime(System.currentTimeMillis());
			Log.d("NavigatedDocument", "follow, now reached milestone");
			diagnose();
		}
	}

	public void stopFollowing()
	{
		mIsBeingFollowed = false;
	}

	public void onScreen(MapView.SubView view)
	{
		mView = view;
	}

	public void offScreen()
	{
		mView = null;
	}

	public boolean isBeingFollowed()
	{
		return mIsBeingFollowed;
	}

	public boolean isOnScreen()
	{
		return mView != null;
	}

	protected void invalidate()
	{
		mStats = new TrackGeoPointRegister.Stats();
		for (TracksView.SegmentDocument register : mTrackSegmentDocuments)
		{
			mStats.add(register.geoPoints().stats());
		}
		mStats.add(mPOIs.bounds());
		if (mView != null)
		{
			mView.invalidate(ReferenceMap.MapViews.BForce);
		}
	}

	public Vector<TracksView.SegmentDocument> documents()
	{
		return mTrackSegmentDocuments;
	}

	public void diagnose()
	{
		mTrackSegmentDocuments.firstElement().geoPoints().diagnose();
	}
}
