//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************


package net.remekzajac.tracks;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.remekzajac.Geo.Topo;
import net.remekzajac.maps.MockLocationProvider;
import net.zikes.OSMMapsApplication;
import android.content.Context;


public class TrackDocuments extends OSMMapsApplication.Document
{
	private ArrayList<MultiSegmentTrackDocument>    mDocuments;
	private final Context 		  					mContext;
	private static final String 					KDocName = "OSMMapsApplication.TrackDocuments";

	public TrackDocuments() {
		super(KDocName);
		mDocuments = new ArrayList<MultiSegmentTrackDocument>();
		mContext = application();
	}

	public void add(Topo.CollectionSource collection) throws Exception {
		MultiSegmentTrackDocument trackDocument = new MultiSegmentTrackDocument(mContext);
		trackDocument.open(collection);
		mDocuments.add(trackDocument);
		if (activity() != null) {
			OSMMapsApplication.instance().referenceMap()
				.newRequest()
				.region(trackDocument.stats().bounds())
				.execute();
			trackDocument.show(activity().tracksView(), activity().locationIndicatorView());
		}
    	/*new MockLocationProvider("mockLocationProvider",
			 OSMMapsApplication.instance().locationTracker(),
			 0,
			 40,
			 25).test(trackDocument);*/
	}

	public static TrackDocuments instance() {
		TrackDocuments result = (TrackDocuments)OSMMapsApplication.instance().getDocument(KDocName);
		return result != null ? result : new TrackDocuments();
	}

	@Override
	public void clear() {
		for (MultiSegmentTrackDocument document : mDocuments) {
			document.hide(activity().tracksView(), activity().locationIndicatorView());
		}
		mDocuments = null;
		super.clear();
	}

	@Override
	public void newActivity() {
		for (MultiSegmentTrackDocument document : mDocuments) {
			document.show(activity().tracksView(), activity().locationIndicatorView());
		}
	}
}

