//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************


package net.remekzajac.tracks;

import net.zikes.OSMMapsApplication;
import net.zikes.R;
import net.remekzajac.maps.ReferenceMap;
import net.remekzajac.tracks.TracksView.TrackDocument;
import net.remekzajac.utils.DoubleLineSegment;
import net.remekzajac.utils.IntegerLineSegment;
import net.remekzajac.utils.IntegerPoint;
import net.remekzajac.utils.IntegerVector;
import net.remekzajac.utils.ListElement;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;

/*****************************************************************************************************************
 *
 *  This class (TrackSegmentPixPointRegister) is responsible for storing and (when hinted) updating track pix points.
 *  It is a natural extension of a view class that can supply the ReferenceMap (for mapping geo coordinates
 *  to pix coordinates) and signal on when to recalculate the pix coordinates (say, in response to pan or zoom).
 *  Definitions:
 *  Track 				- Formally not a subject to this implementation, but for the record: a list of track
 *                        segments
 *  Track Segment		- What this implementation represents. An ordered sequence of track points forming
 *  					  a continuous track (consistent with this definition:
 *  					  http://www.topografix.com/gpx/1/1/#type_trksegType)
 *  Track Segment Cut 	- a visible section of a track segment. This is an artifact of the algorithm used
 *  					  by the TrackSegmentPixPointRegister.TrackSegment class.
 *  Track Line 			- a line connecting two (typically visible) track points.
 *
 *****************************************************************************************************************/

/*************************************************************************************************************
 * A track segment is here represented by:
 *
 * Cuts     : [s]               [s]                                 [s]
 * Visible     |                 |                                   |
 *             v                 v                                   v
 * PixPoints: [p]      <->      [p]   <-speed->   [p]      <->      [p]   <-speed->   [p]
 * Visible     |                 |                 |                 |                 |
 *             v                 v                 v                 v                 v
 * GeoPoints: [g]<->[g]<->[g]<->[g]<->[g]<->[g]<->[g]<->[g]<->[g]<->[g]<->[g]<->[g]<->[g]<->[g]<->[g]<->[g]
 *
 * The problem this is trying to solve is that some track segments are very long (thousands of geo points). Compared to
 * that there is very rarely more than tens of pix points to be displayed during a draw. That is: If a path is long
 * and stretches long distances, then at higher zoom levels it will skip a lot of geo points (which are too close
 * to each other). At lower zoom levels of course most of the path won't be visible and so the pix coordinates
 * of the invisible points won't need recalculating. When thinking this way, the concept of a track segment cut
 * emerges. I.e.: there can be a twisted track segment, with a number of visible cuts and as a consequence
 * the invisible points will not be just on the cursor's ends, but also in its middle. Whenever invalidate
 * is called, the invisible points need to be visited if, by any chance, they came to become visible.
 *
 * At the bottom, a segment is a linked list of its geo coordinates [g]. The pix representation [p] is build on top of
 * the geo list joining every other geo point where the distance between these ponts is worthy of drawing (the pix
 * distance is long enough as one doesn't want to draw a line between two geo points that, thanks to a zoom level,
 * map to the same pix point).
 *
 * ************************************************************************************************************/
public class TrackSegmentsView implements TracksView.TrackView
{
	/*****************************************************************************************************************
	 *
	 *
	 * A representation of a pix track segment point
	 *
	 *
	  *****************************************************************************************************************/
	public class TrackPoint extends ListElement<TrackPoint>
	{
		private final TrackGeoPointRegister.TrackPoint 	mGeoPoint;
		private IntegerPoint 							mStartPoint;
		private int 									mColorSincePrevious;
		private TrackGeoPointRegister.TrackPoint.Stats	mStatsSinceOrigin;
		public TrackPoint(TrackGeoPointRegister.TrackPoint geoPoint)
		{
			mGeoPoint = geoPoint;

			//TODO, geo2pix won't convert for longer distances
			mStartPoint = mReferenceMap.geo2pix(geoPoint.location());
			mColorSincePrevious = KBeginingOfVisibleCutColour;
			mStatsSinceOrigin = mGeoPoint.stats();
		}

		public void invalidate(IntegerVector pixMoveBy, TrackPointProximitySensor trackPointVisitor)
		{
			if (pixMoveBy != null) {
				mStartPoint = mStartPoint.add(pixMoveBy);
			} else {
				//TODO, geo2pix won't convert for larger distances
				mStartPoint = mReferenceMap.geo2pix(mGeoPoint.location());
			}
			trackPointVisitor.newCandidate(this);
		}

		@Override
		public boolean cutOffPrev() {
			return super.cutOffPrev();
		}

		private boolean nextVisible()
		{
			return next() != null ? trackLineVisible(this, next()) : true;
		}

		private boolean beginningOfCut()
		{
			return mColorSincePrevious == KBeginingOfVisibleCutColour;
		}

		public TrackPoint remove()
		{
			TrackPoint next = next();
			if (next != null)
			{
				next.mColorSincePrevious = KBeginingOfVisibleCutColour;
			}
			return super.remove(RemovalOutcome.ELeft);
		}

		@Override
		public TrackPoint append(TrackPoint trackPoint)
		{
			super.append(trackPoint);
			trackPoint.setStats(mTrackDocument.geoPoints().stats());
			return trackPoint;
		}

		public TrackSegmentsView parentView()
		{
			return TrackSegmentsView.this;
		}

		private void setStats(TrackGeoPointRegister.Stats statsForSegment) {
			assert prev() != null;
			mStatsSinceOrigin = mGeoPoint.stats();
			mStatsSinceOrigin.traceBackUntil(
				new TrackGeoPointRegister.TrackPoint.TrackPointFilter(prev().geoPoint())
			);

			double speedSincePrevious = mStatsSinceOrigin.avgSpeedKph();
			double positionInRange = speedSincePrevious/statsForSegment.maxSpeedKph();
            int red = (int)(Color.red(mColourFast)*positionInRange+Color.red(mColourSlow)*(1-positionInRange));
            int green = (int)(Color.green(mColourFast)*positionInRange+Color.green(mColourSlow)*(1-positionInRange));
            int blue = (int)(Color.blue(mColourFast)*positionInRange+Color.blue(mColourSlow)*(1-positionInRange));
            int alpha = (int)(2*Math.abs(0.5-positionInRange)*128)+128-1;
            mColorSincePrevious = Color.argb(alpha, red, green, blue);
            assert (prev().mStatsSinceOrigin.mDistanceMts != 0 || prev().prev() == null);
			mStatsSinceOrigin.add(prev().mStatsSinceOrigin);
		}

		public TrackGeoPointRegister.TrackPoint.Stats statsSinceOrigin()
		{
			return mStatsSinceOrigin;
		}

		public int colourSincePrevious()
		{
			return mColorSincePrevious;
		}

		public IntegerPoint pixPoint()
		{
			return mStartPoint;
		}

		public TrackGeoPointRegister.TrackPoint geoPoint()
		{
			return mGeoPoint;
		}

		public void diagnosePoint(int index) {
			Log.d("TrackView", "["+index+"]"+" I am "+(beginningOfCut()?"segment head ":"")+
					"pix:["+mStartPoint.x()+':'+mStartPoint.y()+"]"+(nextVisible()?"v":"n")+" -geo-> coordinates:["+mGeoPoint.location().getLongitude()+","+mGeoPoint.location().getLatitude()+"]");
		}

		public int diagnoseTrack(int index) {
			diagnosePoint(index);
			if (next() != null) {
				return next().diagnoseTrack(++index);
			}
			return 1;
		}

		public int diagnoseGeo(int trackIndex) {
			diagnosePoint(trackIndex);
			TrackGeoPointRegister.TrackPoint geo = mGeoPoint.next();

			if (next() != null)
			{
				while (next().geoPoint() != geo)
				{
					if (geo == null)
					{
						Log.d("TrackView", "             -geo-> NULL");
						break;
					}
					else
					{
						Log.d("TrackView", "             -geo-> coordinates:["+geo.location().getLongitude()+","+geo.location().getLatitude()+"]");
						geo = geo.next();
					}

				}
				return next().diagnoseGeo(++trackIndex);
			}
			else
			{
				while (true)
				{
					if (geo == null)
					{
						Log.d("TrackView", "             -geo-> NULL");
						break;
					}
					else
					{
						Log.d("TrackView", "             -geo-> coordinates:["+geo.location().getLongitude()+","+geo.location().getLatitude()+"]");
						geo = geo.next();
					}
				}
			}
			return 1;
		}
	}


	/*****************************************************************************************************************
	 *
	 *
	 * A visitor pattern object seeking (and retaining) the TrackPoint nearest to the given reference
	 *
	 *
	  *****************************************************************************************************************/
	static public class TrackPointProximitySensor implements TracksView.TrackView.ProximitySensor
	{
		protected TrackPoint 			mNearestTrackPoint;
		protected long					mDistanceToReference;
		protected final IntegerPoint  	mReferencePoint;
		public TrackPointProximitySensor() {
			mDistanceToReference = Long.MAX_VALUE;
			mReferencePoint = null;
		}

		protected TrackPointProximitySensor(IntegerPoint reference) {
			mDistanceToReference = Long.MAX_VALUE;
			mReferencePoint = reference;
		}

		public void newCandidate(TrackPoint candidate) {
			if (candidate != null) {
				mNearestTrackPoint = candidate;
			}
		}

		public TrackPoint nearestTrackPoint() {
			return mNearestTrackPoint;
		}

		public IntegerPoint referencePoint()
		{
			return mReferencePoint;
		}

		@Override
		public long distanceToReference() {
			return mDistanceToReference;
		}
	}

	static public class ArmedTrackPointProximitySensor extends TrackPointProximitySensor {
		public ArmedTrackPointProximitySensor(IntegerPoint reference) {
			super(reference);
		}

		@Override
		public void newCandidate(TrackPoint candidate) {
			if (candidate != null) {
				long distance = coarseDistanceFor(candidate.mStartPoint);
				if (distance < mDistanceToReference) {
					mNearestTrackPoint = candidate;
					mDistanceToReference = distance;
				}
			}
		}

		private long coarseDistanceFor(IntegerPoint point) {
			return new IntegerVector(mReferencePoint, point).lengthSquared(); //cheap to calculate and representative for the purpose of comparison
		}
	}



	/*****************************************************************************************************************
	 *
	 *
	 * An extension of the TrackPointProximitySensor that can then calculate distances to track lines.
	 *
	 *
	  *****************************************************************************************************************/
	static public class TrackProximitySensor extends TrackPointProximitySensor
	{
		private IntegerLineSegment  	mPreviousPixLine;
		private IntegerLineSegment  	mNextPixLine;
		private DoubleLineSegment		mPreviousGeoLine;
		private DoubleLineSegment		mNextGeoLine;
		private boolean					mBehind;
		private long					mDistanceToTrack;
		public TrackProximitySensor(TrackPointProximitySensor trackPointProximitySensor)
		{
			super(trackPointProximitySensor.mReferencePoint);
			mNearestTrackPoint = trackPointProximitySensor.mNearestTrackPoint;
			mDistanceToReference = trackPointProximitySensor.mDistanceToReference;
			calculate();
		}

		public TrackProximitySensor(IntegerPoint referencePoint)
		{
			super(referencePoint);
		}

		public IntegerLineSegment previousPixLine()
		{
			return mPreviousPixLine;
		}

		public IntegerLineSegment nextPixLine()
		{
			return mNextPixLine;
		}

		public DoubleLineSegment previousGeoLine()
		{
			return mPreviousGeoLine;
		}

		public DoubleLineSegment nextGeoLine()
		{
			return mNextGeoLine;
		}

		public DoubleLineSegment nearestGeoLine()
		{
			return (mBehind && mNextGeoLine != null) || mPreviousGeoLine == null ? mNextGeoLine : mPreviousGeoLine;
		}

		public DoubleLineSegment projectableGeoLine()
		{
			assert mPreviousGeoLine != null || mNextGeoLine != null;
			return mBehind ? mNextGeoLine : mPreviousGeoLine;
		}

		public IntegerLineSegment nearestPixLine()
		{
			return (mBehind && mNextPixLine != null) || mPreviousPixLine == null ? mNextPixLine : mPreviousPixLine;
		}

		public IntegerLineSegment projectablePixLine()
		{
			assert mPreviousPixLine != null || mNextPixLine != null;
			return mBehind ? mNextPixLine : mPreviousPixLine;
		}

		public long pixDistanceToTrack()
		{
			return mDistanceToTrack;
		}

		public TrackProximitySensor calculate()
		{
			assert mNearestTrackPoint != null;
			long pixDistanceToPrevLine = Long.MAX_VALUE;
			long pixDistanceToNextLine = Long.MAX_VALUE;
			if (mNextPixLine == null && mPreviousPixLine == null)
			{
				if (mNearestTrackPoint.next() != null)
				{
					mNextPixLine = new IntegerLineSegment(mNearestTrackPoint.pixPoint(), mNearestTrackPoint.next().pixPoint());
					mNextGeoLine = new DoubleLineSegment(
							mNearestTrackPoint.geoPoint().location().getLongitude(),
							mNearestTrackPoint.geoPoint().location().getLatitude(),
							mNearestTrackPoint.next().geoPoint().location().getLongitude(),
							mNearestTrackPoint.next().geoPoint().location().getLatitude());
					pixDistanceToNextLine = mNextPixLine.distanceToSquared(mReferencePoint);
				}

				if (mNearestTrackPoint.prev() != null)
				{
					mPreviousPixLine = new IntegerLineSegment(mNearestTrackPoint.prev().pixPoint(), mNearestTrackPoint.pixPoint());
					mPreviousGeoLine = new DoubleLineSegment(
							mNearestTrackPoint.prev().geoPoint().location().getLongitude(),
							mNearestTrackPoint.prev().geoPoint().location().getLatitude(),
							mNearestTrackPoint.geoPoint().location().getLongitude(),
							mNearestTrackPoint.geoPoint().location().getLatitude());
					pixDistanceToPrevLine = mPreviousPixLine.distanceToSquared(mReferencePoint);
				}

				mBehind = pixDistanceToNextLine < pixDistanceToPrevLine;
			}


			if (nearestPixLine() != null)
			{
				mDistanceToTrack = nearestPixLine().distanceTo(mReferencePoint);
			}
			else
			{
				mDistanceToTrack = mNearestTrackPoint.pixPoint().distanceFrom(mReferencePoint);
			}

			return this;
		}

		public boolean refIsBehind()
		{
			return mBehind;
		}
	}







	private static final int					KBeginingOfVisibleCutColour = 0;
	private static final int 					KRecalculate 				= ReferenceMap.MapViews.BScale |
		      										   					  	  ReferenceMap.MapViews.BZoom;
	private static final int 					KPathStrokeWidthBase 	    = 10;
	private static final int 					KSpeedPathStrokeWidthBase	= KPathStrokeWidthBase/2;
	private final int							mPathStrokeWidth;
	private final int							mSpeedParthStrokeWith;
	protected final int 						mColourFast;
	protected final int 						mColourSlow;
	protected final int 						mColourPath;
	protected final int							mColourTrackPoint;
	protected final ReferenceMap 				mReferenceMap;
	protected final TracksView.TrackDocument	mTrackDocument;
	protected TrackPoint 						mTrackPointHead;
	protected TrackPoint						mTrackPointTail;
	private final Paint 						mPaint;
	protected TrackPointProximitySensor			mNearestTrackPoint;
	protected TrackProximitySensor				mNearestTrackLine;
	protected final TracksView					mParent;

	public TrackSegmentsView(TracksView tracksView,
							 TracksView.TrackDocument trackDocument,
							 ReferenceMap referenceMap) {
		mColourPath = tracksView.getContext().getResources().getColor(R.color.track);
		mColourTrackPoint = tracksView.getContext().getResources().getColor(R.color.trackPoint);
		mColourFast = tracksView.getContext().getResources().getColor(R.color.fast);
	    mColourSlow = tracksView.getContext().getResources().getColor(R.color.slow);
		mReferenceMap = referenceMap;
		mTrackDocument = trackDocument;
		mTrackPointHead = new TrackPoint(mTrackDocument.geoPoints().head());
		mPaint = new Paint();
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setAntiAlias(true);
		mPaint.setDither(true);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mParent = tracksView;
		mPathStrokeWidth = (int)(
				OSMMapsApplication.instance().preferences().pref_NativeScale.get()
				*KPathStrokeWidthBase);
		mSpeedParthStrokeWith = (int)(
				OSMMapsApplication.instance().preferences().pref_NativeScale.get()
				*KSpeedPathStrokeWidthBase);
	}

	@Override
	public TrackPointProximitySensor invalidate(int bWhatsChangedVector, IntegerPoint referenceForProximitySensor) {
		IntegerVector pixMoveBy = null;
		if (( bWhatsChangedVector & KRecalculate ) == 0 && mNearestTrackPoint != null) {
			IntegerPoint referencePointPix = mReferenceMap.geo2pix(mNearestTrackPoint.nearestTrackPoint().geoPoint().location());
			if (referencePointPix != null) {
				pixMoveBy = new IntegerVector(mNearestTrackPoint.nearestTrackPoint().pixPoint(),referencePointPix);
				pixMoveBy = pixMoveBy.lengthSquared()==0 ? null : pixMoveBy;
			}
		} else if ((bWhatsChangedVector & ReferenceMap.MapViews.BZoom) != 0) {
			//zoom changed, must recalculate everything.
			mTrackPointHead = new TrackPoint(mTrackDocument.geoPoints().head());
		}
		mNearestTrackPoint = createProximitySensor(referenceForProximitySensor);
		doInvalidate(mNearestTrackPoint, pixMoveBy);
		return mNearestTrackPoint;
	}

	private void doInvalidate(TrackPointProximitySensor trackPointVisitor, IntegerVector pixMoveBy)
	{
		TrackGeoPointRegister.TrackPoint geoPoint = mTrackPointHead.mGeoPoint;
		mTrackPointTail = mTrackPointHead;
		boolean previousVisible = true;
		while (geoPoint != null)
		{
			TrackPoint nextPix = mTrackPointTail.next();
			if (geoPoint == mTrackPointTail.mGeoPoint)
			{
				//a previously valid pix point of a cut. If visible (part of a visible, next or previous, track line), move by the given vector, if not, remove.
				boolean nextVisible = mTrackPointTail.nextVisible();
				if (previousVisible || nextVisible)
				{
					mTrackPointTail.invalidate(pixMoveBy, trackPointVisitor);
				}
				else
				{
					mTrackPointTail = mTrackPointTail.remove();
				}
				previousVisible = nextVisible;


				if (nextPix != null && !nextPix.beginningOfCut())
				{
					//the next pix is still within the same cut, advance the pixPoint.
					mTrackPointTail = nextPix;
					geoPoint = nextPix.mGeoPoint;
				}
				else
				{
					//we have reached the end of this cut. Advance the geoPoint beyond the cut so we can try to seek for
					//more pix points to this cut or more cuts.
					geoPoint = geoPoint.next();
				}
			}
			else if (nextPix != null && geoPoint == nextPix.mGeoPoint)
			{
				//advancing geoPoint, we have reached the next cut. Position the pixPoint cursor to allow processing of this cut.
				mTrackPointTail = nextPix;
				if (previousVisible)
				{
					//if previously processed geoPoint was visible, merge the cuts.
					mTrackPointTail.setStats(mTrackDocument.geoPoints().stats());
				}
			}
			else
			{
				assert nextPix == null || nextPix.beginningOfCut();
				TrackGeoPointRegister.TrackPoint untilGeo = nextPix != null?nextPix.mGeoPoint:null;
				TrackPoint previousPixPoint = mTrackPointTail;
				while (geoPoint != untilGeo)
				{
					TrackPoint candidateTrackPoint = new TrackPoint(geoPoint);
					if (candidateTrackPoint.mStartPoint.axisDistanceFrom(mTrackPointTail.mStartPoint) > mPathStrokeWidth ||
					    geoPoint.next() == null)
					{
						if (trackLineVisible(previousPixPoint, candidateTrackPoint))
						{
							if (previousPixPoint != mTrackPointTail)
							{
								mTrackPointTail = mTrackPointTail.append(previousPixPoint);
								trackPointVisitor.newCandidate(previousPixPoint);
								mTrackPointTail.mColorSincePrevious = KBeginingOfVisibleCutColour;
							}
							mTrackPointTail = mTrackPointTail.append(candidateTrackPoint);
							trackPointVisitor.newCandidate(candidateTrackPoint);
							previousVisible = true;
						}
						else
						{
							previousVisible = false;
						}
						previousPixPoint = candidateTrackPoint;
					}
					geoPoint = geoPoint.next();
				}
			}
		}
	}

	private boolean trackLineVisible(TrackPoint from, TrackPoint to) {
		return mReferenceMap.getTraits().mMapPixBounds.mapRect().intersects(
				from.mStartPoint.x(), from.mStartPoint.y(), to.mStartPoint.x(), to.mStartPoint.y());
	}


	@Override
	public float getRotationAngle() {
		float rotationAngle = 0;
		if (mNearestTrackLine != null && mNearestTrackLine.nearestPixLine() != null) {
			//get two vectors, before and after the closest point
			rotationAngle = (float)mNearestTrackLine.nearestPixLine().angleDegRelY();
			if (mTrackDocument.rotationPolicy() == TrackDocument.RotationPolicy.EFollow) {
				rotationAngle = (rotationAngle + 180)%360;
			}
		}

		return rotationAngle;
	}

	@Override
	public void draw(Canvas canvas) {
		draw(canvas, mTrackPointHead);
	}

	@Override
	public void windroseClick() {
		mTrackDocument.nextRotation();
		mParent.invalidate();
	}

	@Override
	public TracksView.TrackView follow(boolean follow, TracksView.TrackView previousFollowedTrack) {
		TracksView.TrackView result = null;
		if (follow) {
			//TODO: this will wrap for larger distances
			mNearestTrackLine = new TrackProximitySensor(mNearestTrackPoint);
			long distanceMts = mReferenceMap.pix2Mts(mNearestTrackLine.pixDistanceToTrack());
			if (distanceMts <= KMaxDistanceForFollowingMts)
			{
				if (previousFollowedTrack != this) {
					mTrackDocument.follow(true);
				} else {
					previousFollowedTrack = null;
				}
				result = this;
			}
			else if (mTrackDocument.isBeingFollowed()) {
				mTrackDocument.follow(false);
			}
		}
		else if (mTrackDocument.isBeingFollowed()) {
			mTrackDocument.follow(false);
		}

		if (previousFollowedTrack != null) {
			previousFollowedTrack.follow(false, null);
		}
		return result;
	}

	public TracksView.Distances distances() {
		long sinceOriginMts = mNearestTrackLine.nearestTrackPoint().statsSinceOrigin().distanceMts();
		TracksView.Distances distances = new TracksView.Distances(
			sinceOriginMts,
			document().geoPoints().stats().distanceMts()-sinceOriginMts
		);

		if (mTrackDocument.rotationPolicy() == TrackDocument.RotationPolicy.EReverseFollow) {
			distances = distances.reverse();
		}

		return distances;
	}

	public TracksView.TrackDocument document() {
		return mTrackDocument;
	}

	protected void draw(Canvas canvas, TrackSegmentsView.TrackPoint from) {
		TrackSegmentsView.TrackPoint next = from;
		while ((next = next.next()) != null) {
			drawLine(canvas, from, next);
			from = next;
		}
		canvas.drawCircle(from.pixPoint().x(), from.pixPoint().y(), mPathStrokeWidth/4, mPaint);
	}

	protected void drawLine(Canvas canvas, TrackSegmentsView.TrackPoint from, TrackSegmentsView.TrackPoint to) {
		drawLine(canvas, from, to, to.colourSincePrevious());
	}

	protected void drawLine(Canvas canvas, TrackSegmentsView.TrackPoint from, TrackSegmentsView.TrackPoint to, int colour) {
		mPaint.setColor(mColourPath);
		mPaint.setStrokeWidth(mPathStrokeWidth);
		canvas.drawLine(from.pixPoint().x(), from.pixPoint().y(),
				to.pixPoint().x(), to.pixPoint().y(), mPaint);
		mPaint.setStrokeWidth(mSpeedParthStrokeWith);
		mPaint.setColor(colour);
		canvas.drawLine(from.pixPoint().x(), from.pixPoint().y(),
				to.pixPoint().x(), to.pixPoint().y(), mPaint);
		mPaint.setColor(mColourTrackPoint);
		canvas.drawCircle(from.pixPoint().x(), from.pixPoint().y(), mPathStrokeWidth/4, mPaint);
	}

	protected TrackPointProximitySensor createProximitySensor(IntegerPoint reference) {
		return reference != null ? new ArmedTrackPointProximitySensor(reference) : new TrackPointProximitySensor();
	}
}
