//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.tracks;

import java.util.NoSuchElementException;

import net.remekzajac.Geo.Topo;
import net.remekzajac.maps.GeoDegPoint;
import net.remekzajac.utils.Filter;
import net.remekzajac.utils.ListElement;
import android.location.Location;
import android.text.format.Time;
import android.util.Log;

public class TrackGeoPointRegister implements Topo.Path {

	static public class Stats {
		public final static float		KMaxReasonableAccellerationKphs = 10; //a very good car does 0->100 in 6sec, yielding a=16kph/s
		public final static float 		KMpMsToKphFactor = (float)((1000.0*60.0*60.0)/1000.0);
		public final static int 		KMinSamplesForRTSpeed = 2;

		protected long 				  	mDistanceMts;
		protected double 				mAvgSpeedKph;
		protected double     			mMaxSpeedKph;
		protected Integer        		mMaxEleMts;
		protected Integer    		    mMinEleMts;
		protected Double				mPrevSpeedKph;
		protected Location 				mPreviousLocation;
		protected GeoDegPoint.Bounds 	mBounds;
		protected long				    mDurationMs;
		protected int					mSamples;
		protected String   				mStatsName;
		protected boolean               mIgnoreSpeed;
        protected Integer               mdTimeSignum; //can be fed back to front
                                                      //or front to back but not
                                                      //mixed and always in sequence
		public Stats() {}
		public Stats(String name) {mStatsName = name;}

		private void addTimestamp(long timestampMs, long distanceMts) {
            if (mPreviousLocation != null && mPreviousLocation.getTime() != 0) {
                long  dtimeMs  = timestampMs-mPreviousLocation.getTime();
				if (dtimeMs == 0) {
					return;
				}
                int dtimeSignum = (int)Math.signum((float)dtimeMs);
                if (mdTimeSignum != null && mdTimeSignum != dtimeSignum) {
                    throw new AssertionError("Mixed time direction given to TrackGeoPointRegister.Stats");
                }
                mdTimeSignum = dtimeSignum;
				dtimeMs = Math.abs(dtimeMs);
				mDurationMs += dtimeMs;
				mAvgSpeedKph = (KMpMsToKphFactor*mDistanceMts)/mDurationMs;
                if (distanceMts > 0) {
                    double speedKph =  KMpMsToKphFactor*distanceMts/dtimeMs;
					if (mPrevSpeedKph == null) {
						if (mSamples >= KMinSamplesForRTSpeed) {
							mPrevSpeedKph = mAvgSpeedKph;
						}
					} else {
						//work out accelleration
						double accelleration = mdTimeSignum * ((speedKph - mPrevSpeedKph) * 1000.0) / dtimeMs;
						if (Math.abs(accelleration) > KMaxReasonableAccellerationKphs) {
							//Log.w("AutomaticRecorder", "Excessive acceleration noted: "+accelleration+" going from "+mPreviousSpeed+"kph to "+speedKph+"kph in "+dtimeSec+"s");
							//cap it
							mPrevSpeedKph += KMaxReasonableAccellerationKphs * dtimeMs / 1000.0;
						} else {
							mPrevSpeedKph = speedKph;
						}
					}
					if (mPrevSpeedKph != null) {
						mMaxSpeedKph = Math.max(
							Math.max(mMaxSpeedKph, mPrevSpeedKph),
							mAvgSpeedKph
						);
					}
                }
            }
		}

		private void addEle(Integer min, Integer max) {
			if (mMaxEleMts != null && max != null) {
				mMaxEleMts = Math.max(mMaxEleMts, max);
			} else if (mMaxEleMts == null){
				mMaxEleMts = max;
			}
			if (mMinEleMts != null && min != null) {
				mMinEleMts = Math.min(mMinEleMts, min);
			} else if (mMinEleMts == null){
				mMinEleMts = min;
			}
		}

		public void add(Location location) {
			if ( mBounds == null) {
				mBounds = new GeoDegPoint.Bounds(location);
			} else {
				long distanceMts = (long)mPreviousLocation.distanceTo(location);
				mDistanceMts += distanceMts;
				mBounds.add(location);
                if (location.hasAltitude()) {
                    Integer eleMts = (int)location.getAltitude();
					addEle(eleMts, eleMts);
                }
				if ( location.getTime() != 0 && !mIgnoreSpeed) {
					addTimestamp(location.getTime(), distanceMts);
				}

			}
			mPreviousLocation = location;
			mSamples++;
		}

		public void add(Stats stats) {
			if (stats.mPreviousLocation == null) {
				return; //nothing to add
			}
			if (mPreviousLocation == null || stats.mPreviousLocation.getTime() > mPreviousLocation.getTime()) {
				mPreviousLocation = stats.mPreviousLocation;
			}
			addEle(stats.minEleMts(), stats.maxEleMts());
			mMaxSpeedKph = Math.max(mMaxSpeedKph, stats.maxSpeedKph());
			mDurationMs += stats.mDurationMs;
			mDistanceMts += stats.distanceMts();
			mAvgSpeedKph = KMpMsToKphFactor*mDistanceMts/mDurationMs;
			if (mBounds == null) {
				mBounds = stats.mBounds;
			}
			else if (stats.mPreviousLocation != null) {
				mBounds.add(stats.mBounds);
			}
			mPreviousLocation = stats.mPreviousLocation;
			mSamples += stats.samples();
		}

		public void add(GeoDegPoint.Bounds bounds) {
			if (mBounds == null) {
				mBounds = new GeoDegPoint.Bounds(bounds);
			} else {
				mBounds.add(bounds);
			}
		}

		public void addTimeStampFromLocalClock() {
			//simulates that the previous location remains unchanged for another time segment (since previous update).
			addTimestamp(System.currentTimeMillis(), 0);
			mSamples++;
		}

		public double avgSpeedKph() {
			return mAvgSpeedKph;
		}

		public double maxSpeedKph() {
			return mMaxSpeedKph;
		}

		public long distanceMts() {
			return mDistanceMts;
		}

        public Integer maxEleMts() { return mMaxEleMts; }

        public Integer minEleMts() { return mMinEleMts; }

        public Integer dEleMts() {
            if (mMaxEleMts != null && mMinEleMts != null) {
                return mMaxEleMts - mMinEleMts;
            }
            return null;
        }

		public GeoDegPoint.Bounds bounds() {
			return mBounds;
		}

		public long durationSec() {
			return mDurationMs/1000;
		}

		public long spanMts() {
			if (mBounds != null) {
				return mBounds.nw().distanceFromMts(mBounds.se());
			}
			return 0;
		}

		public int samples()
		{
			return mSamples;
		}

		@Override
		public String toString() {
			String name = mStatsName == null ? "" : "Stats("+mStatsName+"): ";
			return String.format(
				java.util.Locale.US,
				"%s Samples: %d l:(%dmts/%.2fkms), t:(%ds/%.2fh), v: (max: %.2f, avg: %.2f, cur: %.2f), span: %dmts",
				name, mSamples,
				mDistanceMts, 1.0*mDistanceMts/1000,
				mDurationMs/1000, 1.0*mDurationMs/(1000*60*60),
				mMaxSpeedKph, mAvgSpeedKph, mPrevSpeedKph,
				spanMts()
			);
		}
	}

	public static class TrackPoint extends ListElement<TrackPoint>
	{
        public static class TrackPointFilter implements Filter<TrackPoint> {
            private TrackPoint mPoint;
            public TrackPointFilter(TrackPoint point) {
                mPoint = point;
            }
            @Override
            public boolean matches(TrackPoint trackPoint) {
                return mPoint == trackPoint;
            }
        }
		public class Stats extends TrackGeoPointRegister.Stats {
			private TrackPoint mSince = TrackPoint.this;
            private TrackPoint mTo = TrackPoint.this;

			public Stats(String name) {super(name);}
			public Stats() {}
			public Stats ignoreSpeed() {
				mIgnoreSpeed = true;
				return this;
			}

            /*
             * Note that the traceBack* functions feed the stats back to front.
             * They do that because they sometimes need to work off TrackPoint
             * branches, which are strictly one way - see the implementation of nextMts
             */
            public Stats traceBackAsLong(Filter<TrackPoint> asLongAs) throws NoSuchElementException {
                TrackPoint current = mSince;
                while (current != null && asLongAs.matches(current)) {
                    mSince = current;
                    add(current.location());
                    current = current.prev();
                }
                mPreviousLocation = mTo.location();
                mdTimeSignum = null;
                return this;
            }

			public Stats traceBackUntil(Filter<TrackPoint> until) throws NoSuchElementException {
                TrackPoint current = mSince;
                while (current != null) {
                    mSince = current;
                    add(current.location());
                    if (until.matches(current)) {
						mPreviousLocation = mTo.location();
                        mdTimeSignum = null;
                        return this;
                    }
                    current = current.prev();
                }
                throw new NoSuchElementException();
			}

            public Stats traceForwardUntil(Filter<TrackPoint> until) throws NoSuchElementException {
                TrackPoint current = mTo;
                while (current != null) {
                    mTo = current;
                    add(current.location());
                    if (until.matches(current)) {
                        return this;
                    }
                    current = current.next();
                }
                throw new NoSuchElementException();
            }

			public TrackPoint since()
			{
				return mSince;
			}
            public TrackPoint to()
            {
                return mTo;
            }
            public TrackPoint reference() { return TrackPoint.this; }
			public long crowFlightDistanceMts() {
				return mSince.distanceMts(mTo);
			}
			@Override
			public String toString() {
				return super.toString() + " crow: " + crowFlightDistanceMts() + "mts";
			}
		}

		final private Location mLocation;
		public TrackPoint(Location location)
		{
			mLocation = location;
		}

        public TrackPoint(Location location, TrackPoint prev, TrackPoint next) {
            super(prev,next);
            mLocation = location;
        }

		public Stats stats(String name) {
			return new Stats(name);
		}

		public Stats stats() {
			return new Stats();
		}


		public Location location() {
			return mLocation;
		}

        public long distanceMts(TrackPoint to) {
            return (long)location().distanceTo(to.location());
        }

        public Integer eleMts() {
            if (mLocation.hasAltitude()) {
                return (int)mLocation.getAltitude();
            }
            Filter<TrackPoint> hasAltitude = new Filter<TrackPoint>() {
                @Override
                public boolean matches(TrackPoint trackPoint) {
                    return trackPoint.location().hasAltitude();
                }
            };
            TrackPoint.Stats betweenTwoEles = stats().ignoreSpeed();
            try {
                betweenTwoEles.traceBackUntil(hasAltitude);
            } catch (NoSuchElementException e) {
                //no previous trackpoing with elevation, let's use the one in the front as is
                try {
                    betweenTwoEles.traceForwardUntil(hasAltitude);
                } catch (NoSuchElementException ee) {
                    return null;
                }
                return (int)betweenTwoEles.mTo.location().getAltitude();
            }

            try {
                betweenTwoEles.traceForwardUntil(hasAltitude);
                long distanceFromStart = (long)betweenTwoEles.mSince.distanceMts(
                    betweenTwoEles.reference()
                );
                float factor = (float)distanceFromStart/betweenTwoEles.crowFlightDistanceMts();
                return betweenTwoEles.mSince.eleMts() + (int)(factor * betweenTwoEles.dEleMts());
            } catch (NoSuchElementException e) {
                //no next trackpoint with elevation, let's use the one back
                return (int)betweenTwoEles.mSince.location().getAltitude();
            }
        }

		public void setTime(long timeMs)
		{
			mLocation.setTime(timeMs);
		}

		@Override
		public String toString()
		{
			return "["+this.hashCode()+" t:"+mLocation.getTime()+"]";
		}

		@Override
		protected boolean cutOffNext()
		{
			return super.cutOffNext();
		}

		@Override
		protected boolean cutOffPrev()
		{
			return super.cutOffPrev();
		}

        public TrackPoint nextMts(int metres) {
            TrackPoint current = this;
            while (metres > 0 && current != null) {
                TrackPoint next = current.next();
                if (next != null) {
                    metres -= next.distanceMts(current);
                }
                current = next;
            }
            if (current != null) {
                if (metres < 0) {
                    TrackPoint prev = current.prev();
                    long fromPrevMts = current.distanceMts(prev);
                    float fromPrevFactor = (float)(fromPrevMts + metres)/fromPrevMts;
                    Location location = new Location("");
                    location.setLatitude(
                        prev.location().getLatitude() + (current.location().getLatitude() - prev.location().getLatitude()) * fromPrevFactor
                    );
                    location.setLongitude(
                        prev.location().getLongitude() + (current.location().getLongitude() - prev.location().getLongitude()) * fromPrevFactor
                    );
                    if (current.location().hasSpeed() && prev.location().hasSpeed())
                    {
                        location.setTime(
                            prev.location().getTime() + (long)((current.location().getTime() - prev.location().getTime()) * fromPrevFactor)
                        );
                    }
                    return new TrackPoint(location, prev, current);
                }
                return current;
            }
            return null;
        }
	}


	public static int 			KInfinity = Integer.MAX_VALUE;
	private TrackPoint 			mHead;
	private TrackPoint 			mTail;
	private int 				mCurrentSize;
	private Stats 				mStats;
	public TrackGeoPointRegister(String name) {
		mStats = new Stats(name);
	}

	public TrackGeoPointRegister() {
		this(null);
	}

	public TrackGeoPointRegister append(Location location) {
		if (location != null) {
			if (mTail == null) {
				mTail = mHead = new TrackPoint(location);
			} else {
				mTail = mTail.append(new TrackPoint(location));
			}
			mStats.add(location);
		}
		return this;
	}

	//from Topo.Path
	public void append(Topo.Point aPoint) {
		Location location = new Location("");
		if (aPoint.mEle != null) {
			location.setAltitude(aPoint.mEle);
		}
		if (aPoint.mTime != null) {
			location.setTime(aPoint.mTime);
		}

        location.setLatitude(aPoint.mLat);
        location.setLongitude(aPoint.mLon);
    	append(location);
	}

	//from Topo.Path
	public void addAttribute(String aName, String aValue) {}

	public TrackGeoPointRegister remove(TrackPoint point) {
		if (point != null) {
			if (point == mHead) {
				mHead = mHead.next();
			}
			if (point == mTail) {
				mTail = mTail.prev();
			}
			point.remove(TrackPoint.RemovalOutcome.ELeftRight);
			if (mTail != null) {
				mStats = mTail.stats().traceBackUntil(
                    new TrackPoint.TrackPointFilter(mHead)
                );
			} else {
				mStats = new Stats();
			}
		}

		return this;
	}

	public TrackPoint removeHead() {
		TrackPoint result = head();
		if (result != null) {
			remove(result);
		}
		return result;
	}

	public int noOfElements()
	{
		return mStats.samples();
	}

	public void clear() {
		mHead = null;
		mTail = null;
		mStats = new Stats();
	}

	public void append(TrackGeoPointRegister register) {
		mTail = mTail.append(register.head().next()).tail();
		mStats.add(register.mStats);
	}

	@Override
	public TrackGeoPointRegister clone() {
		TrackGeoPointRegister result = new TrackGeoPointRegister();
		TrackPoint trackPoint = head();
		while (trackPoint != null) {
			result.append(trackPoint.location());
			trackPoint = trackPoint.next();
		}
		return result;
	}


	public TrackPoint head()
	{
		return mHead;
	}

	public TrackPoint tail() { //TODO allows modifying tail (adding things to it) without updating the mTail member
		return mTail;
	}

	public Stats stats() {
		return mStats;
	}

	public int size()
	{
		return mStats.samples();
	}

	public void cutOffBehind(TrackPoint trackPoint) {
		trackPoint.cutOffNext();
		mTail = trackPoint;
		mStats = mTail.stats().traceBackUntil(new TrackPoint.TrackPointFilter(mHead));
	}

	public void cutOffBefore(TrackPoint trackPoint) {
		trackPoint.cutOffPrev();
		mHead = trackPoint;
		mStats = mTail.stats(mStats.mStatsName).traceBackUntil(new TrackPoint.TrackPointFilter(mHead));
	}

	public void diagnose() {
		Log.d("NavigatedDocument", "TrackGeoPointRegister::diagnose>>");
		TrackPoint previous = null;
		TrackPoint trackPoint = head();

		while (trackPoint != null)
		{
			String additionalString = "";
			if (trackPoint.prev() != previous)
			{
				additionalString += "[not pointing back properly]";
			}
			if (trackPoint == tail())
			{
				additionalString += "<<tail ";
			}
			Log.d("NavigatedDocument", trackPoint.toString()+additionalString);
			previous = trackPoint;
			trackPoint = trackPoint.next();
		}
		Log.d("NavigatedDocument", "<<TrackGeoPointRegister::diagnose");
	}
}
