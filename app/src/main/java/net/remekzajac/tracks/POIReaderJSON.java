//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.tracks;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import net.remekzajac.maps.GeoDegPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class POIReaderJSON
{
	private final InputStream mInputStream;
	private final Context mContext;
	public POIReaderJSON(Context context, InputStream inputStream) {
		mInputStream = inputStream;
		mContext = context;
	}

	public GeoPOIRegister getGeoPOIRegister() throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(
        		mInputStream, "iso-8859-1"), 8);
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }

        GeoPOIRegister result = new GeoPOIRegister(mContext);
        JSONObject jObj = new JSONObject(sb.toString());
        JSONArray features = jObj.getJSONArray("features");
        for (int i = 0; i < features.length(); i++) {
        	JSONObject centeroid = features.getJSONObject(i);
        	parseCenteroid(centeroid, result);
        }

        return result;
	}

	private void parseCenteroid(JSONObject centeroid, GeoPOIRegister register) throws JSONException {
		JSONArray bounds = centeroid.getJSONArray("bounds");
		JSONArray nw = bounds.getJSONArray(0);
		JSONArray se = bounds.getJSONArray(1);
		JSONObject properties = centeroid.getJSONObject("properties");
		String description = properties.getString("name");
		register.append(
				new GeoPOIRegister.POI(
						new GeoDegPoint.Bounds(
								GeoDegPoint.fromStrings(nw.getString(0), nw.getString(1)),
								GeoDegPoint.fromStrings(se.getString(0), se.getString(1))
								),
						description)
				);
	}
}
