//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.remekzajac.tracks;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import net.zikes.R;

import java.util.ArrayList;
import java.util.List;

public class ProfileProgressView extends View {
    static final private int KPixWidthStep         = 10;
    static final private int KSoften_dEleBelowMts  = 800;

    private TrackGeoPointRegister mTrack;
    private Paint mProfilePolygonPaint;
    private Paint mProgressPaint;
    private Paint mReminderPaint;
    private Path  mProfilePath;
    private Path  mProgressPath;
    private Path  mReminderPath;
    private List<Integer> mProfile;

    public ProfileProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mProfilePolygonPaint = new Paint();
        mProfilePolygonPaint.setColor(getContext().getResources().getColor(R.color.zikesBrightBlueBkgd));
        //mProfilePolygonPaint.setAlpha(20);
        mProfilePolygonPaint.setStyle(Paint.Style.FILL);

        mProgressPaint = new Paint();
        mProgressPaint.setColor(Color.CYAN);
        mProgressPaint.setStrokeWidth(15);
        mProgressPaint.setStyle(Paint.Style.STROKE);

        mReminderPaint = new Paint();
        mReminderPaint.setColor(Color.GRAY);
        mReminderPaint.setStrokeWidth(8);
        mReminderPaint.setStyle(Paint.Style.STROKE);

        mProfilePath  = new Path();
        mProgressPath = new Path();
        mReminderPath = new Path();
    }

    public void setProgress(float progress, TrackGeoPointRegister track) {
        if (mTrack != track) {
            mTrack = track;
            prepareProfile();
        }
        prepareProgress(progress);
        invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w,h,oldw,oldh);
        prepareProfile();
    }

    private void prepareProfile() {
        if (mTrack != null && getWidth() != 0 && getWidth() != 0) {
            mProfile = new ArrayList<Integer>();
            int width  = getWidth();
            int height = getHeight();

            float distanceFactor = (float)mTrack.stats().distanceMts() / getWidth();
            int minEle = 0;
            float eleFactor = 1;
            if (mTrack.stats().dEleMts() != null) {
                minEle = mTrack.stats().minEleMts();
                int dEle = mTrack.stats().dEleMts();
                if (dEle < KSoften_dEleBelowMts) {
                    int inflateDele = (KSoften_dEleBelowMts-dEle)/10;
                    dEle += inflateDele;
                    minEle -= inflateDele/2;
                }
                eleFactor = (float)height / dEle;
            }
            int distanceStepMts = (int)(KPixWidthStep * distanceFactor);
            mProfilePath.reset();
            mProfilePath.moveTo(0,height);
            mReminderPath.reset();

            TrackGeoPointRegister.TrackPoint point = mTrack.head();
            for (int x = 0; x < width && point != null; x+=KPixWidthStep) {
                Integer eleMts = point.eleMts();
                int y = height;
                if (eleMts != null) {
                    y = (int)((eleMts - minEle) * eleFactor);
                }
                y = height - y;

                mProfilePath.lineTo(x,y);
                mProfile.add(y);
                if (x == 0) {
                    mReminderPath.moveTo(x,y);
                } else {
                    mReminderPath.lineTo(x,y);
                }
                point = point.nextMts(distanceStepMts);
            }
            mProfilePath.lineTo(width, 0);
            mProfilePath.lineTo(width, getHeight());
            mProfilePath.lineTo(0,height);
        }
    }

    private void prepareProgress(float currentProgress) {
        if (mProfile != null) {
            mProgressPath.reset();
            mReminderPath.reset();
            int currentProgressX = (int)(currentProgress * getWidth());
            int x = 0;
            boolean reminderStarted = false;
            for (Integer y : mProfile) {
                if (x == 0) {
                    mProgressPath.moveTo(x, y);
                } else if (x <= currentProgressX) {
                    mProgressPath.lineTo(x, y);
                } else if (!reminderStarted) {
                    mReminderPath.moveTo(x, y);
                    reminderStarted = true;
                } else {
                    mReminderPath.lineTo(x, y);
                }
                x += KPixWidthStep;
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawPath(mProfilePath,  mProfilePolygonPaint);
        canvas.drawPath(mProgressPath, mProgressPaint);
        canvas.drawPath(mReminderPath, mReminderPaint);
    }
}
