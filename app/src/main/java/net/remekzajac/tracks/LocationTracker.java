//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.tracks;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import net.zikes.OSMMapsApplication;
import net.zikes.SettingsActivity;
import net.remekzajac.maps.ReferenceMap;
import net.remekzajac.tracks.LocationTracker.Recorder.State;
import net.remekzajac.utils.IntegerPoint;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

public class LocationTracker implements LocationListener
{
    public interface Recorder
    {
    	public enum State {
    		EIdle,
    		ERecording,
    		EThinking
    	};

    	interface Listener {
    		void onStateChange(File recordedTrack);
    	}

    	public State  	state();
    	public void 	newLocation(Location location);
    }


	public interface Listener {
		public void onRecordingStateChanged(File recordedTrack);
	}


    class StaleLocationTimer
    {
    	public final int mHotLocationTimeoutMs;
    	private Timer    mTimer;
    	public StaleLocationTimer(int currentLocationTimeoutMs) {
    		mHotLocationTimeoutMs = currentLocationTimeoutMs;
    	}

    	public void start() {
    		if (mTimer != null)
    		{
    			mTimer.cancel();
    			mTimer.purge();
    		}
    		mTimer = new Timer();
			final Handler handler = new Handler();
			TimerTask timerTask = new TimerTask()
			{
				@Override
				public void run() {
	                handler.post(new Runnable() {
	                    public void run() {
	                    	loseFix(false);
	                    }});
				}
			};
			if (mHotLocation != null)
			{
				mTimer.schedule(timerTask, mHotLocationTimeoutMs);
			}
    	}
    }

    /*
     * TODO, I would normally expect the location updates to arrive at least as often
     * as requested with requestLocationUpdates. But they don't, so we need some extra
     * laxity in considering the current location stale.
     */
    private static final int 			KCurrentLocationGoingStaleMultipler = 2;

	private Location					mHotLocation;
	private Location 					mWarmLocation;
	private Location 					mColdLocation;
	private final AutomaticRecorder 	mAutomaticRecorder;
	private final ManualRecorder		mManualRecorder;
	private final Context 				mContext;
	private final StaleLocationTimer 	mStaleLocationTimer;
	public LocationTracker(Context context, final Listener listener) {
	    mContext = context;
		mAutomaticRecorder = new AutomaticRecorder(
			context,
			new Recorder.Listener() {
				@Override
				public void onStateChange(File recordedTrack) {
					listener.onRecordingStateChanged(recordedTrack);
				}
			}
		);
		mManualRecorder = new ManualRecorder(
				context,
				new Recorder.Listener() {
					public void onStateChange(File recordedTrack) {
						listener.onRecordingStateChanged(recordedTrack);
					}
				});

	    mStaleLocationTimer = new StaleLocationTimer(
	    		OSMMapsApplication.instance().preferences().pref_Location_minTimeSec.get()*
	    		1000 * KCurrentLocationGoingStaleMultipler
		);
	    mColdLocation = ((LocationManager)mContext.getSystemService(Context.LOCATION_SERVICE)).getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
	    if (mColdLocation == null) {
	    	mColdLocation = new Location("");
	    	mColdLocation.setLatitude(
					OSMMapsApplication.instance().preferences().pref_InitialLocation_Lat.get()
            );
	    	mColdLocation.setLongitude(
					OSMMapsApplication.instance().preferences().pref_InitialLocation_Lon.get()
            );
	    	mColdLocation.setTime(0); //mark it real stale
	    }

	    Criteria criteria = new Criteria();
	    criteria.setAccuracy(Criteria.ACCURACY_FINE);
	    criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
	    criteria.setAltitudeRequired(
	    		OSMMapsApplication.instance().preferences().pref_Location_RqAltitude.get()
        );
	    criteria.setCostAllowed(true);

	    ((LocationManager)mContext.getSystemService(Context.LOCATION_SERVICE)).requestLocationUpdates(
            (long) OSMMapsApplication.instance().preferences().pref_Location_minTimeSec.get()*1000,
            OSMMapsApplication.instance().preferences().pref_Location_minDistMts.get(),
            criteria, this, null
        );
	}


	public AutomaticRecorder automaticRecorder()
	{
		return mAutomaticRecorder;
	}

	public ManualRecorder manualRecorder()
	{
		return mManualRecorder;
	}

	/*Returns true if only now started following*/
	public boolean startFollowing(int atZoom) {
		boolean result = false;
		if (mWarmLocation != null) {
			result = !isFollowing();
			OSMMapsApplication.instance().referenceMap()
				.newRequest()
				.pan(mWarmLocation)
				.zoom(Math.max(OSMMapsApplication.instance().referenceMap().getTraits().mZoom, atZoom))
				.execute();
		}
		return result;
	}

	public void startRecording() throws IOException {
		if (mWarmLocation != null) {
			mManualRecorder.start();
			mManualRecorder.newLocation(mWarmLocation);
		}
	}

	public void stopRecording(String trackName) throws IOException {
		mManualRecorder.setTrackName(trackName);
		mManualRecorder.stop(true, null);
	}

	public Location hotLocation()
	{
		return mHotLocation;
	}

	public Location warmLocation() {
		return mWarmLocation;
	}

	public Location coldLocation()
	{
		return mColdLocation;
	}

	public boolean isFollowing() {
		IntegerPoint previousPixLocation = null;
		if ((previousPixLocation = OSMMapsApplication.instance().referenceMap().geo2pix(warmLocation())) != null) {
			return previousPixLocation.equals(
					OSMMapsApplication.instance().referenceMap().getTraits().mMapPixBounds.mapCentre());
		}
		return false;
	}

	public void onLocationChanged(Location location) {
		boolean isFollowing = isFollowing(); //capture whether is following before we update the location members.
		mColdLocation = mWarmLocation = mHotLocation = location;
		validateLocation();
		if (mHotLocation != null) {
			mManualRecorder.newLocation(mHotLocation);
			mAutomaticRecorder.newLocation(mHotLocation);
		}
        if (isFollowing && mWarmLocation != null) {
        	//if the received location valid and we're following,
			OSMMapsApplication.instance().referenceMap()
				.newRequest()
				.pan(location)
				.notify(ReferenceMap.MapViews.BLocation)
				.execute();
		} else {
        	OSMMapsApplication.instance().referenceMap().invalidate(ReferenceMap.MapViews.BLocation);
        }
	}


	private boolean validateLocation() {
		boolean somethingChanged = false;
		long timeNow = System.currentTimeMillis();
		if (mHotLocation != null) {
			long locationMsOld = timeNow-mHotLocation.getTime();
	   		//Log.d("LocationTracker", "last hot location update from "+locationMsOld/1000+"sec ago");
			if (locationMsOld > mStaleLocationTimer.mHotLocationTimeoutMs) {
	    		mHotLocation = null;
	    		somethingChanged = true;
			}
        }
		loseFix(true);
		return somethingChanged;
	}

	public void onProviderDisabled(String provider) {
		if (mHotLocation != null && mHotLocation.getProvider().equals(provider)) {
			Log.d("LocationTracker", String.format("Positioning provider %s onProviderDisabled", provider));
			loseFix(false);
		}
	}

	public void onProviderEnabled(String provider) {
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		switch (status) {
		case LocationProvider.TEMPORARILY_UNAVAILABLE:
		case LocationProvider.OUT_OF_SERVICE:
			Log.d("LocationTracker", String.format("Positioning provider %s onStatusChanged to %s", provider, status==LocationProvider.OUT_OF_SERVICE ? "OUT_OF_SERVICE":"TEMPORARILY_UNAVAILABLE"));
			if (mHotLocation != null && mHotLocation.getProvider().equals(provider))
			{
				loseFix(true);
			}
			break;
		case LocationProvider.AVAILABLE:
			Log.d("LocationTracker", String.format("Positioning provider %s onStatusChanged to AVAILABLE", provider));
			break;
		}
	}

	private void loseFix(boolean onTimer)
	{
		if (onTimer) {
			mStaleLocationTimer.start();
		} else if (validateLocation()) {
			OSMMapsApplication.instance().referenceMap().invalidate(ReferenceMap.MapViews.BLocation);
		}
	}
}
