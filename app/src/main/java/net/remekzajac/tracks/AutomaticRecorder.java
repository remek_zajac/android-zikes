//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.tracks;

import java.io.File;
import java.io.IOException;

import net.zikes.OSMMapsApplication;
import net.zikes.SettingsActivity;
import net.remekzajac.utils.Filter;
import net.remekzajac.tracks.TrackGeoPointRegister.TrackPoint;
import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.util.Log;

public class AutomaticRecorder implements LocationTracker.Recorder {

	private static final String     KDefaultDescription = "Bicycle activity (auto) recorded with Zikes Android App";
	private static final int        KMinDistanceBetweenSamplesMts = 350;
	private final ManualRecorder    mManualRecorder;
	private final int 				mMinDinstanceMts;
	private final int 				mMinCrowFlightDinstanceMtsForWindow;
	private final int				mGracePeriodSec;
	private State 					mState;
	private final TrackGeoPointRegister	mTrackPointRegister;
	private boolean					mPulseOn;
	private long					mGracePeriodBegin;
	private final float 			mMinAvgSpeedKph;
	private final float 			mMaxMaxSpeedKph;
	private final int   			mWindowSizeSec;
	private final int               mMinSamplesInWindow;
	private boolean                 mWaitUntilItStops;
	private TrackPoint.Stats		mStatsForWindow;
	private final Listener      	mListener;

	public AutomaticRecorder(Context context, Listener listener) {
		mTrackPointRegister = new TrackGeoPointRegister("AR");
		mWindowSizeSec 		= OSMMapsApplication.instance().preferences().pref_Rec_sampleChunkSizeSec.get();
		mMinAvgSpeedKph 	= OSMMapsApplication.instance().preferences().pref_Rec_minAvgSpeed.get();
		mMaxMaxSpeedKph 	= OSMMapsApplication.instance().preferences().pref_Rec_maxMaxSpeed.get();
		mMinDinstanceMts 	= OSMMapsApplication.instance().preferences().pref_Rec_minDistMts.get();
		mMinCrowFlightDinstanceMtsForWindow = (int)((mMinAvgSpeedKph*1000/(60*60))*mWindowSizeSec*0.7);
		mMinSamplesInWindow = (int)((mWindowSizeSec * mMaxMaxSpeedKph*1000/(60*60))/KMinDistanceBetweenSamplesMts);
		mGracePeriodSec 	= OSMMapsApplication.instance().preferences().pref_Rec_graceSamples.get()*mWindowSizeSec;
		mState 				= State.EIdle;
		mListener 			= listener;
		mManualRecorder 	= new ManualRecorder(context, new Listener() {
			@Override
			public void onStateChange(File recordedTrack) {
				mListener.onStateChange(recordedTrack);
			}
		});
	}

	public State state()
	{
		return mState;
	}

	public void newLocation(Location location) {
		mTrackPointRegister.append(location);
		if (!mPulseOn) {
			pulse();
		}
		//Log.d("AutomaticRecorder", mTrackPointRegister.stats().toString());
	}

	public TrackGeoPointRegister geoRegister()
	{
		return mTrackPointRegister;
	}

	private void pulse() {
       	mPulseOn = true;
		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
            public void run() {
            	processSample();
            	if (mTrackPointRegister.noOfElements() > 1) {
            		pulse();
            	} else {
					mWaitUntilItStops = mPulseOn = false;
            	}
            }}, mWindowSizeSec*500);
	}

	private void processSample()
	{
		long currentTime = System.currentTimeMillis();
		final long traceTo = currentTime - (mWindowSizeSec*1000);
		mStatsForWindow = mTrackPointRegister
			.tail()
			.stats("ARCanary")
			.traceBackAsLong(new Filter<TrackGeoPointRegister.TrackPoint>() {
            @Override
            public boolean matches(TrackGeoPointRegister.TrackPoint trackPoint) {
                return trackPoint.location().getTime() > traceTo;
            }
        });

		Log.d("AutomaticRecorder", mStatsForWindow.toString());
		mStatsForWindow.addTimeStampFromLocalClock();
		boolean tooFast = (mStatsForWindow.maxSpeedKph() > mMaxMaxSpeedKph) &&
						  (mStatsForWindow.avgSpeedKph() > mMaxMaxSpeedKph*0.60) || mWaitUntilItStops;
		boolean tooSlow = mStatsForWindow.avgSpeedKph()  < mMinAvgSpeedKph;
		boolean windowSaturated = mStatsForWindow.spanMts() > mMinCrowFlightDinstanceMtsForWindow &&
							  2 * mStatsForWindow.spanMts() > mStatsForWindow.distanceMts() &&
								  mStatsForWindow.samples() >= mMinSamplesInWindow;
		if (windowSaturated && !tooSlow && !tooFast) {
			//the stats for the window meet the movement parameters
			if (mState == State.EIdle) {
				setState(State.EThinking);
			} else if (mState == State.EThinking) {
				if (mTrackPointRegister.stats().distanceMts() > mMinDinstanceMts) {
					setState(State.ERecording);
				}
			}
			mGracePeriodBegin = currentTime;
		} else if (mState != State.EIdle && !tooFast) {
			long dtime = (currentTime-mGracePeriodBegin)/1000;
			if  ( dtime > mGracePeriodSec) {
				mTrackPointRegister.cutOffBehind(mStatsForWindow.since());
				stop();
				setState(State.EIdle);
			}
		} else {
			setState(State.EIdle);
			mTrackPointRegister.cutOffBefore(mStatsForWindow.since());
			if (tooFast) {
				mWaitUntilItStops = true;
			}
			if (tooSlow) {
				mWaitUntilItStops = false;
			}
		}
	}

	public String diagnosticString() {
		String result = "AutoRecorder state:"+mState
				+" Canary:["
				+(mStatsForWindow != null ? mStatsForWindow.toString():"null")
				+"] So far:["+mTrackPointRegister.stats().toString()+"]";
		return result;
	}

	public void stop() {
		if (mState == State.ERecording) {
			Log.d("AutomaticRecorder", "Saving track");
			try {
				mManualRecorder.start();
				flushRegister();
				mManualRecorder.stop(true,KDefaultDescription);
				Log.d("AutomaticRecorder", "Saved track");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		mTrackPointRegister.clear();
	}

	private void flushRegister() {
		TrackGeoPointRegister.TrackPoint trackPoint = null;
		while ((trackPoint = mTrackPointRegister.removeHead()) != null) {
			mManualRecorder.newLocation(trackPoint.location());
		}
		mTrackPointRegister.clear();
	}

	private void setState(State newState) {
		if (mState != newState) {
			mState = newState;
			if (mListener != null) {
				mListener.onStateChange(null);
			}
		}
	}
}
