package net.remekzajac.tracks;

import net.zikes.R;
import net.zikes.backend.json.JourneyReader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ZikesJourneyDocument {
    public static class FileRenderer implements com.ipaulpro.afilechooser.FileRenderer {

        @Override
        public Tuple render(File file) {
            if (file.getName().matches(JourneyReader.KJourneyFilter)) {
                try(
                    FileReader freader = new FileReader(file)
                ) {
                    JourneyReader jreader = new JourneyReader(freader);
                    JourneyReader.Meta meta = jreader.meta();
                    return new Tuple(file)
                        .setText(meta.name)
                        .setIcon(R.drawable.ic_trackfile)
                        .setMime(JourneyReader.KMimeType);
                } catch (JourneyReader.Exception e) {
                    return new Tuple(file)
                    .setText(file.getName() + " (unparsable)");
                } catch (IOException e) {}
            }
            return null;
        }
    }
}
