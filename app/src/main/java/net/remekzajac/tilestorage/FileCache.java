//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
// 
//********************************************************************************

package net.remekzajac.tilestorage;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.jakewharton.DiskLruCache;
import android.content.Context;
import android.os.Environment;


public class FileCache {
	
    private static final int APP_VERSION = 1;
    private static final int VALUE_COUNT = 1;
    private static final int KKBytesInMB = 1024;
    private static final int KBytesInMB = 1024*KKBytesInMB;
    private static final int IO_BUFFER_SIZE = 1024;
    
    protected DiskLruCache mDiskCache;
    
	
	public void open(File uniqueFolder, int sizeMB) throws IOException
	{
		mDiskCache = DiskLruCache.open( uniqueFolder, APP_VERSION, VALUE_COUNT, sizeMB*KBytesInMB );		
	}
	
	public void close()
	{
		try 
		{
			mDiskCache.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	public InputStream getFile(String key)
	{
		DiskLruCache.Snapshot snapshot = null;
		try {
			snapshot = mDiskCache.get( key );
			if (snapshot != null)
			{
				return GetFileFromCache(snapshot);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public int getFile(String key, OutputStream out)
	{
        int count = 0;
        InputStream input = getFile(key);
        byte data[] = new byte[IO_BUFFER_SIZE];
        try
		{
			while ((count = input.read(data)) != -1) 
			{
				out.write(data, 0, count);
			}
		} catch (IOException e)
		{
			count = 0;
		}	
        finally
        {
            try
			{
				input.close();
			} 
            catch (IOException e)
			{
				e.printStackTrace();
			}
        }
        return count;
	}
	
	public void putFile(String key, File file) throws IOException
	{		
		DiskLruCache.Editor editor = mDiskCache.edit(key);
        if ( editor == null ) 
        {   //another edit of that key is in progress
        	throw new IOException();
        }
        OutputStream cachedOut = new BufferedOutputStream( editor.newOutputStream( 0 ), IO_BUFFER_SIZE );
        int count = 0;
        byte data[] = new byte[IO_BUFFER_SIZE];
        BufferedInputStream input = new BufferedInputStream(new FileInputStream(file));
        while ((count = input.read(data)) != -1) {
        	cachedOut.write(data, 0, count);
        	}
        input.close();
        cachedOut.close();
		editor.commit();
		mDiskCache.flush();
	}
	
	protected InputStream GetFileFromCache(DiskLruCache.Snapshot snapshot) throws IOException
	{
        final InputStream input = snapshot.getInputStream( 0 );
        final BufferedInputStream buffIn = new BufferedInputStream( input, IO_BUFFER_SIZE );
        return buffIn;
	}

    public static String getDiskCacheDir(Context context) 
    {
    	String cachePath = null;
    	// Check if media is mounted and storage is built-in, if so, try and use external cache dir
    	// otherwise use internal cache dir
    	String externalStorageState = Environment.getExternalStorageState();
    	if (externalStorageState.equals(Environment.MEDIA_MOUNTED) &&
			!Environment.isExternalStorageRemovable() &&
			context.getExternalCacheDir() != null)
    	{
    		cachePath = context.getExternalCacheDir().getPath();
    	}
    	else
    	{
    		cachePath = context.getCacheDir().getPath();
    	}           

        return cachePath;
    }
}
