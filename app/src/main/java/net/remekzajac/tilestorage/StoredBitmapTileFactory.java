//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
// 
//********************************************************************************
package net.remekzajac.tilestorage;

import java.io.IOException;
import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class StoredBitmapTileFactory
{
	public abstract class Tile implements IITileStorage.ProcessedTile
	{	
		private IITileStorage.ProcessedTile.State	mState;
		private IITileStorage.ProcessedTile.Status	mStatus;
		private Bitmap 								mBitmap;
		private IITileStorage.GetFileTask 			mGetFileTask;

		public Tile()
		{
			mState = IITileStorage.ProcessedTile.State.EUnitialised;
			mStatus = IITileStorage.ProcessedTile.Status.EFailed;
			mBitmap = mInfoBitmaps.waiting();
		}
		
		public boolean request()
		{
			if (mGetFileTask == null && mState != IITileStorage.ProcessedTile.State.EPrepared)
			{		
				(mGetFileTask = mTileStorage.getFile(this)).execute();
				mState = IITileStorage.ProcessedTile.State.EPreparing;
				return false;
			}
			return true;
		}
		
		synchronized public void backgroundProcess(InputStream inStream) throws IOException 
		{		
			Bitmap bitmap = BitmapFactory.decodeStream(inStream);
			inStream.close();
			if (bitmap != null)
			{
				mBitmap = bitmap;
			}
			else
			{
				throw new IOException("Bitmap failed to load"+subpath());
			}
		}

		public void foregroundFinished(IITileStorage.ProcessedTile.Status status) 
		{
			mStatus = status;
			if (status == IITileStorage.ProcessedTile.Status.EFailed ||
				status == IITileStorage.ProcessedTile.Status.ECancelled)
			{
				mBitmap = mInfoBitmaps.failed();
				mState = IITileStorage.ProcessedTile.State.EFailed;
			}
			else
			{
				mState = IITileStorage.ProcessedTile.State.EPrepared;
			}
			mGetFileTask = null;
		}


		synchronized public boolean draw(Canvas canvas, Rect rect, boolean showInfo ) 
		{
			assert mBitmap != null;
			boolean result = true;
			Paint paint = new Paint();
			if (mState != IITileStorage.ProcessedTile.State.EPrepared )
			{
				result = false;
				if (showInfo)
				{
					paint.setAlpha(mInfoBitmaps.opacity());
					canvas.drawBitmap(mBitmap, null, rect, paint);
				}
			}
			else
			{
				canvas.drawBitmap(mBitmap, null, rect, paint);
				if (mStatus == IITileStorage.ProcessedTile.Status.EDownloaded && showInfo)
				{
					paint.setAlpha(mInfoBitmaps.opacity());
					canvas.drawBitmap(mInfoBitmaps.downloaded(), null, rect, paint);					
				}
			}

			return result;
		}

		public void close() 
		{
			if (mGetFileTask != null)
			{
				mGetFileTask.cancel();
				mGetFileTask = null;				
			}
		}
		
    	public State state()
    	{
    		return mState;
    	}
    	
    	public Status status()
    	{
    		return mStatus;
    	}
	}
	
	public interface InfoBitmaps
	{
		public Bitmap 	failed();
		public Bitmap 	downloaded();
		public Bitmap 	waiting();
		public int 		opacity();
	}
	
	private final IITileStorage mTileStorage;
	private final InfoBitmaps   mInfoBitmaps;
	public StoredBitmapTileFactory(InfoBitmaps infoBitmaps, IITileStorage tileStorage)
	{
		mTileStorage = tileStorage;
		mInfoBitmaps = infoBitmaps;
	}
}
