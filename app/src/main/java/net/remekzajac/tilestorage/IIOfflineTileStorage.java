//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
// 
//********************************************************************************

package net.remekzajac.tilestorage;

import net.remekzajac.maps.GeoDegPoint;
import net.remekzajac.tracks.TrackDocuments;
import net.remekzajac.tracks.TracksView;

/*
 * An abstraction of a tile repository adding the write function to the read-only IITileStorage
 */
public interface IIOfflineTileStorage extends IITileStorage
{
	public interface Tile extends IITileStorage.Tile
	{
    	public void delete();
    	public boolean exists();
	}
	
	public interface Observer
	{
		public void tileChange(String subpath, boolean exists);
	}
	
	public interface OfflineTilesDownloadTask
	{
		public interface DownloadObserver
		{
			public void progress(long progress, long ofMax );
			public void error();
		}
		
		public interface SizeEstimateObserver
		{
			public void sizeEstimate(long newSizeKb, long alreadyOnDiskKB, boolean finished);			
		}
		
		public void sizeEstimate(SizeEstimateObserver observer);
		public void execute(DownloadObserver observer);
		public void cancel();
		public void setRangeMts(int rangeMts);
	}
	public OfflineTilesDownloadTask newOfflineTilesDownloadTask(GeoDegPoint.Bounds bounds, int fromZoom);
	public OfflineTilesDownloadTask newOfflineTilesDownloadTask(TracksView.TrackDocument[] trackDocuments, int fromZoom);
	public OfflineTilesDownloadTask runningOfflineTilesDownloadTask();
	public void delete(IITileStorage.Tile tile);
	public void subscribe(Observer observer);
	public long sizeOnDiscKB();
}
