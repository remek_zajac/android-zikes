//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.tilestorage;


import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import net.remekzajac.utils.MustacheReplace;
import net.zikes.OSMMapsApplication;
import net.zikes.SettingsActivity;
import net.remekzajac.network.FileDownloadTask;
import android.annotation.TargetApi;
import android.os.AsyncTask;

import com.jakewharton.DiskLruCache;


@TargetApi(9)
public class TileFileCache extends FileCache implements IITileStorage
{
    interface Config extends IITileStorage.Config
    {
    	URL url();
    	int  sizeMB();
    }

    private String        mBaseUrl;
    private String[]      mMirrors;
    private AtomicInteger mRoundRobinMirror;

	public void open() throws IOException
	{
        mRoundRobinMirror = new AtomicInteger();
        Set<String> mirrors = OSMMapsApplication.instance().preferences().pref_Osm_TileServerMirrors.get();
		mMirrors = mirrors.toArray(new String[mirrors.size()]);
        String style = OSMMapsApplication.instance().preferences().pref_Osm_TileServerStyle.get();
        mBaseUrl = new MustacheReplace(
            OSMMapsApplication.instance().preferences().pref_osm_TileServerUrl.get()
        ).expand("style", style)
         .expand("apikey", OSMMapsApplication.instance().preferences().pref_OsmTileServerAPIKey.get())
         .finish();

		super.open(
            OSMMapsApplication.instance().preferences().pref_tileCache_Location.get(true),
            OSMMapsApplication.instance().preferences().pref_tileCache_SizeMB.get()
        );
	}

	public GetFileTask getFile(IITileStorage.ProcessedTile tileRequest)
	{
		return new FileCacheTask(tileRequest);
	}

    public IITileStorage.ProcessedTile.Status getFile(
        IITileStorage.Tile tileRequest,
        OutputStream out, //if not provided, the result will be written to cache
        boolean downloadIfNecessary) {

        IITileStorage.ProcessedTile.Status result = ProcessedTile.Status.EFetchedLocally;

        String key = subpathToKey(tileRequest.subpath());
        try(DiskLruCache.Snapshot snapshot = mDiskCache.get(key)) {
            if (snapshot == null && downloadIfNecessary) {
                result = ProcessedTile.Status.EDownloaded;
                if (out == null) {
                    downloadFileToCache(tileRequest);
                } else {
                    downloadFile(tileRequest, out);
                }
            } else if (out != null) {
                getFile(key, out);
			}
        } catch (IOException e) {
            result = ProcessedTile.Status.EFailed;
        }
        return result;
    }

	public boolean exists(Tile tile)
	{
		try
		{
			return mDiskCache.get( subpathToKey(tile.subpath())) != null;
		}
		catch (IOException e)
		{
			//This generally means that the cache is inaccessible, corrupt or other really fatal thing.
			e.printStackTrace();
		}
		return false;
	}

	static private String subpathToKey(String subpath)
	{
		//The daft LRUCache appears to be using keys as filenames.
		//and so keys cannot be paths (with slashes or backslashes).
		//will replace here all slashes to something less troubling
		//the filesystem.
		String key = subpath.replace('\\', '.');
		key = key.replace('/', '.');
		return key;
	}

	private void downloadFileToCache(Tile tile) throws IOException
	{
		int bytes = 0;
		DiskLruCache.Editor editor = mDiskCache.edit( subpathToKey(tile.subpath()) );
        if ( editor == null )
        {   //another edit of that key is in progress
        	throw new IOException();
        }
		try
		{
			OutputStream cachedOut = new BufferedOutputStream( editor.newOutputStream( 0 ), IO_BUFFER_SIZE );
			bytes = downloadFile(tile, cachedOut);
		}
		catch (IOException e)
		{
			bytes = 0;
		}

		if (bytes == 0)
		{
			editor.abort();
			throw new IOException();
		}
		editor.commit();
		mDiskCache.flush();
	}

	//Foreground download method
	private int downloadFile(Tile tile, OutputStream out) throws IOException
	{
        String mirror = mMirrors[mRoundRobinMirror.addAndGet(1) % mMirrors.length];
		URL fileUrl;
		try
		{
			fileUrl = new URL(
                new MustacheReplace(mBaseUrl)
                .expand("subpath", tile.subpath())
                .expand("mirror", mirror)
                .finish()
            );
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
			throw new IOException();
		}
		return new FileDownloadTask(out).download(fileUrl);
	}

    public class FileCacheTask  extends AsyncTask<Void, Void, IITileStorage.ProcessedTile.Status> implements GetFileTask
    {
    	private IITileStorage.ProcessedTile mTileRequest;
    	private FileCacheTask(IITileStorage.ProcessedTile fileCacheProcessor)
    	{
    		mTileRequest = fileCacheProcessor;
    	}

		public void cancel()  { super.cancel(false); }
        public void execute()
        {
            super.execute();
        }

    	@Override
    	protected IITileStorage.ProcessedTile.Status doInBackground(Void... params) {
            if (isCancelled())
            {
                return IITileStorage.ProcessedTile.Status.ECancelled;
            }
            IITileStorage.ProcessedTile.Status result = getFile(mTileRequest, null, true);
            if (result != IITileStorage.ProcessedTile.Status.EFailed)
            {
                //file now should be cached
                try
                {
                    mTileRequest.backgroundProcess(getFile(subpathToKey(mTileRequest.subpath())));
                }
                catch (IOException e)
                {
                    result = IITileStorage.ProcessedTile.Status.EFailed;
                }
            }
            return result;
    	}

    	@Override
        protected void onPostExecute(IITileStorage.ProcessedTile.Status result)
    	{
    		mTileRequest.foregroundFinished(result);
    		mTileRequest = null;
        }

    	@Override
    	protected void onCancelled (IITileStorage.ProcessedTile.Status result)
    	{
    		onPostExecute(IITileStorage.ProcessedTile.Status.ECancelled);
    	}
    }
}
