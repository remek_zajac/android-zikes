//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.remekzajac.tilestorage;

import net.zikes.OSMMapsApplication;
import net.zikes.R;
import net.remekzajac.maps.IITileFactory;
import net.remekzajac.maps.MapView;
import net.remekzajac.maps.ReferenceMap;
import net.remekzajac.maps.TileWindow;
import net.remekzajac.utils.IntegerPoint;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.widget.Toast;

public class OfflineTileView extends MapView.SubView implements TileWindow.Callback
{
	private static int			KWarnUserOfAlgorihmicalComplexityWhenTileComesAfterMs = 1000;
	private TileWindow 			mTileWindow;
	private ReferenceMap 		mReferenceMap;
	private Long				mTileRetrievalStartedMs;
	public OfflineTileView(MapView parent)
	{
		super(parent);
		mReferenceMap = OSMMapsApplication.instance().referenceMap();
	}

	@Override
	public void onCreate() {}

	@Override
	public void onDestroy() {
		if (mTileWindow != null) {
			mTileWindow.close();
		}
    	hide();
	}

	public void show()
	{
		if (mReferenceMap.isShown() && mTileWindow == null) {
			mTileWindow = new TileWindow(
					mReferenceMap.getTraits().mGeoCentre,
					mReferenceMap.getTraits().mZoom,
					mReferenceMap.getTraits().mScale,
					OSMMapsApplication.instance().offlineViewTileFactory(),
					mReferenceMap.getTraits().mMapPixBounds.mapRect(),
					this);
		}
		setVisible(true);
	}

	public void hide()
	{
		setVisible(false);
		if (mTileWindow != null)
		{
			mTileWindow.close();
			mTileWindow = null;
		}
	}


	@Override
	public void doInvalidate(int bWhatsChangedVector)
	{
		if (!this.isShown())
		{
			return;
		}

		if (mReferenceMap.getTraits().mZoom != mTileWindow.zoom())
		{
			mTileWindow.close();
			mTileWindow = new TileWindow(
					mReferenceMap.getTraits().mGeoCentre,
					mReferenceMap.getTraits().mZoom,
					mReferenceMap.getTraits().mScale,
					OSMMapsApplication.instance().offlineViewTileFactory(),
					mReferenceMap.getTraits().mMapPixBounds.mapRect(),
					this);
		}
		if ((bWhatsChangedVector & ReferenceMap.MapViews.BZoom) != 0)
		{
			mTileRetrievalStartedMs = Long.valueOf(System.currentTimeMillis());
		}
		mTileWindow.set(mReferenceMap.getTraits().mGeoCentre, mReferenceMap.getTraits().mScale, true);
	}

	@Override
	public void draw(Canvas canvas)
	{
		mTileWindow.draw(canvas, true);
	}

	public boolean tilesToDeleteAt(IntegerPoint pixxy)
	{
		if (this.isShown() && mTileWindow != null)
		{
			IITileFactory.Tile tile = mTileWindow.get(pixxy);
			if (tile != null)
			{
				IIOfflineTileStorage.Tile storedTile = IIOfflineTileStorage.Tile.class.cast(tile);
				return storedTile.exists();
			}
		}
		return false;
	}

	public void deleteTilesAt(IntegerPoint pixxy)
	{
		if (this.isShown() && mTileWindow != null)
		{
			IITileFactory.Tile tile = mTileWindow.get(pixxy);
			if (tile != null)
			{
				IIOfflineTileStorage.Tile storedTile = IIOfflineTileStorage.Tile.class.cast(tile);
				storedTile.delete();
			}
		}
	}

	public void callback(IITileFactory.Tile tile, Rect rect)
	{
		this.invalidate(rect);
		if (mTileRetrievalStartedMs != null && mReferenceMap.getTraits().mZoom == tile.zoom())
		{
			long durationMs = System.currentTimeMillis() - mTileRetrievalStartedMs;
			if (durationMs > KWarnUserOfAlgorihmicalComplexityWhenTileComesAfterMs)
			{
				Toast.makeText(getContext(), getContext().getString(R.string.warn_offlineTilesSlow), Toast.LENGTH_LONG).show();
			}
			mTileRetrievalStartedMs = null;
		}
	}
}
