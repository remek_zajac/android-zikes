//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.tilestorage;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import net.remekzajac.osm.OSMTileFactory;
import net.remekzajac.tracks.TracksView;
import net.remekzajac.utils.IntegerPoint;
import net.zikes.OSMMapsApplication;
import net.zikes.SettingsActivity;
import net.remekzajac.maps.GeoDegPoint;
import net.remekzajac.osm.OSMOfflineTilesDownloadTaskForRegion;
import net.remekzajac.osm.OSMOfflineTilesDownloadTaskForTrack;
import net.remekzajac.tracks.TrackDocuments;
import net.remekzajac.utils.FileOp;
import android.os.AsyncTask;
import android.util.Log;

public class OfflineTileStorage implements IIOfflineTileStorage
{
	private final static int 			KTileFileStaleAfterDays = 30;
	private final File 					mFolder;
	private final TileFileCache 		mFileCache;
	private Observer					mObserver;
	private OfflineTilesDownloadTask 	mRunningOfflineTileDownloadTask;
	public OfflineTileStorage(TileFileCache fileCache )
	{
		mFileCache = fileCache;
		mFolder = OSMMapsApplication.instance().preferences().pref_offlineTileCache_Location.get(true);
		mFolder.mkdirs();
	}

	public class Tile implements IITileStorage.Tile
	{
		private final OSMTileFactory.Tile mOSMTile;
		private Tile(IntegerPoint tileXY, int zoom) {
			mOSMTile = new OSMTileFactory.Tile(tileXY, zoom);
		}

		private Tile(OSMTileFactory.Tile osmTile) {
			mOSMTile = osmTile;
		}


        @Override
        public String subpath() {
            return mOSMTile.subpath();
        }

		public IITileStorage.ProcessedTile.Status download() throws IOException {
			if (exists())
			{
                int fileAgeDays = (int)(System.currentTimeMillis() - file().lastModified())/(1000*60*60*24);
                if (fileAgeDays < KTileFileStaleAfterDays)
                {
                    return IITileStorage.ProcessedTile.Status.EFetchedLocally;
                }
			}
			File file = file();
            file.getParentFile().mkdirs();
            IITileStorage.ProcessedTile.Status result = ProcessedTile.Status.EFailed;
            try(OutputStream out = new BufferedOutputStream(new FileOutputStream(file))) {
                result = mFileCache.getFile(this, out, true);
            };
            return result;
		}

		@Override
		public boolean equals(Object other) {
			if((other == null) || (getClass() != other.getClass())){
				return false;
			}
			Tile otherTile = (Tile)other;
			return otherTile.mOSMTile.equals(mOSMTile);
		}

		@Override
		public int hashCode() {
			return mOSMTile.tileXY().hashCode()+zoom();
		}

		public boolean exists() {
            return file().exists();
        }
        public IntegerPoint tileXY() {
            return mOSMTile.tileXY();
        }
		public int gridDistance(Tile from) { return mOSMTile.gridDistance(from.mOSMTile); }
        public int zoom() {
            return mOSMTile.zoom();
        }
		public Tile up() { return new Tile(mOSMTile.up()); }
		public Tile down() { return new Tile(mOSMTile.down()); }
		public Tile left() { return new Tile(mOSMTile.left()); }
		public Tile right() { return new Tile(mOSMTile.right()); }

        private File file() { return new File(mFolder, subpath()); }
	}

	public GetFileTask getFile(IITileStorage.ProcessedTile tile)
	{
		File file = _exists(tile);
		if (file != null)
		{
			OfflineTileTask task = new OfflineTileTask(file, tile);
			return task;
		}
		return null;
	}

    public Tile futureTile(IntegerPoint coords, int zoom) {
        return new Tile(coords, zoom);
    }

	public boolean exists(IITileStorage.Tile tile)
	{
		return _exists(tile)!=null;
	}

    private File _exists(IITileStorage.Tile tile) { //TODO, remove me, use .Tile
        File file = new File(mFolder, tile.subpath());
        return file.exists() ? file : null;
    }


	public void delete(IITileStorage.Tile tile)
	{
		File file = _exists(tile);
		if (file != null)
		{
			FileOp.deleteAndClearUp(file);
			if (mObserver != null)
			{
				mObserver.tileChange(tile.subpath(), false);
			}
			Log.d("OfflineStorage", "deleted tile:"+tile.subpath());
		}
	}


	public OfflineTilesDownloadTask newOfflineTilesDownloadTask(GeoDegPoint.Bounds bounds, int fromZoom)
	{
		return new OSMOfflineTilesDownloadTaskForRegion(this, bounds.nw(), bounds.se(), fromZoom);
	}

	public OfflineTilesDownloadTask newOfflineTilesDownloadTask(TracksView.TrackDocument[] trackDocuments, int fromZoom)
	{
		return new OSMOfflineTilesDownloadTaskForTrack(this, trackDocuments, fromZoom);
	}

	public OfflineTilesDownloadTask runningOfflineTilesDownloadTask()
	{
		return mRunningOfflineTileDownloadTask;
	}

	public void offlineTaskStatusChange(OfflineTilesDownloadTask task)
	{
		mRunningOfflineTileDownloadTask = task;
	}

    private class OfflineTileTask extends AsyncTask<Void, Void, IITileStorage.ProcessedTile.Status> implements GetFileTask
    {
    	private IITileStorage.ProcessedTile mTile;
    	private final File		   			mFile;
    	public OfflineTileTask(File file, IITileStorage.ProcessedTile tile)
    	{
    		mFile = file;
    		mTile = tile;
    	}

		public void execute()
		{
			super.execute();
		}

		public void cancel()
		{
			super.cancel(false);
		}

		@Override
		protected IITileStorage.ProcessedTile.Status doInBackground(Void... arg0)
		{
			if (mFile.exists())
			{
				BufferedInputStream input;
				try
				{
					input = new BufferedInputStream(new FileInputStream(mFile));
					mTile.backgroundProcess(input);
				}
				catch (FileNotFoundException e1)
				{
					return IITileStorage.ProcessedTile.Status.EFailed;
				}
				catch (IOException e)
				{
					return IITileStorage.ProcessedTile.Status.EFailed;
				}
				return IITileStorage.ProcessedTile.Status.EFetchedLocally;
			}
			return IITileStorage.ProcessedTile.Status.EFailed;
		}

    	@Override
        protected void onPostExecute(IITileStorage.ProcessedTile.Status result)
    	{
    		mTile.foregroundFinished(result);
    		mTile = null;
        }
    }

	public void subscribe(Observer observer)
	{
		assert(mObserver == null);
		mObserver = observer;
	}

	public long sizeOnDiscKB()
	{
		return FileOp.folderSizeBytes(mFolder)/1024;
	}
}
