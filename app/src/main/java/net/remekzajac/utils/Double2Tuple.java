//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.utils;

public class Double2Tuple
{
	protected double mX;
	protected double mY;

	public Double2Tuple(double x, double y) {
		mX = x;
		mY = y;
	}

	public Double2Tuple(Double2Tuple from) {
		mX = from.mX;
		mY = from.mY;
	}

	public double x()
	{
		return mX;
	}

	public double y()
	{
		return mY;
	}

	@Override
	public boolean equals(Object other) {
		if((other == null) || (getClass() != other.getClass())){
			return false;
		}
		Double2Tuple otherInteger2Tuple = (Double2Tuple)other;
		return mX == otherInteger2Tuple.x() && mY == otherInteger2Tuple.y();
	}

	public double bigger()
	{
		return Math.max(mX, mY);
	}

	public double smaller()
	{
		return Math.min(mX, mY);
	}

	public String toString()
	{
		return new String(""+ mX +","+ mY);
	}

	public String toDebugString()
	{
		return new String("[x:"+ mX +",y:"+ mY +"]");
	}
}
