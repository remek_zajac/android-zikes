//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
// 
//********************************************************************************
package net.remekzajac.utils;

import android.util.Log;

public class ZoomTree2DBase
{
	private static class Node
	{
		public Node 				mParent;
		public Node[]				mChildren;
		private final IntegerRange 	mBaseX;
		private final IntegerRange 	mBaseY;
		private Value				mValue;
		protected final int			mX;
		protected final int			mY;
		protected final int			mZoom;
		private Node(Value value, int x, int y, int zoom)
		{
			mX = x;
			mY = y;
			mZoom = zoom;
			mChildren = new Node[4];
			int factor = (int)Math.pow(2, zoom);
			mBaseX = new IntegerRange(x * factor, ((x+1) * factor)-1);
			mBaseY = new IntegerRange(y * factor, ((y+1) * factor)-1);
			mValue = value;
		}
		
		//TODO, this does not put child and parent in relationship as
		//the caller simply may want to create a Node template in order
		//to see if such a child already exists.
		private Node(Value value, Node parent, Value.ChildIndex placement)
		{
			this(value, parent.mX*2 + (placement.ordinal() & 0x1), 
						parent.mY*2 + ((placement.ordinal() & 0x2) >> 1), 
						parent.mZoom-1);
		}
		
		private static void setParentChild(Node parent, Node child, Value.ChildIndex placement)
		{	
			assert (child.mParent == null || child.mParent == parent);	
			assert (parent.mChildren[placement.ordinal()] == null || parent.mChildren[placement.ordinal()] == child);			
			child.mParent = parent;	
			parent.mChildren[placement.ordinal()] = child;
		}
		
		private static void setParentChild(Node parent, Node child)
		{
			setParentChild(parent, child, parent.childIndexForRange(child.mBaseX, child.mBaseY));
		}
		
		private Value value()
		{
			return mValue;
		}
		
		private Value.ChildIndex childIndexForRange(IntegerRange x, IntegerRange y)
		{
			assert(mBaseX.contains(x) && mBaseY.contains(y));
			if (mBaseX.upperHalf().contains(x))
			{//right
				return mBaseY.upperHalf().contains(y) ? Value.ChildIndex.EBottomRight : Value.ChildIndex.ETopRight;
			}
			else
			{//left
				return mBaseY.upperHalf().contains(y) ? Value.ChildIndex.EBottomLeft : Value.ChildIndex.ETopLeft;				
			}
		}
		
		private Node get(Node node, boolean put)
		{
			assert(mValue == null || mValue.mNode == this);
			if (mBaseX.contains(node.mBaseX) && mBaseY.contains(node.mBaseY))
			{
				if (node.equals(this))
				{
					if (mValue == null && put)
					{
						mValue = node.mValue;
						mValue.mNode = this;
					}
					return this;
				}
				
				Value.ChildIndex childIndex = childIndexForRange(node.mBaseX, node.mBaseY);
				if (mChildren[childIndex.ordinal()] == null && put)
				{
					setParentChild(this, new Node(null, this, childIndex));
				}
				return mChildren[childIndex.ordinal()] != null ? mChildren[childIndex.ordinal()].get(node, put): null;
			}
			
			if (mParent == null)
			{
				setParentChild(new Node(null, mX/2, mY/2, mZoom+1), this );
			}
			return mParent.get(node, put);
		}
		
		private Node get(int x, int y, int zoom)
		{
			return get(new Node(null, x, y, zoom), false);
		}
		
		public boolean equals(Node node)
		{
			return mX == node.mX && mY == node.mY && mZoom == node.mZoom;
		}
		
		private void deleteChild(Node child)
		{
			boolean hasChildren = false;
			for (int i = 0; i < 4; i++)
			{
				if (mChildren[i] == child)
				{
					mChildren[i] = null;
				}
				else if (mChildren[i] != null)
				{
					hasChildren = true;
				}
			}
			if (!hasChildren && mParent != null && mValue == null)
			{
				mParent.deleteChild(this);
			}
		}
		
		private Node root()
		{
			Node result = this;
			while (result.mParent != null)
			{
				result = result.mParent;
			}
			
			int childCount = 1;			
			while(childCount == 1)
			{
				int index = 0;
				for (int i = 0; i < 4; i++)
				{
					if (result.mChildren[i] != null)
					{
						childCount++;
						index = i;
					}
				}
				if (childCount == 1 && result.mValue != null)
				{
					result = result.mChildren[index];
				}
			}
			return result;
		}
		
		public int diagnose(int indentation)
		{
			int count = 1;
			String indent = new String();
			for (int i = 0; i < indentation; i++)
			{
				indent += ' ';
			}
			Log.d("ZoomTree2D", indent+(mValue != null ? mValue.diagnoseString():diagnoseString()));
			for (int i = 0; i < 4; i++)
			{
				if (mChildren[i] != null)
				{
					count += mChildren[i].diagnose(indentation+1);
				}
			}
			return count;
		}
		
		public String diagnoseString()
		{
			return "Node, zoom:"+mZoom+" x:"+mX+mBaseX.toString()+" y:"+mY+mBaseY.toString();
		}
	}
	
	public static class Value
	{
		public enum ChildIndex
		{
			ETopLeft,
			ETopRight,
			EBottomLeft,
			EBottomRight,
		}
		
		private Node mNode;
		protected Value(int x, int y, int zoom)
		{
			mNode = new Node(this, x, y, zoom);
		}
		
		protected Value(Value parent, ChildIndex placement)
		{
			mNode = new Node(this, parent.mNode, placement);
		}

		public final int x()
		{
			return mNode.mX;
		}
		
		public final int y()
		{
			return mNode.mY;
		}
		
		//give it a funny name, as the Value class has an exotic approach to zoom. 
		//Namely, its zoom integer value is greater for coarser zooms. This is the chosen
		//approach as otherwise Value would need to be parameterised with the value of
		//the finest zoom. 
		protected final int _zoom()
		{
			return mNode.mZoom;
		}
		
		protected Value parent()
		{
			return mNode.mParent != null ? mNode.mParent.value() : null;
		}
		
		protected Value child(ChildIndex placement)
		{
			return mNode.mChildren[placement.ordinal()] != null ? mNode.mChildren[placement.ordinal()].value() : null;
		}
		
		protected Value get(int x, int y, int zoom)
		{
			Node result = mNode.get(x, y, zoom);
			return result != null ? result.value() : null;
		}
		
		protected Value doGetOrPut(Value value)
		{
			assert(!value.hasChildren() && value.parent() == null); //we don't support merging trees. Only loose elements.
			Node result = mNode.get(value.mNode, true);
			assert(result != null);
			return result.mValue;
		}
		
		public int noOfChildren()
		{
			int result = 0;
			for (ChildrenIterator it = new ChildrenIterator(); it.hasNext(); it.next())
			{
				result++;
			}
			return result;
		}
		
		public boolean hasChildren()
		{
			return noOfChildren() > 0;
		}
		
		public class ChildrenIterator implements java.util.Iterator<Value>
		{
			private int mIndex;
			public ChildrenIterator()
			{
				ff();
			}
			
			private void ff()
			{
				while (mIndex < 5 && !hasNext())
				{
					mIndex++;
				}
			}
			
			public boolean hasNext()
			{
				return  mIndex < 4 && mNode.mChildren[mIndex] != null && mNode.mChildren[mIndex].value() != null;
			}

			public Value next()
			{
				Value result = hasNext() ? mNode.mChildren[mIndex++].value() : null;
				ff();
				return result;
			}

			public void remove()
			{
				if (hasNext())
				{
					mNode.mChildren[mIndex++] = null;
				}
				ff();
			}
		}
		
		public class SiblingIterator implements java.util.Iterator<Value>
		{
			private int mStepIndex;
			private Value mNext;
			public SiblingIterator()
			{
				ff();
			}
			
			private void ff()
			{
				mNext = null;
				if (mNode.mParent != null)
				{
					while (mStepIndex < 4)
					{
						Node node = mNode.mParent.get(x()+KSiblingIterationSteps[mStepIndex][0], 
											          y()+KSiblingIterationSteps[mStepIndex][1], _zoom());
						mNext = node != null?node.value():null;
						if (mNext != null)
						{
							return;
						}
						mStepIndex++;
					}
				}
				mStepIndex = 4;
			}
			
			public boolean hasNext()
			{
				return mNext != null;
			}

			public Value next()
			{
				Value result = mNext;
				mStepIndex++;
				ff();
				return result;
			}

			public void remove()
			{
				assert(false);
			}
		}		
		
		public void delete(ChildIndex placement)
		{
			if (child(placement) != null)
			{
				assert(mNode.mChildren[placement.ordinal()].mParent == mNode);
				mNode.mChildren[placement.ordinal()].mParent = null;
				mNode.mChildren[placement.ordinal()] = null;
			}
		}
		
		public void deleteMe()
		{
			mNode.mValue = null;
			if (mNode.mParent != null)
			{
				mNode.mParent.deleteChild(mNode);
			}
			assert(this.noOfChildren() == 0);
		}
		
		protected void newChild(ChildIndex placement)
		{
		}
		
		public String diagnoseString()
		{
			return mNode.diagnoseString();
		}
	}
	
	public static int[][] KSiblingIterationSteps = new int[][] {/*up*/{0,-1},/*right*/{1,0},/*below*/{0,1}, /*left*/{-1,0}};
	private Node mRoot;	
	protected Value doGetOrPut(Value value)
	{
		assert(!value.hasChildren() && value.parent() == null); //we don't support merging trees. Only loose elements.
		if (mRoot == null)
		{
			mRoot = value.mNode;				
			return value;
		}

		Node result = mRoot.get(value.mNode, true);
		assert(result != null);
		mRoot = mRoot.root();
		return result.mValue;
	}
	
	protected Value doGet(Value like)
	{
		if (mRoot != null)
		{
			Node result = mRoot.get(like.mNode, false);
			return result != null ? result.value() : null;
		}
		return null;
	}
	
	protected void reset()
	{
		mRoot = null;
		Log.d("ZoomTree2D", "Resetting tree");		
	}
	
	public void diagnose()
	{
		if (mRoot != null)
		{
			Log.d("ZoomTree2D", ">>TreeDump**************************************************************");
			int count = mRoot.diagnose(0);
			Log.d("ZoomTree2D", "<<TreeDump("+count+")********************************************************");
		}
	}
}
