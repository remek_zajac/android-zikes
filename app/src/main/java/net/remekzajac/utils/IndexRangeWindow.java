//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
// 
//********************************************************************************

package net.remekzajac.utils;

import java.io.Closeable;

/*
 * IndexRangeWindow is an extension of the WindowBuffer with an index offset, putting
 * the index allowed ranges from WindowBuffer's [0..capacity-1] to IndexRangeWindow's [offset..capacity+offset-1]
 */
public class IndexRangeWindow<T extends Closeable> extends WindowBuffer<T> {
	
	private 		int 		 mIndexShift;
	protected final IntegerRange mIndexLimits;
	private 		IntegerRange mRangeCapacity;
	public IndexRangeWindow(int capacity, int indexShift, IntegerRange indexLimits) 
	{
		super(capacity);
		mIndexShift = indexShift;
		mIndexLimits = indexLimits;
		mRangeCapacity = new IntegerRange(mIndexShift, mIndexShift+capacity()-1).capTo(mIndexLimits);
	}

	public IndexRangeWindow<T> move(int offset)
	{
		super.move(offset);
		mIndexShift += offset;
		mRangeCapacity = new IntegerRange(mIndexShift, mIndexShift+capacity()-1).capTo(mIndexLimits);		
		return this;
	}
	
	public IntegerRange rangeCapacity()
	{
		return mRangeCapacity;
	}
	
	public T assign(int index, T value)
	{
		assert(mIndexLimits.contains(index));
		return super.assign(toBaseIndex(index), value);
	}
	
	public T get(int index)
	{
		return super.get(toBaseIndex(index));
	}
	
	public T getFirst()
	{
		return super.getFirst();
	}
	
	private int toBaseIndex(int index)
	{
		return index-mIndexShift;
	}
}
