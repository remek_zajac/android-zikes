//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.utils;

public class IntegerLineSegment
{
	private final IntegerPoint  mBegining;
	private final IntegerPoint  mEnd;
	private final IntegerVector mVector;
	public IntegerLineSegment(IntegerPoint begining, IntegerPoint end)
	{
		mBegining = begining;
		mEnd = end;
		mVector = new IntegerVector(begining, end);
	}

	public long distanceTo(IntegerPoint to)
	{
		return (int)Math.sqrt(distanceToSquared(to));
	}

	//returns 0 if on the line, - or + value depending which side of the segment the point lies
	public int side(IntegerPoint point)
	{
		int result = (mEnd.mX - mBegining.mX) * (point.mY - mBegining.mY) - (mEnd.mY - mBegining.mY) * (point.mX - mBegining.mX);
		return result;
	}

	public long distanceToSquared(IntegerPoint to)
	{
		float t = projectionRatio(to);
		if (t < 0)
		{
			// Beyond the mEnd end of the segment
			return mBegining.distanceSquaredFrom(to);
		}
		else if ( t > 1)
		{
			//beyond the mBegining end of the segment
			return mEnd.distanceSquaredFrom(to);
		}
		return to.distanceSquaredFrom(projection(to, t));
	}

	private float projectionRatio(IntegerPoint to) {
		return ((to.mX -mBegining.mX)*(mEnd.mX -mBegining.mX)+(to.mY -mBegining.mY)*(mEnd.mY -mBegining.mY))/(float)mVector.lengthSquared();
	}

	private IntegerPoint projection(IntegerPoint to, float projectionRatio) {
		return new IntegerPoint((int)(mBegining.mX + projectionRatio*(mEnd.mX -mBegining.mX)),
				 		        (int)(mBegining.mY + projectionRatio*(mEnd.mY -mBegining.mY)));
	}

	//Projection on the line that goes between mBegining and mEnd
	public IntegerPoint projection(IntegerPoint to)
	{
		return projection(to, projectionRatio(to));
	}

	public IntegerPoint begining()
	{
		return mBegining;
	}

	public IntegerPoint end()
	{
		return mEnd;
	}

	public double angleDegRelY()
	{
		return mVector.angleDegRelY();
	}

	public long lenght()
	{
		return mVector.lenght();
	}

	public long lengthSquared()
	{
		return mVector.lengthSquared();
	}

	@Override
	public String toString() {
		return new String("Line: ["+mBegining.toString()+","+mEnd.toString()+"]");
	}

	public int dy()
	{
		return mVector.y();
	}

	public int dx()
	{
		return mVector.x();
	}
}
