//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.utils;

import java.util.Map;

public class MustacheReplace {

    private String mTemplate;
    public MustacheReplace(String template) {
        mTemplate = template;
    }

    public MustacheReplace expand(Map<String, String> mappings) {
        mTemplate = expand(mTemplate, mappings);
        return this;
    }

    public MustacheReplace expand(String what, String withWhat) {
        mTemplate = expand(mTemplate, what, withWhat);
        return this;
    }

    public String finish() {
        return mTemplate;
    }

    static public String expand(String template, Map<String, String> mappings) {
        for (String key : mappings.keySet()) {
            template = expand(template, key, mappings.get(key));
        }
        return template;
    }

    static public String expand(String template, String what, String withWhat) {
        return template.replace("{{"+what+"}}", withWhat);
    }
}
