//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.utils;

public class DoubleLineSegment
{
	private final DoublePoint  mBegining;
	private final DoublePoint  mEnd;
	private final DoubleVector mVector;
	public DoubleLineSegment(DoublePoint begining, DoublePoint end)
	{
		mBegining = begining;
		mEnd = end;
		mVector = new DoubleVector(begining, end);
	}

	public DoubleLineSegment(double beginingX, double beginingY, double endX, double endY)
	{
		this(new DoublePoint(beginingX, beginingY), new DoublePoint(endX, endY));
	}

	public double distanceTo(DoublePoint to)
	{
		return Math.sqrt(distanceToSquared(to));
	}

	public double distanceToSquared(DoublePoint to)
	{
		double t = projectionRatio(to);
		if (t < 0)
		{
			// Beyond the mEnd end of the segment
			return mBegining.distanceSquaredFrom(to);
		}
		else if ( t > 1)
		{
			//beyond the mBegining end of the segment
			return mEnd.distanceSquaredFrom(to);
		}
		return to.distanceSquaredFrom(projection(to, t));
	}

	private double projectionRatio(DoublePoint to) {
		return ((to.mX -mBegining.mX)*(mEnd.mX -mBegining.mX)+(to.mY -mBegining.mY)*(mEnd.mY -mBegining.mY))/(float)mVector.lengthSquared();
	}

	private DoublePoint projection(DoublePoint to, double projectionRatio) {
		return new DoublePoint((mBegining.mX + projectionRatio*(mEnd.mX -mBegining.mX)),
				 		        (mBegining.mY + projectionRatio*(mEnd.mY -mBegining.mY)));
	}

	public DoublePoint projection(DoublePoint to)
	{
		return projection(to, projectionRatio(to));
	}

	public DoublePoint begining()
	{
		return mBegining;
	}

	public DoublePoint end()
	{
		return mEnd;
	}

	public DoublePoint centre() {
        return new DoublePoint(
            begining().x() + (end().x() - begining().x())/2.0,
            begining().y() + (end().y() - begining().y())/2.0
        );
    }

	public double angleDegRelY()
	{
		return mVector.angleDegRelY();
	}

	public double lenght()
	{
		return mVector.lenght();
	}

	public double lengthSquared()
	{
		return mVector.lengthSquared();
	}

	@Override
	public String toString()
	{
		return new String("Line: ["+mBegining.toString()+","+mEnd.toString()+"]");
	}
}
