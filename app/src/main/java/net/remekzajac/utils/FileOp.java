//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
// 
//********************************************************************************


package net.remekzajac.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileOp
{
	static private final int IO_BUFFER_SIZE = 1024;
	static public void copy(File src, File dst) throws IOException
	{
		BufferedInputStream input = null;
		BufferedOutputStream output = null;
		if (dst.isDirectory())
		{
			dst = new File(dst, src.getName());
		}
		
		try
		{
			dst.createNewFile();
			input = new BufferedInputStream(new FileInputStream(src));
			output = new BufferedOutputStream(new FileOutputStream(dst));
	        int count = 0;
	        byte data[] = new byte[IO_BUFFER_SIZE];
	        while ((count = input.read(data)) != -1) 
	        {
	            output.write(data, 0, count);
	        }
		}
		catch (IOException e)
		{
			throw e;
		}
		finally
		{
			if (input != null)
			{
				input.close();
			}
			if (output != null)
			{
				output.close();
			}
		}
	}
	
	//recursive delete.
	static public void delete(File fileOrDirectory)
	{
	    if (fileOrDirectory.isDirectory())
	        for (File child : fileOrDirectory.listFiles())
	        	delete(child);

	    fileOrDirectory.delete();	
	}
	
	static public void deleteAndClearUp(File fileOrDirectory)
	{
	    fileOrDirectory.delete();
	    if (fileOrDirectory.getParentFile().listFiles().length == 0)
	    {
	    	deleteAndClearUp(fileOrDirectory.getParentFile());
	    }
	}
	
	public static long folderSizeBytes(File dir) {
	    if (dir.exists()) {
	        long result = 0;
	        File[] fileList = dir.listFiles();
	        for(int i = 0; i < fileList.length; i++) {
	            // Recursive call if it's a directory
	            if(fileList[i].isDirectory()) {
	                result += folderSizeBytes(fileList [i]);
	            } else {
	                // Sum the file size in bytes
	                result += fileList[i].length();
	            }
	        }
	        return result; // return the file size
	    }
	    return 0;
	}

	public static void write(InputStream in, File file ) {
		try {
			OutputStream out = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int len;
			while((len=in.read(buf))>0){
				out.write(buf,0,len);
			}
			out.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
