//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.utils;

public class IntegerVector extends IntegerPoint
{
	public IntegerVector(int x, int y) {
		super(x,y);
	}

	public IntegerVector(IntegerPoint begin, IntegerPoint end) {
		super(end.mX -begin.mX, end.mY -begin.mY);
	}

	public IntegerVector(IntegerPoint from)
	{
		super(from);
	}

	public long lenght() {
		return (long)Math.sqrt(lengthSquared());
	}

	public long lengthSquared() {
		return distanceSquaredFrom(KZero);
	}

	public IntegerVector withLength(int lenght) {
		return multiply((double)lenght/lenght());
	}

	public IntegerVector setAxisLengthX(int x) {
		float ratio = (float)x/mX;
		mY *= ratio;
		mX = x;
		return this;
	}

	public IntegerVector setAxisLengthY(int y) {
		float ratio = (float)y/mY;
		mX *= ratio;
		mY = y;
		return this;
	}

	public IntegerVector multiply(double factor) {
		return new IntegerVector(
			(int)(mX * factor),
			(int)(mY * factor)
		);
	}

	public IntegerVector divide(double factor) {
		return new IntegerVector(
			(int)(mX / factor),
			(int)(mY / factor)
		);
	}

	public double angleDegRelY()
	{
		return Math.toDegrees(Math.atan2(mX, mY));
	}

	public IntegerVector orthogonal() {
		return new IntegerVector(
			-mY, mX
		);
	}

	public IntegerVector reverse() {
		return new IntegerVector(
			-mX, -mY
		);
	}

	public IntegerVector add(IntegerVector vector) {
		return new IntegerVector(super.add(vector));
	}

	public IntegerVector rotate(double angle) {
		if (angle != 0) {
			//the algorithm below performs counterclockwise rotation.
			//whereas the rest of the framework does clockwise.
			//we need to have this method perform clockwise rotation
			//therefore:
			double radians = Math.toRadians(360-angle);
			double sinAngle = Math.sin(radians);
			double cosAngle = Math.cos(radians);
			return new IntegerVector(
				(int)(cosAngle*mX - sinAngle*mY),
				(int)(sinAngle*mX + cosAngle*mY)
			);
		}
		return new IntegerVector(this);
	}
}

