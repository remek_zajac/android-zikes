//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
// 
//********************************************************************************
package net.remekzajac.utils;

import java.util.Timer;
import java.util.TimerTask;

import android.os.Handler;
import android.util.Log;

public class TimeDamHandler extends Handler
{
	private final int 		mDamMs;
	private final Runnable 	mRunnable;
	private long 			mLastPosted;
	public TimeDamHandler(int damMs, Runnable r)
	{
		mDamMs = damMs;
		mRunnable = r;
	}

	public final void postDam()
	{
		long timeNow = System.currentTimeMillis();
		if (timeNow-mLastPosted > mDamMs)
		{
			mLastPosted = timeNow;
			Timer timer = new Timer();			
			TimerTask timerTask = new TimerTask()
			{
				@Override
				public void run() 
				{
	                post(mRunnable);
				}
			};
			timer.schedule(timerTask, mDamMs);
		}
	}
	
	public final void execute()
	{
		mRunnable.run();
	}
}
