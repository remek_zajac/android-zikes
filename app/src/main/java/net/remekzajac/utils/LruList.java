//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
// 
//********************************************************************************

package net.remekzajac.utils;

import net.remekzajac.utils.ListElementBase.RemovalOutcome;

public class LruList<T>
{
	private static class Elem<T> extends ListElement<Elem<T>>
	{
		private final T mValue;
		private Elem(T value)
		{
			mValue = value;
		}
	}
	
	private Elem<T> 	mHead;
	private Elem<T> 	mTail;
	private final int	mMaxSize;
	private int			mSize;
	public LruList(int size)
	{
		mMaxSize = size;
	}
	
	public void prepend(T elem)
	{
		Elem<T> newElem = new Elem<T>(elem);
		if (mHead == null)
		{
			mTail = mHead = newElem;
		}
		mHead.insert(newElem);
		if (++mSize > mMaxSize)
		{
			mTail = mTail.remove(RemovalOutcome.ELeft);
			--mSize;
		}
	}
	
	public T find(T elem) //TODO, if found, should pop the element towards the head of the list.
	{
		Elem<T> current = mHead;
		while(current != null && !current.mValue.equals(elem))
		{
			current = current.next();
		}
		return current.mValue;
	}
}
