//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
// 
//********************************************************************************

package net.remekzajac.utils;


public class HopScotchRangeIterator {

	final private IntegerRange mRange;
	private int mCursor;
	private int mNextHopAbs;
	private int mSign;
	public HopScotchRangeIterator(IntegerRange range) {
		mRange = range;
		mCursor = range.middle();
		mNextHopAbs = 1;
		mSign = 1;
	}

	public boolean isEnd() 
	{	
		return !mRange.contains(mCursor);
	}

	public void next() {
		mCursor = mCursor+(mSign*mNextHopAbs);
		mSign *= -1;
		mNextHopAbs++;
	}
	
	public int get()
	{
		return mCursor;
	}
}
