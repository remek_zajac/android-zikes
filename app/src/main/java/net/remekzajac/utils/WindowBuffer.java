//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
// 
//********************************************************************************

package net.remekzajac.utils;

import java.io.Closeable;
import java.io.IOException;
import java.util.Vector;

/*
 * The WindowBuffer class implements what can be described as a two-way sliding window
 * holding a number of instances of T. The main public operations for the WindowBuffer
 * are:
 * * put  - given an index and an instance of T, stores the instance of T under the given index;
 * * move - given an offset, moves the window so that what was returned for index x is now returned by index (x-offset).
 *          Note that the range of the indices is at all times between 0 and the window's capacity-1 and so the
 *          move operation has the semantics of moving the content of the window within a buffer rather
 *          than moving a window over a buffer. The content that falls beyond the allowed index boundaries
 *          is obliterated.
 * This function is implemented using a circular buffer. 
 */
public class WindowBuffer<T extends Closeable> implements Closeable 
{
	private static class WindowElement<T extends Closeable>
	{
		private T mValue;
		private void assign(T value)
		{
			close();
			mValue = value;
		}
		
		private T value()
		{
			return mValue;
		}
		
		private void close()
		{
			if (mValue != null)
			{
				try 
				{
					mValue.close();
				} 
				catch (IOException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	private Vector<WindowElement<T> > mElements;
	private int mShift;
	private int mFirst;
	private int mLast;
	public WindowBuffer(int capacity) {
		mElements = new Vector<WindowElement<T> >(capacity);
		for (int i = 0; i < capacity(); i++)
		{
			mElements.add(new WindowElement<T>());
		}
		mShift = 0;
		mLast = -1; 		 //initialise to illegal values, the collection is empty until assigned
		mFirst = capacity();
	}
	
	public int capacity()
	{
		return mElements.capacity();
	}
	
	public int liveSize()
	{
		return mLast - mFirst + 1;
	}

	/*Moves the window buffer so that what was returned for index[x] is now returned by index[x-offset]
	  Elements that fall beyond the array boundaries are removed. */
	public WindowBuffer<T> move(int offset)
	{
		if (mFirst > mLast || offset == 0)
		{
			return this;
		}		
		int swipeBegin = mFirst;
		int swipeEnd = mLast+1;

		mLast = Math.max(mLast-offset, -1); //may fall one ahead of the array
		mFirst = Math.min(mFirst-offset, capacity()); //may fall one behind the array
		
		if (offset>0)
		{
			swipeEnd = Math.min(swipeBegin-mFirst, capacity());
			mFirst = Math.max(mFirst, 0);
		}
		else
		{
			swipeBegin = Math.max(swipeEnd-(mLast-capacity()+1),0);
			mLast = Math.min(mLast, capacity()-1); 
		}
		
		for (int i = swipeBegin; i < swipeEnd && i < capacity(); i++)
		{
			getWindowElement(i).assign(null);
		}
		mShift = (mShift+offset)%capacity();
		if (mShift < 0)
		{
			assert Math.abs(mShift) < capacity();
			mShift+=capacity();
		}
		return this;
	}
	
	private WindowElement<T> getWindowElement(int index)
	{
		assert index < capacity() && index >= 0;
		return mElements.get((index+mShift)%capacity());
	}
	
	public T get(int index)
	{
		return getWindowElement(index).value();
	}
	
	public T assign(int index, T value)
	{
		getWindowElement(index).assign(value);
		mFirst = mFirst > index?index:mFirst;
		mLast = mLast < index?index:mLast;
		return value;
	}
	
	public T getFirst()
	{
		return mFirst<capacity()?getWindowElement(mFirst).value():null;
	}

	public void close() 
	{
		for (int i = mFirst; i <= mLast; i++)
		{
			getWindowElement(i).close();
		}
	}
}
