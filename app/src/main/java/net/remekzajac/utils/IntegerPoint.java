//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.utils;

public class IntegerPoint extends Integer2Tuple
{
	static public final IntegerPoint KZero = new IntegerPoint(0,0);

	public IntegerPoint(int x, int y)
	{
		super(x, y);
	}

	public IntegerPoint(IntegerPoint from)
	{
		super(from.mX, from.mY);
	}

	public long distanceFrom(IntegerPoint point) {
		return (long)(Math.sqrt(distanceSquaredFrom(point)));
	}

	public long distanceSquaredFrom(IntegerPoint point) {
		long dx = mX - point.mX;
		long dy = mY - point.mY;
		return (dx*dx)+(dy*dy);
	}

	public int axisDistanceFrom(IntegerPoint point)
	{
		return axisDistanceFrom(point.x(), point.y());
	}

	public int axisDistanceFrom(int x, int y) {
		return Math.max(Math.abs(mX - x), Math.abs(mY - y));
	}

	public IntegerPoint add(IntegerVector vector) {
		return new IntegerPoint(
			mX + vector.x(),
			mY + vector.y()
		);
	}

	public IntegerPoint rotate(double angle, IntegerPoint against) {
		if (angle != 0) {
			IntegerVector betweenVector = new IntegerVector(this, against);
			IntegerVector rotateVector = betweenVector.reverse().rotate(angle);
			return this.add(betweenVector).add(rotateVector);
		}
		return new IntegerPoint(this);
	}
}
