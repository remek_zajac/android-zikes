//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.utils;

import java.io.Closeable;

public class CenteredIndexRangeWindow<T extends Closeable> extends IndexRangeWindow<T> {

	private int mCentre;
	public CenteredIndexRangeWindow(int maxCapacity, int initialCentre, IntegerRange indexLimits) {
		this(Math.min(maxCapacity, indexLimits.size()), indexLimits, initialCentre);
	}

	private CenteredIndexRangeWindow(int exactCapacity, IntegerRange indexLimits, int initialCentre) {
		super(exactCapacity, initialCentre-exactCapacity/2, indexLimits);
		assert initialCentre >= indexLimits.smaller() && initialCentre <= indexLimits.bigger();
		mCentre = initialCentre;
	}

	public CenteredIndexRangeWindow<T> set(int newCentre) {
		move(newCentre - mCentre);
		mCentre = newCentre;
		return this;
	}

	public T getCentre() {
		return get(mCentre);
	}

	public int getCentreIndex() {
		return mCentre;
	}

	public int leftToCentre()
	{
		return mCentre - rangeCapacity().smaller();
	}
}
