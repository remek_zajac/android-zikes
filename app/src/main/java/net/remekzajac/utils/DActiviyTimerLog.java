//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
// 
//********************************************************************************

package net.remekzajac.utils;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.util.Log;

public class DActiviyTimerLog 
{
	private Vector<DActiviyTimerLog>	mChildren;
	private final String 				mActivityName;
	private String 						mLogTraceTag;
	private long  						mTimeStart;
	private long  						mDuration;
	
	public DActiviyTimerLog(String activityName)
	{
		mActivityName = activityName;
		mLogTraceTag = activityName;
		mChildren = new Vector<DActiviyTimerLog>();
	}
	
	public void setLogTraceTag(String logTraceTag)
	{
		mLogTraceTag = logTraceTag;
		for (DActiviyTimerLog child : mChildren)
		{
			child.setLogTraceTag(logTraceTag);
		}
	}
	
	public void start()
	{
		mTimeStart = System.currentTimeMillis();
	}
	
	public void stop()
	{
		mDuration = System.currentTimeMillis() - mTimeStart;
	}
	
	public void log()
	{
		log(0,0);
	}
	
	public void append(DActiviyTimerLog child)
	{
		child.setLogTraceTag(mLogTraceTag);
		mChildren.add(child);
	}
	
	@SuppressLint("SimpleDateFormat")
	private long log(int indent, long parentDuration)
	{
		String indentStr = indent(indent);
		String logString = indentStr+"***Activity:"+mActivityName+" took "+new SimpleDateFormat("HH:mm:ss:SSS").format(new Date(mDuration));
		if (parentDuration > 0)
		{
			logString += " ("+100*mDuration/parentDuration+"%)";
		}
		Log.d(mLogTraceTag, logString);
		if (mChildren.size() > 0)
		{
			long accountedFor = 0;
			for (DActiviyTimerLog child : mChildren)
			{
				accountedFor += child.log(indent+2, mDuration);
			}
			long unaccountedFor = mDuration-accountedFor;
			Log.d(mLogTraceTag, indentStr+"Unaccounted for:"+new SimpleDateFormat("HH:mm:ss:SSS").format(new Date(unaccountedFor))+" ("+" ("+100*unaccountedFor/mDuration+"%)");
		}
		Log.d(mLogTraceTag, "");
		return mDuration;
	}
	
	private String indent(int indent)
	{
		StringBuilder sb = new StringBuilder();
		for (int i=0; i>indent; i++) {
		    sb.append(' ');
		}
		return sb.toString();
	}
}
