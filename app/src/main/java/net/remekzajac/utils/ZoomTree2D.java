//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
// 
//********************************************************************************
package net.remekzajac.utils;


public class ZoomTree2D<T extends ZoomTree2DBase.Value> extends ZoomTree2DBase
{
	public static class ZoomTree2DElement<T extends ZoomTree2DBase.Value> extends ZoomTree2DBase.Value
	{
		public ZoomTree2DElement(int x, int y, int zoom)
		{
			super(x,y,zoom);
		}
		
		public ZoomTree2DElement(ZoomTree2DElement<T> parent, ChildIndex placement)
		{
			super(parent, placement);
		}
		
		@SuppressWarnings("unchecked")
		public T parent()
		{
			return (T)super.parent();
		}
		
		@SuppressWarnings("unchecked")
		public T child(ChildIndex placement)
		{
			return (T)super.child(placement);
		}
		
		public class Iterator implements java.util.Iterator<T>
		{
			private final java.util.Iterator<ZoomTree2DBase.Value> mSrcIt;
			private Iterator(java.util.Iterator<ZoomTree2DBase.Value> src)
			{
				mSrcIt = src;
			}
			
			@SuppressWarnings("unchecked")
			public T next()
			{
				return (T)mSrcIt.next();
			}

			public boolean hasNext()
			{
				return mSrcIt.hasNext();
			}

			public void remove()
			{
				mSrcIt.remove();
			}
		}
		public Iterator childrenIterator()
		{
			return new Iterator(new ChildrenIterator());
		}
		
		public Iterator siblingIterator()
		{
			return new Iterator(new SiblingIterator());
		}		
		
		@SuppressWarnings("unchecked")
		public T getOrPut(T value)
		{
			return (T)super.doGetOrPut(value);
		}
		
		@SuppressWarnings("unchecked")
		public T get(int x, int y, int zoom)
		{
			return (T)super.get(x, y, zoom);
		}		
	}
	
	@SuppressWarnings("unchecked")
	public T getOrPut(T value)
	{
		return (T)super.doGetOrPut(value);
	}
	
	@SuppressWarnings("unchecked")
	public T get(T like)
	{
		return (T)super.doGet(like);
	}
	
	public void reset()
	{
		super.reset();
	}
}
