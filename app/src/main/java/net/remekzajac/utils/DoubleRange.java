//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.utils;

public class DoubleRange extends Double2Tuple {
	public DoubleRange(double x, double y)
	{
		super(x,y);
	}

	public DoubleRange(DoubleRange from)
	{
		super(from.x(), from.y());
	}

	@Override
	public DoubleRange clone()
	{
		return new DoubleRange(this);
	}

	public boolean contains(double number)
	{
		return smaller() <= number && number <= bigger();
	}

	public boolean contains(DoubleRange subrange)
	{
		return contains(subrange.smaller()) && contains(subrange.bigger());
	}

	public DoubleRange add(double number)
	{
		mY = Math.max(bigger(), number);
		mX = Math.min(smaller(), number);
		return this;
	}

	public DoubleRange add(DoubleRange range)
	{
		mY = Math.max(bigger(), range.bigger());
		mX = Math.min(smaller(), range.smaller());
		return this;
	}

	public double middle()
	{
		return (mX + mY)/2;
	}

	public double size()
	{
		return bigger() - smaller();
	}

	public DoubleRange move(double shift)
	{
		mX +=shift;
		mY +=shift;
		return this;
	}

	public DoubleRange capTo(DoubleRange limits)
	{
		mX = Math.min(limits.bigger(), Math.max(limits.smaller(), mX));
		mY = Math.min(limits.bigger(), Math.max(limits.smaller(), mY));
		return this;
	}

	@Override
	public String toString()
	{
		return new String("["+ mX +".."+ mY +"]");
	}
}
