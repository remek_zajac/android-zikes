//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************


package net.remekzajac.utils;

public class ListElementBase
{
	public enum RemovalOutcome {
	    ELeft, 		//return previous (possibly null)
	    ERight,		//return next     (possibly null)
	    ELeftRight, //try previous, if null, return next (possibly null)
	}

	private ListElementBase mNext;
	private ListElementBase mPrev;

	protected ListElementBase()
	{}

	protected ListElementBase(ListElementBase prev, ListElementBase next)
	{
		mPrev = prev;
		mNext = next;
	}

	protected ListElementBase baseappend(ListElementBase elem)
	{
		ListElementBase next = mNext;
		ListElementBase elemHead = elem.head();
		ListElementBase elemTail = elem.tail();

		//fix head
		mNext = elemHead;
		elemHead.mPrev = this;

		//fix tail
		elemTail.mNext = next;
		if (next != null)
		{
			next.mPrev = elemTail;
		}

		return elem;
	}

	protected ListElementBase baseinsert(ListElementBase elem)
	{
		ListElementBase prev = mPrev;
		ListElementBase elemHead = elem.head();
		ListElementBase elemTail = elem.tail();

		//fix tail
		mPrev = elemTail;
		elemTail.mNext = this;

		//fix head
		elemHead.mPrev = prev;
		if (prev != null)
		{
			prev.mNext = elemHead;
		}

		return elem;
	}

	public ListElementBase head()
	{
		return mPrev!=null?mPrev.head():this;
	}

	public ListElementBase tail()
	{
		return mNext!=null?mNext.tail():this;
	}

	public int size()
	{
		int result = 1;
		ListElementBase current = this;
		while (current != null)
		{
			current = current.mNext;
			result++;
		}
		return result;
	}

	protected ListElementBase baseremove(RemovalOutcome outcome)
	{
		ListElementBase result = outcome==RemovalOutcome.ERight?mNext:mPrev;
		if (mPrev != null )
		{
			mPrev.mNext = mNext;
		}
		if (mNext != null)
		{
			mNext.mPrev = mPrev;
			if (outcome == RemovalOutcome.ELeftRight && result == null)
			{
				result = mNext;
			}
		}
		mNext = null;
		mPrev = null;

		return result;
	}

	public ListElementBase next()
	{
		return mNext;
	}

	public ListElementBase prev()
	{
		return mPrev;
	}

	public ListElementBase next(int step)
	{
		return (step==0||mNext==null)?mNext:next(--step);
	}

	public ListElementBase prev(int step)
	{
		return (step==0||mPrev==null)?mPrev:prev(--step);
	}

	protected boolean cutOffNext()
	{
		if (mNext != null)
		{
			mNext.mPrev = null;
			mNext = null;
			return true;
		}
		return false;
	}

	protected boolean cutOffPrev()
	{
		if (mPrev != null)
		{
			mPrev.mNext = null;
			mPrev = null;
			return true;
		}
		return false;
	}

	public ListElementBase find(ListElementBase elem)
	{
		if (equals(elem))
		{
			return this;
		}
		else if (next() != null)
		{
			return next().find(elem);
		}
		return null;
	}
}

