//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.remekzajac.utils;

import android.os.Handler;
import android.os.Looper;

import net.zikes.OSMMapsApplication;
import net.zikes.backend.AuthenticatedBackend;

/*
 * A variation/improvement on Android's AsyncTask, where it:
 * - spawns it on a dedicated thread (no pool)
 * - acknowledges tasks can fail
 */
abstract public class SAsyncTask<Result, Progress> {

    public static class TaskResult<Result> {
        private final Result mResult;
        private final Exception mException;

        private TaskResult(Result r) {
            mResult = r;
            mException = null;
        }

        private TaskResult(Exception e) {
            mResult = null;
            mException = e;
        }

        public Result result() throws Exception {
            if (mResult == null) {
                throw mException;
            }
            return mResult;
        }
    }

    private boolean mCancelled;
    private final String mThreadName;
    public SAsyncTask() {
        mCancelled = false;
        mThreadName = "SAsyncTask";
    }

    public SAsyncTask(String taskName) {
        mCancelled = false;
        mThreadName = taskName;
    }

    private void signalToUiThread(final TaskResult<Result> result) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                onTaskComplete(result);
            }
        });
    }

    public void execute() {
        Thread t = new Thread(new Runnable() {
            public void run() {
                try {
                    signalToUiThread(
                        new TaskResult<Result>(
                            backgroundProcess()
                        )
                    );
                } catch (Exception e) {
                    signalToUiThread(
                        new TaskResult<Result>(e)
                    );
                }
            }
        });
        t.setPriority(
            Thread.MIN_PRIORITY
        );
        t.start();
    }

    public void cancel() {
        mCancelled = true;
    }

    public boolean isCancelled() {
        return mCancelled;
    }

    abstract protected Result backgroundProcess() throws Exception;
    abstract protected void onTaskComplete(TaskResult<Result> result);

    protected void postProgress(final Progress progress) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                onProgress(progress);
            }
        });
    }

    protected void onProgress(Progress progress) {}
}
