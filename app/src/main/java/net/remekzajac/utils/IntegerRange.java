//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.utils;

public class IntegerRange extends Integer2Tuple {
	public IntegerRange(int x, int y)
	{
		super(x,y);
	}

	public IntegerRange(IntegerRange from)
	{
		super(from.x(), from.y());
	}

	public boolean contains(int number)
	{
		return smaller() <= number && number <= bigger();
	}

	public boolean contains(IntegerRange subrange)
	{
		return contains(subrange.smaller()) && contains(subrange.bigger());
	}

	public IntegerRange add(int number)
	{
		mY = Math.max(bigger(), number);
		mX = Math.min(smaller(), number);
		return this;
	}

	public IntegerRange add(IntegerRange range)
	{
		mY = Math.max(bigger(), range.bigger());
		mX = Math.min(smaller(), range.smaller());
		return this;
	}

	public int middle()
	{
		return (mX + mY)/2;
	}

	public IntegerRange lowerHalf()
	{
		return new IntegerRange(smaller(), middle());
	}

	public IntegerRange upperHalf()
	{
		return middle() != bigger() ? new IntegerRange(middle()+1, bigger()) : new IntegerRange(bigger(), bigger());
	}

	public int size()
	{
		return bigger() - smaller() + 1;
	}

	public IntegerRange move(int shift)
	{
		mX +=shift;
		mY +=shift;
		return this;
	}

	public IntegerRange capTo(IntegerRange limits)
	{
		mX = Math.min(limits.bigger(), Math.max(limits.smaller(), mX));
		mY = Math.min(limits.bigger(), Math.max(limits.smaller(), mY));
		return this;
	}

	public String toString()
	{
		return new String("["+ mX +".."+ mY +"]");
	}
}
