//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.utils;

public class DoubleVector extends DoublePoint
{
	public DoubleVector(double x, double y) {
		super(x,y);
	}

	public DoubleVector(DoublePoint begin, DoublePoint end) {
		super(end.mX -begin.mX, end.mY -begin.mY);
	}

	public DoubleVector(DoublePoint from) {
		super(from);
	}

	public DoubleVector(IntegerPoint from) {
		super(from.x(), from.y());
	}

	public double lenght()
	{
		return super.distanceFrom(KZero);
	}
	public double lengthSquared() {
		return super.distanceSquaredFrom(KZero);
	}

	public DoubleVector setLength(double lenght) {
		double currentLenght = lenght();
		mX = (mX*lenght/currentLenght);
		mY = (mY*lenght/currentLenght);
		return this;
	}

	public double angleDegRelY() {
		return Math.toDegrees(Math.atan2(mX, mY));
	}

	public DoubleVector add(IntegerVector vector) {
		return new DoubleVector(super.add(vector));
	}

	public DoubleVector add(DoubleVector vector) {
		return new DoubleVector(super.add(vector));
	}

	public DoubleVector orthogonal() {
		return new DoubleVector(
			-mY,
			mX
		);
	}

	public DoubleVector multiply(double factor) {
		return new DoubleVector(
			mX * factor,
			mY * factor
		);
	}

	public DoubleVector divide(double factor) {
		return new DoubleVector(
			mX / factor,
			mY / factor
		);
	}

	public DoubleVector reverse() {
		return new DoubleVector(-mX, -mY);
	}

	public DoubleVector rotate(double angle) {
		if (angle != 0) {
			//the algorithm below performs counterclockwise rotation.
			//whereas the rest of the framework does clockwise.
			//we need to have this method perform clockwise rotation
			//therefore:
			double radians = Math.toRadians(360-angle);
			double sinAngle = Math.sin(radians);
			double cosAngle = Math.cos(radians);
			return new DoubleVector(
				cosAngle*mX - sinAngle*mY,
				sinAngle*mX + cosAngle*mY
			);
		}
		return new DoubleVector(this);
	}
}
