//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.utils;

public class Integer2Tuple implements Cloneable
{
	protected int mX;
	protected int mY;

	public Integer2Tuple(int x, int y) {
		mX = x;
		mY = y;
	}

	public Integer2Tuple(Integer2Tuple from) {
		mX = from.mX;
		mY = from.mY;
	}

	public int x() {
		return mX;
	}
	public int y() {
		return mY;
	}

	public int bigger()
	{
		return Math.max(mX, mY);
	}
	public int smaller()
	{
		return Math.min(mX, mY);
	}

	@Override
	public boolean equals(Object other) {
		if((other == null) || (getClass() != other.getClass())){
			return false;
		}
        Integer2Tuple otherInteger2Tuple = (Integer2Tuple)other;
		return mX == otherInteger2Tuple.x() && mY == otherInteger2Tuple.y();
	}

    @Override
    public int hashCode() {
        return mX + mY;
    }

	public String toString()
	{
		return new String(""+ mX +","+ mY);
	}
	public String toDebugString()
	{
		return new String("[x:"+ mX +",y:"+ mY +"]");
	}
}
