//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
// 
//********************************************************************************
package net.remekzajac.utils;

import java.util.concurrent.ConcurrentLinkedQueue;
import android.os.AsyncTask;
import android.util.Log;

public class AsyncTaskQueue<Result>
{
	public interface Task<Result>
	{
		public Result backgroundProcess();
		public void   foregroundFinished(Result result);
	}
	
	private ConcurrentLinkedQueue<Task<Result> >	mTileQueue;
	private PrivateTask								mAsyncTask;
	public AsyncTaskQueue()
	{
		mTileQueue = new ConcurrentLinkedQueue<Task<Result> >();
	}
	
	public void add(Task<Result> task)
	{
		mTileQueue.add(task);
		Log.d("AsyncTaskQueue", "AsyncTaskQueue::add(), now tasks queued:"+mTileQueue.size());
		resumeTask();
	}
	
	public void cancel(Task<Result> task)
	{
		if (task != null)
		{
			mTileQueue.remove(task);
			Log.d("AsyncTaskQueue", "AsyncTaskQueue::cancel(), now tasks queued:"+mTileQueue.size());
		}
	}
	
	private void resumeTask()
	{
		if (mAsyncTask == null || mAsyncTask.getStatus() == AsyncTask.Status.FINISHED)
		{
			mAsyncTask = new PrivateTask();
			Log.d("AsyncTaskQueue", "AsyncTaskQueue::resumeTask(), no workhorse AsyncTasks found, creating");
		}
		if (mAsyncTask.getStatus() == AsyncTask.Status.PENDING)
		{
			Log.d("AsyncTaskQueue", "AsyncTaskQueue::resumeTask(), workhorse AsyncTask found pending, resuming");
			mAsyncTask.execute();
		}
	}
	
	private static class TaskResult<T, R>
	{
		public final T mTask;
		public final R mResult;
		public TaskResult(T task, R result)
		{
			mTask = task;
			mResult = result;
		}
	}

	private class PrivateTask extends AsyncTask<Void, TaskResult<Task<Result>, Result>, Void>
	{		
		@SuppressWarnings("unchecked")
		@Override
		protected Void doInBackground(Void... params) 
		{
			Task<Result> task = null;
			while ((task = mTileQueue.poll()) != null)
			{
				Result result = task.backgroundProcess();
				this.publishProgress(new TaskResult<Task<Result>, Result>(task, result));
			}
			return null;
		}
		
	     @Override
	     protected void onProgressUpdate(TaskResult<Task<Result>, Result>... taskResults) 
	     {
	    	 Log.d("AsyncTaskQueue", "AsyncTaskQueue::onProgressUpdate(), task finished, now tasks queued:"+mTileQueue.size());
	    	 taskResults[0].mTask.foregroundFinished(taskResults[0].mResult);
	     }
	     
	     @Override
	     protected void onPostExecute(Void result) 
	     {
	    	 Log.d("AsyncTaskQueue", "AsyncTaskQueue::onPostExecute(), queue was exhaused last time in background, now queue size:"+mTileQueue.size());
	    	 mAsyncTask = null;
	    	 if (!mTileQueue.isEmpty())
	    	 {
		    	 Log.d("AsyncTaskQueue", "AsyncTaskQueue::onPostExecute(), more tasks added since, resuming");
	    		 resumeTask();
	    	 }
	     }	     
	}
}
