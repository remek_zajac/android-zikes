package net.remekzajac.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.os.AsyncTask;

public class URLFileDownloadTask extends AsyncTask<Void, Integer, File> 
{
	private final URL 	mURL;
	private final File 	mDstFile;
	public URLFileDownloadTask(URL url, File dstFile)
	{
		mURL = url;
		mDstFile = dstFile;
	}
	
    @Override
    protected File doInBackground(Void... params) {
    	InputStream input = null;
    	OutputStream output = null;
        try {
            URLConnection connection = mURL.openConnection();
            connection.connect();
            // this will be useful so that you can show a typical 0-100% progress bar
            int fileLength = connection.getContentLength();

            // download the file
            input = new BufferedInputStream(mURL.openStream());
            output = new FileOutputStream(mDstFile);

            byte data[] = new byte[1024];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);
            }

            output.flush();

        } 
        catch (Exception e) 
        {
        	return null;
        }
        finally
        {
        	try 
        	{
	        	if (output != null)
	        	{
	        		output.close();
	        	}
	            if (input != null)
	            {
	            	input.close();
	            }
        	} catch (IOException e) 
        	{
				e.printStackTrace();
			}
        }
        return mDstFile;
    }
}
