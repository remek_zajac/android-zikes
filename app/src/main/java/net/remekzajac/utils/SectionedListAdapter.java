//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class SectionedListAdapter<SectionItem> extends BaseAdapter
{
	@SuppressWarnings("serial")
	public class Section extends ArrayList<SectionItem>
	{
		private boolean mOpen;
		public Section(boolean open)
		{
			mOpen = open;
		}

		public boolean isOpen()
		{
			return mOpen;
		}

		public void open()
		{
			mOpen = true;
		}

		public void close()
		{
			mOpen = false;
		}
	}

	public class Tuple
	{
		public final Section 		mSection;
		public final SectionItem 	mSectionItem;
		public final int			mIndexOffset;
		public Tuple(Section section, SectionItem sectionItem, int indexOffset)
		{
			mSection = section;
			mSectionItem = sectionItem;
			mIndexOffset = indexOffset;
		}
	}

	public class SectionItemIterator implements Iterator<SectionItem>
	{
		private int mIndex;
		public boolean hasNext()
		{
			Tuple tuple = getTuple(mIndex);
			if (tuple != null && tuple.mSectionItem == null)
			{
				mIndex++;
				return hasNext();
			}
			return tuple != null;
		}

		public SectionItem next()
		{
			SectionItem result = getTuple(mIndex++).mSectionItem;
			if (result == null)
			{
				result = getTuple(mIndex++).mSectionItem;
			}
			return result;
		}

		public void remove()
		{
			assert(mIndex > 0);
			Tuple tuple = getTuple(--mIndex);
			tuple.mSection.remove(mIndex-tuple.mIndexOffset);
		}
	}

	private int							mSectionId;
	private int							mSectionItemId;
	private final Map<Object, Section>	mSections;
	public SectionedListAdapter(Context context)
	{
		mSections = new HashMap<Object, Section>();
		mSectionId = View.NO_ID;
		mSectionItemId = View.NO_ID;
	}

	public Section addSection(Object key, Section section)
	{
		assert !mSections.containsKey(key);
		mSections.put(key, section);
		return section;
	}

	public Section getSection(Object key)
	{
		return mSections.get(key);
	}

	public void removeSection(Object key)
	{
		mSections.remove(key);
	}

	public int getCount()
	{
		int result = 0;
		for (Section section : mSections.values())
		{
			if (section.isOpen())
			{
				result += section.size();
			}
			result += 1;
		}
		return result;
	}

	public Object getItem(int id)
	{
		Tuple tuple = getTuple(id);
		return tuple.mSectionItem != null ? tuple.mSectionItem : tuple.mSection;
	}

	public Tuple getTuple(int id)
	{
		int offset = 0;
		for (Section section : mSections.values())
		{
			if (id-- == 0)
			{
				return new Tuple(section, null, offset);
			}
			offset++;
			if (section.isOpen())
			{
				if (id < section.size())
				{
					return new Tuple(section, section.get(id), offset);
				}
				id -= section.size();
				offset += section.size();
			}
		}
		return null;
	}

	public long getItemId(int arg0)
	{
		return arg0;
	}

	public Iterator<SectionItem> iterator()
	{
		return new SectionItemIterator();
	}

	final public View getView(int position, View covertView, ViewGroup arg2)
	{
		Tuple tuple = getTuple(position);
		if (tuple.mSectionItem != null)
		{
			return _getSectionItemView(tuple.mSectionItem, covertView);
		}
		return _getSectionView(tuple.mSection, covertView);
	}

	protected View _getSectionView(Section section, View covertView)
	{
		if (covertView != null && covertView.getId() != mSectionId)
		{
			covertView = null;
		}
		View result = getSectionView(section, covertView);
		if (mSectionId == View.NO_ID)
		{
			mSectionId = result.getId();
			assert(mSectionId != View.NO_ID); //section layout must have id
		}
		return result;
	}

	protected View _getSectionItemView(SectionItem sectionItem, View covertView)
	{
		if (covertView != null && covertView.getId() != mSectionItemId)
		{
			covertView = null;
		}
		View result = getSectionItemView(sectionItem, covertView);
		if (mSectionItemId == View.NO_ID)
		{
			mSectionItemId = result.getId();
			assert(mSectionItemId != View.NO_ID); //section layout must have id
		}
		return result;
	}

	protected abstract View getSectionView(Section section, View covertView);
	protected abstract View getSectionItemView(SectionItem section, View covertView);
}
