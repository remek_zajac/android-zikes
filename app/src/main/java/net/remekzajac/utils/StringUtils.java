//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.utils;

import android.annotation.SuppressLint;

public class StringUtils {
	private static final String[] KSStorageUnits = {"kB", "MB", "GB", "TB"};
	private static final int KSStorageUnitsArraySize = 4;
	private static final int KStorageUnitLimitDisplay = 1024;
	private static final double KKm2Miles = 0.621371192;
	private static final double KMetres2Feet = 3.2808399;

	public enum DistanceUnits {
		EMetric,
		EImperial,
	}

	final private DistanceUnits mUnitType;

	public StringUtils(DistanceUnits type) {
		mUnitType = type;
	}

	@SuppressLint("DefaultLocale")
	static public class ReadableDiastance {
		public String unit;
		public double distance;

		public ReadableDiastance(double aDistance, String aUnit) {
			unit = aUnit;
			distance = aDistance;
		}

		public String toString(String separator) {
			if (distance > 10.0) {
				return String.format(java.util.Locale.US, "%.0f%s%s", distance, separator, unit);
			} else {
				return String.format(java.util.Locale.US, "%.1f%s%s", distance, separator, unit);
			}
		}

		public String toString() {
			return toString("");
		}
	}

	public static String kBtoString(long kB) {
		int i = 0;
		long previous = 0;
		for (; i < KSStorageUnitsArraySize && kB > KStorageUnitLimitDisplay; i++) {
			previous = kB;
			kB /= KStorageUnitLimitDisplay;
		}
		return String.format(
			java.util.Locale.US,
			"%d.%d%s", kB, ((int) previous % KStorageUnitLimitDisplay) / (KStorageUnitLimitDisplay / 10), KSStorageUnits[i]
		);
	}

	public ReadableDiastance metres(long m) {
		if (mUnitType == DistanceUnits.EMetric) {
			return metresToMetric(m);
		} else {
			return metresToImperial(m);
		}
	}

	public ReadableDiastance metresToMetric(long m) {
		if (m > 1000) {
			return new ReadableDiastance(m / 1000.0, "km");
		} else {
			return new ReadableDiastance(m, "m");
		}
	}

	public ReadableDiastance metresToImperial(long m) {
		if (m > 1000 / KMetres2Feet) {
			return new ReadableDiastance((double) (m / 1000 * KKm2Miles), "m");
		} else {
			return new ReadableDiastance((double) (m * KMetres2Feet), "ft");
		}
	}

	static public String bigNumberToString(Long number) {
		if (number < 1000) {
			return number.toString();
		} else if (number < 1000000) {
			return new Long(number / 1000).toString() + "K";
		}
		return new Long(number / 1000000).toString() + "M";
	}
}
