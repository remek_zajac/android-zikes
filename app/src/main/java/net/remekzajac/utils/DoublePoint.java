//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.utils;

public class DoublePoint extends Double2Tuple
{
	final static DoublePoint KZero = new DoublePoint(0,0);
	public DoublePoint(double x, double y)
	{
		super(x, y);
	}
	public DoublePoint(DoublePoint from)
	{
		super(from.mX, from.mY);
	}


	public double distanceFrom(DoublePoint point) {
		return Math.sqrt(distanceSquaredFrom(point));
	}

	public double distanceSquaredFrom(DoublePoint point) {
		double dx = mX - point.mX;
		double dy = mY - point.mY;
		return (dx*dx)+(dy*dy);
	}

	public double axisDistanceFrom(DoublePoint point) {
		return axisDistanceFrom(point.x(), point.y());
	}

	public double axisDistanceFrom(double x, double y) {
		return Math.max(Math.abs(mX - x), Math.abs(mY - y));
	}

	public DoublePoint add(DoubleVector vector) {
		return new DoublePoint(
			mX + vector.x(),
			mY + vector.y()
		);
	}

	public DoublePoint add(IntegerVector vector) {
		return new DoublePoint(
			mX + vector.x(),
			mY + vector.y()
		);
	}

	public DoublePoint rotate(double angle, DoublePoint against) {
		DoubleVector betweenVector = new DoubleVector(this, against);
		DoubleVector rotateVector = betweenVector.reverse().rotate(angle);
		return this.add(betweenVector).add(rotateVector);
	}
}
