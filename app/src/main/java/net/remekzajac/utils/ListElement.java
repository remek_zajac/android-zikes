//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.utils;


public class ListElement<T extends ListElementBase> extends ListElementBase
{
    protected ListElement() {
        super();
    }

    protected ListElement(ListElement<T> prev,
                          ListElement<T> next) {
        super(prev, next);
    }

	@SuppressWarnings("unchecked")
	public T append(T elem)
	{
		return (T)super.baseappend(elem);
	}

	@SuppressWarnings("unchecked")
	public T insert(T elem)
	{
		return (T)super.baseinsert(elem);
	}

	@SuppressWarnings("unchecked")
	public T remove(RemovalOutcome outcome)
	{
		return (T)super.baseremove(outcome);
	}

	@Override
	@SuppressWarnings("unchecked")
	public T next()
	{
		return (T)super.next();
	}

	@Override
	@SuppressWarnings("unchecked")
	public T prev()
	{
		return (T)super.prev();
	}

	@Override
	@SuppressWarnings("unchecked")
	public T next(int step)
	{
		return (T)super.next(step);
	}

	@Override
	@SuppressWarnings("unchecked")
	public T prev(int step)
	{
		return (T)super.prev(step);
	}

	@Override
	@SuppressWarnings("unchecked")
	public T head()
	{
		return (T)super.head();
	}

	@Override
	@SuppressWarnings("unchecked")
	public T tail()
	{
		return (T)super.tail();
	}
}
