//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.remekzajac.maps;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import net.zikes.OSMMapsApplication;
import net.zikes.SettingsActivity;
import net.remekzajac.tracks.GeoPOIRegister;
import net.remekzajac.tracks.GeoPOIRegister.POI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

public class GeoSearch {

	public interface Observer
	{
		public void finished(GeoPOIRegister result);
	}

	private final Context mContext;
	public GeoSearch(Context context)
	{
		mContext = context;
	}

	public SearchRequest newRequest(String searchString)
	{
		SearchRequest request = null;
		if (searchString.length() > 0)
		{
			//Construct URL
			String urlStr = OSMMapsApplication.instance().preferences().pref_geoSearchServerUrl.get()
				+Uri.encode(searchString.replace(" ", ","));
			try {
				request = new SearchRequest(new URL(urlStr));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return request;
	}

	public class SearchRequest extends AsyncTask<Void, Void, GeoPOIRegister>
	{
		private Observer 	mObserver;
		private final URL	mURL;
		public void execute(Observer observer)
		{
			mObserver = observer;
			super.execute();
		}

		private SearchRequest(URL url)
		{
			mURL = url;
		}

		@Override
		protected void onPostExecute(GeoPOIRegister results)
		{
			mObserver.finished(results);
		}

		private POI poiFromJSON(JSONObject poiJSON) throws JSONException {
			JSONObject viewport = poiJSON.getJSONObject("geometry").getJSONObject("viewport");
			GeoDegPoint.Bounds bounds = new GeoDegPoint.Bounds(
				new GeoDegPoint(
					viewport.getJSONObject("northeast").getDouble("lng"),
					viewport.getJSONObject("northeast").getDouble("lat")
				),
				new GeoDegPoint(
					viewport.getJSONObject("southwest").getDouble("lng"),
					viewport.getJSONObject("southwest").getDouble("lat")
				)
			);
			return new POI(bounds, poiJSON.getString("formatted_address"), null);
		}

		@Override
		protected GeoPOIRegister doInBackground(Void... arg0) {
			GeoPOIRegister result = null;
	    	InputStream input = null;
	        try {
	            URLConnection connection = mURL.openConnection();
	            connection.connect();
	            input = new BufferedInputStream(mURL.openStream());
	            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
	            StringBuilder sb = new StringBuilder();
	            String line = null;
	            while ((line = reader.readLine()) != null) {
	                sb.append(line + "\n");
	            }

	            JSONObject responseJSON = new JSONObject(sb.toString());
	            if (responseJSON.getString("status").equals("OK")) {
		            JSONArray resultsJSON = responseJSON.getJSONArray("results");
		            if (resultsJSON != null) {
		            	result = new GeoPOIRegister(mContext);
		            	for (int i = 0; i < resultsJSON.length(); i++) {
		            		result.append(poiFromJSON(resultsJSON.getJSONObject(i)));
		            	}
		            }
	            }
	        }
	        catch (Exception e)
	        {
	        	e.printStackTrace();
	        	return null;
	        }
	        finally
	        {
	        	try
	        	{
		            if (input != null)
		            {
		            	input.close();
		            }
	        	} catch (IOException e)
	        	{
					e.printStackTrace();
				}
	        }
	        return result;
		}
	}
}
