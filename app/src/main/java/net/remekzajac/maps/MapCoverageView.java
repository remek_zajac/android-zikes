//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.remekzajac.maps;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Region;
import android.os.Handler;
import android.util.Log;

import net.remekzajac.Geo.Topo;
import net.remekzajac.utils.IntegerPoint;
import net.remekzajac.utils.IntegerVector;
import net.zikes.OSMMapsApplication;

import java.util.ArrayList;

public class MapCoverageView extends MapView.SubView {

    private final Paint mPaint = new Paint();

    private Path mScreenPath;
    private Path mCroppedRegionsPath;
    private Path mFullRegionsPath;
    private GeoDegPoint mPrevMapCentre;
    private Region mRegion;

    public MapCoverageView(MapView parent) {
        super(parent);
        mPaint.setColor(Color.RED);
        mPaint.setAlpha(64);
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        setVisible(true);
    }

    private Path prepareRegions(int zoom) {
        Topo.Point[][] coverage = OSMMapsApplication.instance().authenticatedBackend().serverMeta().coverage();
        if (coverage == null) {
            return null;
        }

        int minPixSqrdDistance = (zoom+1) * 10;
        minPixSqrdDistance *= minPixSqrdDistance;
        Path result = new Path();
        for (Topo.Point[] region : coverage) {
            IntegerPoint prevPix = null;
            for (Topo.Point point : region) {
                IntegerPoint pix = referenceMap().geo2pix(
                    new GeoDegPoint(point.mLon, point.mLat)
                );
                if (prevPix == null) {
                    result.moveTo(pix.x(), pix.y());
                    prevPix = pix;
                } else {
                    IntegerVector fromPrevious = new IntegerVector(prevPix, pix);

                    if (fromPrevious.lengthSquared() > minPixSqrdDistance) {
                        result.lineTo(pix.x(), pix.y());
                        prevPix = pix;
                    }
                }
            }
            result.close();
        }
        return result;
    }

    @Override
    public void doInvalidate(final int bWhatsChangedVector) {
        mCroppedRegionsPath.rewind();
        if (mFullRegionsPath == null || (bWhatsChangedVector & ReferenceMap.MapViews.BZoom) != 0 ) {
            mFullRegionsPath = prepareRegions(referenceMap().getTraits().mZoom);
            if (mFullRegionsPath == null) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        mParent.invalidate(0);
                    }}, 1000);
                return;
            }
        } else if ((bWhatsChangedVector & ReferenceMap.MapViews.BScale) != 0) {
            return;
        } else if ((bWhatsChangedVector & ReferenceMap.MapViews.BPan) != 0) {
            IntegerVector pixMoveBy = new IntegerVector(
                referenceMap().getTraits().mMapPixBounds.mapCentre(),
                referenceMap().geo2pix(mPrevMapCentre)
            );
            mFullRegionsPath.offset(pixMoveBy.x(), pixMoveBy.y());
        }
        mCroppedRegionsPath.addPath(mScreenPath);
        mCroppedRegionsPath.addPath(mFullRegionsPath);
        mCroppedRegionsPath.op(mScreenPath, Path.Op.INTERSECT);
        mPrevMapCentre = referenceMap().getTraits().mGeoCentre;
    }


    @Override
    public void draw(Canvas canvas) {
        canvas.drawPath(mCroppedRegionsPath, mPaint);
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onCreate() {
        Rect mapPixBounds = referenceMap().getTraits().mMapPixBounds.mapRect();
        mPrevMapCentre = referenceMap().getTraits().mGeoCentre;
        mScreenPath = new Path();
        mScreenPath.moveTo(mapPixBounds.left, mapPixBounds.top);
        mScreenPath.lineTo(mapPixBounds.right, mapPixBounds.top);
        mScreenPath.lineTo(mapPixBounds.right, mapPixBounds.bottom);
        mScreenPath.lineTo(mapPixBounds.left, mapPixBounds.bottom);
        mScreenPath.close();
        mCroppedRegionsPath = new Path();
        mCroppedRegionsPath.setFillType(Path.FillType.EVEN_ODD);
    }
}
