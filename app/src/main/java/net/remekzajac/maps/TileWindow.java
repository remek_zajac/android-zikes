//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.maps;

import net.remekzajac.utils.CenteredIndexRangeWindow;
import net.remekzajac.utils.HopScotchRangeIterator;
import net.remekzajac.utils.IntegerPoint;
import net.remekzajac.utils.IntegerRange;
import net.remekzajac.utils.IntegerVector;

import android.graphics.Canvas;
import android.graphics.Rect;

/*
 * A TileWindow object maintains and draws a matrix of map tail bitmaps for a given zoom
 * and geo centre. The geo centre is mutable as, during a pan, the TileWindow can typically reuse
 * many of tiles it currently maintains. Conversely; no tile can be reused during a re-zoom
 * hence zoom is immutable (create a new TileWindow object instead).
 *
 * There are two public operations that ZoomPlane implements:
 * * set  - given the new geo centre, the TileWindow repositions itself for that new centre
 *          trying to reuse as many tile bitmaps as possible.
 * * draw - given a Canvas, draw the currently managed tile bitmaps with reference to the
 *          previously set map geo centre.
 */
public class TileWindow extends CenteredIndexRangeWindow<CenteredIndexRangeWindow<IITileFactory.Tile> >
						implements IITileFactory.Tile.Callback
{
	public interface Callback
	{
		public void callback(IITileFactory.Tile tile, Rect dirty);
	}

	private static final int 	KTileWindowMargin = 1;
	private final IITileFactory mTileFactory;
	private final IntegerPoint 	mNativeTileSize;
	private final IntegerPoint 	mPixCentre;
	private final int 			mWindowSize; 		//single dimension
	private final int 			mWindowSizeSquared;	//both dimensions
	private Callback 			mCallback;
	private GeoDegPoint 		mGeoCentre;
	private final int 			mZoom;
	private IntegerPoint 		mPixOffset;
	private int 				mOutstandingRequests;
	private final IntegerRange 	mIndexLimits;
	private float				mScale;
	private IntegerPoint		mTopLeft;
	private IntegerPoint		mTileDimensions;
	public TileWindow(GeoDegPoint geoCentre, int zoom, float nativeScale, IITileFactory tileFactory, Rect pixMapRect, Callback callback)
	{
		this(
				geoCentre,
				tileFactory.deg2num(geoCentre, zoom),
				zoom,
				new IntegerRange(0, tileFactory.maxTiles(zoom)-1),
				windowSize(pixMapRect, tileFactory.nativeTileSize(), nativeScale),
				tileFactory,
				new IntegerPoint(pixMapRect.width()/2, pixMapRect.height()/2),
				callback);

	}

	static private int windowSize(Rect pixMapRect, IntegerPoint nativeTileSize, float nativeScale)
	{
		int tileSpan = (int)Math.max(pixMapRect.width()/(nativeTileSize.x()*nativeScale), pixMapRect.height()/(nativeTileSize.y()*nativeScale));
		return (tileSpan - tileSpan%2 + 1)+2*KTileWindowMargin; //make it odd as our drawing algorithm depends on it assuming offset from the centre.
	}

	private TileWindow(
			GeoDegPoint geoCentre,
			IntegerPoint[] tileCentre,
			int zoom,
			IntegerRange indexLimits,
			int windowSize,
			IITileFactory tileFactory,
			IntegerPoint pixScreenCentre,
			Callback callback)
	{
		super(windowSize, tileCentre[0].y(), indexLimits);
		mWindowSize = windowSize;
		mWindowSizeSquared = mWindowSize*mWindowSize;
		mIndexLimits = indexLimits;
		mZoom = zoom;
		mTileFactory = tileFactory;
		mNativeTileSize = mTileFactory.nativeTileSize();
		mPixCentre = pixScreenCentre;
		mCallback = callback;
	}

	public int percentReady()
	{
		return (100*(mWindowSizeSquared-mOutstandingRequests))/mWindowSizeSquared;
	}

	public int zoom()
	{
		return mZoom;
	}

	private IntegerPoint pixTopLeft(float scale, IntegerPoint tileSize)
	{
		if (getFirst() != null)
		{
			return new IntegerPoint(
	                mPixCentre.x() - (getFirst().leftToCentre()*tileSize.x())+(int)(mPixOffset.x()*scale),
	                mPixCentre.y() - (leftToCentre()*tileSize.y())+(int)(mPixOffset.y()*scale));
		}
		return null;
	}

	private IntegerPoint tileDimensions(float scale)
	{
		return new IntegerPoint((int)(mNativeTileSize.x()*scale), (int)(mNativeTileSize.y()*scale));
	}

	public void set(GeoDegPoint geoCentre, float scale, boolean request)
	{
		if (scale != mScale)
		{
			mTileDimensions = tileDimensions(scale);
		}

		if (mGeoCentre != geoCentre)
		{
			mGeoCentre = geoCentre;
			IntegerVector[] newTileCentre = mTileFactory.deg2num(mGeoCentre, mZoom);
			mPixOffset = newTileCentre[1].reverse();
			if (!(getCentre() != null && getCentreIndex() == newTileCentre[0].y() && getCentre().getCentreIndex() == newTileCentre[0].x())) {
				//we moved far enough
				set(newTileCentre[0], request);
			}
			mTopLeft = pixTopLeft(mScale = scale, mTileDimensions);
		}
		else if (scale != mScale)
		{
			mTopLeft = pixTopLeft(mScale = scale, mTileDimensions);
		}
	}

	private void set(IntegerPoint tileCentre, boolean request)
	{
		set(tileCentre.y());
		for (HopScotchRangeIterator y = new HopScotchRangeIterator(rangeCapacity()); !y.isEnd(); y.next())
		{
			CenteredIndexRangeWindow<IITileFactory.Tile> xTileWindowForZoom = get(y.get());
			if (xTileWindowForZoom == null && request)
			{
				xTileWindowForZoom = assign(y.get(),
						new CenteredIndexRangeWindow<IITileFactory.Tile>(
								mWindowSize,
								tileCentre.x(),
								mIndexLimits));
			}
			else if (xTileWindowForZoom != null)
			{
				xTileWindowForZoom.set(tileCentre.x());
			}
			if (request)
			{
				for (HopScotchRangeIterator x = new HopScotchRangeIterator(xTileWindowForZoom.rangeCapacity()); !x.isEnd(); x.next())
				{
					IITileFactory.Tile tile = xTileWindowForZoom.get(x.get());
					if (tile == null)
					{
						tile = xTileWindowForZoom.assign(x.get(), mTileFactory.createTile(x.get(), y.get(), mZoom));
					}
					mOutstandingRequests += tile.request(this) ? 0 : 1;
				}
			}
		}
	}

	public IITileFactory.Tile get(IntegerPoint pixxy)
	{
		if (getFirst() != null)
		{
			CenteredIndexRangeWindow<IITileFactory.Tile> xRow = get(this.getCentreIndex() - (mPixCentre.y()-pixxy.y()+(int)(mPixOffset.y()*mScale))/mTileDimensions.y());
			if (xRow != null)
			{
				return xRow.get(xRow.getCentreIndex() - (mPixCentre.x()-pixxy.x()+(int)(mPixOffset.x()*mScale))/mTileDimensions.x());
			}
		}
		return null;
	}

	public void draw(Canvas canvas, boolean showInfo)
	{
		if (getFirst() != null)
		{
			draw(canvas, mTopLeft, mTileDimensions, showInfo);
		}
	}

	private void draw(Canvas canvas, IntegerPoint pixTopLeft, IntegerPoint tileSize, boolean showInfo)
	{
		IntegerRange yRange = rangeCapacity();
		IntegerRange xRange = getFirst().rangeCapacity();
		int pixy = pixTopLeft.y();
		for (int tiley = yRange.smaller(); tiley <= yRange.bigger(); tiley++)
		{
			int pixx = pixTopLeft.x();
			CenteredIndexRangeWindow<IITileFactory.Tile> xTileWindow = get(tiley);
			if (xTileWindow != null)
			{
				for (int tilex = xRange.smaller(); tilex <= xRange.bigger(); tilex++)
				{
					IITileFactory.Tile tile = xTileWindow.get(tilex);
					Rect rect = new Rect(pixx, pixy, pixx+tileSize.x(), pixy+tileSize.y());
					if (tile != null)
					{//tile can be null when drawing partially composed TileWindow.
					 //this can happen after panAndZoom for the previous zoom (best effort drawing).
						tile.draw(canvas, rect, showInfo);
					}
					pixx += tileSize.x();
				}
			}
			pixy += tileSize.y();
		}
	}

	public void callback(IITileFactory.Tile tile)
	{
		//calculate the invalidated Rect
		int yTileOffsetFromTopLeft = tile.tileXY().y() - rangeCapacity().smaller();
		int xTileOffsetFromTopLeft = tile.tileXY().x() - getFirst().rangeCapacity().smaller();
		int pixxOffsetFromTopLeft = mTopLeft.x() + xTileOffsetFromTopLeft*mTileDimensions.x();
		int pixyOffsetFromTopLeft = mTopLeft.y() + yTileOffsetFromTopLeft*mTileDimensions.y();

		--mOutstandingRequests;
		mCallback.callback(tile, new Rect(pixxOffsetFromTopLeft,
										  pixyOffsetFromTopLeft,
										  pixxOffsetFromTopLeft+mTileDimensions.x(),
										  pixyOffsetFromTopLeft+mTileDimensions.y()));
	}
}
