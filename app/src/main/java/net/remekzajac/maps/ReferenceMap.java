//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.maps;
import net.remekzajac.utils.IntegerPoint;
import net.remekzajac.utils.IntegerRange;
import net.remekzajac.utils.IntegerVector;
import android.graphics.Rect;
import android.location.Location;

public interface ReferenceMap
{
	public interface MapViews {
	    public static final int BPan          		= 1 << 0; // 1
	    public static final int BRotation	        = 1 << 1; // 2
	    public static final int BZoom			    = 1 << 2; // 4
	    public static final int BScale				= 1 << 3; // 8
	    public static final int BLocation			= 1 << 5; // 16
	    public static final int BForce				= 0xFF;

		public void invalidate(int bWhatsChangedVector);
		public MapPixBounds mapPixBounds();
	}

	public interface Rotation {
		public float 	previous();
		public float 	current();
		public void 	complete(float reachedAngle);
		public boolean 	pending();
	}

	public class MapPixBounds {
		private final Rect mMap;
		private final Rect mScreen;
		private final IntegerPoint mMapCentre;
		public final IntegerVector mScreen2Map;
		public final IntegerVector mMap2Screen;

		public MapPixBounds(IntegerPoint screenDim, IntegerPoint mapDim) {
			int dwidth = mapDim.x()-screenDim.x();
			int dheight = mapDim.y()-screenDim.y();
			mMap2Screen = new IntegerVector(dwidth/2, dheight/2);
			mScreen2Map = mMap2Screen.reverse();
			mMap = new Rect(0,0,mapDim.x(), mapDim.y());
			mMapCentre = new IntegerPoint(mMap.centerX(), mMap.centerY());
			mScreen = new Rect(0,0,screenDim.x(), screenDim.y());
			mScreen.offset(mMap2Screen.x(), mMap2Screen.y());
		}

		public Rect mapRect() {
			return mMap;
		}

		public Rect screenRect()
		{
			return mScreen;
		}

		public IntegerPoint mapCentre()
		{
			return mMapCentre;
		}
	}

	public abstract static class MapTraits {
		/*
		 *   I                  map width                    I
		 * 0 I       I         screen width         I        I
		 *   ooooooooooooooooooooooooooooooooooooooooooooooooo
		 *   o                                               o
		 *   o       oooooooooooooooooooooooooooooooo        o
		 *   o       o								o        o
		 *   o       o								o        o
		 *   o       o								o        o
		 *   o       o								o        o
		 *   o       o								o        o
		 *   o       o								o        o
		 *   o       o								o        o
		 *   o       o								o        o
		 *   o       o								o        o
		 *   o       o								o        o
		 *   o       o								o        o
		 *   o       o								o        o
		 *   o       o								o        o
		 *   o       o								o        o
		 *   o       o								o        o
		 *   o       o								o        o
		 *   o       o								o        o
		 *   o       o								o        o
		 *   o       oooooooooooooooooooooooooooooooo        o
		 *   o                                               o
		 * m ooooooooooooooooooooooooooooooooooooooooooooooooo
		 *
		 * The map is always square and it represents the reference
		 * pix coordinate system, which contains the screen (the screen
		 * is slightly smaller to allow for presenting full content during
		 * rotation).
		 */
		public GeoDegPoint 					mGeoCentre;
		public float						mScale;
		public int							mZoom;
		public IntegerRange 				mZoomRange;
		public MapPixBounds					mMapPixBounds;
		public abstract Rotation			rotation();
		public abstract GeoDegPoint.Bounds	screenGeoBounds();
	}

	public interface Request {
		public Request pan(GeoDegPoint newCentre);
		public Request pan(Location newCentre);
		public Request pan(IntegerPoint newCentre);
		public Request zoom(int newZoom);
		public Request dzoom(int zoomDelta);
		public Request scale(float newScale);
		public Request rotation(float newRotation);
		public Request region(GeoDegPoint.Bounds bounds);
		public Request notify(int whatsAlsoChanged);
		public void execute();
	}

	public Request 			newRequest();
	public void				continuedPanAndScale(GeoDegPoint newCentre, float scale);
	public void				finishPanAndScale();

	public MapTraits		getTraits();
	public void 			invalidate(int bWhatsChangedVector); //TODO could also supply a Rect regional variant.
	public void 			newView(MapViews views); //will replace the previous views.
	public boolean			isShown();

	public GeoDegPoint 		pix2geo(IntegerPoint pix);
	public IntegerPoint 	geo2pix(GeoDegPoint location);
	public IntegerPoint		geo2pix(Location location);
	public int				mts2Pix(int distanceInMts);
	public long				pix2Mts(long distanceInPix);	//TODO: see when this can wrap.

	public float			zoom2Scale(int zoom);
}
