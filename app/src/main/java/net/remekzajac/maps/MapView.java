//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.maps;


import java.util.Vector;

import net.zikes.OSMMapsApplication;
import net.remekzajac.utils.IntegerPoint;
import net.remekzajac.utils.IntegerVector;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;


public class MapView extends View implements OnTouchListener {
	public static abstract class SubView
	{
		protected MapView mParent;
		protected boolean mVisible;
		protected SubView(MapView parent) {
			mParent = parent;
			mParent.mSubViews.add(this);
		}

		public abstract void doInvalidate(int bWhatsChangedVector);
		public abstract void draw(Canvas canvas);
		public abstract void onDestroy();
		public abstract void onCreate();


		public void animationStarts() {}

		public void animationStops() {}

		public Context getContext()
		{
			return mParent.getContext();
		}

		public void setVisible(boolean visible) {
			mVisible = visible;
			if (mParent.isShown()) {
				invalidate();
			}
		}

		public boolean isShown() {
			return mVisible;
		}

		public ReferenceMap referenceMap() {
			return mParent.mReferenceMap;
		}

		public void invalidate(Rect rect) {
			if (mVisible) {
				doInvalidate(ReferenceMap.MapViews.BForce);
			}
			mParent.invalidate(rect);
		}

		public void invalidate() {
			if (mVisible) {
				doInvalidate(ReferenceMap.MapViews.BForce);
			}
			mParent.invalidate();
		}

		public void invalidate(int whatsChangedVector) {
			if (mVisible) {
				doInvalidate(whatsChangedVector);
			}
			mParent.invalidate();
		}
	}


	private static final int	KPanTolerancePix	 		= 10;
	private static final int	KDoubleClickMaxIntervalMs 	= 500;
	public static final int		KAnimationSpeedMs	 		= 1000;
	private IntegerPoint[] 		mPreviousTouchLocation;
	private long				mPreviousSingleClickMs;
	private boolean 			mLongPressConsumed;
	private boolean				mIgnoreMovement;
	private ReferenceMap 		mReferenceMap;
	private Vector<SubView>		mSubViews;
	public MapView(Context context, AttributeSet attrs) {
		super(context, attrs);
	    setOnTouchListener(this);
	    mReferenceMap = OSMMapsApplication.instance().referenceMap();
	    mPreviousTouchLocation = new IntegerPoint[2];
	    mSubViews = new Vector<SubView>();
	    mSubViews.add(new TileMapView(this));

	    setOnLongClickListener(new OnLongClickListener() {
	        public boolean onLongClick(View v) {
	            return mLongPressConsumed || getLastSingleTouchEventPixLocation() == null;
	        }
	    });
	}

	public void onMapReady() {
		for (SubView subview : mSubViews) {
			if (subview.isShown()) {
				subview.onCreate();
			}
		}
		invalidate(ReferenceMap.MapViews.BForce);
	}

	public IntegerPoint getLastSingleTouchEventPixLocation()
	{
		return mPreviousTouchLocation[1]==null?mPreviousTouchLocation[0]:null;
	}

    public boolean onTouch(View v, MotionEvent event) {
    	if (event.getPointerCount() > 2) {
    		return false;
    	}

		int action = event.getAction();
		int actionCode = action & MotionEvent.ACTION_MASK;
		int pointerId = 0;
		boolean consumed = false;
		long now = System.currentTimeMillis();

		switch (actionCode) {
			case MotionEvent.ACTION_POINTER_DOWN:
				pointerId = action >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
			case MotionEvent.ACTION_DOWN:
	    		mPreviousTouchLocation[pointerId] = new IntegerPoint((int)event.getX(pointerId), (int)event.getY(pointerId));
	    		mIgnoreMovement = false;
				break;
			case MotionEvent.ACTION_POINTER_UP:
				pointerId = 1; //can't have 2 without 1.
			case MotionEvent.ACTION_UP:
				if (event.getPointerCount() == 2) {
					if (mPreviousTouchLocation[1] != null) {
						mIgnoreMovement = true; //hard to lift both fingers at the same time, so block movement for the duration.
					}
				} else {
		    		if (now - mPreviousSingleClickMs < KDoubleClickMaxIntervalMs) {
		    			onDoubleTap(event);
		    		}
		    		mPreviousSingleClickMs = now;
				}
	    		//mPreviousTouchLocation[0] = null; //always preserve the last single touch location (see getLastSingleTouchEventPixLocation())
				mReferenceMap.finishPanAndScale();
	    		mPreviousTouchLocation[1] = null;
	    		mLongPressConsumed = false;
				break;
			case MotionEvent.ACTION_MOVE:
				if (mPreviousTouchLocation[0] != null && !mIgnoreMovement) {
		    		if ( event.getPointerCount() == 1 &&
		    			 Math.max(
		    					 Math.abs(mPreviousTouchLocation[0].x() - (int)event.getX()),
		    				     Math.abs(mPreviousTouchLocation[0].y() - (int)event.getY())) > KPanTolerancePix) {
			    		onPan(event);
			    		mPreviousTouchLocation[0] = new IntegerPoint((int)event.getX(), (int)event.getY());
						consumed = true;
						mLongPressConsumed = true;
			    	} else if (event.getPointerCount() == 2 && mPreviousTouchLocation[1] != null) {
			    		onZoom(event);
			    		mPreviousTouchLocation[0] = new IntegerPoint((int)event.getX(0), (int)event.getY(0));
			    		mPreviousTouchLocation[1] = new IntegerPoint((int)event.getX(1), (int)event.getY(1));
						consumed = true;
						mLongPressConsumed = true;
			    	}
				}
		}

    	return consumed;
      }

    private void onZoom(MotionEvent event) {
    	IntegerPoint newPosition0 = new IntegerPoint((int)event.getX(0), (int)event.getY(0));
    	IntegerPoint newPosition1 = new IntegerPoint((int)event.getX(1), (int)event.getY(1));

    	//calculate re-scaling
    	float scale = (float)newPosition0.distanceFrom(newPosition1)/mPreviousTouchLocation[0].distanceFrom(mPreviousTouchLocation[1]);
    	mReferenceMap.continuedPanAndScale(mReferenceMap.getTraits().mGeoCentre, scale);
    }

    private void onPan(MotionEvent event) {
    	IntegerPoint newPosition = new IntegerPoint((int)event.getX(), (int)event.getY());
    	IntegerVector vector = new IntegerVector(newPosition, mPreviousTouchLocation[0]);
    	if (vector.lengthSquared() > 0) {
    		IntegerPoint pixCentre = mReferenceMap.getTraits().mMapPixBounds.mapCentre().add(vector);
    		mReferenceMap.continuedPanAndScale(mReferenceMap.pix2geo(pixCentre), 1f);
    	}
    }

    private void onDoubleTap(MotionEvent event) {
    	mReferenceMap.newRequest()
    	.pan(new IntegerPoint((int)event.getX(), (int)event.getY()))
    	.dzoom(1)
    	.execute();
    }

	public void invalidate(int bWhatsChangedVector) {
		if (mReferenceMap.getTraits().rotation().pending() ) {
			//Log.d("Rotation",
			//		"Rotating from: "+mReferenceMap.getTraits().rotation().previous()+
			//		" to: "+mReferenceMap.getTraits().rotation().current());
			final float destination = mReferenceMap.getTraits().rotation().current();
			ObjectAnimator rotationAnimation = ObjectAnimator.ofFloat(
					this,
					"rotation",
					mReferenceMap.getTraits().rotation().previous(),
					destination);
			rotationAnimation.setDuration(KAnimationSpeedMs);
			rotationAnimation.start();
			rotationAnimation.addListener(new Animator.AnimatorListener() {
				public void onAnimationCancel(Animator arg0) {}
				public void onAnimationRepeat(Animator arg0) {}
				public void onAnimationStart(Animator arg0) {}
				public void onAnimationEnd(Animator arg0)
				{
					mReferenceMap.getTraits().rotation().complete(destination);
				}
			});
			/*RotateAnimation rotate = new RotateAnimation(
					mReferenceMap.getTraits().mPreviousRotation,
					mReferenceMap.getTraits().mRotation,
					mReferenceMap.getTraits().mScreenBounds.centre().x(),
					mReferenceMap.getTraits().mScreenBounds.centre().y());
			rotate.setDuration(KAnimationSpeedMs);
			rotate.setRepeatCount(0);
			rotate.setFillAfter(true);
			this.startAnimation(rotate);*/
		}

		for (SubView subview : mSubViews) {
			if (subview.isShown()) {
				subview.doInvalidate(bWhatsChangedVector);
			}
		}
		super.invalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		for (SubView subview : mSubViews) {
			if (subview.isShown()) {
				subview.draw(canvas);
			}
		}
	}

	public void onDestroy() {
		for (SubView subview : mSubViews) {
			if (subview.isShown()) {
				subview.onDestroy();
			}
		}
    	this.setVisibility(GONE);
	}


	@Override
	protected void onMeasure (int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		//inflate the map area slightly to allow presenting the whole content during rotation
	    int dimension = Math.max(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.getSize(heightMeasureSpec));
	    dimension *= 1.3f;
		setMeasuredDimension(dimension, dimension);
	}
}
