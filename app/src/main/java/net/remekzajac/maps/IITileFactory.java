//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.maps;

import java.io.Closeable;

import net.remekzajac.utils.DoublePoint;
import net.remekzajac.utils.IntegerPoint;
import net.remekzajac.utils.IntegerRange;
import net.remekzajac.utils.IntegerVector;

import android.graphics.Canvas;
import android.graphics.Rect;

/*
 * This represents an abstraction for a drawable tile and its factory.
 * The application's UI elements can:
 * * Compose a map view arranging for a grid of such tiles;
 * * Request that they're individually retrieved from wherever they live.
 * * When retrieved, draw them within the grid.
 * In addition the factory supplies the essential information necessary to compose
 * a map view, like: the tile bitmap dimensions or the mapping between geographic
 * coordinates and the tile system local coordinate system.
 */
public interface IITileFactory
{
	/*
	 * A drawable tile.
	 */
	public interface Tile extends Closeable
	{
		/*
		 * Coordinates of the tile in the tile-system local coordinate system.
		 */
		public IntegerPoint tileXY();
		public int zoom();

		/*
		 * Draw the tile onto the given Canvas at coordinates and scale given by rect
		 * If showInfo, draw user an indication of the bitmap status (e.g.: whether still
		 * downloading, failed to download, etc).
		 */
		public boolean draw(Canvas canvas, Rect rect, boolean showInfo);

		/*
		 * Request the tile is fetched and prepared for drawing.
		 * The method will call the supplied callback when the tile is ready for drawing.
		 * The method must perform all the expensive/lasting/blocking preparation in
		 * a background thread.
		 */
		public interface Callback
		{
			public void callback(Tile tile);
		}
		public boolean request(Callback callback);
	}

	public Tile createTile(int tileX, int tileY, int forZoom);
	public IntegerPoint nativeTileSize();
	public IntegerVector[] deg2num(GeoDegPoint geoPoint, int zoom);
	public GeoDegPoint num2deg(IntegerPoint tileXY, int zoom);
	public int maxTiles(int zoom);
	public IntegerRange zoomRange();
}
