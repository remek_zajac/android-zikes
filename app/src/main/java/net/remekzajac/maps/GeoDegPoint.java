//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.maps;

import net.remekzajac.Geo.Topo;
import net.remekzajac.utils.DoublePoint;
import net.remekzajac.utils.DoubleRange;
import net.remekzajac.utils.DoubleVector;

import android.location.Location;
import android.util.Log;

//TODO, get rid of this class, use Location instead
public class GeoDegPoint extends DoublePoint {
	public static final double KLatExtrema     = 85.05112877980659; //see Mercator projection: http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
	public static final double KLonExtrema     = 180.0;
	public static final int    KEarthRadiusMts = 6372800;

	public static GeoDegPoint createCappedToExtremas(Double x, Double y) {
		return new GeoDegPoint(
				Math.min(Math.max(x, -KLonExtrema), KLonExtrema),
				Math.min(Math.max(y, -KLatExtrema), KLatExtrema)
		);
	}

	static public GeoDegPoint fromStrings(String latStr, String lonStr) {
		double lat = 0, lon = 0;
		try
		{
			lat = Double.parseDouble(latStr);
			lon = Double.parseDouble(lonStr);
		} catch (Exception e) {
			return null;
		}
		return new GeoDegPoint(lon, lat);
	}

	static public class Bounds {
		private final DoubleRange	mLat;
		private final DoubleRange mLon;
		private GeoDegPoint mNW;
		private GeoDegPoint mSE;
		private GeoDegPoint mCentre;

		public Bounds(double firstLon, double firstLat) {
			mLat = new DoubleRange(firstLat, firstLat);
			mLon = new DoubleRange(firstLon, firstLon);
		}

		public Bounds(GeoDegPoint first)
		{
			this(first.lon(), first.lat());
		}

		public Bounds(Bounds from) {
			mLat = new DoubleRange(from.mLat);
			mLon = new DoubleRange(from.mLon);
		}

		public Bounds(Location first) {
			this(first.getLongitude(), first.getLatitude());
		}

		public Bounds(GeoDegPoint first, GeoDegPoint second) {
			this(first);
			add(second);
		}

		public void add(GeoDegPoint next)
		{
			add(next.lat(), next.lon());
		}

		public void add(long mts) {
			long eachWayMts = mts/2;
			add(nw().offsetMts(eachWayMts, eachWayMts));
			add(se().offsetMts(-eachWayMts, -eachWayMts));
		}

		public void add(Location next)
		{
			add(next.getLatitude(), next.getLongitude());
		}

		public void add(Double lat, Double lon) {
			mLat.add(lat);
			mLon.add(lon);
			reset();
		}

		public void add(Bounds bounds) {
			if (bounds != null) {
				mLat.add(bounds.mLat);
				mLon.add(bounds.mLon);
				reset();
			}
		}

		@Override
		public Bounds clone()
		{
			return new Bounds(this);
		}

		public GeoDegPoint nw() {
			return mNW != null ? mNW : (mNW = new GeoDegPoint(mLon.smaller(), mLat.bigger()));
		}

		public GeoDegPoint se() {
			return mSE != null ? mSE : (mSE = new GeoDegPoint(mLon.bigger(), mLat.smaller()));
		}

		public GeoDegPoint centre() {
			return mCentre != null ? mCentre :
				(mCentre = new GeoDegPoint(nw().x()+(se().x()-nw().x())/2, nw().y()+(se().y()-nw().y())/2));
		}

		public boolean contains(GeoDegPoint geoPoint) {
			return mLat.contains(geoPoint.lat()) && mLon.contains(geoPoint.lon());
		}

		public boolean contains(Bounds bounds) {
			return mLat.contains(bounds.mLat) && mLon.contains(bounds.mLon);
		}

		@Override
		public String toString()
		{
			return nw().toString()+','+se().toString();
		}

		public String toString(int precission) {
			return nw().toString(precission)+','+se().toString(precission);
		}

		private void reset() {
			mNW = null;
			mSE = null;
			mCentre = null;
		}
	}

	public GeoDegPoint(Double lon, Double lat) {
		super(lon, lat);
		assert Math.abs(lat) <= KLatExtrema;
		assert Math.abs(lon) <= KLonExtrema;
	}

	public GeoDegPoint(DoublePoint point) {
		super(point);
		assert Math.abs(point.y()) <= KLatExtrema;
		assert Math.abs(point.x()) <= KLonExtrema;
	}

	public GeoDegPoint(Location location)
	{
		super(location.getLongitude(), location.getLatitude());
	}

	public GeoDegPoint(GeoDegPoint from) {
		super(from.mX, from.mY);
	}

	public Double lon()
	{
		return x();
	}

	public Double lat()
	{
		return y();
	}

	public GeoDegPoint add(DoubleVector v) {
		return new GeoDegPoint(mX +v.x(), mY +v.y());
	}

	public Topo.Point topo() {
		return new Topo.Point(lat(), lon());
	}

	@Override
	public String toString()
	{
		return new String(""+ mY +","+ mX);
	}

	public String toString(int precission) {
		String floatFrmt = "%."+precission+"f";
		return String.format(
			java.util.Locale.US,
			floatFrmt, lat()
		) + ',' + String.format(
			java.util.Locale.US,
			floatFrmt,
			lon()
		);
	}

	public long distanceFromMts(GeoDegPoint from) {
        return (long)haversine(lat(), lon(), from.lat(), from.lon());
	}

	private static double haversine(double lat1, double lon1, double lat2, double lon2) {
		double dLat = Math.toRadians(lat2 - lat1);
		double dLon = Math.toRadians(lon2 - lon1);
		lat1 = Math.toRadians(lat1);
		lat2 = Math.toRadians(lat2);

		double a = Math.pow(Math.sin(dLat / 2),2) + Math.pow(Math.sin(dLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
		double c = 2 * Math.asin(Math.sqrt(a));
		return KEarthRadiusMts * c;
	}

	public double comparableDistanceFrom(GeoDegPoint from) {
		return Math.pow(lon()-from.lon(),2.0)+Math.pow(lat()-from.lat(),2.0);
	}

	public GeoDegPoint offsetMts(long mtsNorth, long mtsWest)
	{
        final double newLat = this.lat() + (mtsNorth / KEarthRadiusMts) * 180 / Math.PI;
        final double newLon = this.lon() + (-mtsWest / (KEarthRadiusMts * Math.cos(newLat * 180 / Math.PI))) * 180 / Math.PI;

        return new GeoDegPoint(newLon, newLat);
	}
}
