//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.maps;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import net.remekzajac.tracks.LocationTracker;
import net.remekzajac.tracks.MultiSegmentTrackDocument;
import net.remekzajac.tracks.TrackGeoPointRegister;
import android.location.Location;
import android.os.Handler;
import android.util.Log;

public class MockLocationProvider {
	public final static double		KphToMps = 1000.0/(60*60);
	private final LocationTracker 	mLocationTracker;
	private final String 			mName;
	private final Timer   			mTimer;
	private float 					mSpeedMps;
	private final int 				mStepMts;
	private final int				mVariationMts;
	private Random 					mRandomGenerator;
	private long                    mTimeStart;
	public MockLocationProvider(String name, LocationTracker locationTracker, int variationMts, int stepMts, int desiredSpeedKph) {
		mLocationTracker = locationTracker;
		mName = name;
		mTimer = new Timer();
		mSpeedMps = (float)KphToMps*desiredSpeedKph;
		mStepMts = stepMts;
		if ((mVariationMts = variationMts) > 0) {
			mRandomGenerator = new Random();
		}
	}

	public void test(MultiSegmentTrackDocument trackDocument) {
		TrackGeoPointRegister.TrackPoint trackPoint = trackDocument.documents().firstElement().geoPoints().head();
		mTimeStart = System.currentTimeMillis();
		test(trackPoint.location(), trackPoint.next());
	}

	public void test(Location currentLocation, TrackGeoPointRegister.TrackPoint nextTrackPoint) {
		testNow(currentLocation, mVariationMts, mSpeedMps);
		if (nextTrackPoint == null) {
			return;
		}
		int distanceMts = (int)currentLocation.distanceTo(nextTrackPoint.location());
		long dtimeMs = nextTrackPoint.location().getTime() - currentLocation.getTime();
		if (dtimeMs != 0) {
			mSpeedMps = Math.max(distanceMts * 1000 / dtimeMs, (float)0.5);
		}
		Location nextLocation = nextTrackPoint.location();
		TrackGeoPointRegister.TrackPoint nextNextTrackPoint = nextTrackPoint.next();
		if (mStepMts > 0 && distanceMts > mStepMts) {
			float factor = (float)mStepMts / distanceMts;
			nextLocation = new Location(mName);
			nextLocation.setLatitude(
				currentLocation.getLatitude()
					+ factor * (nextTrackPoint.location().getLatitude() - currentLocation.getLatitude()));
			nextLocation.setLongitude(
				currentLocation.getLongitude()
					+ factor * (nextTrackPoint.location().getLongitude() - currentLocation.getLongitude()));
			nextLocation.setTime(
				currentLocation.getTime() + (long)(dtimeMs * factor)
			);
			distanceMts = (int)currentLocation.distanceTo(nextLocation);
			nextNextTrackPoint = nextTrackPoint;
		}
		int delayMs = (int)((1000*distanceMts)/mSpeedMps);
		final Location finalNextLocation = nextLocation;
		final TrackGeoPointRegister.TrackPoint finalNextNextTrackPoint = nextNextTrackPoint;
		final Handler handler = new Handler();
		TimerTask timerTask = new TimerTask() {
			@Override
			public void run() {
                handler.post(new Runnable() {
                    public void run() {
						test(finalNextLocation, finalNextNextTrackPoint);
                    }});
			}
		};
		mTimer.schedule(timerTask, delayMs );
	}

	public void testNow(Location _location, int accuracy, float speedMps) {
		Location location = new Location(_location);
		double seedLat = 0d;
		double seedLon = 0d;
		if (mVariationMts > 0)
		{
			float[] oneDegLatMts = new float[1];
			float[] oneDegLonMts = new float[1];

			Location.distanceBetween(
					location.getLatitude(), location.getLongitude(),
					location.getLatitude()+1, location.getLongitude(),
					oneDegLatMts);
			Location.distanceBetween(
					location.getLatitude(), location.getLongitude(),
					location.getLatitude(), location.getLongitude()+1,
					oneDegLonMts);

			double lonRangeEachWay = (double)mVariationMts/oneDegLonMts[0];
			double latRangeEachWay = (double)mVariationMts/oneDegLatMts[0];
			double lon_minus1_plus1_Random = 1-(2*mRandomGenerator.nextDouble());
			double lat_minus1_plus1_Random = 1-(2*mRandomGenerator.nextDouble());

			seedLat = lonRangeEachWay * lon_minus1_plus1_Random;
			seedLon = latRangeEachWay * lat_minus1_plus1_Random;
		}
		location.setLatitude(location.getLatitude()+seedLat);
		location.setLongitude(location.getLongitude()+seedLon);
		location.setAccuracy(accuracy);
		location.setSpeed(speedMps);
		location.setTime(System.currentTimeMillis());
		mLocationTracker.onLocationChanged(location);
	}
}
