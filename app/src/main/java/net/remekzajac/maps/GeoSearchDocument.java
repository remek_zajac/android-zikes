package net.remekzajac.maps;

import net.remekzajac.tracks.GeoPOIRegister;
import net.zikes.OSMMapsApplication;

public class GeoSearchDocument extends OSMMapsApplication.Document
{
	private GeoPOIRegister	mCurrentResult;
	private static final String docName = "OSMMapsApplication.GeoSearchDocument";
	private GeoSearchDocument()
	{
		super(docName);
	}
	
	public static GeoSearchDocument instance()
	{
		GeoSearchDocument result = (GeoSearchDocument)OSMMapsApplication.instance().getDocument(docName);
		return result != null ? result : new GeoSearchDocument();
	}
	
	public void show(GeoPOIRegister register)
	{
		GeoPOIRegister currentResult = mCurrentResult;
		mCurrentResult = register;
		if (activity() != null)
		{
			if (currentResult != null)
			{
				activity().locationIndicatorView().remove(currentResult);
			}
			activity().locationIndicatorView().add(mCurrentResult, true);			
		}
	}


	public void newActivity() 
	{
		if (mCurrentResult != null)
		{
			activity().locationIndicatorView().add(mCurrentResult, false);
		}
	}


	public void clear() 
	{
		if (activity() != null && mCurrentResult != null)
		{
			activity().locationIndicatorView().remove(mCurrentResult);
		}
		super.clear();
		mCurrentResult = null;
	}
}
