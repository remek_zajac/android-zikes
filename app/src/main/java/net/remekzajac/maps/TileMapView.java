//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.maps;

import net.zikes.OSMMapsApplication;
import android.graphics.Canvas;
import android.graphics.Rect;

public class TileMapView  extends MapView.SubView implements TileWindow.Callback
{
	private static final int 	KInvalidateForChange = ReferenceMap.MapViews.BPan |
			   					ReferenceMap.MapViews.BZoom |
			   					ReferenceMap.MapViews.BScale;
	private static final int 	KReplacePreviousWindowWhenCurrentAtLeastSmaller = 5;
	private TileWindow 			mCurrentTileWindow;
	private TileWindow 			mPreviousTileWindow;
	private final ReferenceMap 	mReferenceMap;
	public TileMapView(MapView parent)
	{
		super(parent);
		this.mVisible = true;
		mReferenceMap = OSMMapsApplication.instance().referenceMap();
	}

	@Override
	public void doInvalidate(int bWhatsChangedVector)
	{
		if ((bWhatsChangedVector & KInvalidateForChange) != 0)
		{
			if (mReferenceMap.getTraits().mZoom != mCurrentTileWindow.zoom())
			{
				//zoom has changed
				if (mPreviousTileWindow == null ||
					KReplacePreviousWindowWhenCurrentAtLeastSmaller*mCurrentTileWindow.percentReady() >= mPreviousTileWindow.percentReady())
				{
					//if previous tile window not set, not ready or current window now ready
					//set the previous window to point at the current window.
					mPreviousTileWindow = mCurrentTileWindow;
				}
				mCurrentTileWindow.close();
				//instantiate new current window for the new zoom
				mCurrentTileWindow = new TileWindow(
						mReferenceMap.getTraits().mGeoCentre,
						mReferenceMap.getTraits().mZoom,
						mReferenceMap.getTraits().mScale,
						OSMMapsApplication.instance().tileFactory(),
						mReferenceMap.getTraits().mMapPixBounds.mapRect(),
						this);
			}
			mCurrentTileWindow.set(mReferenceMap.getTraits().mGeoCentre, mReferenceMap.getTraits().mScale, true);
			if (mPreviousTileWindow != null)
			{
				mPreviousTileWindow.set(mReferenceMap.getTraits().mGeoCentre, mReferenceMap.zoom2Scale(mPreviousTileWindow.zoom()), false);
			}
		}
	}

	@Override
	public void draw(Canvas canvas)
	{
		if (mPreviousTileWindow != null && mCurrentTileWindow.percentReady() < 100)
		{
			mPreviousTileWindow.draw(canvas, false);
		}
		mCurrentTileWindow.draw(canvas, true);
	}

	@Override
	public void onDestroy()
	{
		if (mCurrentTileWindow != null)
		{
			mCurrentTileWindow.close();
		}
		if (mPreviousTileWindow != null)
		{
			mPreviousTileWindow.close();
		}
	}

	@Override
	public void onCreate()
	{
		mCurrentTileWindow = new TileWindow(
				mReferenceMap.getTraits().mGeoCentre,
				mReferenceMap.getTraits().mZoom,
				mReferenceMap.getTraits().mScale,
				OSMMapsApplication.instance().tileFactory(),
				mReferenceMap.getTraits().mMapPixBounds.mapRect(),
				this);
	}

	public void callback(IITileFactory.Tile tile, Rect rect)
	{
		if ((mCurrentTileWindow.percentReady() == 100) && mPreviousTileWindow != null)
		{
			mPreviousTileWindow.close();
			mPreviousTileWindow = null;
		}
		super.invalidate();
	}
}
