//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.remekzajac.maps;

import java.util.Iterator;
import java.util.Vector;

import net.remekzajac.tracks.NavigatedDocument;
import net.zikes.OSMMapsApplication;
import net.zikes.R;
import net.zikes.SettingsActivity;
import net.remekzajac.tracks.GeoPOIRegister;
import net.remekzajac.tracks.GeoPOIRegister.POI;
import net.remekzajac.utils.DoubleVector;
import net.remekzajac.utils.IntegerPoint;
import net.remekzajac.utils.IntegerVector;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.location.Location;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class LocationIndicatorView extends MapView.SubView
{
	public interface DisplayedLocation {
		public String name();
		public GeoDegPoint geoLocation();
		public GeoPOIRegister.POI poi();
		public void onCreateContextMenu(MenuInflater inflater, ContextMenu menu);
		public void onContextItemSelected(MenuItem item);
	}

	private abstract class ClickableDisplayedLocation implements DisplayedLocation {
		private final static int 		KClickTolerancePix	= 50;
		protected final IntegerPoint 	mPix;
		public ClickableDisplayedLocation(IntegerPoint pix)
		{
			mPix = pix;
		}

		public boolean click(IntegerPoint pix) {
			return mPix != null ? mPix.distanceFrom(pix) <= KClickTolerancePix : false;
		}
	}

	private class BasicDisplayedLocation extends ClickableDisplayedLocation {
		private final String 		mName;
		private final GeoDegPoint 	mLocation;
		public BasicDisplayedLocation(Location location, IntegerPoint pix, String description) {
			this(new GeoDegPoint(location), pix, description);
		}

		public BasicDisplayedLocation(GeoDegPoint location, IntegerPoint pix, String description) {
			super(pix);
			mName = description;
			mLocation = location;
		}

		public String name()
		{
			return mName;
		}

		public GeoDegPoint geoLocation()
		{
			return mLocation;
		}

		public POI poi()
		{
			return null;
		}

		@Override
		public void onCreateContextMenu(MenuInflater inflater, ContextMenu menu) {
			menu.setHeaderTitle(name());

			inflater.inflate(R.menu.mapview_context, menu);
			MenuItem menuItemNavigateTo = menu.findItem(R.id.menu_navigate_to);
			MenuItem menuItemNavigateVia = menu.findItem(R.id.menu_navigate_via);
			if (NavigatedDocument.instance().getFrom() != null) {
				menuItemNavigateTo.setEnabled(true);
				menuItemNavigateVia.setEnabled(true);
			} else {
				menuItemNavigateTo.setEnabled(false);
				menuItemNavigateVia.setEnabled(false);
			}
		}

		@Override
		public void onContextItemSelected(MenuItem item) {
			switch (item.getItemId()) {
				case R.id.menu_navigate_to:
					if (poi() != null) {
						NavigatedDocument.instance().to(poi());
					} else {
						NavigatedDocument.instance().to(geoLocation());
					}
					break;
				case R.id.menu_navigate_from:
					if (poi() != null) {
						NavigatedDocument.instance().from(poi());
					} else {
						NavigatedDocument.instance().from(geoLocation());
					}
					break;
				case R.id.menu_navigate_via:
					if (poi() != null) {
						NavigatedDocument.instance().via(poi());
					} else {
						NavigatedDocument.instance().via(geoLocation());
					}
					break;
			}
		}
	}

	private class PixPOI extends ClickableDisplayedLocation {
		private final GeoPOIRegister.DrawablePOI mGeoPOI;
		public PixPOI(IntegerPoint pix, GeoPOIRegister.DrawablePOI geoPOI) {
			super(pix);
			mGeoPOI = geoPOI;
		}

		public void drawIcon(Canvas canvas)
		{
			mGeoPOI.drawIcon(canvas, mPix);
		}

		public void drawDescription(Canvas canvas)
		{
			mGeoPOI.drawDescription(canvas, mPix);
		}

		public String name()
		{
			return mGeoPOI.name();
		}

		public GeoDegPoint geoLocation()
		{
			return mGeoPOI.bounds().centre();
		}

		public POI poi()
		{
			return mGeoPOI;
		}


		@Override
		public void onCreateContextMenu(MenuInflater inflater, ContextMenu menu) {
			menu.setHeaderTitle(name());
			MenuItem deleteOfflineTilesMenuItem = menu.add(
				0,R.id.menu_delete_poi, 0, R.string.title_menu_delete_poi
			);
			if (!NavigatedDocument.instance().hasPoint(mGeoPOI)) {
				deleteOfflineTilesMenuItem.setEnabled(false);
			}
		}

		@Override
		public void onContextItemSelected(MenuItem item) {
			if (item.getItemId() == R.id.menu_delete_poi) {
				NavigatedDocument.instance().remove(mGeoPOI);
			}
		}
	}

	private final static int KLocationIndicatorRadiusBase   		= 8;
	private final static int KAccuracyRadiusOpacity 				= 0x40;
	private final static int KIndicatorArrowOpacity 				= 0x80;
	private final static int KMinSpeedToDisplayKph  				= 0;
	private final static int KArrowBaseDistanceFromWindroseCentre 	= 50;
	private final static int KArrowLenght 							= 30;
	private final static int KArrowWidth 							= 30;

	private TextView 				mWindroseTextDisplay;
	private ImageButton 			mWindRoseButton;
	private TextView 				mAlternativeSpeedDisplay;
	private final Bitmap 			mLocationIndicatorGoodBitmap;
	private final Bitmap 			mLocationIndicatorStaleBitmap;
	private final int 				mDrawableIndicatorColour;
	private int 					mPixAccuracyRadius;
	private IntegerPoint 			mLocationIndicatorPosition;
	private IntegerPoint 			mWindrosePixCentre;
	private View 					mWindroseControl;
	private float					mSpeedKph;
	private final Vector<GeoPOIRegister> mPOIRegisters;
	private Vector<PixPOI>			mPOIPixRegister;
	private final int mLocationIndiactorRadius;
	public LocationIndicatorView(MapView parent) {
		super(parent);
		mLocationIndiactorRadius = (int)(
				OSMMapsApplication.instance().preferences().pref_NativeScale.get()
				*KLocationIndicatorRadiusBase);
		int locationIndicatorDiameter = 2*mLocationIndiactorRadius;
		mLocationIndicatorGoodBitmap = Bitmap.createScaledBitmap(
				BitmapFactory.decodeResource(
						getContext().getResources(), R.drawable.locationindicatorgood),
						locationIndicatorDiameter, locationIndicatorDiameter, false);
		mLocationIndicatorStaleBitmap = Bitmap.createScaledBitmap(
				BitmapFactory.decodeResource(
						getContext().getResources(), R.drawable.locationindicatorstale),
						locationIndicatorDiameter, locationIndicatorDiameter, false);
		mDrawableIndicatorColour = getContext().getResources().getColor(R.color.blue);
		mPOIRegisters = new Vector<GeoPOIRegister>();
		mPOIPixRegister = new Vector<PixPOI>();
		this.mVisible = true;
	}

	@Override
	public void onCreate() {
		mWindrosePixCentre = new IntegerPoint(
				(mWindroseControl.getLeft()+mWindroseControl.getRight())/2,
				(mWindroseControl.getTop()+mWindroseControl.getBottom())/2)
				.add(referenceMap().getTraits().mMapPixBounds.mMap2Screen);
	}

	public void setUIControls(View locationControl, TextView alternativeSpeedDisplay) {
		mWindroseControl = locationControl;
		mWindroseTextDisplay = (TextView)mWindroseControl.findViewById(R.id.windroseDisplay);
		mAlternativeSpeedDisplay = alternativeSpeedDisplay;
		mWindRoseButton = (ImageButton)mWindroseControl.findViewById(R.id.windroseButton);
	}

	@Override
	public void draw(Canvas canvas) {
		Paint paint = new Paint();
		if (OSMMapsApplication.instance().locationTracker().warmLocation() != null) {
			if (mLocationIndicatorPosition != null) { //we're visible, show our location
				canvas.drawBitmap(
						OSMMapsApplication.instance().locationTracker().hotLocation()== null?mLocationIndicatorStaleBitmap:mLocationIndicatorGoodBitmap,
								mLocationIndicatorPosition.x()-mLocationIndiactorRadius,
								mLocationIndicatorPosition.y()-mLocationIndiactorRadius, paint);
				if (OSMMapsApplication.instance().locationTracker().hotLocation() != null && mPixAccuracyRadius > 0) {
					paint.setColor(mDrawableIndicatorColour);
					paint.setAlpha(KAccuracyRadiusOpacity);
					canvas.drawCircle(mLocationIndicatorPosition.x(), mLocationIndicatorPosition.y(), mPixAccuracyRadius, paint);
				}
			}
			else if (OSMMapsApplication.instance().referenceMap().getTraits().rotation().current() == 0) { //we're not visible, and not rotated show the arrow on where we are
			  //(showing direction arrow when rotated is difficult to calculate so we don't bother for now .
				paint.setAntiAlias(true);
				paint.setColor(mDrawableIndicatorColour);
				paint.setAlpha(KIndicatorArrowOpacity);

				//(1) Compose the general direction vector from the locationIndicator control to the current location.
				IntegerPoint warmLocationPix = referenceMap().geo2pix(
						OSMMapsApplication.instance().locationTracker().warmLocation());
				IntegerVector arrowDirectionVector = null;
				if (warmLocationPix != null) {
					//current location is closer than KGeoOrPixBasedDirectionBreak times the current geo radius (the currently displayed geo region).
					//We won't risk the pix representation of the current location to go beyond the int range. Using pix coordinates to
					//calculate the direction is better as pix coordinates, as opposed to geo coordinates are Euclidian.
					arrowDirectionVector = new IntegerVector(mWindrosePixCentre, warmLocationPix).withLength(KArrowBaseDistanceFromWindroseCentre);
				} else {
					//current location is further than we can project in pix and and therefore we need to use geo coordinates
					//to determine the direction. Using geo coordinates is inferior as geo coordinates are non-Euclidian,
					//i.e.: the closer no the (north or south) pole, the shorter the distance between the consecutive latitudes.
					//Fortunately, for longer distances this goes away.
					DoubleVector arrowDirectionGeoVector = new DoubleVector(
							referenceMap().pix2geo(mWindrosePixCentre),
							new GeoDegPoint(OSMMapsApplication.instance().locationTracker().warmLocation().getLongitude(),
											OSMMapsApplication.instance().locationTracker().warmLocation().getLatitude()));
					arrowDirectionGeoVector.setLength(KArrowBaseDistanceFromWindroseCentre);
					arrowDirectionVector = new IntegerVector(
							(int)arrowDirectionGeoVector.x(),
							-(int)arrowDirectionGeoVector.y()).rotate(-referenceMap().getTraits().rotation().current());
				}

				//(2) set the arrow root (base). Add the previously calculated direction vector to the centre of the
				//locationTracker control element
				IntegerPoint arrowRoot = mWindrosePixCentre.add(arrowDirectionVector);

				//(3) reuse the same direction vector to set it to denote the arrow's lenght;
				arrowDirectionVector = arrowDirectionVector.withLength(KArrowLenght);

                //(4) compose a new vector perpendicular to the arrow direction vector to calculate the arrow's base points
                //set its lenght to half of the arrow's length (we'll project both sides from the base centre so that
                //the arrow base lenght will be the same as the arrow's lenght.
                IntegerVector arrowBaseDirection = arrowDirectionVector.orthogonal().withLength(KArrowWidth/2);

                //(5) Calculate the arrow's base points.
                IntegerPoint arrowRightCorner = arrowRoot.add(arrowBaseDirection);
                IntegerPoint arrowLeftCorner = arrowRoot.add(arrowBaseDirection.reverse());


                //6 Calculate arrow's tip
                IntegerPoint arrowTip = arrowRoot.add(arrowDirectionVector);

                //7 draw everything
                Path path = new Path();
                path.moveTo(arrowRoot.x(), arrowRoot.y());
                path.lineTo(arrowRightCorner.x(), arrowRightCorner.y());
                path.lineTo(arrowTip.x(), arrowTip.y());
                path.lineTo(arrowLeftCorner.x(), arrowLeftCorner.y());
                path.lineTo(arrowRoot.x(), arrowRoot.y());
                path.close();
                canvas.drawPath(path, paint);
			}
		}

		drawPOIs(canvas);
	}

	private void drawPOIs(Canvas canvas) {
		for (PixPOI poipix : mPOIPixRegister) {
			poipix.drawIcon(canvas);
		}
		for (PixPOI poipix : mPOIPixRegister) {
			poipix.drawDescription(canvas);
		}
	}

	@Override
	@SuppressLint("DefaultLocale")
	public void doInvalidate(int bWhatsChangedVector) {
		mLocationIndicatorPosition = null;
		mWindroseTextDisplay.setText("");
		if (OSMMapsApplication.instance().locationTracker().warmLocation() != null) {
			mPixAccuracyRadius = referenceMap().mts2Pix(
					(int)OSMMapsApplication.instance().locationTracker().warmLocation().getAccuracy());
			//find out if we're visible
			IntegerPoint pixPosition = referenceMap().geo2pix(
					OSMMapsApplication.instance().locationTracker().warmLocation());
			if (pixPosition != null && referenceMap().getTraits().mMapPixBounds.screenRect().contains(pixPosition.x(), pixPosition.y())) {//location indicator is visible
				mLocationIndicatorPosition = pixPosition;
				if (mAlternativeSpeedDisplay.isShown()) {
					mAlternativeSpeedDisplay.setText(String.format(
						java.util.Locale.US, "%.1fkph", mSpeedKph)
					);
				} else if (mSpeedKph > 0) {
					mWindroseTextDisplay.setText(String.format(java.util.Locale.US, "%.1f\nkph", mSpeedKph));
				} else {
					mWindroseTextDisplay.setText("");
				}
			} else {//location indicator is not visible, lets display how far is it from the location control
				GeoDegPoint locationControlGeoPosition = referenceMap().pix2geo(mWindrosePixCentre);
				long distanceInMts = locationControlGeoPosition.distanceFromMts(new GeoDegPoint(
	        		OSMMapsApplication.instance().locationTracker().warmLocation().getLongitude(),
	        		OSMMapsApplication.instance().locationTracker().warmLocation().getLatitude()));
				mWindroseTextDisplay.setText(OSMMapsApplication.instance().stringUtils().metres(distanceInMts).toString("\n"));
			}
		}
		mWindroseTextDisplay.invalidate();
		resetGeoPOIRegisters();

		if (referenceMap().getTraits().rotation().pending()) {
			ObjectAnimator rotationAnimation = ObjectAnimator.ofFloat(
					mWindRoseButton,
					"rotation",
					referenceMap().getTraits().rotation().previous(),
					referenceMap().getTraits().rotation().current());
			rotationAnimation.setDuration(MapView.KAnimationSpeedMs);
			rotationAnimation.start();
		}

		if ((bWhatsChangedVector & ReferenceMap.MapViews.BLocation) != 0) {
			mWindRoseButton.setAlpha(
					OSMMapsApplication.instance().locationTracker().hotLocation() != null ?
							 1f
							:0.5f);
			Location warmLocation = OSMMapsApplication.instance().locationTracker().warmLocation();
			if (warmLocation != null) {
				float speedKph = warmLocation.getSpeed()*3600/1000;
				if (speedKph > KMinSpeedToDisplayKph) {
					mSpeedKph = speedKph;
				}
			}
		}
	}

	public void add(GeoPOIRegister register, boolean panAndZoom) {
		mPOIRegisters.add(register);
		if (panAndZoom) {
			OSMMapsApplication.instance().referenceMap().newRequest().region(register.bounds()).execute();
		} else {
			invalidate();
		}
	}

	public void remove(GeoPOIRegister register) {
		mPOIRegisters.remove(register);
		invalidate();
	}

	private void transferGeoPOIRegister(GeoPOIRegister register) {
		for (Iterator<GeoPOIRegister.DrawablePOI> it = register.iterator(); it.hasNext(); ) {
			GeoPOIRegister.DrawablePOI next = it.next();

			//TODO: this won't hold for large areas and fine zooms, geo2pix won't translate everything into pixels
			mPOIPixRegister.add(
				new PixPOI(
					OSMMapsApplication.instance().referenceMap().geo2pix(next.bounds().centre()),
					next)
			);
		}
	}

	private void resetGeoPOIRegisters() {
		mPOIPixRegister = new Vector<PixPOI>();
		for (GeoPOIRegister r : mPOIRegisters) {
			transferGeoPOIRegister(r);
		}
	}

	public DisplayedLocation click(IntegerPoint clickedPix) {
		for (PixPOI poipix : mPOIPixRegister) {
			if (poipix.click(clickedPix)) {
				return poipix;
			}
		}

		if (OSMMapsApplication.instance().locationTracker().warmLocation() != null) {
			BasicDisplayedLocation currentLocation = new BasicDisplayedLocation(
					OSMMapsApplication.instance().locationTracker().warmLocation(),
					mLocationIndicatorPosition,
					getContext().getResources().getString(R.string.title_menu_your_location));
			if (currentLocation.click(clickedPix)) {
				return currentLocation;
			}
		}

		return new BasicDisplayedLocation(
				OSMMapsApplication.instance().referenceMap().pix2geo(clickedPix),
				clickedPix,
				getContext().getResources().getString(R.string.title_menu_clicked_location));
	}

	@Override
	public void onDestroy() {
	}
}
