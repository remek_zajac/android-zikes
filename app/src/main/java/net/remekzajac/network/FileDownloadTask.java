//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
// 
//********************************************************************************

package net.remekzajac.network;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.os.AsyncTask;

public class FileDownloadTask extends AsyncTask<URL, Integer, Integer> {

    private static final int IO_BUFFER_SIZE = 1024;
	private FileDownloadObserver mObserver;
	private Integer mFileLength;
	private OutputStream mOutputStream;
	private URL mUrl;
	
	public interface FileDownloadObserver {
		public void OnFileDownloadProgress(URL url, int percentComplete, int bytesDownloaded);
	}	

	public FileDownloadTask(OutputStream outputStream, FileDownloadObserver observer) {
		mObserver = observer;
		mOutputStream = outputStream;
	}
	
	public FileDownloadTask(OutputStream outputStream) {
		mOutputStream = outputStream;
	}
	
	public Integer download(URL url)
	{
		mUrl = url;
        int total = 0;
		//Log.d("FileDownloadTask", String.format("Downloading>> %s", mUrl.toString()));
        InputStream input = null;
    	try 
    	{
	    	URLConnection connection = mUrl.openConnection();
	    	connection.connect();
	    	mFileLength = connection.getContentLength();
	    	input = new BufferedInputStream(mUrl.openStream());
	
	        int count = 0;
	        byte data[] = new byte[IO_BUFFER_SIZE];
	        while ((count = input.read(data)) != -1) {
	            total += count;
	            publishProgress(total);
	            mOutputStream.write(data, 0, count);
	        	}
	
	        mOutputStream.flush();
    	} 
    	catch (IOException e) 
    	{
    		total = 0;
    		//Log.d("FileDownloadTask", String.format(" Failed downloading<< %s", mUrl.toString()));  	
    	} 
    	finally
    	{
    		if (input != null)
    		{
    			try 
    			{
					input.close();
				} catch (IOException e) 
				{
					e.printStackTrace();
				}
    		}
    		try 
    		{
				mOutputStream.close();
			} catch (IOException e) 
			{
				e.printStackTrace();
			}
    	} 
    	//Log.d("FileDownloadTask", String.format(" Downloaded<< %s", mUrl.toString()));  
        return total;		
	}

	@Override
    protected Integer doInBackground(URL... urls) 
    {
    	return download(urls[0]);
    }

	@Override
    protected void onProgressUpdate(Integer... progress) {
    	int percent = (int)((progress[0]*100)/mFileLength);
    	if (percent < 100 && mObserver != null)
    	{//don't confuse the listener with two progress updates on 100%
    		mObserver.OnFileDownloadProgress(mUrl, percent, progress[0]);
    	}
    }

	@Override
    protected void onPostExecute(Integer result) {      
		if (mObserver != null)
		{
			mObserver.OnFileDownloadProgress(mUrl, 100, result);
		}
    }
}
