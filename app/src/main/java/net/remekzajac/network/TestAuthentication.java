//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
// 
//********************************************************************************

package net.remekzajac.network;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

@SuppressLint("NewApi")
public class TestAuthentication extends AsyncTask<String, Void, Boolean> {

	private final Context mContext;
	private DefaultHttpClient mHttpClient;
	public TestAuthentication(Context context) {
		mContext = context;
		mHttpClient = new DefaultHttpClient();
        AccountManager manager = AccountManager.get(mContext); 
        Account[] accounts = manager.getAccountsByType("com.google");      
        manager.getAuthToken(accounts[0], "ah", null, false, new GetAuthTokenCallback(), null);
	}
	
	class GetAuthTokenCallback implements AccountManagerCallback<Bundle>
	{

		public void run(AccountManagerFuture<Bundle> arg0) {
            Bundle bundle;
            try {
                    bundle = arg0.getResult();
                    Intent intent = (Intent)bundle.get(AccountManager.KEY_INTENT);
                    if(intent != null) {
                            Log.d("TestAuthentication", "User Input Required");
                    } else {
                            onGetAuthToken(bundle);
                    }
            } catch (OperationCanceledException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
            } catch (AuthenticatorException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
            } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
            }			
			
		}
		
	}

    protected void onGetAuthToken(Bundle bundle) {
        String auth_token = bundle.getString(AccountManager.KEY_AUTHTOKEN);
        this.execute(auth_token);
}

	@Override
	protected Boolean doInBackground(String... tokens) {
	    try {
	            // Don't follow redirects
	            mHttpClient.getParams().setBooleanParameter(ClientPNames.HANDLE_REDIRECTS, false);
	            
	            HttpGet http_get = new HttpGet("http://192.168.1.64:8888/_ah/login?auth=" + tokens[0]);
	            HttpResponse response;
	            response = mHttpClient.execute(http_get);
	            if(response.getStatusLine().getStatusCode() != 302)
	                    // Response should be a redirect
	                    return false;
	            
	            for(Cookie cookie : mHttpClient.getCookieStore().getCookies()) {
	                    if(cookie.getName().equals("ACSID"))
	                            return true;
	            }
	    } catch (ClientProtocolException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	    } catch (IOException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	    } finally {
	            mHttpClient.getParams().setBooleanParameter(ClientPNames.HANDLE_REDIRECTS, true);
	    }
    return false;
	}

}
