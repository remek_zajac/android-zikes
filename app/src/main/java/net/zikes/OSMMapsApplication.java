//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.zikes;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import net.remekzajac.maps.GeoDegPoint;
import net.remekzajac.maps.IITileFactory;
import net.remekzajac.maps.ReferenceMap;
import net.remekzajac.osm.OSMBitmapTileFactory;
import net.remekzajac.osm.OSMOfflineTileFactory;
import net.remekzajac.osm.OSMTilesReferenceMap;
import net.remekzajac.tilestorage.FileCache;
import net.remekzajac.tilestorage.IIOfflineTileStorage;
import net.remekzajac.tilestorage.OfflineTileStorage;
import net.remekzajac.tilestorage.TileFileCache;
import net.remekzajac.tilestorage.TileStorageWrapper;
import net.remekzajac.tracks.LocationTracker;
import net.remekzajac.tracks.NavigatedDocument;
import net.remekzajac.utils.StringUtils;
import net.zikes.backend.AuthenticatedBackend;
import net.zikes.backend.ZikesBackendActivity;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class OSMMapsApplication extends Application {

    /*
     * Rotation resistant dialog
     */
    public abstract static class RotationResistantDialog
    {
        protected RotationResistantDialog()
        {
            OSMMapsApplication app = OSMMapsApplication.instance();
            if (app.mDialogResistingRotation != null)
            {
                app.mDialogResistingRotation.kill();
            }
            app.mDialogResistingRotation = this;
        }

        abstract public void dismiss();
        abstract public void show(MainActivity context);
        protected void kill()
        {
            OSMMapsApplication.instance().mDialogResistingRotation = null;
            dismiss();
        }
    }

    public static abstract class Document
    {
        protected final String mDocName;
        protected Document(String docName)
        {
            mDocName = docName;
            application().mDocuments.put(mDocName, this);
        }

        public abstract void newActivity();
        public void outgoingActivity()
        {
        }

        public void clear()
        {
            OSMMapsApplication.instance().mDocuments.remove(mDocName);
        }

        protected MainActivity activity() {
            MainActivity result = OSMMapsApplication.instance().mActivity;
            return result;
        }

        protected OSMMapsApplication application()
        {
            return OSMMapsApplication.instance();
        }
    }

    private static OSMMapsApplication         mInstance;
    private OSMTilesReferenceMap              mReferenceMap;
    private IITileFactory                     mTileFactory;
    private IITileFactory                     mOfflineViewTileFactory;
    private OfflineTileStorage                mOfflineTileStorage;
    private LocationTracker                   mLocationTracker;
    private StringUtils                       mStringUtils;
    private SettingsActivity.Preferences      mPreferences;
    private boolean                           mHighlightOfflineTiles;
    public RotationResistantDialog            mDialogResistingRotation;
    private final Map<String, Document>       mDocuments;
    private MainActivity                      mActivity;
    private AuthenticatedBackend              mAuthenticatedBackend;


    public OSMMapsApplication() {
        mDocuments = new TreeMap<String, Document>();
    }

    public SettingsActivity.Preferences preferences() {
        return mPreferences;
    }

    public ReferenceMap referenceMap() {
        return mReferenceMap;
    }

    public Document getDocument(String docName)
    {
        return mDocuments.get(docName);
    }

    public boolean hasDocuments()
    {
        return mDocuments.size() > 0;
    }

    public void clearDocuments()
    {
        Iterator<Document> it = null;
        int size = mDocuments.size();
        while (true) {
            if (it == null) {
                it = mDocuments.values().iterator();
            }
            if (!it.hasNext()) {
                break;
            }
            it.next().clear();
            int newSize = mDocuments.size();
            if ( newSize < size ) {
                size = newSize;
                it = null;
            }
        }
    }

    public void onActivityGlobalLayout() {
        if (mDialogResistingRotation != null) {
            mDialogResistingRotation.show(mActivity);
        }

        for (Document document : mDocuments.values()) {
            document.newActivity();
        }
    }

    public void newActivity(MainActivity activity)
    {
        mActivity = activity;
    }

    public void outgoingActivity(MainActivity activity)
    {
        if (mDialogResistingRotation != null) {
            mDialogResistingRotation.dismiss();
        }
        for (Document document : mDocuments.values()) {
            document.outgoingActivity();
        }
        mActivity = null;
    }

    public void restart() {
        if (mActivity != null) {
            mActivity.recreate();
        }
        int initalZoom = preferences().pref_InitialZoom.get();
        float nativeScale = preferences().pref_NativeScale.get();
        GeoDegPoint initialLocation = new GeoDegPoint(mLocationTracker.coldLocation());
        if (mReferenceMap != null) {
            initalZoom = mReferenceMap.getTraits().mZoom;
            initialLocation = mReferenceMap.getTraits().mGeoCentre;
        }
        mReferenceMap = new OSMTilesReferenceMap(initialLocation, initalZoom, nativeScale);
        mStringUtils = new StringUtils(StringUtils.DistanceUnits.EMetric); //TODO, add support for imperial
    }

    public LocationTracker locationTracker()
    {
        return mLocationTracker;
    }

    public boolean highlightOfflineTiles() {
        return mHighlightOfflineTiles;
    }

    public void setHighlightOfflineTiles(boolean set)
    {
        mHighlightOfflineTiles = set;
    }

    public IITileFactory tileFactory()
    {
        return mTileFactory;
    }

    public IITileFactory offlineViewTileFactory()
    {
        return mOfflineViewTileFactory;
    }

    public IIOfflineTileStorage offlineTileStorage()
    {
        return mOfflineTileStorage;
    }

    public StringUtils stringUtils()
    {
        return mStringUtils;
    }

    public AuthenticatedBackend authenticatedBackend() { return mAuthenticatedBackend; }

    public void showWindroseTip() {
        if (mActivity != null) {
            mActivity.showWindroseTip();
        }
    }

    public String appVersion() {
        String version = "N/A";
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    public String defaultCacheDir()
    {
        return FileCache.getDiskCacheDir(OSMMapsApplication.this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mPreferences  = new SettingsActivity.Preferences(this);
        mAuthenticatedBackend = new AuthenticatedBackend(
            this,
            preferences().pref_BackendSyncFolder.get(true)
        );
        //this is a bit delicate, routing preference needs backend and backend needs a preference
        mPreferences.pref_RoutingPreference.init();

        /*
         * Instantiate tools
         */
        TileFileCache tileFileCache = new TileFileCache();
        try {
            tileFileCache.open();
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), "Failed to open tile's cache, due to:"+e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        mOfflineTileStorage = new OfflineTileStorage(tileFileCache);
        mTileFactory = new OSMBitmapTileFactory(new TileStorageWrapper(mOfflineTileStorage, tileFileCache), getApplicationContext());
        mOfflineViewTileFactory = new OSMOfflineTileFactory(getApplicationContext(), mOfflineTileStorage);
        if (mLocationTracker == null) {
            mLocationTracker = new LocationTracker(
                getApplicationContext(),
                new LocationTracker.Listener() {
                    public void onRecordingStateChanged(File recordedTrack) {
                        if (recordedTrack != null) {
                            mAuthenticatedBackend.newTrackRecorded(recordedTrack);
                        }
                        mActivity.invalidateOptionsMenu();
                    }
                });
        }
        restart();
    }


    public static OSMMapsApplication instance() {
        assert mInstance != null;
        return mInstance;
    }
}
