//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.zikes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import net.zikes.backend.AuthenticatedBackend;

import org.json.JSONException;
import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class SettingsActivity extends Activity
{
    static class PreferenceBase {
        final public String key;
        protected  PreferenceBase(String _key) {
            key = _key;
        }

        public void initUiElement(final android.preference.Preference pref, String value, final CharSequence staticSummary, final android.preference.Preference.OnPreferenceChangeListener onPrefChangeListener) {
            resetSummary(pref, value, staticSummary);
            pref.setOnPreferenceChangeListener(new android.preference.Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(android.preference.Preference preference, Object newValue) {
                    if (onPrefChangeListener == null || onPrefChangeListener.onPreferenceChange(preference, newValue)) {
                        if (pref instanceof ListPreference) {
                            ListPreference lpref = (ListPreference) pref;
                            newValue = lpref.getEntries()[lpref.findIndexOfValue(newValue.toString())];
                        }
                        resetSummary(preference, newValue.toString(), staticSummary);
                        return true;
                    }
                    return false;
                }
            });
        }

        private void resetSummary(android.preference.Preference preference, String value, CharSequence staticSummary) {
            preference.setSummary(value+(
                staticSummary != null && staticSummary.length() > 0 ?
                    (" ("+staticSummary+")"):
                    ""));
        }
    }


    static public class Preferences {
        private OSMMapsApplication application;
        private SharedPreferences sharedPref;
        private List<PreferenceBase> allPrefs = new LinkedList<PreferenceBase>();;

        public Preferences(OSMMapsApplication app) {
            application = app;
            sharedPref = PreferenceManager.getDefaultSharedPreferences(application);
            pref_InitialLocation_Lat       = new PreferenceFloat("pref_InitialLocation_Lat", 51.513116);
            pref_InitialLocation_Lon       = new PreferenceFloat("pref_InitialLocation_Lon", -0.120506);
            pref_InitialZoom               = new PreferenceInteger("pref_InitialZoom", 13);
            pref_NativeScale	           = new PreferenceFloat("pref_NativeScale", Math.round(application.getResources().getDisplayMetrics().density));
            pref_ScreenOrientation	       = new PreferenceString("pref_ScreenOrientation", application.getResources().getString(R.string.pref_screenOrientation_Portrait));
            pref_UseNetwork	               = new PreferenceString("pref_UseNetwork", "Always");
            pref_OsmTileServerAPIKey	   = new PreferenceString("pref_OsmTileServerAPIKey", application.getString(R.string.MAPBOX_TILESERVER_KEY));
            pref_osm_TileServerUrl	       = new PreferenceString("pref_osm_TileServerUrl", "https://{{mirror}}.tiles.mapbox.com/v4/{{style}}/{{subpath}}@2x.png?access_token={{apikey}}");
            pref_Osm_TileServerMirrors     = new PreferenceStringSet("pref_Osm_TileServerMirrors", new String[] {"a","b","c","d"});
            pref_Osm_TileServerStyles      = new PreferenceStringSet("pref_Osm_TileServerStyles", new String[] {"mapbox.outdoors"});
            pref_Osm_TileServerStyle       = new PreferenceString("pref_Osm_TileServerStyle", "mapbox.outdoors");
            pref_DefaultCacheLocation      = new PreferenceString("pref_DefaultCacheLocation", application.defaultCacheDir());
            pref_BackendSyncFolder         = new PreferenceFolder("pref_BackendSyncFolder", pref_DefaultCacheLocation.get() + "/Backend");
            pref_RoutingPreference         = new RoutingPreference("pref_RoutingPreference", pref_BackendSyncFolder.get(true));
            pref_TipWindrose               = new PreferenceBoolean("pref_TipWindrose", true);
            pref_TipNavigate               = new PreferenceBoolean("pref_TipNavigate", true);
            pref_MinZoomWhenFollowing      = new PreferenceInteger("pref_MinZoomWhenFollowing", 16);
            pref_Location_minDistMts       = new PreferenceInteger("pref_Location_minDistMts", 10);
            pref_Location_minTimeSec       = new PreferenceInteger("pref_Location_minTimeSec", 10);
            pref_Location_RqAltitude       = new PreferenceBoolean("pref_Location_RqAltitude", true);
            pref_Location_AutoRecord       = new PreferenceBoolean("pref_Location_AutoRecord", true);
            pref_Location_recAccuracyMts   = new PreferenceInteger("pref_Location_recAccuracyMts", 30);
            pref_Rec_minDistMts            = new PreferenceInteger("pref_Rec_minDistMts", 2000);
            pref_Rec_minAvgSpeed           = new PreferenceInteger("pref_Rec_minAvgSpeed", 6);
            pref_Rec_maxMaxSpeed           = new PreferenceInteger("pref_Rec_maxMaxSpeed", 50);
            pref_Rec_sampleChunkSizeSec    = new PreferenceInteger("pref_Rec_sampleChunkSizeSec", 120);
            pref_Rec_graceSamples          = new PreferenceInteger("pref_Rec_graceSamples", 3);
            pref_trackUpld_URL             = new PreferenceString("pref_trackUpld_URL", "");
            pref_trackUpld_everyTrack      = new PreferenceInteger("pref_trackUpld_everyTrack", 4);
            pref_trackUpld_backlogFolder   = new PreferenceFolder("pref_trackUpld_backlogFolder", pref_DefaultCacheLocation.get()+"/ForUpload");
            pref_tileCache_Location        = new PreferenceFolder("pref_tileCache_Location", pref_DefaultCacheLocation.get()+"/TileCache");
            pref_tileCache_SizeMB          = new PreferenceInteger("pref_tileCache_SizeMB", 30);
            pref_offlineTileCache_Location = new PreferenceFolder("pref_offlineTileCache_Location", pref_DefaultCacheLocation.get()+"/OfflineTiles");
            pref_mapquest_geoSearchRootUrl = new PreferenceString("pref_mapquest_geoSearchRootUrl", "https://maps.googleapis.com/maps/api/geocode/json?");
            pref_geoSearchServerUrl        = new PreferenceString("pref_geoSearchServerUrl", pref_mapquest_geoSearchRootUrl.get() + "?sensor=false&address=");
        }

        public final PreferenceFloat pref_InitialLocation_Lat;
        public final PreferenceFloat pref_InitialLocation_Lon;
        public final PreferenceInteger pref_InitialZoom;
        public final PreferenceFloat pref_NativeScale;
        public final PreferenceString pref_ScreenOrientation;
        public final PreferenceString pref_UseNetwork;
        public final PreferenceString pref_OsmTileServerAPIKey;
        public final PreferenceString pref_osm_TileServerUrl;
        public final PreferenceStringSet pref_Osm_TileServerMirrors;
        public final PreferenceStringSet pref_Osm_TileServerStyles;
        public final PreferenceString pref_Osm_TileServerStyle;
        public final PreferenceString pref_DefaultCacheLocation;
        public final PreferenceFolder pref_BackendSyncFolder;
        public final RoutingPreference pref_RoutingPreference;
        public final PreferenceBoolean pref_TipWindrose;
        public final PreferenceBoolean pref_TipNavigate;
        public final PreferenceInteger pref_MinZoomWhenFollowing;
        public final PreferenceInteger pref_Location_minDistMts;
        public final PreferenceInteger pref_Location_minTimeSec;
        public final PreferenceBoolean pref_Location_RqAltitude;
        public final PreferenceBoolean pref_Location_AutoRecord;
        public final PreferenceInteger pref_Location_recAccuracyMts;
        public final PreferenceInteger pref_Rec_minDistMts;
        public final PreferenceInteger pref_Rec_minAvgSpeed;
        public final PreferenceInteger pref_Rec_maxMaxSpeed;
        public final PreferenceInteger pref_Rec_sampleChunkSizeSec;
        public final PreferenceInteger pref_Rec_graceSamples;
        public final PreferenceString pref_trackUpld_URL;
        public final PreferenceInteger pref_trackUpld_everyTrack;
        public final PreferenceFolder pref_trackUpld_backlogFolder;
        public final PreferenceFolder pref_tileCache_Location;
        public final PreferenceInteger pref_tileCache_SizeMB;
        public final PreferenceFolder pref_offlineTileCache_Location;
        public final PreferenceString pref_mapquest_geoSearchRootUrl;
        public final PreferenceString pref_geoSearchServerUrl;

        class Preference<T> extends PreferenceBase {
            protected T mDeftValue;

            public Preference(String key, T _deftValue) {
                super(key);
                mDeftValue = _deftValue;
                allPrefs.add(this);
            }
        }

        public class PreferenceString extends Preference<String> {
            public PreferenceString(String key, String deftValue) {super(key, deftValue);}
            public String get() {
                try {
                    return sharedPref.getString(key, mDeftValue);
                } catch (Exception e) {
                    sharedPref.edit().remove(key).apply();
                }
                return sharedPref.getString(key, mDeftValue);
            }
            public void put(String value) {
                sharedPref.edit().putString(key, value).apply();
            }

            @Override
            public String toString() {
                return get();
            }
        }

        public class PreferenceFolder extends PreferenceString {
            public PreferenceFolder(String key, String deftValue) {super(key, deftValue);}
            public File get(boolean createIfNecessary) {
                File result = new File(get());
                if (!result.exists() && createIfNecessary) {
                    result.mkdirs();
                }
                return result;
            }
        }

        public class PreferenceFloat extends Preference<Float> {
            public PreferenceFloat(String key, double deftValue) {super(key, (float)deftValue);}
            public PreferenceFloat(String key, Float deftValue) {super(key, deftValue);}
            public Float get() {
                try {
                    return sharedPref.getFloat(key, mDeftValue);
                } catch (Exception e) {
                    sharedPref.edit().remove(key).apply();
                }
                return sharedPref.getFloat(key, mDeftValue);
            }
            public void put(Float value) {
                sharedPref.edit().putFloat(key, value).apply();
            }

            @Override
            public String toString() {
                return get().toString();
            }
        }

        public class PreferenceInteger extends Preference<Integer> {
            public PreferenceInteger(String key, Integer deftValue) {super(key, deftValue);}
            public Integer get() {
                try {
                    return sharedPref.getInt(key, mDeftValue);
                } catch (Exception e) {
                    sharedPref.edit().remove(key).apply();
                }
                return sharedPref.getInt(key, mDeftValue);
            }
            public void put(Integer value) {
                sharedPref.edit().putInt(key, value).apply();
            }

            @Override
            public String toString() {
                return get().toString();
            }
        }

        public class PreferenceBoolean extends Preference<Boolean> {
            public PreferenceBoolean(String key, Boolean deftValue) {super(key, deftValue);}
            public Boolean get() {
                try {
                    return sharedPref.getBoolean(key, mDeftValue);
                } catch (Exception e) {
                    sharedPref.edit().remove(key).apply();
                }
                return sharedPref.getBoolean(key, mDeftValue);
            }
            public void put(Boolean value) {
                sharedPref.edit().putBoolean(key, value).apply();
            }

            @Override
            public String toString() {
                return get().toString();
            }
        }

        public class PreferenceStringSet extends Preference<Set<String>> {
            public PreferenceStringSet(String key, Set<String> deftValue) {super(key, deftValue);}
            public PreferenceStringSet(String key, String[] deftValue) {super(key, new HashSet<String>(Arrays.asList(deftValue)));}
            public Set<String> get() {
                try {
                    return sharedPref.getStringSet(key, mDeftValue);
                } catch (Exception e) {
                    sharedPref.edit().remove(key).apply();
                }
                return sharedPref.getStringSet(key, mDeftValue);
            }
            public void put(Set<String> value) {
                sharedPref.edit().putStringSet(key, value).apply();
            }

            @Override
            public String toString() {
                return get().toString();
            }
        }

        public class RoutingPreference extends PreferenceString {
            private final File mInFolder;
            private String[] allPrefFiles;
            private String[] allPrefNames;
            public RoutingPreference(String key, File inFolder) {
                super(key, "");
                mInFolder = inFolder;
            }

            public void init() {
                populate();
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            OSMMapsApplication.instance().authenticatedBackend().serverMeta().sync();
                            populate();
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }

            synchronized private void populate() {
                try {
                    net.zikes.backend.json.Preference.Routing[] prefs = OSMMapsApplication
                        .instance()
                        .authenticatedBackend()
                        .routingPreferences();
                    if (prefs.length > 0) {
                        mDeftValue = prefs[0].file().getAbsolutePath();
                        ArrayList<String> allPrefFilesAL = new ArrayList<String>();
                        ArrayList<String> allPrefNamesAL = new ArrayList<String>();
                        for (int i = 0; i < prefs.length; i++) {
                            allPrefFilesAL.add(prefs[i].file().getAbsolutePath());
                            allPrefNamesAL.add(prefs[i].meta().name);
                        }
                        allPrefFiles = allPrefFilesAL.toArray(new String[allPrefFilesAL.size()]);
                        allPrefNames = allPrefNamesAL.toArray(new String[allPrefNamesAL.size()]);
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            private void doInitUiElement(final ListPreference uiPref) {
                try {
                    populate();
                    uiPref.setEntries(allPrefNames);
                    uiPref.setEntryValues(allPrefFiles);
                    net.zikes.backend.json.Preference.Routing preference = new net.zikes.backend.json.Preference.Routing(
                        new File(get())
                    ).parseMeta();
                    if (new AuthenticatedBackend(
                        application,
                        mInFolder
                    ).isLoggedIn()) {
                        initUiElement(uiPref, preference.meta().name, application.getResources().getText(R.string.pref_routingPreferences_Summary_LoggedIn), null);
                    } else {
                        initUiElement(uiPref, preference.meta().name, application.getResources().getText(R.string.pref_routingPreferences_Summary), null);
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            public void initUiElement(final ListPreference uiPref, final Activity activity) {
                doInitUiElement(uiPref);
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            OSMMapsApplication.instance().authenticatedBackend().sync(
                                new AuthenticatedBackend.ZikesAuthenticator(
                                    activity, AuthenticatedBackend.KZikesBackendRootUrl, null
                                )
                            );
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                activity.setProgressBarIndeterminateVisibility(false);
                                doInitUiElement(uiPref);
                            }
                        });
                    }
                }).start();
            }
        }

        public void restoreDefaults() {
            SharedPreferences.Editor editor = sharedPref.edit();
            for (PreferenceBase preference : allPrefs) {
                editor.remove(preference.key);
            }
            editor.commit();
        }
    };


	public static class SettingsFragment extends PreferenceFragment {
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
            Preferences preferences = OSMMapsApplication.instance().preferences();
	        // Load the preferences from an XML resource
	        addPreferencesFromResource(R.xml.preferences);
	        initListPreference(preferences.pref_UseNetwork, null, null);

            preferences.pref_RoutingPreference.initUiElement(
                (ListPreference)findPreference(preferences.pref_RoutingPreference.key),
                getActivity()
            );

            initTextEditPreference(preferences.pref_InitialZoom, OSMMapsApplication.instance().referenceMap().getTraits().mZoomRange.toString(),
                new android.preference.Preference.OnPreferenceChangeListener() {

                    public boolean onPreferenceChange(android.preference.Preference preference, Object newValue) {
                        try {
                            int zoom = Integer.parseInt((String) newValue);
                            if (!OSMMapsApplication.instance().referenceMap().getTraits().mZoomRange.contains(zoom))
                            {
                                return false;
                            }
                        }
                        catch (Exception e)
                        {
                            return false;
                        }
                        return true;
                    }
                });

	        initListPreference(preferences.pref_ScreenOrientation, null, null);

	        initTextEditPreference(preferences.pref_NativeScale, getText(R.string.pref_nativeScale_Summary),
	        		new android.preference.Preference.OnPreferenceChangeListener() {
	                public boolean onPreferenceChange(android.preference.Preference preference, Object newValue) {
	                	try {
	                		Float.parseFloat((String) newValue);
	                	}
	                	catch (Exception e)
	                	{
	                		return false;
	                	}
		            	return true;
	                }
	            });


	        initTextEditPreference(preferences.pref_MinZoomWhenFollowing,
	        		OSMMapsApplication.instance().referenceMap().getTraits().mZoomRange.toString()+" "+
	        		getText(R.string.pref_minZoomWhenFollowing_Summary),
	        		new android.preference.Preference.OnPreferenceChangeListener() {

	                public boolean onPreferenceChange(android.preference.Preference preference, Object newValue) {
	                	try {
			            	int zoom = Integer.parseInt((String) newValue);
			            	if (!OSMMapsApplication.instance().referenceMap().getTraits().mZoomRange.contains(zoom))
			            	{
			            		return false;
			            	}
	                	}
	                	catch (Exception e)
	                	{
	                		return false;
	                	}
		            	return true;
	                }
	            });

	        initTextEditPreference(preferences.pref_Location_minDistMts, null, null);
	        initTextEditPreference(preferences.pref_Location_minTimeSec, null, null);
	        initCheckboxPreference(preferences.pref_Location_RqAltitude, null);
	        initCheckboxPreference(preferences.pref_Location_AutoRecord,
	        		getText(R.string.pref_location_recordAutomatically_Summary));
	    }

        @Override
        public void onStart() {
            super.onStart();
            getActivity().setProgressBarIndeterminateVisibility(true);
        }

	    private void initTextEditPreference(PreferenceBase preference, final CharSequence staticSummary, final android.preference.Preference.OnPreferenceChangeListener onPrefChangeListener )
	    {
	    	EditTextPreference pref = (EditTextPreference)findPreference(preference.key);
            String stringValue = preference.toString();
	    	pref.setText(stringValue);
            preference.initUiElement(pref, stringValue, staticSummary, onPrefChangeListener);
	    }

	    private void initListPreference(PreferenceBase preference, final CharSequence staticSummary, final android.preference.Preference.OnPreferenceChangeListener onPrefChangeListener )
	    {
	    	ListPreference uiPref = (ListPreference)findPreference(preference.key);
            String stringValue = preference.toString();
            uiPref.setValue(stringValue);
            preference.initUiElement(uiPref, stringValue, staticSummary, onPrefChangeListener);
	    }

	    private void initCheckboxPreference(Preferences.PreferenceBoolean preference, final CharSequence staticSummary)
	    {
	    	CheckBoxPreference pref = (CheckBoxPreference)findPreference(preference.key);
	    	Boolean value = preference.get();
	    	pref.setChecked(value);
            preference.initUiElement(pref, value.toString(), staticSummary, null);
	    }
	}


	public void restoreDefaults() {
        OSMMapsApplication.instance().preferences().restoreDefaults();
	}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setProgressBarIndeterminate(true);
        setProgressBarIndeterminateVisibility(true);
        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }

    @Override
	public void onDestroy() {
    	OSMMapsApplication.instance().restart();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_settings, menu);
        return true;
    }

    private void showAbout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.title_menu_about_app));
        final View view = getLayoutInflater().inflate(R.layout.about_dialog, null);

        TextView welcomeTextView = ((TextView)view.findViewById(R.id.welcomeMessage));
        welcomeTextView.setText(
            Html.fromHtml(
                String.format(
                    getString(R.string.dialog_about_welcome),
                    getString(R.string.zikes_frontend_url)
                )
            )
        );
        welcomeTextView.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(
                    "http://"+getString(R.string.zikes_frontend_url)
                ));
                startActivity(intent);
            }
        });
        ((TextView)view.findViewById(R.id.version)).setText(
            Html.fromHtml(
                String.format(
                    getString(R.string.dialog_about_version),
                    OSMMapsApplication.instance().appVersion()
                )
            )
        );
        builder.setView(view)
            .setCancelable(false)
            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //do things
                }
            });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_restore_defaults:
            	restoreDefaults();
            	recreate();
                return true;
            case R.id.menu_about_app:
                showAbout();
                return true;
        }
        return false;
    }
}
