//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.zikes;
import net.remekzajac.tracks.TrackDocuments;
import net.remekzajac.tracks.TracksView;

import android.content.Context;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

public class DownloadOfflineTaskDialogTrack extends DownloadOfflineTaskDialog
{
	private static final int	KMinBandWidthMts = 30;
	private static final int	KSliderLogBase   = 2;
    private static final int    KInterractionCoolOffMs = 800;

	public DownloadOfflineTaskDialogTrack(Context context, TracksView.TrackDocument[] trackDocuments) {
		super(context,
			  R.layout.offlinedownloaddialog_track,
			  R.string.title_dialog_title_offlineTiles_track,
			  OSMMapsApplication.instance().offlineTileStorage().newOfflineTilesDownloadTask(
				  trackDocuments,
				  OSMMapsApplication.instance().referenceMap().getTraits().mZoom)
		);
	}

	public void setWidth(int progress, TextView textView) {
		int widthRatio = (int)Math.pow(KSliderLogBase, progress);
		int widthMts = widthRatio*KMinBandWidthMts;
		String text = String.format(mDialog.getContext().getResources().getString(R.string.title_dialog_offlineTiles_welcome_track),
				mFinishedColour, OSMMapsApplication.instance().stringUtils().metres(widthMts).toString());
		textView.setText(Html.fromHtml(text));
		mTask.setRangeMts(widthMts);
		sizeEstimate();
	}

	@Override
	protected View construct(final MainActivity context) {
		View view = super.construct(context);
		final TextView widthTextView = (TextView)view.findViewById(R.id.trackwidth_text);
		final SeekBar widthSeekBar = (SeekBar)view.findViewById(R.id.trackwidth_seekbar);
		setWidth(widthSeekBar.getProgress(), widthTextView);
		widthSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			public void onProgressChanged(SeekBar seekBar, final int progress,
					boolean fromUser) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (progress == widthSeekBar.getProgress()) {
                            setWidth(progress, widthTextView);
                        }
                    }
                }, KInterractionCoolOffMs);
			}

			public void onStartTrackingTouch(SeekBar seekBar) {}

			public void onStopTrackingTouch(SeekBar seekBar) {}

		});
		return view;
	}
}
