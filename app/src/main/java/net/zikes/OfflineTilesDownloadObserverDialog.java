//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
// 
//********************************************************************************
package net.zikes;

import net.remekzajac.tilestorage.IIOfflineTileStorage;
import net.remekzajac.tilestorage.OfflineTileStorage;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class OfflineTilesDownloadObserverDialog extends OSMMapsApplication.RotationResistantDialog implements OfflineTileStorage.OfflineTilesDownloadTask.DownloadObserver
{
	public final IIOfflineTileStorage.OfflineTilesDownloadTask	mTask;
	private ProgressBar 					  					mProgressBar;
	private TextView											mProgressText;
	private AlertDialog											mDialog;
	private Context												mContext;
	public OfflineTilesDownloadObserverDialog(IIOfflineTileStorage.OfflineTilesDownloadTask task)
	{
		mTask = task;
	}
	
	protected View construct(final MainActivity context)
	{
	    AlertDialog.Builder builder = new AlertDialog.Builder(context);
	    builder.setTitle(context.getString(R.string.title_dialog_title_offlineTiles_observer));

	    // Inflate and set the layout for the dialog
	    // Pass null as the parent view because its going in the dialog layout
	    final View view = context.getLayoutInflater().inflate(R.layout.offlinetilesobserverdialog, null);
	    builder.setView(view)
	    // Add action buttons
	           .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id) 
	               	{
	   					mTask.cancel();
	   					kill();
	               	}
	           });
	    mDialog = builder.create();
	    mDialog.setCanceledOnTouchOutside(false);
	    mProgressBar = (ProgressBar)view.findViewById(R.id.progressBar);
        mProgressText = (TextView)view.findViewById(R.id.progressText);
		return view;
	}
	
	public void progress(long prog, long ofWhat)
	{
        int percent = (int)(100*prog/ofWhat);
		mProgressBar.setProgress(percent);
        mProgressText.setText(prog + "/" + ofWhat);
		if (percent == 100) {
			kill();
		}
	}
	
	public void error()
	{
		Toast.makeText(mContext, mContext.getString(R.string.error_CantDownloadTiles), Toast.LENGTH_SHORT).show();
		kill();
	}
	
	public void show(MainActivity context)
	{	
		construct(context);
		mDialog.show();
	}

	@Override
	public void dismiss() 
	{
		mDialog.dismiss();
	}	
}
