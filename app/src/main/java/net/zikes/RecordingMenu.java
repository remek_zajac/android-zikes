//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.zikes;

import android.os.Handler;
import android.view.MenuItem;

import net.remekzajac.tracks.LocationTracker;

public class RecordingMenu {
    private final static int KBlinkIntervalMSec = 800;
    private final Invalidate mInvalidateObserver;
    private boolean mBlinkScheduled;
    private boolean mAutoIconRed;

    public static interface Invalidate {
        public void invalidate();
    }

    public RecordingMenu(Invalidate invalidateObserver) {
        mInvalidateObserver = invalidateObserver;
    }

    public void invalidate(MenuItem menuItem) {
        LocationTracker.Recorder.State manualRecordingState =
            OSMMapsApplication.instance().locationTracker().manualRecorder().state();
        LocationTracker.Recorder.State automaticRecordingState =
            OSMMapsApplication.instance().locationTracker().automaticRecorder().state();
        if (automaticRecordingState == LocationTracker.Recorder.State.EThinking) {
            if (!mBlinkScheduled) {
                mBlinkScheduled = true;
                mAutoIconRed = !mAutoIconRed;
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        mBlinkScheduled = false;
                        mInvalidateObserver.invalidate();
                    }}, KBlinkIntervalMSec);
            }
        } else {
            mAutoIconRed = automaticRecordingState == LocationTracker.Recorder.State.ERecording;
        }

        menuItem.setVisible(OSMMapsApplication.instance().locationTracker().warmLocation() != null);
        int iconResId = R.drawable.record_gg;
        if (manualRecordingState == LocationTracker.Recorder.State.ERecording) {
            if (mAutoIconRed) {
                iconResId = R.drawable.record_rr;
            } else {
                iconResId = R.drawable.record_gr;
            }
        } else if (mAutoIconRed) {
            iconResId = R.drawable.record_rg;
        }
        menuItem.setIcon(iconResId);
    }
}
