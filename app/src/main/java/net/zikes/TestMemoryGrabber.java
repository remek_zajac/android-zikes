package net.zikes;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import net.remekzajac.maps.GeoDegPoint;
import android.graphics.Bitmap;
import android.os.Handler;
import android.util.Log;

public class TestMemoryGrabber
{
	private final int mIntervalMs;
    private int	mSteps;
    private Vector<Bitmap> mBitmaps;
	public TestMemoryGrabber(int intervalMs)
	{
		mIntervalMs = intervalMs;
		mBitmaps = new Vector<Bitmap>();
	}
	
	public void test()
	{
		schedule();
	}	
	
	private void schedule()
	{
		Log.d("Tiles", "TestMemoryGrabber in action, step: " + ++mSteps + " bitmaps held:" + mBitmaps.size());
		
		Timer timer = new Timer();
		final Handler handler = new Handler();
		TimerTask timerTask = new TimerTask()
		{
			@Override
			public void run() {
                handler.post(new Runnable() {
                    public void run() {
                    	grab();
                    	schedule();
                    }});
			}
		};
		if ((mSteps % 20) == 0)
		{
			Log.d("Tiles", "TestMemoryGrabber in action, resting for a bit longer");
			timer.schedule(timerTask, mIntervalMs*20);
		}
		else
		{
			timer.schedule(timerTask, mIntervalMs);
		}
	}

	public void grab()
	{
		mBitmaps.add(Bitmap.createBitmap(256, 256, Bitmap.Config.ARGB_8888));
	}	
}
