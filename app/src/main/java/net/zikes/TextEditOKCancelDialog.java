//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.zikes;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;

public class TextEditOKCancelDialog extends OSMMapsApplication.RotationResistantDialog
{
	protected AlertDialog 			mDialog;
	protected final Observer 		mObserver;
	protected final String 			mTitle;
	protected String			    mContents;
	protected EditText 				mSearchTextView;

	public interface Observer
	{
		public void onOK(String result);
		public void onCancel();
	}

	public TextEditOKCancelDialog(String title, String initialContent, Observer observer)
	{
		mObserver = observer;
		mTitle = title;
	}

	protected View construct(MainActivity context)
	{
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		// set title
		alertDialogBuilder.setTitle(mTitle);
	    final View view = context.getLayoutInflater().inflate(R.layout.search_dialog, null);
	    mSearchTextView = (EditText)view.findViewById(R.id.searchEditText);
	    if (mContents != null) {
	    	mSearchTextView.setText(mContents);
	    }
	    alertDialogBuilder.setView(view);

		// set dialog message
		alertDialogBuilder
			.setPositiveButton("OK",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					mObserver.onOK(mSearchTextView.getText().toString());
				}
			  })
			.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					mObserver.onCancel();
					dialog.cancel();
				}
			});

		mDialog = alertDialogBuilder.create();
		return view;
	}


	@Override
	public void show(MainActivity context)
	{
		construct(context);
		mDialog.show();
	}

	@Override
	public void dismiss()
	{
		mContents = mSearchTextView.getText().toString();
		mDialog.dismiss();
	}
}
