//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.zikes;
import net.remekzajac.tilestorage.IIOfflineTileStorage;
import net.remekzajac.utils.StringUtils;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class DownloadOfflineTaskDialog extends OSMMapsApplication.RotationResistantDialog
{
	protected final IIOfflineTileStorage.OfflineTilesDownloadTask	mTask;
	protected final int												mLayoutResId;
	protected final int												mTitleResId;
	protected final int												mUnfinishedColour;
	protected final int												mFinishedColour;
	protected AlertDialog											mDialog;
	private String													mEstimatedSize;
	private String													mAlreadyOnDisc;
	private TextView												mEstimatedSizeView;
	private TextView												mAlreadyOnDiscView;
	private Button													mButtonOk;
	public DownloadOfflineTaskDialog(Context context, int layoutResId, int titleResId, IIOfflineTileStorage.OfflineTilesDownloadTask task)
	{
		mUnfinishedColour = context.getResources().getColor(R.color.magenta);
		mFinishedColour = context.getResources().getColor(R.color.blue);
		mLayoutResId = layoutResId;
		mTitleResId = titleResId;
		mTask = task;
	}

	protected View construct(final MainActivity context)
	{
	    AlertDialog.Builder builder = new AlertDialog.Builder(context);
	    builder.setTitle(context.getString(mTitleResId));

	    // Inflate and set the layout for the dialog
	    // Pass null as the parent view because its going in the dialog layout
	    final View view = context.getLayoutInflater().inflate(mLayoutResId, null);
	    builder.setView(view)
	    // Add action buttons
	           .setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id)
					{
						kill();
						OfflineTilesDownloadObserverDialog observerDialog = new OfflineTilesDownloadObserverDialog(mTask);
						mTask.execute(observerDialog);
						observerDialog.show(context);
					}
	           }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	               public void onClick(DialogInterface dialog, int id)
	               	{
	            	   kill();
	               	}
	           });

	    mDialog = builder.create();
		final ImageView image = (ImageView)view.findViewById(R.id.image);
		image.setOnClickListener(new View.OnClickListener(){
		    public void onClick(View v){
		        Intent intent = new Intent();
		        intent.setAction(Intent.ACTION_VIEW);
		        intent.addCategory(Intent.CATEGORY_BROWSABLE);
		        intent.setData(Uri.parse(
					"https://sites.google.com/site/andrdzikes/#TOC-Offline-Maps"
				));
		        context.startActivity(intent);
		    }
		});
		mEstimatedSizeView = ((TextView)view.findViewById(R.id.estimatedSize));
		mAlreadyOnDiscView = ((TextView)view.findViewById(R.id.alreadyOnDisc));

		if (mEstimatedSize != null) {
			sizeEstimate();
		}
		else {
			mEstimatedSizeView.setText(mEstimatedSize);
			mAlreadyOnDiscView.setText(mAlreadyOnDisc);
		}
		return view;
	}

	public void sizeEstimate()
	{
		mTask.sizeEstimate(new IIOfflineTileStorage.OfflineTilesDownloadTask.SizeEstimateObserver()
		{
			public void sizeEstimate(long newSizeKb, long alreadyOnDiskKB, boolean finished)
			{
				String estimatedSizeTemp = StringUtils.kBtoString(newSizeKb);
				String alreadyOnDiscTemp = StringUtils.kBtoString(alreadyOnDiskKB);
				if (finished)
				{
					mEstimatedSizeView.setTextColor(mFinishedColour);
					mEstimatedSize = estimatedSizeTemp;
					mAlreadyOnDisc = alreadyOnDiscTemp;
					if (mButtonOk != null)
					{
						mButtonOk.setEnabled(true);
					}
				}
				else
				{
					mEstimatedSizeView.setTextColor(mUnfinishedColour);
					if (mButtonOk != null)
					{
						mButtonOk.setEnabled(false);
					}
				}


                String text = String.format(
                		mDialog.getContext().getResources().getString(R.string.title_dialog_offlineTiles_alreadyOnDisc),
                		mFinishedColour, StringUtils.kBtoString(alreadyOnDiskKB));

				mEstimatedSizeView.setText(estimatedSizeTemp);
				mAlreadyOnDiscView.setText(Html.fromHtml(text));
			}
		});
	}

	public void show(MainActivity context)
	{
		construct(context);
		sizeEstimate();
		mDialog.show();
		mButtonOk = mDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        mButtonOk.setEnabled(false);
	}

	@Override
	public void dismiss()
	{
		mDialog.dismiss();
	}
}
