//********************************************************************************
//
// Copyright [2012] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.zikes;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Set;

import net.remekzajac.Geo.Topo;
import net.remekzajac.Gpx.Gpx;
import net.remekzajac.maps.GeoSearch;
import net.remekzajac.maps.GeoSearchDocument;
import net.remekzajac.maps.LocationIndicatorView;
import net.remekzajac.maps.MapCoverageView;
import net.remekzajac.maps.MapView;
import net.remekzajac.maps.ReferenceMap;
import net.remekzajac.maps.ReferenceMap.MapPixBounds;
import net.remekzajac.tilestorage.OfflineTileView;
import net.remekzajac.tracks.GeoPOIRegister;
import net.remekzajac.tracks.LocationTracker;
import net.remekzajac.tracks.NavigatedDocument;
import net.remekzajac.tracks.TrackDocuments;
import net.remekzajac.tracks.TracksView;
import net.remekzajac.utils.IntegerPoint;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import net.zikes.backend.ZikesBackendActivity;
import net.zikes.backend.json.JourneyReader;

import com.ipaulpro.afilechooser.FileChooserActivity;

@SuppressLint("NewApi")
public class MainActivity extends Activity implements ReferenceMap.MapViews
{
    private static final int                        KTrackActivityRequestCode = 6384;
    private static final int                        KShowTracks = KTrackActivityRequestCode+1;
    private static final int                        KStopSpinning = KShowTracks+1;

    private MapView                                 mMapView;
    private TracksView                              mTracksView;
    private OfflineTileView                         mOfflineTileView;
    private LocationIndicatorView                   mLocationIndicatorView;
    private LocationIndicatorView.DisplayedLocation mLastClickedPOI;
    private View                                    mWindRoseControl;
    private View                                    mTipBlanket;
    private RecordingMenu                           mRecordingMenu;

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        net.zikes.OSMMapsApplication.instance().newActivity(MainActivity.this);
        getWindow().requestFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setProgressBarIndeterminate(true);
        setContentView(R.layout.activity_main);

        mMapView = (MapView)findViewById(R.id.mapView);
        mMapView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener()
        {
            public void onGlobalLayout()
            {
                mMapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                net.zikes.OSMMapsApplication.instance().referenceMap().newView(MainActivity.this);
                mMapView.onMapReady();
                net.zikes.OSMMapsApplication.instance().onActivityGlobalLayout();
            }
        });
        registerForContextMenu(mMapView);

        mOfflineTileView    = new OfflineTileView(mMapView);
        View cockpitView    = findViewById(R.id.navigationCockpit);
        mTipBlanket         = findViewById(R.id.tipBlanketView);
        mWindRoseControl    = findViewById(R.id.windroseControl);
        mTracksView         = new TracksView(mMapView);
        mRecordingMenu      = new RecordingMenu(new RecordingMenu.Invalidate() {
            @Override
            public void invalidate() {
                invalidateOptionsMenu();
            }
        });
        mLocationIndicatorView = new LocationIndicatorView(mMapView);
        new MapCoverageView(mMapView);
        mLocationIndicatorView.setUIControls(mWindRoseControl, (TextView)cockpitView.findViewById(R.id.speed));
        mTracksView.setCockpitControl(cockpitView);

        if (OSMMapsApplication.instance().highlightOfflineTiles()) {
            mOfflineTileView.show();
        }

        findViewById(R.id.windroseButton).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            mTracksView.windroseClick();
        }});

        String orientation = OSMMapsApplication.instance().preferences().pref_ScreenOrientation.get();
        if (orientation.equals(getString(R.string.pref_screenOrientation_Sticky))) {
            disableOrientationChanges();
        } else if (orientation.equals(getString(R.string.pref_screenOrientation_Portrait))) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else if (orientation.equals(getString(R.string.pref_screenOrientation_Landscape))) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            enableOrientationChanges();
        }

        Thread.setDefaultUncaughtExceptionHandler (new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException (Thread thread, Throwable e)
            {
                Log.e("MainActivity", e.getMessage());
                e.printStackTrace();
            }
        });

        /*new MockLocationProvider("mockLocationProvider",
                   OSMMapsApplication.instance().locationTracker()).test(12000,
                           new GeoDegPoint(-0.205648, 51.546042), 10, 10, true, true);*/
    }

    public void disableOrientationChanges() {
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    public void enableOrientationChanges() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        IntegerPoint lastTouchEventPixLocation = mMapView.getLastSingleTouchEventPixLocation();
        mLastClickedPOI = locationIndicatorView().click(lastTouchEventPixLocation);
        mLastClickedPOI.onCreateContextMenu(getMenuInflater(), menu);
        MenuItem deleteOfflineTilesMenuItem = menu.add(
            0,R.id.menu_delete_offline_tiles, 99, R.string.title_delete_offline_tiles
        );
        deleteOfflineTilesMenuItem.setVisible(true);
        if (mOfflineTileView.tilesToDeleteAt(lastTouchEventPixLocation)) {
            deleteOfflineTilesMenuItem.setVisible(true);
        } else {
            deleteOfflineTilesMenuItem.setVisible(false);
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_delete_offline_tiles) {
            mOfflineTileView.deleteTilesAt(mMapView.getLastSingleTouchEventPixLocation());
        } else if (mLastClickedPOI != null) {
            mLastClickedPOI.onContextItemSelected(item);
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {
        MenuItem menuItemHighlightOfflineTiles = menu.findItem(R.id.menu_highlightOfflineTiles);
        menuItemHighlightOfflineTiles.setChecked(OSMMapsApplication.instance().highlightOfflineTiles());

        MenuItem navigateTo = menu.findItem(R.id.menu_navigate_to);
        if (OSMMapsApplication.instance().preferences().pref_TipNavigate.get()) {
            navigateTo.setVisible(true);
        } else {
            navigateTo.setVisible(false);
        }

        mRecordingMenu.invalidate(menu.findItem(R.id.menu_record));
        MenuItem menuItemClearMap = menu.findItem(R.id.menu_clearMap);
        if (OSMMapsApplication.instance().hasDocuments()) {
            menuItemClearMap.setVisible(true);
        } else {
            menuItemClearMap.setVisible(false);
        }

        MenuItem menuItemLogin = menu.findItem(R.id.menu_login);
        MenuItem menuItemLogout = menu.findItem(R.id.menu_logout);
        if (OSMMapsApplication.instance().authenticatedBackend().isLoggedIn()) {
            menuItemLogout.setVisible(true);
            menuItemLogin.setVisible(false);
        } else {
            menuItemLogin.setVisible(true);
            menuItemLogout.setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    private Intent backendSyncIntent() {
        Intent intent = new Intent(this, ZikesBackendActivity.class);
        Bundle intentParams = new Bundle();
        intentParams.putString(
            ZikesBackendActivity.EXTRA_SYNC_DEST, OSMMapsApplication.instance().preferences().pref_BackendSyncFolder.get(true).toString()
        );
        intent.putExtras(intentParams);
        return intent;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        switch (menuItem.getItemId())
        {
            case R.id.menu_record:
                Toast.makeText(getApplicationContext(), getString(R.string.info_recordingMenu), Toast.LENGTH_LONG).show();
                return true;
            case R.id.menu_navigate_to:
                Toast.makeText(getApplicationContext(), getString(R.string.tip_longClickToNavigate), Toast.LENGTH_LONG).show();
                OSMMapsApplication.instance().preferences().pref_TipNavigate.put(false);
                invalidateOptionsMenu();
                return true;

            case R.id.menu_tracks:
                if (OSMMapsApplication.instance().authenticatedBackend().isLoggedIn()) {
                    startActivityForResult(backendSyncIntent(), KShowTracks);
                    setProgressBarIndeterminateVisibility(true);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.tip_logInToSeeTracks), Toast.LENGTH_LONG).show();
                    showTracks();
                }
                return true;
            case R.id.menu_login:
                startActivityForResult(backendSyncIntent(), KStopSpinning);
                setProgressBarIndeterminateVisibility(true);
                return true;
            case R.id.menu_logout:
                OSMMapsApplication.instance().authenticatedBackend().logOut();
                invalidateOptionsMenu();
                return true;
            case R.id.menu_makeAvailableOffline:
                if (mTracksView.isShown()) {
                    Set<TracksView.TrackDocument> shownTracks = mTracksView.shownDocuments();
                    new DownloadOfflineTaskDialogTrack(
                        this,
                        shownTracks.toArray(new TracksView.TrackDocument[shownTracks.size()])
                    ).show(this);
                } else {
                    new DownloadOfflineTaskDialogRegion(this).show(this);
                }
                return true;
            case R.id.menu_highlightOfflineTiles:
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                    mOfflineTileView.hide();
                    OSMMapsApplication.instance().setHighlightOfflineTiles(false);
                } else {
                    menuItem.setChecked(true);
                    mOfflineTileView.show();
                    OSMMapsApplication.instance().setHighlightOfflineTiles(true);
                }
                return true;
            case R.id.menu_settings:
                this.startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.menu_clearMap:
                OSMMapsApplication.instance().clearDocuments();
                return true;
            case R.id.menu_geo_search:
                TextEditOKCancelDialog searchDialog = new TextEditOKCancelDialog("Search (with Google)", null, new TextEditOKCancelDialog.Observer() {
                    public void onOK(String result)
                    {
                        setProgressBarIndeterminateVisibility(true);
                        new GeoSearch(MainActivity.this).newRequest(result).execute(new GeoSearch.Observer() {

                            public void finished(GeoPOIRegister result) {
                                setProgressBarIndeterminateVisibility(false);
                                if (result != null) {
                                    GeoSearchDocument.instance().show(result);
                                } else {
                                    Toast.makeText(getApplicationContext(), getString(R.string.error_CouldntFind), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                    public void onCancel() {}
                });

                searchDialog.show(this);
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void showTracks() {
        Intent intent = new Intent(this, FileChooserActivity.class);
        Bundle intentParams = new Bundle();
        intentParams.putString(
            FileChooserActivity.EXTRA_ROOT,
            OSMMapsApplication.instance().authenticatedBackend().trackFolder().getAbsolutePath()
        );
        intentParams.putStringArray(
            FileChooserActivity.EXTRA_FILE_FILTERS,
            new String[] {
                "net.remekzajac.tracks.MultiSegmentTrackDocument$GPXFileRenderer",
                "net.remekzajac.tracks.ZikesJourneyDocument$FileRenderer",
                "com.ipaulpro.afilechooser.FileLoader$DirRenderer"
            }
        );
        intentParams.putStringArray(FileChooserActivity.EXTRA_ROOT_SYMLINKS, new String[]{"Device Root", FileChooserActivity.EXTERNAL_BASE_PATH});

        intent.putExtras(intentParams);
        startActivityForResult(intent, KTrackActivityRequestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case KShowTracks:
                showTracks();
            case KStopSpinning:
                invalidateOptionsMenu();
                setProgressBarIndeterminateVisibility(false);
            case KTrackActivityRequestCode:
                setProgressBarIndeterminateVisibility(false);
                // If the file selection was successful
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        // Get the URI of the selected file
                        final Uri uri = data.getData();
                        final String mime = data.getExtras().getString(FileChooserActivity.EXTRA_RESULT_MIME);
                        Log.d("Tracks", "got Uri = " + uri.toString() + " with mime: " + mime);
                        File sourceFile = new File(uri.getPath());
                        try(
                            FileReader sourceReader = new FileReader(sourceFile)
                        ) {
                            Topo.CollectionSource source = null;
                            switch (mime) {
                                case JourneyReader.KMimeType:
                                    source = new JourneyReader(
                                        sourceReader
                                    );
                                    break;
                                case Gpx.KMimeType:
                                    source = new Gpx.Reader(
                                        sourceReader, sourceFile.getName()
                                    );
                                    break;
                                default:
                                    Toast.makeText(getApplicationContext(), getString(R.string.error_cantReadTrack), Toast.LENGTH_SHORT).show();

                            }
                            TrackDocuments.instance().add(source);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public TracksView tracksView()
    {
        return mTracksView;
    }

    public LocationIndicatorView locationIndicatorView()
    {
        return mLocationIndicatorView;
    }

    @Override
    protected void onDestroy() {
        mMapView.onDestroy(); //need to hurry to cancel any pending downloads
        OSMMapsApplication.instance().outgoingActivity(this);
        super.onDestroy();
    }

    public void invalidate(int bWhatsChangedVector) {
        mMapView.invalidate(bWhatsChangedVector);
        if ((bWhatsChangedVector & ReferenceMap.MapViews.BLocation) != 0) {
            invalidateOptionsMenu();
        }
    }

    public MapPixBounds mapPixBounds() {
        View content = getWindow().findViewById(Window.ID_ANDROID_CONTENT);
        return new MapPixBounds(
                new IntegerPoint(content.getWidth(), content.getHeight()),
                new IntegerPoint(mMapView.getWidth(), mMapView.getHeight()));
    }

    public void showWindroseTip() {
        if (OSMMapsApplication.instance().preferences().pref_TipWindrose.get()) {
            Animation windroseAnimation = AnimationUtils.loadAnimation(this, R.anim.windrose_animation);
            Animation blanketAnimation = AnimationUtils.loadAnimation(this, R.anim.tip_blanket_animation);
            mTipBlanket.setVisibility(View.VISIBLE);
            mWindRoseControl.startAnimation(windroseAnimation);
            mTipBlanket.startAnimation(blanketAnimation);
            Button okButton = (Button)mTipBlanket.findViewById(R.id.okButton);
            okButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mTipBlanket.setVisibility(View.GONE);
                OSMMapsApplication.instance().preferences().pref_TipWindrose.put(false);
            }});
        }
    }
}


