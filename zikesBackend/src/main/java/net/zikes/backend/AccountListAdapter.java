//********************************************************************************
//
// Copyright [2016] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.zikes.backend;

import android.accounts.Account;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * List adapter for Accounts.
 */
public class AccountListAdapter extends BaseAdapter {

    private final static int ICON_GOOGLE = R.drawable.ic_google;
    private final static int ICON_FACEBOOK = R.drawable.ic_facebook;

    private final LayoutInflater mInflater;

    private List<Account> mData = new ArrayList<Account>();

    public AccountListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public Account getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    public List<Account> getListItems() {
        return mData;
    }

    /**
     * Set the list items without notifying on the clear. This prevents loss of
     * scroll position.
     *
     * @param data
     */
    public void setListItems(List<Account> data) {
        mData = data;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        if (row == null)
            row = mInflater.inflate(R.layout.account, parent, false);

        TextView view = (TextView) row;
        final Account account = getItem(position);

        view.setText(account.name);
        int icon = ICON_GOOGLE;
        if (account.type.equals(AccountListFragment.FACEBOOK_ACCOUNT_TYPE)) {
            icon = ICON_FACEBOOK;
            view.setText(R.string.facebook_user_name); //account.name is useless (number)
        }

        view.setCompoundDrawablesWithIntrinsicBounds(icon, 0, 0, 0);

        return row;
    }

}
