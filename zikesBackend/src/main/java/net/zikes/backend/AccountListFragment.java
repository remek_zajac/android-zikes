//********************************************************************************
//
// Copyright [2016] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.zikes.backend;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ListFragment;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import net.remekzajac.Geo.Topo;

/**
 * Fragment that displays the list of qualifying accounts to choose from
 *
 * @version 2016-07-14
 * @author remek.zajac@gmail.com
 */
public class AccountListFragment extends ListFragment implements AdapterView.OnItemClickListener {

    public final static String GOOGLE_ACCOUNT_TYPE   = "com.google";
    public final static String FACEBOOK_ACCOUNT_TYPE = "com.facebook.auth.login";
    /**
     * Interface to listen for events.
     */
    public interface Callbacks {
        public String onAccountSelected(Account account);
    }

    private AccountListAdapter   mAdapter;
    private ZikesBackendActivity mActivity;

    public static AccountListFragment newInstance() {
        AccountListFragment fragment = new AccountListFragment();
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mActivity = (ZikesBackendActivity) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement FileListFragment.Callbacks");
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        setEmptyText(getString(R.string.no_accounts));
        if (mActivity.alreadyLoggedIn()) {
            setEmptyText(
                mActivity.onAccountSelected(null)
            );
        } else {
            AccountManager manager = AccountManager.get(getActivity());
            List<Account> accounts = new ArrayList<Account>();
            accounts.addAll(Arrays.asList(manager.getAccountsByType(GOOGLE_ACCOUNT_TYPE)));
            accounts.addAll(Arrays.asList(manager.getAccountsByType(FACEBOOK_ACCOUNT_TYPE)));

            mAdapter.setListItems(accounts);
            setListShown(true);
            if (isResumed()) {
                setListShown(true);
            } else {
                setListShownNoAnimation(true);
            }
        }

        TextView empty = (TextView)(getListView().getEmptyView());
        empty.setTextSize(14);
        empty.setPadding(0,0,0,20);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new AccountListAdapter(getActivity());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        setListAdapter(mAdapter);
        getListView().setOnItemClickListener(this);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        AccountListAdapter adapter = (AccountListAdapter) parent.getAdapter();
        setEmptyText(
            mActivity.onAccountSelected(adapter.getItem(position))
        );
        adapter.setListItems(new ArrayList<Account>());
        adapter.notifyDataSetChanged();
    }

}
