//********************************************************************************
//
// Copyright [2016] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.zikes.backend.json;

import android.util.Log;

import org.json.simple.parser.ContentHandler;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Stack;

public class JsonReader {

    public interface JSONStream {
        public boolean startObject() throws ParseException;
        public boolean endObject() throws ParseException;
        public boolean startObjectEntry(String key) throws ParseException;
        public boolean endObjectEntry() throws ParseException;
        public boolean startArray() throws ParseException;
        public boolean endArray() throws ParseException;
        public boolean primitive(Object value) throws ParseException;
    }

    private static class JSONStreamHarsh implements JSONStream {

        public boolean startObject() throws ParseException {
            throw new ParseException(ParseException.ERROR_UNEXPECTED_TOKEN, "{");
        }
        public boolean endObject() throws ParseException {
            throw new ParseException(ParseException.ERROR_UNEXPECTED_TOKEN, "}");
        }
        public boolean startObjectEntry(String key)throws ParseException {
            throw new ParseException(ParseException.ERROR_UNEXPECTED_TOKEN, key);
        }
        public boolean endObjectEntry() throws ParseException {
            throw new ParseException(ParseException.ERROR_UNEXPECTED_TOKEN);
        }
        public boolean startArray() throws ParseException {
            throw new ParseException(ParseException.ERROR_UNEXPECTED_TOKEN, "[");
        }
        public boolean endArray() throws ParseException {
            throw new ParseException(ParseException.ERROR_UNEXPECTED_TOKEN, "]");
        }
        public boolean primitive(Object value) throws ParseException {
            throw new ParseException(ParseException.ERROR_UNEXPECTED_TOKEN, "");
        }
    }

    private static class JSONStreamLenient implements JSONStream {

        public boolean startObject() throws ParseException {
            return true;
        }
        public boolean endObject() throws ParseException {
            return true;
        }
        public boolean startObjectEntry(String key) throws ParseException{
            return true;
        }
        public boolean endObjectEntry() throws ParseException {
            return true;
        }
        public boolean startArray() throws ParseException {
            return true;
        }
        public boolean endArray() throws ParseException {
            return true;
        }
        public boolean primitive(Object value) throws ParseException {
            return true;
        }
    }

    public class RootHandler implements ContentHandler {

        private LinkedList<String> mNodeToParse;
        private int mStack;
        public RootHandler(String[] nodeToParse) {
            mNodeToParse = new LinkedList<String>(Arrays.asList(nodeToParse));
        }

        @Override
        public void startJSON() throws ParseException {}

        @Override
        public void endJSON() throws ParseException {}

        @Override
        public boolean startObject() throws ParseException {
            if (capturing()) {
                mStack++;
                mMainStack.peek().startObject();
            }
            return true;
        }

        @Override
        public boolean endObject() throws ParseException {
            if (capturing()) {
                mMainStack.peek().endObject();
                if (--mStack == 0) {
                    return false;
                }
            }
            return true;
        }

        @Override
        public boolean startObjectEntry(String key) throws ParseException {
            if (!capturing()) {
                if (key.equals(mNodeToParse.getFirst())) {
                    mNodeToParse.pop();
                }
            } else {
                //capturing
                mMainStack.peek().startObjectEntry(key);
            }
            return true;
        }

        @Override
        public boolean endObjectEntry() throws ParseException {
            if (capturing()) {
                mMainStack.peek().endObjectEntry();
            }
            return true;
        }

        @Override
        public boolean startArray() throws ParseException {
            if (capturing()) {
                mStack++;
                mMainStack.peek().startArray();
            }
            return true;
        }

        @Override
        public boolean endArray() throws ParseException {
            if (capturing()) {
                mMainStack.peek().endArray();
                if (--mStack == 0) {
                    return false;
                }
            }
            return true;
        }

        @Override
        public boolean primitive(Object value) throws ParseException, IOException {
            if (capturing()) {
                mMainStack.peek().primitive(value);
            }
            return true;
        }

        protected boolean capturing() {
            return mNodeToParse.isEmpty();
        }

        public Stack<JSONStream> nodeStack() { return mMainStack; }
    }

    public class BaseNode extends JSONStreamLenient {

        protected Stack<String> mLocalStack;
        protected BaseNode(String initalStackNode) {
            mLocalStack = new Stack<String>();
            if (initalStackNode != null) {
                mLocalStack.push(initalStackNode);
            }
        }

        @Override
        public boolean endObject() throws ParseException {
            mLocalStack.pop();
            if (mLocalStack.size() == 0) {
                onFinish();
                mMainStack.pop();
            }
            return true;
        }

        @Override
        public boolean endArray() throws ParseException {
            mLocalStack.pop();
            if (mLocalStack.size() == 0) {
                onFinish();
                mMainStack.pop();
            }
            return true;
        }

        @Override
        public boolean startObject() throws ParseException {
            mLocalStack.push("{");
            return true;
        }

        public boolean startObject(BaseNode handler) throws ParseException {
            mMainStack.push(handler);
            return true;
        }


        @Override
        public boolean startArray() throws ParseException {
            mLocalStack.push("[");
            return true;
        }

        public boolean startArray(BaseNode handler) throws ParseException {
            mMainStack.push(handler);
            return true;
        }


        @Override
        public boolean startObjectEntry(String key) throws ParseException {
            mLocalStack.push(key);
            return true;
        }

        public boolean startObjectEntry(BaseNode handler) throws ParseException {
            mMainStack.push(handler);
            return true;
        }

        @Override
        public boolean endObjectEntry() throws ParseException {
            mLocalStack.pop();
            if (mLocalStack.size() == 0) {
                onFinish();
                mMainStack.pop();
            }
            return true;
        }

        protected void onFinish() {}
    }

    public class BaseObjectNode extends BaseNode {
        protected BaseObjectNode() {
            super("{");
        }
    }

    public class BaseArrayNode extends BaseNode {
        protected BaseArrayNode() {
            super(null);
        }
    }

    public class BaseObjectEntryNode extends BaseNode {
        protected BaseObjectEntryNode() {
            super("{");
        }
    }

    public abstract class PrimitiveNode<T> extends BaseObjectNode {

        @SuppressWarnings (value="unchecked")
        @Override
        public boolean primitive(Object value) throws ParseException {
            value((T)value);
            return true;
        }

        public abstract void value(T value) throws ParseException;
    }

    public class IgnoreNode extends BaseObjectNode {}

    public abstract class ArrayOfNodes extends BaseObjectNode {

        public abstract BaseNode nodeNeeded();
        private boolean mArrayOpen = false;

        @Override
        public boolean startArray() throws ParseException {
            if (mArrayOpen) {
                return startArray(nodeNeeded());
            } else {
                mArrayOpen = true;
                return super.startArray();
            }
        }

        @Override
        public boolean startObject() throws ParseException {
            if (!mArrayOpen) {
                throw new IllegalArgumentException();
            }
            return startObject(nodeNeeded());
        }

        @Override
        public boolean startObjectEntry(String key) throws ParseException {
            if (!mArrayOpen) {
                throw new IllegalArgumentException();
            }
            return startObject(nodeNeeded());
        }
    }

    public class Capture extends BaseObjectNode {

        protected String mCapturedContent = "";

        @Override
        public boolean startArray() throws ParseException {
            mCapturedContent += "[";
            return super.startArray();
        }

        @Override
        public boolean startObject() throws ParseException {
            mCapturedContent += "{";
            return super.startObject();
        }

        @Override
        public boolean startObjectEntry(String key) throws ParseException {
            mCapturedContent += key+":";
            return super.startObjectEntry(key);
        }

        @Override
        public boolean endArray() throws ParseException {
            mCapturedContent += "]";
            return super.startArray();
        }

        @Override
        public boolean endObject() throws ParseException {
            mCapturedContent += "}";
            return super.startObject();
        }

        @Override
        public boolean primitive(Object value) throws ParseException {
            mCapturedContent += value.toString();
            return super.primitive(value);
        }
    }

    public final Stack<JSONStream> mMainStack;
    public JsonReader() {
        mMainStack = new Stack<JSONStream>();
    }
}
