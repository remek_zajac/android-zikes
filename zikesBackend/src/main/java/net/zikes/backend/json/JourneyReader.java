//********************************************************************************
//
// Copyright [2016] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.zikes.backend.json;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.Reader;

import net.remekzajac.Geo.Topo;
import net.zikes.backend.AuthenticatedBackend;

public class JourneyReader implements Topo.CollectionSource {

    public static final String KMimeType         = "application/zikes+journey+json";
    public static final String KJourneyFilter = ".+\\"+ AuthenticatedBackend.KJourneyExtension;

    Reader mReader;
    public JourneyReader(Reader reader) {
        mReader = reader;
    }

    @Override
    public void copy(Topo.Collection aDstCollection) throws Exception, IOException {
        new JourneyParser(aDstCollection).parse();
    }

    @Override
    public String name() {
        return null;
    }

    public static class Meta {
        public long lastModified;
        public String name;
    }

    private class MetaParser extends JsonReader {
        private Meta meta;
        private class MetaNode extends JsonReader.BaseObjectNode {
            @Override
            public boolean startObjectEntry(String key) throws ParseException {
                if (key.equals("lastModified")) {
                    return startObjectEntry(new PrimitiveNode<Long>() {
                        @Override
                        public void value(Long value) {
                        meta.lastModified = value;
                        }
                    });
                } else if (key.equals("opaque")) {
                    return startObjectEntry(new OpaqueNode());
                }
                return super.startObjectEntry(key);
            }
        }

        private class OpaqueNode extends JsonReader.BaseObjectNode {
            @Override
            public boolean startObjectEntry(String key) throws ParseException {
                if (key.equals("name")) {
                    return startObjectEntry(new PrimitiveNode<String>() {
                        @Override
                        public void value(String value) {
                            meta.name = value;
                        }
                    });
                }
                return super.startObjectEntry(key);
            }
        }

        Meta parse() throws Exception, IOException {
            JSONParser parser = new JSONParser();
            mMainStack.push(new MetaNode());
            try {
                parser.parse(
                    mReader,
                    new JsonReader.RootHandler(
                        new String[] { "metadata" }
                    )
                );
            } catch (ParseException e) {
                throw new Exception(e.getMessage());
            }
            return meta;
        }

        public MetaParser() {
            meta = new Meta();
        }
    }

    public Meta meta() throws IOException, Exception {
        return new MetaParser().parse();
    }

    private class JourneyParser extends SectionReader {
        private Topo.Collection mDstCollection;
        public JourneyParser(Topo.Collection aDstCollection) {
            mDstCollection = aDstCollection;
        }

        public void parse() throws Exception, IOException {
            JSONParser parser = new JSONParser();
            mMainStack.push(new Items());
            try {
                parser.parse(
                    mReader,
                    new JsonReader.RootHandler(
                        new String[] { "journey", "items"}
                    )
                );
            } catch (ParseException e) {
                throw new Exception(e.getMessage());
            }
        }

        private class Route extends JsonReader.BaseObjectNode {
            private Topo.Path  mPath;

            public Route() {
                mPath = mDstCollection.addPath();
            }

            @Override
            public boolean startObjectEntry(String key) throws ParseException {
                if (key.equals("sections")) {
                    return startObjectEntry(new ArrayOfNodes() {
                        @Override
                        public BaseNode nodeNeeded() {
                            return new Section(mPath);
                        }
                    });
                }
                return super.startObjectEntry(key);
            }
        }

        private class Poi extends JsonReader.BaseObjectNode {
            private Topo.PointBuilder mPoint = new Topo.PointBuilder();

            @Override
            public boolean startObjectEntry(String key) throws ParseException {
                if (key.equals("name")) {
                    return startObjectEntry(new PrimitiveNode<String>() {
                        @Override
                        public void value(String value) {
                            mPoint.addName(value);
                        }
                    });
                }
                return super.startObjectEntry(key);
            }

            @Override
            public boolean primitive(Object value) throws ParseException {
                if (mLocalStack.search("latlng") == 2) {
                    if (!mPoint.hasLat()) {
                        mPoint.addLat((Double)value);
                    } else {
                        mPoint.addLon((Double)value);
                    }
                }
                return true;
            }

            @Override
            protected void onFinish() {
                mDstCollection.append(
                    mPoint.build()
                );
            }
        }

        private class Items extends JsonReader.BaseArrayNode {

            @Override
            public boolean startObjectEntry(String key) throws ParseException {
                if (key.equals("route")) {
                    return startObjectEntry(new Route());
                } else if (key.equals("poi")) {
                    return startObjectEntry(new Poi());
                }
                return super.startObjectEntry(key);
            }
        }
    }

}
