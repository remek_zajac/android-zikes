//********************************************************************************
//
// Copyright [2016] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.zikes.backend.json;

import net.zikes.backend.AuthenticatedBackend;
import net.zikes.backend.StringAndFileUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Preference {

    public final static String KKeyMetadata     = "metadata";
    public final static String KKeyOpaque       = "opaque";
    public final static String KKeyId           = "id";
    public final static String KKeyType         = "type";
    public final static String KKeyLastModified = "lastModified";

    public static class CommonMeta {
        public final String type;
        public final String id;
        public final Long   lastModifiedMs;

        protected JSONObject mMetadata;
        protected CommonMeta(JSONObject content) throws JSONException {
            if (content.has(KKeyMetadata)) {
                mMetadata = content.getJSONObject(KKeyMetadata);
            } else {
                mMetadata = content;
            }
            type = opaque().getString(KKeyType);
            lastModifiedMs = mMetadata.getLong(KKeyLastModified);
            id = mMetadata.has(KKeyId) ? mMetadata.getString(KKeyId) : null;
        }

        public JSONObject opaque() throws JSONException {
            return mMetadata.getJSONObject(KKeyOpaque);
        }
    }

    public final static String KKeyName         = "name";
    public static class RoutingMeta extends CommonMeta {
        public final String name;
        public RoutingMeta(JSONObject content) throws JSONException {
            super(content);
            name = opaque().getString(KKeyName);
        }


    }


    public final static String KKeyContent      = "preferences";
    public final static String KKeyGenerated    = "generated";
    public static class Routing {

        protected final File  mFile;
        private RoutingMeta mMeta;
        public Routing(File fromFile) {
            mFile = fromFile;
        }

        public Routing parseMeta() throws IOException, JSONException {
            try (FileInputStream in = new FileInputStream(mFile)) {
                JSONObject content = new JSONObject(StringAndFileUtils.fromStream(in));
                mMeta = new RoutingMeta(content);
            }
            return this;
        }

        public RoutingMeta meta() {
            return mMeta;
        }

        public File file() { return mFile; }

        public JSONObject preferencesForRouting() throws IOException, JSONException {
            try (FileInputStream in = new FileInputStream(mFile)) {
                return new JSONObject(StringAndFileUtils.fromStream(in))
                    .getJSONObject(KKeyContent)
                    .getJSONObject(KKeyGenerated);
            }
        }
    }

    public final static String KKeyCurrentProfile = "currentProfile";
    public static class User {

        public class Content {
            final public String currentProfile;
            private Content(String _currentProfile) {
                currentProfile = _currentProfile;
            }
        }

        protected final File  mFile;
        private CommonMeta mMeta;
        private Content    mContent;
        public User(File fromFile) {
            mFile = fromFile;
        }

        public User parse() throws IOException, JSONException {
            try (FileInputStream in = new FileInputStream(mFile)) {
                JSONObject everything = new JSONObject(StringAndFileUtils.fromStream(in));
                mMeta = new CommonMeta(everything);
                JSONObject content = everything.getJSONObject(KKeyContent);
                mContent = new Content(content.getString(KKeyCurrentProfile));
            }
            return this;
        }

        public CommonMeta meta() {
            return mMeta;
        }

        public Content content() {
            return mContent;
        }

    }
}
