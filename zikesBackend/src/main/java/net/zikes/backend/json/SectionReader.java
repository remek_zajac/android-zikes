//********************************************************************************
//
// Copyright [2016] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.zikes.backend.json;

import android.util.Log;

import net.remekzajac.Geo.Topo;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.Reader;

public class SectionReader extends JsonReader {

    protected class WayPoint extends JsonReader.BaseObjectNode {
        private Topo.PointBuilder mPoint;
        private double          mLat;
        final private Topo.Path mPath;

        public WayPoint(Topo.Path aPath) {
            mPath = aPath;
        }

        @Override
        public boolean startObjectEntry(String key) throws ParseException {
            if (key.equals("e")) {
                return startObjectEntry(new PrimitiveNode<Long>() {
                    @Override
                    public void value(Long value) {
                        mPoint.addEle(value.intValue());
                    }
                });
            } else {
                return startObjectEntry(new IgnoreNode());
            }
        }

        @Override
        public boolean primitive(Object value) throws ParseException {
            if (mPoint == null) {
                mPoint = new Topo.PointBuilder().addLat((Double)value);
            } else {
                mPoint.addLon((Double)value);
            }
            return true;
        }

        @Override
        public boolean endArray() throws ParseException {
            if (mPoint != null) {
                mPath.append(mPoint.build());
                mPoint = null;
            }
            return super.endArray();
        }
    }

    protected class Section extends JsonReader.BaseObjectNode {
        private Topo.Path  mPath;

        public Section(Topo.Path aPath) {
            mPath = aPath;
        }

        @Override
        public boolean startObjectEntry(String key) throws ParseException {
            if (key.equals("from")) {
                return startObjectEntry(new WayPoint(mPath));
            } else if (key.equals("waypoints")) {
                return startObjectEntry(new ArrayOfNodes() {
                    @Override
                    public BaseNode nodeNeeded() {
                        return new WayPoint(mPath);
                    }
                });
            }
            return super.startObjectEntry(key);
        }
    }

    public interface ErrorCallback {
        public void onError(String serverMessage);
    }

    protected class SectionOrError extends Section {
        private final ErrorCallback mErrorCb;
        public SectionOrError(Topo.Path aPath, ErrorCallback errorCb) {
            super(aPath);
            mErrorCb = errorCb;
        }

        @Override
        public boolean startObjectEntry(String key) throws ParseException {
            if (key.equals("message")) {
                return startObjectEntry(new PrimitiveNode<String>() {
                    @Override
                    public void value(String value) throws ParseException {
                        mErrorCb.onError(value);
                    }
                });
            }
            return super.startObjectEntry(key);
        }
    }

    public void getPath(Reader reader, Topo.Path result) throws Exception {
        parse(reader, new Section(result));
    }

    public void getPathOrError(Reader reader, Topo.Path result, ErrorCallback cb) throws Exception {
        parse(reader, new SectionOrError(result, cb));
    }

    private void parse(Reader reader, JSONStream withReader) throws Exception {
        JSONParser parser = new JSONParser();
        mMainStack.push(withReader);
        try {
            parser.parse(
                reader,
                new JsonReader.RootHandler(
                    new String[] {}
                )
            );
        } catch (ParseException e) {
            throw new Topo.CollectionSource.Exception(e.getMessage());
        }
    }
}
