//********************************************************************************
//
// Copyright [2016] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************

package net.zikes.backend;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONObject;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

class GoogleAuthenticator implements AuthenticatedBackend.ProviderAuthenticator {
    private final static String KScope = "oauth2:https://www.googleapis.com/auth/userinfo.profile";
    private final static String KVerifyTokenUrl = "https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=";
    private final Activity mActivity;
    private final Account mAccount;
    public GoogleAuthenticator(Activity activity, Account account) {
        mActivity = activity;
        mAccount  = account;
    }

    public AuthenticatedBackend.AuthToken getAuthToken() throws AuthenticatorException {
        try {
            return new AuthenticatedBackend.AuthToken(
                doGetAuthToken(true),
                AuthenticatedBackend.ZIKES_GOOGLE
            );
        } catch (Exception e) {
            throw new AuthenticatorException(e);
        }
    }

    private String doGetAuthToken(boolean retry) throws Exception {
        AccountManager manager = AccountManager.get(mActivity);
        AccountManagerFuture<Bundle> future = manager.getAuthToken(mAccount, KScope, null, mActivity, null, null);
        Bundle bundle = null;
        bundle = future.getResult();
        String authToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
        try {
            verifyToken(authToken);
        } catch (Exception e) {
            if (retry) {
                manager.invalidateAuthToken("com.google", authToken);
                return doGetAuthToken(false);
            } else {
                throw e;
            }
        }
        return authToken;
    }

    private void verifyToken(String authToken) throws Exception {
        OkHttpClient client = client = new OkHttpClient.Builder().build();
        Request request = new Request.Builder()
            .url(KVerifyTokenUrl+authToken)
            .get().build();
        Response response = client.newCall(request).execute();
            /* expecting:
            {
                "issued_to": "608941808256-43vtfndets79kf5hac8ieujto8837660.apps.googleusercontent.com",
                "audience": "608941808256-43vtfndets79kf5hac8ieujto8837660.apps.googleusercontent.com",
                "user_id": "107046534809469736555",
                "scope": "https://www.googleapis.com/auth/userinfo.profile",
                "expires_in": 3595,
                "access_type": "offline"
            }*/

        //TODO: try this expiring one: ya29.ClwiA5mELQOhB8F_0xPJ5CIgAbdwR-EU7SlnKz0dXSfhs9Uircn5Ww1LKtGoRIVL3ZLE-JiH_34OxdDoevdRjwbdgDud_zys5mPVSDRrbYmaVCkoglo_RCr98XdI_A
        String stringResponse = response.body().string();
        Log.d("AuthenticatedBackend", "token: " + authToken + ", verification: " + stringResponse);
        JSONObject jsonResponse = new JSONObject(
            stringResponse
        );
        if (jsonResponse.getInt("expires_in") < KAssumeStaleTokenSec) {
            throw new Exception("Auth token soon expiring.");
        }
    }
}
