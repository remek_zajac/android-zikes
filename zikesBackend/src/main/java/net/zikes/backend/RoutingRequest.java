//********************************************************************************
//
// Copyright [2016] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.zikes.backend;

import android.text.TextUtils;
import android.util.Log;

import net.remekzajac.Geo.Topo;
import net.zikes.backend.json.Preference;
import net.zikes.backend.json.SectionReader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class RoutingRequest {

    public static class Exception extends java.lang.Exception {
        public Exception() {super("Unable to route");}
    }

    public static class NearestPointNotFound extends Exception {
        final public static String KName = "RoutingException::NearestPointNotFound";
        final public int toleranceMts;
        final public Topo.Point culprit;
        public NearestPointNotFound(int _toleranceMts, Topo.Point _culprit) {
            toleranceMts = _toleranceMts;
            culprit = _culprit;
        }
    }

    public static class NoRoute extends Exception {
        final public static String KName = "RoutingException::NoRoute";
    }

    public static class ServerOverload extends Exception {
        final public static String KName = "RoutingException::ServerOverload";
        final public long analysedJunctions;
        public ServerOverload(long _analysedJunctions) {
            analysedJunctions = _analysedJunctions;
        }
    }

    public static class ErrorCatcher implements SectionReader.ErrorCallback {
        public Exception caughtException = null;
        @Override
        public void onError(String serverMessage)  {
            JSONObject message = null;
            try {
                message = new JSONObject(serverMessage);
                if (message.getString(KCode).equals(NearestPointNotFound.KName)) {
                    JSONArray culprit = message.getJSONArray("culprit");
                    caughtException = new NearestPointNotFound(
                        message.optInt("attemptedToleranceMts", 1200),
                        new Topo.Point(culprit.getDouble(0), culprit.getDouble(1))
                    );
                } else if (message.getString(KCode).equals(NoRoute.KName)) {
                    caughtException = new NoRoute();
                } else if (message.getString(KCode).equals(ServerOverload.KName)) {
                    caughtException = new ServerOverload(
                        message.optLong("junctionsLimitExceeded", 2000000)
                    );
                } else {
                    caughtException = new Exception();
                }
            } catch (JSONException e) {
                e.printStackTrace();
                caughtException = new Exception();
            }
        }
    }


    private static String KPrecission = "%.6f";
    private static String KCode = "code";
    public static final MediaType MediaTypeJSON = MediaType.parse("application/json; charset=utf-8");

    public static void route(
        List<Topo.Point> milestones,
        Preference.Routing preference,
        Topo.Path result) throws java.lang.Exception {


        List<String> milestonesStr = new ArrayList<String>();
        for (Topo.Point milestone : milestones) {
            milestonesStr.add(
                String.format(java.util.Locale.US, KPrecission, milestone.mLat )
            );
            milestonesStr.add(
                String.format(java.util.Locale.US, KPrecission, milestone.mLon )
            );
        }
        String url = AuthenticatedBackend.KZikesBackendRootUrl +"/route?milestones="+ TextUtils.join(",", milestonesStr);
        Log.d("RoutingRequest", ">> Requesting route "+url+" with preferences "+preference.meta().name);
        Backend.Client client = client = new Backend.Client();
        Request request = new Request.Builder()
            .url(url)
            .post(
                RequestBody.create(
                    MediaTypeJSON,
                    preference.preferencesForRouting().toString()
                )
            ).build();
        Response response = client.execute(request);
        ErrorCatcher errorCatcher = new ErrorCatcher();
        try(ResponseBody body = response.body()) {
            new SectionReader().getPathOrError(
                body.charStream(),
                result,
                errorCatcher
            );
        }
        if (errorCatcher.caughtException != null) {
            throw errorCatcher.caughtException;
        }
        Log.d("RoutingRequest", "<< Got route "+url+" with preferences "+preference.meta().name);
    }
}
