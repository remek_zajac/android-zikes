//********************************************************************************
//
// Copyright [2016] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.zikes.backend;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;

import net.zikes.backend.json.JourneyReader;
import net.zikes.backend.json.Preference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSink;

public class AuthenticatedBackend extends Backend {
    private final static String KAuthenticatedSubfolder   = "Authenticated";
    private final static String KTracksForUploadSubfolder = "ForUpload";
    private final static String KErrorsSubfolder =          "Errors";

    public final static String ZIKES_GOOGLE         = "GOOGLE";
    public final static String ZIKES_FACEBOOK       = "FACEBOOK";

    public final static String ACCOUNT_TYPE_GOOGLE   = "com.google";
    public final static String ACCOUNT_TYPE_FACEBOOK = "com.facebook.auth.login";

    public static final String KErrorsExtension     = ".error.txt";
    public static final String KJourneyExtension    = "."+KJourneyResource;
    public static final String KPrefsExtension      = "."+KPreferenceResource;

    public static class User {
        public final static String KPref_authProvider    = "net.zikes.backend.authProvider";
        public final static String KPref_authAccountName = "net.zikes.backend.authAccountName";
        public final static String KPref_zikesUserId     = "net.zikes.backend.zikesUserId";
        public User(Account account, String zikesUserId) {
            mAccount = account;
            mZikesUserId = zikesUserId;
        }
        public Account mAccount;
        public String  mZikesUserId;
    }

    static class AuthenticatedUser extends User {
        public AuthenticatedUser(Account account, String zikesUserId, AuthToken token, Client client) {
            super(account, zikesUserId);
            mAuthToken = token;
            mClient = client;
        }
        public final AuthToken mAuthToken;
        public final Client    mClient;
    }

    static class AuthToken {
        String authToken;
        String provider;
        public AuthToken(String _authToken, String _provider) {
            authToken = _authToken;
            provider  = _provider;
        }
    }

    private final File mBackendSyncFolder;
    private final File mAuthenticatedSyncFolder;
    private final File mTracksForUploadFolder;
    private final File mErrorsFolder;
    private final Application mApplication;
    private final ServerMeta mServerMeta;
    public AuthenticatedBackend(Application application, File syncFolder) {
        mBackendSyncFolder = syncFolder;
        mAuthenticatedSyncFolder = new File(syncFolder, KAuthenticatedSubfolder);
        mAuthenticatedSyncFolder.mkdirs();
        mTracksForUploadFolder = new File(mBackendSyncFolder, KTracksForUploadSubfolder);
        mErrorsFolder = new File(mBackendSyncFolder, KErrorsSubfolder);
        mApplication = application;
        mServerMeta = new ServerMeta(syncFolder);
    }

    public File trackFolder() {
        return mAuthenticatedSyncFolder;
    }

    public void newTrackRecorded(File file) {
        mTracksForUploadFolder.mkdirs();
        try {
            StringAndFileUtils.copy(file, mTracksForUploadFolder);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void logException(Exception e, String context) {
        mErrorsFolder.mkdirs();
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        try(  PrintWriter out = new PrintWriter(
            new File(
                mErrorsFolder,
                "zikesAndroidError_" + System.currentTimeMillis()+KErrorsExtension
            )
        )  ){
            e.printStackTrace(out);
            out.write(manufacturer);
            out.write(model);
            out.write(context);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
    }

    public ServerMeta serverMeta() { return mServerMeta; }

    public boolean isLoggedIn() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mApplication);
        return prefs.getString(User.KPref_authAccountName, null) != null;
    }

    public void logOut() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mApplication);
        prefs.edit().remove(AuthenticatedBackend.User.KPref_authAccountName)
            .remove(AuthenticatedBackend.User.KPref_authProvider)
            .remove(AuthenticatedBackend.User.KPref_zikesUserId)
            .apply();
        StringAndFileUtils.delete(mAuthenticatedSyncFolder);
        StringAndFileUtils.delete(mTracksForUploadFolder);
        mAuthenticatedSyncFolder.mkdirs();
    }

    public Preference.Routing[] routingPreferences() throws IOException, JSONException {
        ArrayList<Preference.Routing> result = mServerMeta.routingPreferences();
        File[] allPreferencFiles = mAuthenticatedSyncFolder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return filename.endsWith(KPrefsExtension);
            }
        });
        if (allPreferencFiles != null) {
            Preference.User userPref = null;
            for (int i = 0; i < allPreferencFiles.length; i++) {
                File preferenceFile = allPreferencFiles[i];
                try {
                    result.add(
                        new Preference.Routing(preferenceFile).parseMeta()
                    );

                } catch (JSONException e) {
                    userPref = new Preference.User(preferenceFile).parse();
                }
            }

            if (userPref != null) {
                final Preference.User fUserPref = userPref;
                Collections.sort(result, new Comparator<Preference.Routing>() {
                    @Override
                    public int compare(Preference.Routing lhs, Preference.Routing rhs) {
                        boolean isLhsDefault = fUserPref.content().currentProfile.equals(lhs.meta().id) ||
                            fUserPref.content().currentProfile.equals(lhs.meta().name);
                        return isLhsDefault ? -1 : 1;
                    }
                });
            }
        }
        return result.toArray(new Preference.Routing[result.size()]);
    }

    public void sync(ZikesAuthenticator authenticator) throws Exception {
        Log.d("AuthenticatedBackend", "Starting backend sync into folder "+mAuthenticatedSyncFolder.getAbsolutePath());
        if (!authenticator.hasAccount()) {
            Log.d("AuthenticatedBackend", "Backend sync abandoned, no user info, assuming speculative sync.");
            return;
        }
        AuthenticatedUser user = authenticator.authenticate();
        syncResources(authenticator.mBackendUrl, user, KJourneyResource);
        syncResources(authenticator.mBackendUrl, user, KPreferenceResource);
        uploadTracks(authenticator.mBackendUrl, user);
        Log.d("AuthenticatedBackend", "Finished backend sync into folder "+mAuthenticatedSyncFolder.getAbsolutePath());
    }

    private static String zikesUrlUser(String url, String zikesUserId) {
        return url + "/users/" + zikesUserId;
    }

    private static String zikesUrlPlan(String url, String zikesUserId) {
        return zikesUrlUser(url, zikesUserId) + "/plan";
    }

    private static String zikesUrlPlannedResource(String url, String zikesUserId, String resourceType) {
        return zikesUrlPlan(url, zikesUserId) + "/" + resourceType+"s";
    }

    public static final MediaType MEDIA_TYPE_ZIP = MediaType.parse("application/zip");
    public static final String ZippedFileName = "allTracksForUpload.zip";
    private void uploadTracks(String backendUrl, AuthenticatedUser user) throws IOException {
        File allTracksZipped = new File(mTracksForUploadFolder, ZippedFileName);
        if (allTracksZipped.exists()) {
            allTracksZipped.delete();
        }

        final File[] tracksForUpload = mTracksForUploadFolder.listFiles();
        if (tracksForUpload != null && tracksForUpload.length > 0) {

            Client client = client = new Client();
            try(ZipOutputStream zos = new ZipOutputStream(
                new BufferedOutputStream(
                    new FileOutputStream(allTracksZipped)
                )
            )) {
                for (File trackForUpload : tracksForUpload) {
                    try(InputStream in = new BufferedInputStream(new FileInputStream(trackForUpload))) {
                        zos.putNextEntry(new ZipEntry(trackForUpload.getName()));
                        StringAndFileUtils.pipe(in, zos, false);
                        zos.closeEntry();
                    }
                }
            }

            String url = backendUrl+"/trackUpload";
            Log.d("AuthenticatedBackend",
                String.format("Uploading %d recorded tracks to %s", tracksForUpload.length, url)
            );

            Request request = new Request.Builder()
                .url(url)
                .addHeader("Authorization-Provider", user.mAuthToken.provider)
                .addHeader("Authorization", "Bearer "+ user.mAuthToken.authToken)
                .post(RequestBody.create(MEDIA_TYPE_ZIP, allTracksZipped))
                .build();
            Response response = client.execute(request);
        }
        StringAndFileUtils.delete(mTracksForUploadFolder);
    }

    private void syncResources(String backendUrl, AuthenticatedUser user, String resourceType) throws Exception {
        String url = zikesUrlPlannedResource(backendUrl, user.mZikesUserId, resourceType);
        Log.d("AuthenticatedBackend", "  Syncing "+url);
        JSONArray resourceMetas = new JSONArray(
            zikesGet(user.mClient, url, user.mAuthToken)
        );
        final String extension = "."+resourceType;
        Set<File> currentFiles = new HashSet<File>(Arrays.asList(
            mAuthenticatedSyncFolder.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String filename) {
                    return filename.endsWith(extension);
                }
            })
        ));
        for (int i = 0; i < resourceMetas.length(); i++) {
            currentFiles.remove(
                syncResource(url, user, mAuthenticatedSyncFolder, resourceMetas.getJSONObject(i), extension)
            );
        }
        for (File file : currentFiles) {
            file.delete(); //delete resources no longer present on the server
        }
    }

    private static File syncResource(String url, AuthenticatedUser user, File dstPath, JSONObject meta, String fileExtension) throws Exception {
        /*expecting:
        "{"id":"1fwoywQPRzeghLaUlf1ILQ","created":1468053659876,"lastModified":1468347903983,"owner":1,"access":{"ro":"public","rw":""},"opaque":{"name":"Remek's -> Rob's (Rob's edit)","givenName":"Remek's -> Rob's (Rob's edit)","autosave":true}}"
         */
        String itemId = meta.getString("id");
        File dstFile = new File(dstPath, itemId+ fileExtension);
        if (dstFile.exists()) {
            try(
                FileReader reader = new FileReader(dstFile)
            ) {
                JourneyReader existingFile = new JourneyReader(reader);
                try {
                    if (existingFile.meta().lastModified == meta.getLong("lastModified")) {
                        return dstFile; //already up to date
                    }
                } catch (JSONException | IOException e) {}
                dstFile.delete();
            }
        }
        url = url+"/"+itemId;
        Log.d("AuthenticatedBackend", "  Syncing resource "+url);
        JSONObject itemJSON = zikesGetJSON(user.mClient, url, user.mAuthToken);
        FileOutputStream out = new FileOutputStream(dstFile);
        try {
            out.write(itemJSON.toString().getBytes());
        } finally {
            out.close();
        }
        return dstFile;
    }

    public static class ZikesAuthenticator {

        private final Account mAccount;
        private final String  mBackendUrl;
        private Activity      mActivity;

        public ZikesAuthenticator(Activity activity, String backendUrl, Account authenticateAccount) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
            String accountName = prefs.getString(User.KPref_authAccountName, null);
            String provider    = prefs.getString(User.KPref_authProvider, null);

            if (accountName != null && authenticateAccount == null) {
                AccountManager manager = AccountManager.get(activity);
                Account[] accounts = manager.getAccounts();
                for (Account account : accounts) {
                    if (account.name.equals(accountName) && account.type.equals(provider)) {
                        authenticateAccount = account;
                    }
                }
            }

            mAccount    = authenticateAccount;
            mBackendUrl = backendUrl;
            mActivity   = activity;
        }

        public boolean hasAccount() {
            return mAccount != null;
        }

        public AuthenticatedUser authenticate() throws Exception {
            ProviderAuthenticator providerAuthenticator = null;
            if (mAccount.type.equals(ACCOUNT_TYPE_FACEBOOK)) {
                providerAuthenticator = new FacebookAuthenticator(mActivity, mAccount);
            } else if (mAccount.type.equals(ACCOUNT_TYPE_GOOGLE)) {
                providerAuthenticator = new GoogleAuthenticator(mActivity, mAccount);
            } else {
                throw new Exception("unsupported account type: " + mAccount.type);
            }

            AuthToken token = providerAuthenticator.getAuthToken();
            if (token == null) {
                throw new AuthenticatorException("Unable to authenticate");
            }
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
            String zikesUserId = prefs.getString(User.KPref_zikesUserId, null);
            Client client = new Client();
            if (zikesUserId == null) {
                JSONObject jsonResponse = zikesGetJSON(
                    client,
                    mBackendUrl+"/users/metadata",
                    token
                );
                /* expecting:
                {"providedId":"google.107046534809469736555","encodedId":"AQ","opaque":{"name":"Remek Zajac"},"created":1465054501069,"lastModified":1465054503859}
                 */
                zikesUserId = jsonResponse.getString("encodedId");
            }

            prefs.edit().putString(
                AuthenticatedBackend.User.KPref_authProvider,
                mAccount.type
            ).putString(
                AuthenticatedBackend.User.KPref_authAccountName,
                mAccount.name
            ).putString(
                AuthenticatedBackend.User.KPref_zikesUserId,
                zikesUserId
            ).apply();

            return new AuthenticatedUser(
                mAccount,
                zikesUserId,
                token,
                client
            );
        }

    }

    private static JSONObject zikesGetJSON(Client client, String backendUrl, AuthToken token) throws IOException, JSONException {
        String responseString = zikesGet(client, backendUrl, token);
        try {
            return new JSONObject(responseString);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("AuthenticatedBackend", "Unable to parse response: "+ responseString);
            throw e;
        }
    }

    private static String zikesGet(Client client, String backendUrl, AuthToken token) throws IOException {
        Request request = new Request.Builder()
                .url(backendUrl)
                .addHeader("Authorization-Provider", token.provider)
                .addHeader("Authorization", "Bearer "+ token.authToken)
                .get().build();
        Response response = client.execute(request);
        try(ResponseBody body = response.body()) {
            return body.string();
        }
    }

    public interface ProviderAuthenticator {
        final static int KAssumeStaleTokenSec = 60;
        public AuthToken getAuthToken() throws AuthenticatorException;
    }
}

