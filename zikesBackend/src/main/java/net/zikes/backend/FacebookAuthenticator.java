//********************************************************************************
//
// Copyright [2016] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.zikes.backend;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Semaphore;

import android.accounts.Account;
import android.accounts.AuthenticatorException;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

public class FacebookAuthenticator implements AuthenticatedBackend.ProviderAuthenticator {

    private final static int[] KFlimsyFacebookAuthRetryMessages = {
        R.string.facebook_retry_3, R.string.facebook_retry_2, R.string.facebook_retry_1
    };
    private final Activity mActivity;
    private final Account mAccount;
    private final CallbackManager mCallbackManager;

    private class ShitShitFragment extends Fragment {

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        };
    }

    public FacebookAuthenticator(Activity activity, Account account) {
        mActivity = activity;
        mAccount  = account;
        FacebookSdk.sdkInitialize(activity.getApplicationContext());
        mCallbackManager = CallbackManager.Factory.create();
    }

    private AuthenticatedBackend.AuthToken fromFBToken(AccessToken token) {
        if (token != null &&
            token.getExpires().getTime() > System.currentTimeMillis() + KAssumeStaleTokenSec) {
            return new AuthenticatedBackend.AuthToken(
                token.getToken(),
                AuthenticatedBackend.ZIKES_FACEBOOK
            );
        }
        return null;
    }

    private void refreshAuthToken() {
        List<String> permissionNeeds = Arrays.asList("email");
        final Semaphore wait = new Semaphore(0);

        LoginManager.getInstance().registerCallback(mCallbackManager,
            new FacebookCallback<LoginResult>() {

                @Override
                public void onSuccess(LoginResult loginResults) {
                    AccessToken accessToken = loginResults.getAccessToken();
                    Log.d("dd", "access token: "+accessToken.getToken());
                    fromFBToken(loginResults.getAccessToken());
                    wait.release();
                }

                @Override
                public void onCancel() {
                    Log.e("dd","facebook login canceled");
                    wait.release();
                }

                @Override
                public void onError(FacebookException e) {
                    Log.e("dd", "facebook login failed error", e);
                    wait.release();
                }
            }
        );
        ShitShitFragment ssf = new ShitShitFragment();
        FragmentTransaction fragmentTransaction = mActivity.getFragmentManager().beginTransaction();
        fragmentTransaction.add(ssf, "listenFacebookToken");
        fragmentTransaction.commit();

        LoginManager.getInstance().logInWithReadPermissions(
            ssf,
            permissionNeeds
        );
        try {
            wait.acquire();
        } catch (InterruptedException e) {}
    }

    @Override
    public AuthenticatedBackend.AuthToken getAuthToken() throws AuthenticatorException {
        return getAuthToken(KFlimsyFacebookAuthRetryMessages);
    }

    public AuthenticatedBackend.AuthToken getAuthToken(int[] retryMessageIds) throws AuthenticatorException {
        int messageIdx = retryMessageIds.length;
        while (messageIdx-- > 0) {
            AuthenticatedBackend.AuthToken token = fromFBToken(AccessToken.getCurrentAccessToken());
            if (token == null) {
                refreshAuthToken();
            }
            if (token != null) {
                return token;
            } else {
                final String str = mActivity.getString(retryMessageIds[messageIdx]);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mActivity, str, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
        throw new AuthenticatorException("Unable to authenticate with Facebook");
    }
}
