//********************************************************************************
//
// Copyright [2016] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.zikes.backend;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Backend {
    //TODO, make this a preference or at least somehow configurable
    public final static String KZikesBackendRootUrl = "https://api-dot-zikes-web-client.appspot.com";
    //public final static String KZikesBackendRootUrl = "http://10.0.2.2:7777"; //emulator
    public static final String KJourneyResource     = "journey";
    public static final String KPreferenceResource  = "preference";
    public static final int    KZikesConnectionTimeoutMs = 24*1000; //it sometimes sleeps

    public static class Client {
        private final OkHttpClient mClient;
        private int mRetries;
        public Client() {
            mClient = new OkHttpClient
                .Builder()
                .connectTimeout(AuthenticatedBackend.KZikesConnectionTimeoutMs, TimeUnit.MILLISECONDS)
                .readTimeout(AuthenticatedBackend.KZikesConnectionTimeoutMs, TimeUnit.MILLISECONDS)
                .writeTimeout(AuthenticatedBackend.KZikesConnectionTimeoutMs, TimeUnit.MILLISECONDS)
                .build();
            mRetries = 0;
        }

        public Client withRetries(int retries) {
            mRetries = retries;
            return this;
        }

        public Response execute(Request request) throws IOException {
            int sleepMs = 1000;
            while (true) {
                try {
                    return mClient.newCall(request).execute();
                } catch (IOException e) {
                    if (--mRetries < 0) {
                        throw e;
                    }
                    try {
                        Thread.sleep(sleepMs);
                        sleepMs*=2;
                    } catch (InterruptedException e1) {
                        throw e;
                    }
                }
            }
        }
    }
}
