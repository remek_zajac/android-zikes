//********************************************************************************
//
// Copyright [2016] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.zikes.backend;

import android.util.Log;

import net.remekzajac.Geo.Topo;
import net.zikes.backend.json.Preference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class ServerMeta {

    static private final String KServerMetaSubfolder = "ServerMeta";
    static private final String KCoverageFileName    = "coverage.json";
    private Topo.Point[][] mCoverage;
    private File      mSyncFolder;
    private boolean   mAlreadySynced;
    public ServerMeta(File dstFolder) {
        mSyncFolder = new File(dstFolder, KServerMetaSubfolder);
    }

    private void clear() {
        StringAndFileUtils.delete(mSyncFolder);
        mSyncFolder.mkdirs();
    }

    public synchronized ServerMeta sync() throws IOException, JSONException {
        if (mAlreadySynced) {
            return this;
        }
        Log.d("ServerMeta", "Starting server meta sync into folder "+mSyncFolder.getAbsolutePath());
        Backend.Client client = client = new Backend.Client().withRetries(3);

        Request request = new Request.Builder()
            .url(AuthenticatedBackend.KZikesBackendRootUrl +"/meta")
            .get().build();
        Response response = client.execute(request);

        try(ResponseBody body = response.body()) {
            JSONObject responseJson = new JSONObject(
                body.string()
            );
            JSONArray allRoutingPreferences = responseJson.getJSONArray("defaultRoutingPrefs");
            clear();
            for (int i = 0; i < allRoutingPreferences.length(); i++) {
                try(FileOutputStream out = new FileOutputStream(
                    new File(mSyncFolder, String.valueOf(i) + AuthenticatedBackend.KPrefsExtension)
                )) {
                    out.write(
                        allRoutingPreferences.get(i).toString().getBytes()
                    );
                }
            }
            JSONArray coverageJSON = responseJson.getJSONObject("map").getJSONArray("coverage");
            try(FileOutputStream out = new FileOutputStream(coverageFile())) {
                out.write(
                    coverageJSON.toString().getBytes()
                );
            }
            parseCoverage();
        }

        mAlreadySynced = true;
        Log.d("ServerMeta", "Finished server meta sync into folder "+mSyncFolder.getAbsolutePath());
        return this;
    }

    private File coverageFile() {
        return new File(mSyncFolder, KCoverageFileName);
    }

    private void parseCoverage() throws JSONException {
        File coverageFile = coverageFile();
        if (!coverageFile.exists()) {
            return;
        }
        try {
            try(FileInputStream in = new FileInputStream(coverageFile)) {
                JSONArray coverageJSON = new JSONArray(StringAndFileUtils.fromStream(in));
                ArrayList<Topo.Point[]> coverage = new ArrayList<Topo.Point[]>();
                for (int r = 0; r < coverageJSON.length(); r++) {
                    JSONArray regionJSON = coverageJSON.getJSONArray(r);
                    LinkedList<Topo.Point> region = new LinkedList<Topo.Point>();
                    for (int p = 0; p < regionJSON.length(); p++ ) {
                        JSONArray pointJSON = regionJSON.getJSONArray(p);
                        Topo.Point point = new Topo.Point(
                            pointJSON.getDouble(0),
                            pointJSON.getDouble(1)
                        );
                        region.add(0, point);
                    }
                    coverage.add(
                        region.toArray(new Topo.Point[region.size()])
                    );
                }
                mCoverage = coverage.toArray(new Topo.Point[coverage.size()][]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Topo.Point[][] coverage() { return mCoverage; }

    public ArrayList<Preference.Routing> routingPreferences() throws IOException, JSONException {
        ArrayList<Preference.Routing> result = new ArrayList<Preference.Routing>();
        File[] allPreferencFiles = mSyncFolder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return filename.endsWith(AuthenticatedBackend.KPrefsExtension);
            }
        });
        if (allPreferencFiles != null) {
            for (int i = 0; i < allPreferencFiles.length; i++) {
                result.add(
                    new Preference.Routing(allPreferencFiles[i]).parseMeta()
                );
            }
            Collections.sort(result, new Comparator<Preference.Routing>() {
                @Override
                public int compare(Preference.Routing lhs, Preference.Routing rhs) {
                    return lhs.meta().name.compareTo(rhs.meta().name);
                }
            });
        }
        return result;
    }
}
