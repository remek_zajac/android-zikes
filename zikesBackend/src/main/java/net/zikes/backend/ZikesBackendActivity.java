//********************************************************************************
//
// Copyright [2016] [Remek Zajac]
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//
//********************************************************************************
package net.zikes.backend;

import java.io.File;

import android.accounts.Account;
import android.app.Activity;
import android.app.ListFragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

/**
 * Main Activity that handles the Zikes Backend
 *
 * @version 2016-07-14
 * @author remek.zajac@gmail.com
 */
public class ZikesBackendActivity extends Activity implements AccountListFragment.Callbacks {

    public static final String EXTRA_SYNC_DEST = "SYNC_DEST";
    private File mSyncDest;
    private AuthenticatedBackend mAuthenticatedBackend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(
            getString(R.string.choose_account) + " " + getString(R.string.zikes_frontend_url)
        );
        if (savedInstanceState == null) {
            String syncDest = null;
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                if (extras.containsKey(EXTRA_SYNC_DEST)) {
                    mSyncDest = new File(extras.getString(EXTRA_SYNC_DEST));
                }
            }
            mAuthenticatedBackend = new AuthenticatedBackend(
                this.getApplication(),
                this.mSyncDest
            );
            addFragment();
        }
    }



    private void sync(final Account account) {
        setTitle(
            getString(R.string.syncing)
        );
        ListFragment l = (ListFragment)(getFragmentManager().findFragmentById(android.R.id.content));
        l.setEmptyText(getString(R.string.press_back_to_skip));
        new AsyncTask<Void, Void, Exception>()
        {
            @Override
            protected void onPostExecute(Exception e) {
                if (e != null) {
                    Toast.makeText(ZikesBackendActivity.this, getText(R.string.auth_fail), Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
                finish();
            }

            @Override
            protected Exception doInBackground(Void... params) {
                try {
                    mAuthenticatedBackend.sync(
                        new AuthenticatedBackend.ZikesAuthenticator(
                            ZikesBackendActivity.this,
                            AuthenticatedBackend.KZikesBackendRootUrl,
                            account
                        )
                    );
                } catch (Exception e) {
                    return e;
                }
                return null;
            }
        }.execute();
    }

    /**
     * Add the initial Fragment with given path.
     */
    private void addFragment() {
        AccountListFragment fragment = AccountListFragment.newInstance();
        getFragmentManager().beginTransaction()
        .add(android.R.id.content, fragment).commit();
    }

    public boolean alreadyLoggedIn() {
        return mAuthenticatedBackend.isLoggedIn();
    }

    @Override
    public String onAccountSelected(Account account) {
        sync(account);
        return getString(R.string.press_back_to_skip);
    }

}
